#! /usr/bin/env python
# BEGIN PROGRAM: CHANGEO
# Started: 2004.058
# By: Bob Greschke
#   Tests and programs Reftek RT130 units.
from sys import argv, exit, platform, stderr, stdout
PROGSystem = platform[:3].lower()
PROG_NAME = "CHANGEO"
PROG_NAMELC = "changeo"
PROG_VERSION = "2019.262"
PROG_LONGNAME = "RT130 Controller"
PROG_SETUPSVERS = "A"
PFILE_VERSION = "B"

# Allow a serial port on the command line.
CLAPort = ""
PROGSmallScreen = False
PROGIgnoreSetups = False
# Not used in this program.
PROG_SETUPSUSECWD = False
for Arg in argv[1:]:
    if Arg == "-#":
        stdout.write("%s\n"%PROG_VERSION)
        exit(0)
    elif Arg == "-s":
        PROGSmallScreen = True
    elif Arg == "-x":
        PROGIgnoreSetups = True
    else:
        CLAPort = Arg

########################
# BEGIN: versionChecks()
# LIB:versionChecks():2019.214
#   Checks the current version of Python and sets up a couple of things for the
#   rest of the program to use.
#   Obviously this is not a real function. It's just a collection of things for
#   all programs to check and making it look like a library function makes it
#   easy to update everywhere.
#   This version is for Tkinter programs vs command line programs.
from sys import version_info
PROG_PYVERSION = "%d.%d.%d"%(version_info[0], version_info[1], version_info[2])
#PROG_PYVERSION = "4.0.0"
if PROG_PYVERSION.startswith("2"):
# This should be good enough for the rest of the code.
    PROG_PYVERS = 2
# The isinstance(X, *tuple*) stuff needs 2.5 and the b"" things need 2.6.
    if version_info[1] < 6:
        stdout.write("%s only runs on Python 2.6 and above.\n"%PROG_NAME)
        exit(1)
    from Tkinter import *
    from tkFont import Font
    from urllib import urlopen
    if PROG_NAME == "ITS":
#%%%% still working on this
        from thread import interrupt_main, start_new_thread
    if PROG_NAME == "WEBEDIT":
        from HTMLParser import HTMLParser
# Pillow runs under Py2, so it may be there.
        try:
            from PIL import Image, ImageTk
        except:
            import Image, ImageTk
    astring = basestring
    anint = (int, long)
    arange = xrange
    aninput = raw_input
elif PROG_PYVERSION.startswith("3"):
    PROG_PYVERS = 3
    from tkinter import *
    from tkinter.font import Font
    from urllib.request import urlopen
    if PROG_NAME == "ITS":
# This is a 'low-level' module but it is being used nicely.
        from _thread import interrupt_main, start_new_thread
#%%%%%
    if PROG_NAME == "WEBEDIT":
        from html.parser import HTMLParser
# From Pillow. I don't know if Python Imaging Library will ever be for Py3.
        from PIL import Image, ImageTk
    astring = str
    anint = int
    arange = range
    aninput = input
else:
    stdout.write("Unsupported Python version: %s\nStopping.\n"%PROG_PYVERSION)
    exit(0)
# Nice. Right-click on a Mac using Anaconda Tkinter generates a <Button-2>
# event, instead of <Button-3>.
B2Glitch = False
if PROGSystem == "dar":
    B2Glitch = True
# These should be big enough for my programs, and not bigger than any system
# they run on can handle. These are around in some form on some systems with
# some versions, but with these figuring out what is where is moot.
maxInt = 1E100
maxFloat = 1.0E100
# END: versionChecks

from calendar import monthcalendar, setfirstweekday
from fnmatch import fnmatch
from os import access, environ, listdir, makedirs, remove, sep, W_OK
from os.path import abspath, basename, dirname, exists, getsize, isdir
from socket import socket, AF_INET, SOCK_STREAM
from struct import pack
from time import sleep, gmtime, localtime, strftime, time

setfirstweekday(6)

# This is way up here so StringVars and IntVars can be declared throughout the
# code and for the option_add stuff.
Root = Tk()
Root.withdraw()

# For the program's forms, message areas, etc.
PROGButs = {}
PROGCan = {}
PROGEnt = {}
PROGFrm = {}
PROGMsg = {}
PROGTxt = {}

#####################
# BEGIN: option_add()
# LIB:option_add():2019.220
#   A collection of setup items common to most of my programs.
# Where all of the vars for saving and loading setups are kept.
PROGSetups = []
# These may be altered by loading the setups. If they end up the same as
# PROGScreen<Height/Width>OrigNow then the program will know that it is
# running on the same screen as before.
PROGScreenHeightSaved = IntVar()
PROGScreenWidthSaved = IntVar()
PROGScreenHeightSaved.set(Root.winfo_screenheight())
PROGScreenWidthSaved.set(Root.winfo_screenwidth())
PROGSetups += ["PROGScreenHeightSaved", "PROGScreenWidthSaved"]
# Alter these if you want to fool the program into doing something on a small
# screen.
PROGScreenHeightNow = Root.winfo_screenheight()
PROGScreenWidthNow = Root.winfo_screenwidth()
# Fonts: a constant nagging problem, though less now. I've stopped trying
# to micromanage them and mostly let the user suffer with what the system
# decides to use.
# Some items (like ToolTips and Help text) may not want to have their fonts
# resizable. So in that case use these Orig fonts whose values do not get saved
# to the setups.
PROGOrigMonoFont = Text().cget("font")
PROGOrigPropFont = Entry().cget("font")
# Only two fonts. If something needs more it will have to modify these.
PROGMonoFont = Font(font = Text()["font"])
PROGMonoFontSize = IntVar()
PROGMonoFontSize.set(PROGMonoFont["size"])
# I think this is some damn Linux-Tcl/Tk bug of some kind, but CentOS7/Tk8.5
# (in 2018) reported a "size" of 0. Just set these to -12 if it happens, so
# resizing the font does something sensible. Same below.
if PROGMonoFontSize.get() == 0:
    PROGMonoFont["size"] = -12
    PROGMonoFontSize.set(-12)
# Entry() is used because it seems to be messed with less on different
# systems unlike Label() font which can be set to some bizarre stuff.
PROGPropFont = Font(font = Entry()["font"])
# Used by some plotting routines.
PROGPropFontHeight = PROGPropFont.metrics("ascent")+ \
        PROGPropFont.metrics("descent")
PROGPropFontSize = IntVar()
PROGPropFontSize.set(PROGPropFont["size"])
if PROGPropFontSize.get() == 0:
    PROGPropFont["size"] = -12
    PROGPropFontSize.set(-12)
Root.option_add("*Font", PROGPropFont)
Root.option_add("*Text*Font", PROGMonoFont)
if PROGSystem == "dar":
    PROGSystemName = "Darwin"
elif PROGSystem == "lin":
    PROGSystemName = "Linux"
elif PROGSystem == "win":
    PROGSystemName = "Windows"
elif PROGSystem == "sun":
    PROGSystemName = "Sun"
else:
    PROGSystemName = "Unknown (%s)"%PROGSystem
# Depending on the Tkinter version or how it was compiled the scroll bars can
# get pretty narrow and hard to grab.
if Scrollbar().cget("width") < "16":
    Root.option_add("*Scrollbar*width", "16")
# Just using RGB for everything since some things don't handle color names
# correctly, like PIL on macOS doesn't handle "green" very well.
# b = dark blue, was the U value for years, but it can be hard to see, so U
#     was lightened up a bit.
# Orange should be #FF7F00, but #DD5F00 is easier to see on a white background
# and it still looks OK on a black background.
# Purple should be A020F0, but that was a little dark.
# "X" should not be used. Including X at the end of a passed color pair (or by
# itself) indicates that a Toplevel or dialog box should use grab_set_global()
# which is not a color.
Clr = {"B":"#000000", "C":"#00FFFF", "G":"#00FF00", "M":"#FF00FF", \
        "R":"#FF0000", "O":"#FF7F00", "W":"#FFFFFF", "Y":"#FFFF00", \
        "E":"#DFDFDF", "A":"#8F8F8F", "K":"#3F3F3F", "U":"#0070FF", \
        "N":"#007F00", "S":"#7F0000", "y":"#7F7F00", "u":"#ADD8E6", \
        "s":"#FA8072", "p":"#FFB6C1", "g":"#90EE90", "r":"#EFEFEF", \
        "P":"#AA22FF", "b":"#0000FF"}
# This is just if the program wants to let the user know what the possibilities
# are.
ClrDesc = {"B":"black", "C":"cyan", "G":"green", "M":"magenta", \
        "R":"red", "O":"orange", "W":"white", "Y":"yellow", \
        "E":"light gray", "A":"gray", "K":"dark gray", "U":"blue", \
        "N":"dark green", "S":"dark red", "y":"dark yellow", \
        "u":"light blue", "s":"salmon", "p":"light pink", "g":"light green", \
        "r":"very light gray", "P":"purple", "b":"dark blue"}
# Now things get ugly. cget("bg") can return a hex triplet, or a color word,
# or something like "systemWindowBody". All of my programs will attempt to use
# the Root background value as their default value. This will determine what
# needs to be done to get a hex triplet value for the Root background color.
Clr["D"] = Root.cget("background")
if Clr["D"].startswith("#"):
    pass
else:
    Value = Root.winfo_rgb(Clr["D"])
    if max(Value) < 256:
        Clr["D"] = "#%02X%02X%02X"%Value
    else:
        Clr["D"] = "#%04X%04X%04X"%Value
ClrDesc["D"] = "default"
# The color of a button (like a "Stop" button) may be checked to see if a
# command is still active. Specifically set this so things just work later on.
Root.option_add("*Button*background", Clr["D"])
Root.option_add("*Button*takeFocus", "0")
# Newer versions of Tkinter are setting this to 1. That's too small.
Root.option_add("*Button*borderWidth", "2")
Root.option_add("*Canvas*borderWidth", "0")
Root.option_add("*Canvas*highlightThickness", "0")
Root.option_add("*Checkbutton*anchor", "w")
Root.option_add("*Checkbutton*takeFocus", "0")
Root.option_add("*Checkbutton*borderWidth", "1")
Root.option_add("*Entry*background", Clr["W"])
Root.option_add("*Entry*foreground", Clr["B"])
Root.option_add("*Entry*highlightThickness", "2")
Root.option_add("*Entry*insertWidth", "3")
Root.option_add("*Entry*highlightColor", Clr["B"])
Root.option_add("*Entry*disabledBackground", Clr["D"])
Root.option_add("*Entry*disabledForeground", Clr["B"])
# Same as for Button.
Root.option_add("*Label*background", Clr["D"])
Root.option_add("*Listbox*background", Clr["W"])
Root.option_add("*Listbox*foreground", Clr["B"])
Root.option_add("*Listbox*selectBackground", Clr["G"])
Root.option_add("*Listbox*selectForeground", Clr["B"])
Root.option_add("*Listbox*takeFocus", "0")
Root.option_add("*Listbox*exportSelection", "0")
Root.option_add("*Radiobutton*takeFocus", "0")
Root.option_add("*Radiobutton*borderWidth", "1")
Root.option_add("*Scrollbar*takeFocus", "0")
# When the slider is really small this might help make it easier to see, but
# I don't know what the system might set for the slider color.
Root.option_add("*Scrollbar*troughColor", Clr["A"])
Root.option_add("*Text*background", Clr["W"])
Root.option_add("*Text*foreground", Clr["B"])
Root.option_add("*Text*takeFocus", "0")
Root.option_add("*Text*highlightThickness", "0")
Root.option_add("*Text*insertWidth", "0")
Root.option_add("*Text*width", "0")
Root.option_add("*Text*padX", "3")
Root.option_add("*Text*padY", "3")
# To control the color of the buttons better in X-Windows programs.
Root.option_add("*Button*activeBackground", Clr["D"])
Root.option_add("*Checkbutton*activeBackground", Clr["D"])
Root.option_add("*Radiobutton*activeBackground", Clr["D"])
# Used by various time functions.
# First day of the month for each non-leap year month MINUS 1. This will get
# subtracted from the DOY, so a DOY of 91, minus the first day of April 90
# (91-90) will leave the 1st of April. The 365 is the 1st of Jan of the next
# year.
PROG_FDOM = (0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365)
# Max days per month.
PROG_MAXDPMNLY = (0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
PROG_MAXDPMLY = (0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
# Not very friendly to other countries, but...
PROG_CALMON = ("", "JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY", "JUNE", \
        "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER")
PROG_CALMONS = ("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", \
        "SEP", "OCT", "NOV", "DEC")
PROG_MONNUM = {"JAN":1, "FEB":2, "MAR":3, "APR":4, "MAY":5, "JUN":6, "JUL":7, \
        "AUG":8, "SEP":9, "OCT":10, "NOV":11, "DEC":12}
# For use with the return of the calendar module weekday function.
PROG_DOW = ("Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun")
# Stores the number of seconds to the beginning of a year so they don't have
# to be recalculated all the time.
Y2EPOCH = {}
# First characters that can be used to modifiy searches (only used in ITS so
# far).
PROG_SEARCHMODS = [">=","<=","!=","!~",">","<","=","~","_","*"]
# A second set of help lines that describes error messages that may be seen by
# the user. It gets filled in by the headers of the modules that generate the
# messages, and the contents will be inserted in the regular help by
# formHELP().
HELPError = ""
# END: option_add

# Changes to option_adds for CHANGEO.
Root.option_add("*Checkbutton*takeFocus", "1")
Root.option_add("*Radiobutton*takeFocus", "1")

# For sendToPIS().
PROG_INCOMINGSERVER = "piss.passcal.nmt.edu"
PROG_INCOMINGPORT = 15142




# ==============================================
# BEGIN: ========== PROGRAM FUNCTIONS ==========
# ==============================================


######################
# BEGIN: allPortsCmd()
# FUNC:allPortsCmd():2019.191
#   Quickly cycles the serial port on and off 15 times to get the serial
#   controller to enable all of the serial ports.
def allPortsCmd():
    msgLn(1, "C", "All Ports Command...", False)
    buttonControl("ALLPORTS", "Y", 1)
    for i in arange(0, 15):
        Ret = cmdPortOpen()
        if Ret == False:
            break
        cmdPortClose()
        sleep(0.1)
        updateMe(0)
#&&&&& needs to check running

    msgLn(1, "", "done.", True, 1)
    buttonControl("ALLPORTS", "", 0)
    return
# END: allPortsCmd




##############################
# BEGIN: class BButton(Button)
# LIB:BButton():2009.239
#   A sub-class of Button() that adds a bit of additional color control and
#   that adds a space before and after the text on Windows systems, otherwise
#   the edge of the button is right at the edge of the text.
class BButton(Button):
    def __init__(self, master = None, **kw):
        if PROGSystem == "win" and "text" in kw:
            if kw["text"].find("\n") == -1:
                kw["text"] = " "+kw["text"]+" "
            else:
# Add " " to each end of each line so all lines get centered.
                parts = kw["text"].split("\n")
                ntext = ""
                for part in parts:
                    ntext += " "+part+" \n"
                kw["text"] = ntext[:-1]
# Some systems have the button change color when rolled over.
        if "bg" in kw:
            kw["activebackground"] = kw["bg"]
        if "fg" in kw:
            kw["activeforeground"] = kw["fg"]
        Button.__init__(self, master, **kw)
# END: BButton




################################
# BEGIN: beep(Howmany, e = None)
# LIB:beep():2018.235
#   Just rings the terminal bell the number of times requested.
# NEEDS: from time import sleep
#        updateMe()
PROGNoBeepingCRVar = IntVar()
PROGSetups += ["PROGNoBeepingCRVar"]

def beep(Howmany, e = None):
    if PROGNoBeepingCRVar.get() == 0:
# In case someone passes something wild.
        if Howmany > 20:
            Howmany = 20
        for i in arange(0, Howmany):
            Root.bell()
            if i < Howmany-1:
                updateMe(0)
                sleep(.15)
    return
# END: beep




#########################################
# BEGIN: buttonBG(Butt, Colr, State = "")
# LIB:buttonBG():2018.234
def buttonBG(Butt, Colr, State = ""):
# Try since this may get called without the button even existing.
    try:
        if isinstance(Butt, astring):
# Set both items to keep the button from changing colors on *NIXs when the
# mouse rolls over. Also only use the first character of the passed value so
# we can pass bg/fg color pairs.
# If the State is "" leave it alone, otherwise change it to what we are told.
            if len(State) == 0:
                PROGButs[Butt].configure(bg = Clr[Colr[0]], \
                        activebackground = Clr[Colr[0]])
            else:
                PROGButs[Butt].configure(bg = Clr[Colr[0]], \
                        activebackground = Clr[Colr[0]], state = State)
        else:
            if len(State) == 0:
                Butt.configure(bg = Clr[Colr[0]], \
                        activebackground = Clr[Colr[0]])
            else:
                Butt.configure(bg = Clr[Colr[0]], \
                        activebackground = Clr[Colr[0]], state = State)
        updateMe(0)
    except:
        pass
    return
# END: buttonBG




############################################
# BEGIN: buttonControl(Button, Colur, State)
# FUNC:buttonControl():2018.289
#   Change the state of the passed button and change the state of the STOP
#   button as appropriate.
def buttonControl(Butt, Colur, State):
    global WorkState
# Test over.
    if State == 0:
        if Butt is not None:
            buttonBG(Butt, "D")
        buttonBG("STOP", "D")
        PROGButs["STOP"].configure(state = DISABLED)
        WorkState = 0
# Test starting.
    elif State == 1:
        if Butt is not None:
            buttonBG(Butt, Colur)
        PROGButs["STOP"].configure(state = NORMAL)
        buttonBG("STOP", "R")
    if Butt is not None:
        PROGButs[Butt].update()
    PROGButs["STOP"].update()
    return
# END: buttonControl




#######################
# BEGIN: calCmd(VarSet)
# FUNC:calCmd():2018.289
def calCmd(VarSet):
    global WorkState
    Cmd = ""
    Status, ListOfDASs = isItOKToStart(3)
    if Status == False:
        return
    cleanCALS(VarSet)
    Which = eval("%sWhichRVar"%VarSet).get()
    if isgoodCALS(VarSet, Which) == False:
        return
    WorkState = 100
    WhichButton = ""
    if Which == "STEP":
        WhichButton = "STEPCAL"
    elif Which.startswith("S"):
        WhichButton = "SINECAL"
    else:
        WhichButton = "NOISECAL"
    buttonControl(WhichButton, "G", 1)
    formSTATSetStatus("all", "B")
    Chans = eval("%sChannelsRVar"%VarSet).get()
    Duration = eval("%sDurationVar"%VarSet).get()
    Amplitude = eval("%sAmplitudeVar"%VarSet).get()
    StepInterval = eval("%sStepIntervalVar"%VarSet).get()
    StepWidth = eval("%sStepWidthVar"%VarSet).get()
    Frequency = eval("%sFrequencyRVar"%VarSet).get()
    if cmdPortOpen() == False:
        buttonControl(WhichButton, "", 0)
        return
    Count = 0
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        if DAS != "0000" and Duration == "8":
            Answer = formMYD(Root, (("Send", LEFT, "send"), \
                    ("Stop", LEFT, "stop")), "stop", "", "Next!", \
                    "Send short cal commands to DAS %s?"%DAS)
            if Answer == "stop":
                WorkState = 999
                break
            updateMe(0)
        formSTATSetStatus(DAS, "C")
        AllOK = True
        Count += 1
        if Which == "STEP":
            msgLn(1, "", "%d. %s: Start Step Cal Channels %s... (%s)"% \
                    (Count, DAS, Chans, getGMT(0)))
        elif Which.startswith("S"):
            msgLn(1, "", \
                    "%d. %s: Start Sine Cal (%s) Channels %s... (%s)"% \
                    (Count, DAS, Which, Chans, getGMT(0)))
        else:
            msgLn(1, "", \
                    "%d. %s: Start Noise Cal (%s) Channels %s... (%s)"% \
                    (Count, DAS, Which, Chans, getGMT(0)))
# Things go more smoothly when a reset is done to first turn off any
# calibration command that may already be running.
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "C", "   Resetting...")
        Cmd = rt130CmdFormat(DAS, "RS  RS")
        cmdPortWrite(DAS, Cmd)
# If we are talking to all of the DASs then don't bother reading the response,
# but still delay for a bit to allow the DASs time to finish resetting. The
# amount of time will be from about 4 to about 20 seconds depending on how
# many CF cards need to be checked.
        if DAS == "0000":
            delay(20)
            if WorkState == 999:
                continue
        else:
            Rp = rt130GetRp(1, 5, 4)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", "   No Reset command response!", True, 3)
                AllOK = False
                WorkState = 999
                continue
# In general we will always break out and stop when something like a DAS starts
# talking out of turn since once something like that starts there's no way to
# stop it, except to just stop and wait.
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# Since we know who we are working with try the IDID command until we get a
# response so we don't wait the full 20 seconds if we don't have to (like will
# be the case when using DAS 0000).
            Cmd = rt130CmdFormat(DAS, "IDID")
            for i in arange(0, 20):
                cmdPortWrite(DAS, Cmd)
                Rp = rt130GetRp(1, 1, 0)
                if len(Rp) != 0 or WorkState == 999:
                    break
            if WorkState == 999:
                continue
            if i == 20:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", "   No command response after reset!", True, 3)
                AllOK = False
                continue
# Send the calibration parameters.
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "C", "   Sending parameters...")
        Sensor = ""
        if Chans == "123":
            Sensor = "1"
        elif Chans == "456":
            Sensor = "2"
        Cmd = rt130CmdFormat(DAS, "PK"+Sensor+"E  "+padR(Duration, 4)+ \
                padR(Amplitude, 4)+Which+padR(StepInterval, 4)+ \
                padR(StepWidth, 4)+padR(Frequency, 4)+"PK")
        cmdPortWrite(DAS, Cmd)
        if DAS == "0000":
            delay(14)
            if WorkState == 999:
                continue
        else:
            Rp = rt130GetRp(1, 10, 2)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", \
                        "   No Calibration Parameters command response!", \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# Implement parameters.
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "C", "   Implementing...")
        Cmd = rt130CmdFormat(DAS, "PIPI")
        cmdPortWrite(DAS, Cmd)
        if DAS == "0000":
            delay(9)
            if WorkState == 999:
                continue
        else:
            Rp = rt130GetRp(1, 10, 2)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", "   No Implement Parameters command response!", \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# Send Sensor Calibration command to start
        msgLn(1, "C", "   Starting calibration signal...")
        Cmd = rt130CmdFormat(DAS, "SK"+Sensor+"SSK")
        cmdPortWrite(DAS, Cmd)
        if DAS == "0000":
            delay(10)
            if WorkState == 999:
                continue
        else:
            Rp = rt130GetRp(1, 10, 0)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", "   No Start Calibration command response!", \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
        formSTATSetStatus(DAS, "G")
    cmdPortClose()
    if WorkState == 999 and AllOK == True:
        formSTATSetStatus("working", "Y")
        msgLn(1, "Y", "Some DASs may still be working.")
    stoppedMe(WorkState, DAS)
    buttonControl(WhichButton, "", 0)
    return
# END: calCmd




###############################################################################
# BEGIN: center(Parent, TheFrame, Where, InOut, Show = True, CenterX = 0, \
#                CenterY = 0)
# LIB:center():2019.002
#   Where tells the function where in relation to the Parent TheFrame should
#   show up. Use the diagram below to figure out where things will end up.
#   Where can also be NX,SX,EX,etc. to force edge of the display checking for\
#   when you absolutely, positively don't want something coming up off the
#   edge of the display. "C" or "" can be used to put TheFrame in the center
#   of Parent.
#
#      +---------------+
#      | NW    N    NE |
#      |               |
#      | W     C     E |
#      |               |
#      | SW    S    SE |
#      +---------------+
#
#   Set Parent to None to use the whole display as the parent.
#   Set InOut to "I" or "O" to control if TheFrame shows "I"nside or "O"outside
#   the Parent (does not apply if the Parent is None).
#
#   CenterX and CenterY not equal to zero overrides everything.
#
def center(Parent, TheFrame, Where, InOut, Show = True, CenterX = 0, \
        CenterY = 0):
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    if isinstance(TheFrame, astring):
        TheFrame = PROGFrm[TheFrame]
# Size of the display(s). Still won't be good for dual displays, but...
    DW = PROGScreenWidthNow
    DH = PROGScreenHeightNow
# Kiosk mode. Just take over the whole screen. Doesn't check for, but only
# works for Root. The -30 is a fudge, because systems lie about their height
# (but not their age).
    if Where == "K":
        Root.geometry("%dx%d+0+0"%(DW, DH-30))
        Root.deiconify()
        Root.lift()
        updateMe(0)
        return
# So all of the dimensions get updated.
    updateMe(0)
    FW = TheFrame.winfo_reqwidth()
    if TheFrame == Root:
# Different systems have to be compensated for a little because of differences
# in the reported heights (mostly title and menu bar heights). Some systems
# include the height and some don't, and, of course, it all depends on the
# font sizes, so there is little chance of this fudge ever being 100% correct.
        if PROGSystem == "dar" or PROGSystem == "win":
            FH = TheFrame.winfo_reqheight()
        else:
            FH = TheFrame.winfo_reqheight()+50
    else:
        FH = TheFrame.winfo_reqheight()
# Find the center of the Parent.
    if CenterX == 0 and CenterY == 0:
        if Parent is None:
            PX = 0
            PY = 0
            PW = PROGScreenWidthNow
            PH = PROGScreenHeightNow
# A PW of >2560 (the width of a 27" iMac) probably means the user has two
# monitors. Tkinter just gets fed the total width and the smallest display's
# height, so just set the size to 1024x768 and then let the user resize and
# reposition as needed. It's what they get for being so lucky.
            if PW > 2560:
                PW = 1024
                PH = 768
            CenterX = PW/2
            CenterY = PH/2-25
        elif Parent == Root:
            PX = Parent.winfo_x()
            PW = Parent.winfo_width()
            CenterX = PX+PW/2
# Macs, Linux and Suns think the top of the Root window is below the title
# and menu bars.  Windows thinks the top of the window is the top of the
# window, so adjust the window heights accordingly to try and cover that up.
# Same problem as the title and menu bars.
            if PROGSystem == "win":
                PY = Parent.winfo_y()
                PH = Parent.winfo_height()
            else:
                PY = Parent.winfo_y()-50
                PH = Parent.winfo_height()+50
            CenterY = PY+PH/2
        else:
            PX = Parent.winfo_x()
            PW = Parent.winfo_width()
            CenterX = PX+PW/2
            PY = Parent.winfo_y()
            PH = Parent.winfo_height()
            CenterY = PY+PH/2
# Can't put forms outside the whole display.
        if Parent is None or InOut == "I":
            InOut = 1
        else:
            InOut = -1
        HadX = False
        if Where.find("X") != -1:
            Where = Where.replace("X", "")
            HadX = True
        if Where == "C":
            XX = CenterX-FW/2
            YY = CenterY-FH/2
        elif Where == "N":
            XX = CenterX-FW/2
            YY = PY+(50*InOut)
        elif Where == "NE":
            XX = PX+PW-FW-(50*InOut)
            YY = PY+(50*InOut)
        elif Where == "E":
            XX = PX+PW-FW-(50*InOut)
            YY = CenterY-TheFrame.winfo_reqheight()/2
        elif Where == "SE":
            XX = PX+PW-FW-(50*InOut)
            YY = PY+PH-FH-(50*InOut)
        elif Where == "S":
            XX = CenterX-TheFrame.winfo_reqwidth()/2
            YY = PY+PH-FH-(50*InOut)
        elif Where == "SW":
            XX = PX+(50*InOut)
            YY = PY+PH-FH-(50*InOut)
        elif Where == "W":
            XX = PX+(50*InOut)
            YY = CenterY-TheFrame.winfo_reqheight()/2
        elif Where == "NW":
            XX = PX+(50*InOut)
            YY = PY+(50*InOut)
# Try to make sure the system's title bar buttons are visible (which may not
# always be functioning, but there you go).
        if HadX == True:
# None are on the bottom.
            if (CenterY+FH/2) > DH:
                YY = YY-((CenterY+FH/2)-DH+20)
# Never want things off the top.
            if YY < 0:
                YY = 10
# But now it is OS-dependent.
# Buttons in upper-left. Fix the right edge, but then check to see if it needs
# to be moved back to the right.
            if PROGSystem == "dar" or PROGSystem == "sun":
                if (CenterX+FW/2) > DW:
                    XX = XX-((CenterX+FW/2)-DW+20)
                if XX < 0:
                    XX = 10
# Opposite corner.
            elif PROGSystem == "lin" or PROGSystem == "win":
                if XX < 0:
                    XX = 10
                if (CenterX+FW/2) > DW:
                    XX = XX-((CenterX+FW/2)-DW+20)
        TheFrame.geometry("+%i+%i"%(XX, YY))
    else:
# Just do what we're told.
        TheFrame.geometry("+%i+%i"%(CenterX-FW/2, CenterY-FH/2))
    if Show == True:
        TheFrame.deiconify()
        TheFrame.lift()
    updateMe(0)
    return
# END: center




############################
# BEGIN: centeringVoltsCmd()
# FUNC:centeringVoltsCmd():2018.289
#   Steps through the list of DASs and prints the centering voltages.
def centeringVoltsCmd():
    global WorkState
    Status, ListOfDASs = isItOKToStart(2)
    if Status == False:
        return
    if CV123Var.get() == 0 and CV456Var.get() == 0:
        msgLn(0, "R", "Select something to get the status of.", True, 2)
        return
    WorkState = 100
    buttonControl("CENTVOLTS", "G", 1)
    formSTATSetStatus("all", "B")
    if cmdPortOpen() == False:
        buttonControl("CENTVOLTS", "", 0)
        return
    Count = 0
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        formSTATSetStatus(DAS, "C")
        AllOK = True
        Count += 1
        msgLn(1, "W", "%d. %s: Centering voltages... (%s)"% \
                (Count, DAS, getGMT(0)))
# Auxiliary Data request.
        Cmd = rt130CmdFormat(DAS, "SSAD              SS")
        cmdPortWrite(DAS, Cmd)
# No Lag. Info request.
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   No Auxiliary Data command response!", True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
# The request always seems to return voltages for "both" sensors (ch 123 456)
# even though most units only have channels 123 (it would return more if there
# were hardware installed for more than 6 channels -- that's not supported
# here -- just use the value of Sensor Count and add a loop).
        if CV123Var.get() != 0:
            msgLn(1, "", "   Channels 123:  ", False)
            Value = floatt(Rp[52:52+4])
            if Value > -1.5 and Value < 1.5:
                msgLn(1, "G", Rp[52:52+4]+"  ", False)
            else:
                msgLn(1, "Y", Rp[52:52+4]+"  ", False)
            Value = floatt(Rp[56:56+4])
            if Value > -1.5 and Value < 1.5:
                msgLn(1, "G", Rp[56:56+4]+"  ", False)
            else:
                msgLn(1, "Y", Rp[56:56+4]+"  ", False)
            Value = floatt(Rp[60:60+4])
            if Value > -1.5 and Value < 1.5:
                msgLn(1, "G", Rp[60:60+4])
            else:
                msgLn(1, "Y", Rp[60:60+4])
        if CV456Var.get() != 0:
            msgLn(1, "", "   Channels 456:  ", False)
            Value = floatt(Rp[82:82+4])
            if Value > -1.5 and Value < 1.5:
                msgLn(1, "G", Rp[82:82+4]+"  ", False)
            else:
                msgLn(1, "Y", Rp[82:82+4]+"  ", False)
            Value = floatt(Rp[86:86+4])
            if Value > -1.5 and Value < 1.5:
                msgLn(1, "G", Rp[86:86+4]+"  ", False)
            else:
                msgLn(1, "Y", Rp[86:86+4]+"  ", False)
            Value = floatt(Rp[90:90+4])
            if Value > -1.5 and Value < 1.5:
                msgLn(1, "G", Rp[90:90+4])
            else:
                msgLn(1, "Y", Rp[90:90+4])
        formSTATSetStatus(DAS, "G")
    cmdPortClose()
    if WorkState == 999 and AllOK == True:
        formSTATSetStatus("working", "Y")
    stoppedMe(WorkState, DAS)
    buttonControl("CENTVOLTS", "", 0)
    return
# END: centeringVoltsCmd




#####################################################################
# BEGIN: changeMainDirs(Parent, Which, Mode, Var, Title, WhichDir="")
# LIB:changeMainDirs():2018.234
# Needs PROGFrm, formMYDF(), and msgLn().
#   Mode = formMYDF() mode value (some callers may not want to allow directory
#          creation, for example).
#      1 = just picking
#      2 = picking and creating
#  D,W,M = may be added to the Mode for the main directories Default button
#          (see formMYDF()).
#   Var = if not None the selected directory will be placed there, instead of
#         one of the "main" Vars. May not be used in all programs.
#   Title = Will be used for the title of the form if Var is not None.
# WhichDir = If supplied this will be 'which directory?' was changed in the
#            change message when Which is "self".
#   Not all programs will use all items.
def changeMainDirs(Parent, Which, Mode, Var, Title, WhichDir=""):
# The caller can pass either.
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    if Which == "theall":
# Just use the directory as a starting point.
        Answer = formMYDF(Parent, Mode, "Pick A Main Directory For All", \
                PROGMsgsDirVar.get(), "")
        if len(Answer) == 0:
            return (1, "", "Nothing done.", 0, "")
        else:
# Some of these may not exist in a program.
            try:
                PROGDataDirVar.set(Answer)
            except:
                pass
            try:
                PROGMsgsDirVar.set(Answer)
            except:
                pass
            try:
                PROGWorkDirVar.set(Answer)
            except:
                pass
            return (0, "WB", "All main directories changed to\n   %s"% \
                    Answer, 0, "")
    elif Which == "thedata":
        Answer = formMYDF(Parent, Mode, "Pick A Main Data Directory", \
                PROGDataDirVar.get(), "")
        if len(Answer) == 0:
            return (1, )
        elif Answer == PROGDataDirVar.get():
            return (0, "", "Main data directory unchanged.", 0, "")
        else:
            PROGDataDirVar.set(Answer)
            return (0, "WB", "Main Data directory changed to\n   %s"% \
                    Answer, 0, "")
    elif Which == "themsgs":
        Answer = formMYDF(Parent, Mode, "Pick A Main Messages Directory", \
                PROGMsgsDirVar.get(), "")
        if len(Answer) == 0:
            return (1, )
        elif Answer == PROGMsgsDirVar.get():
            return (0, "", "Main messages directory unchanged.", 0, "")
        else:
            PROGMsgsDirVar.set(Answer)
            return (0, "WB", "Main Messages directory changed to\n   %s"% \
                    Answer, 0, "")
    elif Which == "thework":
        Answer = formMYDF(Parent, Mode, "Pick A Main Work Directory", \
                PROGWorkDirVar.get(), "")
        if len(Answer) == 0:
            return (1, )
        elif Answer == PROGWorkDirVar.get():
            return (0, "", "Main work directory unchanged.", 0, "")
        else:
            PROGWorkDirVar.set(Answer)
            return (0, "WB", "Main work directory changed to\n   %s"% \
                    Answer, 0, "")
# Var and Title must be set for "self".
    elif Which == "self":
        Answer = formMYDF(Parent, Mode, Title, Var.get(), Title)
        if len(Answer) == 0:
            return (1, )
        elif Answer == Var.get():
            if len(WhichDir) == 0:
                return (0, "", "Directory unchanged.", 0, "")
            else:
                return (0, "", "%s directory unchanged."%WhichDir, 0, "")
        else:
            Var.set(Answer)
        if len(WhichDir) == 0:
            return (0, "WB", "Directory changed to\n   %s"%Answer, 0, "")
        else:
            return (0, "WB", "%s directory changed to\n   %s"%(WhichDir, \
                    Answer), 0, "")
    return
###############################################################################
# BEGIN: changeMainDirsCmd(Parent, Which, Mode, Var, Title, WhichDir, e = None)
# FUNC:changeMainDirsCmd():2014.062
def changeMainDirsCmd(Parent, Which, Mode, Var, Title, WhichDir, e = None):
    Ret = changeMainDirs(Parent, Which, Mode, Var, Title, WhichDir)
    if Ret[0] == 0:
# Some programs may not have a messages area.
        try:
            msgLn(0, Ret[1], Ret[2], True, Ret[3])
        except:
            pass
    return
# END: changeMainDirs




#######################
# BEGIN: check130ID(ID)
# FUNC:check130ID():2012.334
#   Returns True if the passed Unit ID is OK, False if not.
def check130ID(ID):
    if len(ID) != 4:
        return (1, "RW", "ID length is not 4 characters: %s"%ID, 2)
    ID = ID.strip().upper()
    if isHex(ID) == False:
        return (1, "RW", "ID is not a hex number: %s"%ID, 2)
    if ID < "9000":
        return (1, "RW", "ID is < 9000: %s"%ID, 2)
    return (0, )
# END: check130ID




##########################################################
# BEGIN: checkCParms(Ret, Chan, Gain, Name, Gain, Comment)
# FUNC:checkCParms():2009.082
#   Checks the Channel parameters and generates error messages if anything
#   is wrong. Returns True if OK, False if not.
def checkCParms(Ret, Chan, Gain, Name, Comment):
    MAXCHANNAMELEN = 10
    MAXCHANCOMMENTLEN = 40
# If the Gain.get() is 0 then this channel will be considered not in use.
    if Gain.get() == 0:
        return True
    AllOK = True
    Name.set(Name.get().strip())
    if len(Name.get()) > MAXCHANNAMELEN:
        msgLn(0, "R", "Channel %d name is too long (%dchrs)."%(Chan, \
                MAXCHANNAMELEN), True, 2)
        if Ret == True:
            return False
        AllOK = False
    Comment.set(Comment.get().strip())
    if len(Comment.get()) > MAXCHANCOMMENTLEN:
        msgLn(0, "R", "Channel %d comment is too long (%dchrs)."%(Chan, \
                MAXCHANCOMMENTLEN), True, 2)
        if Ret == True:
            return False
        AllOK = False
    return AllOK
# END: checkCParms




#############################
# BEGIN: checkDSParms(Ret, d)
# FUNC:checkDSParms():2018.289
#   Checks the data stream  parameters and generates error messages if anything
#   is wrong. Returns True if OK, False if not.
def checkDSParms(Ret, d):
# If the type is 0 then this data stream will be considered not used.
    if eval("PDS%dTType"%d).get() == 0:
        return True
    AllOK = True
    if eval("PDS%dChan1"%d).get() == 0 and \
            eval("PDS%dChan2"%d).get() == 0 and \
            eval("PDS%dChan3"%d).get() == 0 and \
            eval("PDS%dChan4"%d).get() == 0 and \
            eval("PDS%dChan5"%d).get() == 0 and \
            eval("PDS%dChan6"%d).get() == 0:
        msgLn(0, "R", "DS%d no channels selected."%d, True, 2)
        if Ret == True:
            return False
        AllOK = False
    else:
# If some channels are selected make sure that the gain for those channels
# have been set to something (i.e. check that the channels have been
# activated).
        for i in arange(1, 1+6):
            if eval("PDS%dChan%d"%(d,i)).get() != 0:
                if eval("PC%dGain"%i).get() == 0:
                    msgLn(0, "R", \
                            "DS%d channel %d used, but no gain selected."% \
                            (d, i), True, 2)
                    if Ret == True:
                        return False
                    AllOK = False
    eval("PDS%dSampRate"%d).set(eval("PDS%dSampRate"%d).get().strip())
    if rt72130CheckSampRate(eval("PDS%dSampRate"%d).get()) == False:
        msgLn(0, "R", "DS%d sample rate is bad."%d, True, 2)
        if Ret == True:
            return False
        AllOK = False
    if len(eval("PDS%dSampRate"%d).get()) == 0:
        msgLn(0, "R", "DS%d no sample rate entered."%d, True, 2)
        if Ret == True:
            return False
        AllOK = False
    Value = eval("PDS%dFormat"%d).get()
    if len(eval("PDS%dFormat"%d).get()) == 0:
        msgLn(0, "R", "DS%d no data format selected."%d, True, 2)
        if Ret == True:
            return False
        AllOK = False
    elif Value not in PARMSFormatCodes:
        msgLn(0, "R", "DS%d bad format code: %s"%(d, Value), True, 2)
        if Ret == True:
            return False
        AllOK = False
    eval("PDS%dName"%d).set(eval("PDS%dName"%d).get().strip())
    if len(eval("PDS%dName"%d).get()) > 16:
        msgLn(0, "R", "DS%d name is too long."%d, True, 2)
        if Ret == True:
            return False
        AllOK = False
    if eval("PDS%dDisk"%d).get() == 0 and eval("PDS%dEther"%d).get() == 0 and \
            eval("PDS%dSerial"%d).get() == 0 and eval("PDS%dRam"%d).get() == 0:
        msgLn(0, "R", "DS%d no data destination selected."%d, True, 2)
        if Ret == True:
            return False
        AllOK = False
    return AllOK
# END: checkDSParms




#######################################
# BEGIN: checkForUpdates(Parent = Root)
# LIB:checkForUpdates():2019.260
#   Finds the "new"+PROG_NAMELC+".txt" file created by the program webvers at
#   the URL and checks to see if the version in that file matches the version
#   of this program.
VERS_DLALLOW = True
VERS_VERSURL = "http://www.passcal.nmt.edu/~bob/passoft/"
VERS_PARTS = 4
VERS_NAME = 0
VERS_VERS = 1
VERS_USIZ = 2
VERS_ZSIZ = 3

def checkForUpdates(Parent = Root):
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    formMYD(Parent, (), "", "CB", "", "Checking...")
# Otherwise the menu doesn't go away on slow connections while url'ing.
    updateMe(0)
# May prevent an ssl CERTIFICATE_VERIFY_FAILED error if things are not right.
# If this fails the urlopen() may too, so just go on.
    try:
        import ssl
        if (environ.get("PYTHONHTTPSVERIFY") == None or \
                getattr(ssl, "_create_unverified_context", "") == ""):
            ssl._create_default_https_context = ssl._create_unverified_context
    except:
        pass
# Get the file that tells us about the current version on the server.
# One line:  PROG; version; original size; compressed size
    try:
        Fp = urlopen(VERS_VERSURL+"new"+PROG_NAMELC+".txt")
        Line = Fp.readlines()
        Fp.close()
        formMYDReturn("")
# If nothing was returned Line will be [] and that will except, also do the
# decode for Py3, otherwise it comes with a b' in front.
        Line = Line[0].decode("latin-1")
# If the file doesn't exist you get something like
#     <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
# How unhandy.
        if Line.find("DOCTYPE") != -1:
            Line = ""
# We won't say anything in particular.
        Error = ""
    except Exception as e:
        Line = ""
        Error = str(e)
# If we didn't get this then there must have been a problem.
    if len(Line) == 0:
        formMYDReturn("")
        formMYD(Parent, (("(OK)", TOP, "ok"), ), "ok", "RW", \
                "It's Probably The Net.", \
        "There was an error obtaining the version information from PASSCAL.", \
                Error, 2)
        return
    Parts2 = Line.split(";")
    Parts = []
    for Part in Parts2:
        Parts.append(Part.strip())
    Parts += (VERS_PARTS-len(Parts))*[""]
    if PROG_VERSION < Parts[VERS_VERS]:
# Some programs don't need to be professionally installed, some do.
        if VERS_DLALLOW == True:
            Answer = formMYD(Parent, (("Download New Version", TOP, \
                    "dlprod"), ("(Don't)", TOP, "dont"), ), "dont", \
                    "YB", "Oh Oh...", \
                    "This is an old version of %s.\nThe current version generally available is %s."% \
                    (PROG_NAME, Parts[VERS_VERS]), "", 2)
            if Answer == "dont":
                return
            if Answer == "dlprod":
                Ret = checkForUpdatesDownload(Parent, Answer, Parts)
            if Ret == "quit":
                progQuitter(True)
                return
        elif VERS_DLALLOW == False:
            Answer = formMYD(Parent, (("(OK)", TOP, "ok"), ), "ok", \
                    "YB", "Tell Someone.", \
                    "This is an old version of %s.\nThe current version generally available is %s."% \
                    (PROG_NAME, Parts[VERS_VERS]), "", 2)
            return
    elif PROG_VERSION == Parts[VERS_VERS]:
        Answer = formMYD(Parent, (("Download Anyway", TOP, "dlprod"), \
                ("(OK)", TOP, "ok")), "ok", "", "Good To Go.", \
                "This copy of %s is up to date."%PROG_NAME)
        if Answer == "ok":
            return
        if Answer == "dlprod":
            Ret = checkForUpdatesDownload(Parent, Answer, Parts)
        if Ret == "quit":
            progQuitter(True)
        return
    elif PROG_VERSION > Parts[VERS_VERS]:
        if VERS_DLALLOW == False:
            Answer = formMYD(Parent, (("(OK)", TOP, "ok"), ), "ok", "GB", \
                    "All Right!", \
                    "Congratulations! This is a newer version of %s than is generally available. Everyone else probably still has version %s."% \
                    (PROG_NAME, Parts[VERS_VERS]))
        elif VERS_DLALLOW == True:
            Answer = formMYD(Parent, (("Download Older Version", TOP, \
                    "dlprod"), ("(No, Thanks.)", TOP, "ok"), ), "ok", \
                    "GB", "All Right!!", \
                    "Congratulations! This is a newer version of %s than is generally available. Everyone else probably still has version %s. You can download and use the older version if you want."% \
                    (PROG_NAME, Parts[VERS_VERS]))
        if Answer == "ok" or Answer == "keep":
            return
        if Answer == "dlprod":
            Ret = checkForUpdatesDownload(Parent, Answer, Parts)
        if Ret == "quit":
            progQuitter(True)
        return
    return
######################################################
# BEGIN: checkForUpdatesDownload(Parent, Which, Parts)
# FUNC:checkForUpdatesDownload():2019.028
def checkForUpdatesDownload(Parent, Which, Parts):
    formMYD(Parent, (), "", "CB", "", "Downloading...")
    ZSize = int(Parts[VERS_ZSIZ])
    try:
        if Which == "dlprod":
            GetFile = "new%s.zip"%PROG_NAMELC
            Fpr = urlopen(VERS_VERSURL+GetFile)
# SetupsDir may not be the best place to put it, but at least it will be
# consistant and not dependent on where the user was working (like it was).
# If a program does not use PROGSetupsDirVar it will be the current working
# directory.
        SetupsDir = PROGSetupsDirVar.get()
        if len(SetupsDir) == 0:
            SetupsDir = "%s%s"%(abspath("."), sep)
        try:
# Just open() so I don't interfere with what the OS wants to do.
            Fpw = open(SetupsDir+GetFile, "w")
        except Exception as e:
            formMYDReturn("")
            formMYD(Parent, (("(OK)", TOP, "ok"),), "ok", "RW", "Gasp!", \
                    "Error downloading %s\n\n%s"%(GetFile, e))
            return
        DLSize = 0
        while 1:
            if PROG_PYVERS == 2:
                Buffer = Fpr.read(20000)
            elif PROG_PYVERS == 3:
                Buffer = Fpr.read(20000).decode("latin-1")
            if len(Buffer) == 0:
                break
            Fpw.write(Buffer)
            DLSize += 20000
            formMYDMsg("Downloading (%d%%)...\n"%(100*DLSize/ZSize))
    except Exception as e:
# They may not exist.
        try:
            Fpr.close()
        except:
            pass
        try:
            Fpw.close()
        except:
            pass
        formMYDReturn("")
        formMYD(Parent, (("(OK)", TOP, "ok"), ), "ok", "MW", "Ooops.", \
                "Error downloading new version.\n\n%s"%e, "", 3)
        return ""
    Fpr.close()
    Fpw.close()
    formMYDReturn("")
    if Which == "dlprod":
        Answer = formMYD(Parent, (("Quit %s"%PROG_NAME, TOP, "quit"), \
                ("Don't Quit", TOP, "cont")), "cont", "GB", "Finished?", \
                "The downloaded program file has been saved as\n\n%s\n\nYou should quit %s using the Quit button below, unzip the downloaded file, test the new program file to make sure it is OK, then rename it %s.py and move it to the proper location to replace the old version.\n\nTAKE NOTE OF WHERE THE FILE HAS BEEN DOWNLOADED TO!"% \
                (SetupsDir+GetFile, PROG_NAME, PROG_NAMELC))
    if Answer == "quit":
        return "quit"
    return ""
# END: checkForUpdates




######################
# BEGIN: checkIP(Addr)
# FUNC:checkIP():2018.289
#   Takes the input and check to make sure it is something in the neighborhood
#   of a valid Ethernet address. Returns "" if it is not, otherwise the fixed
#   up address.
def checkIP(Addr):
    New = ""
# First off there needs to be 4 or 6 tuples.
    Tups = Addr.split(".")
    if len(Tups) != 4 and len(Tups) != 6:
        return New
# If there is a *.* group make sure it is just *.*.
    for i in arange(0, len(Tups)):
        Value = str(Tups[i])
        if Value == "*" or Value == "**" or Value == "***":
            Tups[i] = "*"
            continue
# If not a * then only integers.
        try:
            Value = int(Value)
        except ValueError:
            return New
        if Value < 0 or Value > 255:
            return New
# Rebuild the passed IP address.
    for i in arange(0, len(Tups)):
        if Tups[i] == "*":
            New = New+"*."
        else:
            New = New+"%d."%int(Tups[i])
    New = New[:len(New)-1]
    return New
# END: checkIP




########################
# BEGIN: checkParms(Ret)
# FUNC:checkParms():2019.044
#   Runs through all of the parameters and checks them to see if everything
#   is OK. If Ret == True returns False as soon as something is found to
#   not be OK. If everything is OK returns True. If Ret == False then True
#   will always be returned after examining all of the parameters.
def checkParms(Ret):
    AllOK = True
# Check the station parameters.
    PExpName.set(PExpName.get().strip())
    if len(PExpName.get()) > 24:
        msgLn(0, "R", "Experiment name is too long (24chrs).", True, 2)
        if Ret == True:
            return False
        AllOK = False
    PExpComment.set(PExpComment.get().strip())
    if len(PExpComment.get()) > 40:
        msgLn(0, "R", "Experiment comment is too long (40chrs).", True, 2)
        if Ret == True:
            return False
        AllOK = False
# Check to see that there are some channels marked "in use".
# Setting the gain to something "activates" a channel.
    if PC1Gain.get() == 0 and PC2Gain.get() == 0 and PC3Gain.get() == 0 and \
            PC4Gain.get() == 0 and PC5Gain.get() == 0 and PC6Gain.get() == 0:
        msgLn(0, "R", "No channels have been set up.", True, 2)
        if Ret == True:
            return False
        AllOK = False
# Check each channel.
    for c in arange(1, 1+6):
        if checkCParms(Ret, c, eval("PC%dGain"%c), eval("PC%dName"%c), \
                eval("PC%dComment"%c)) == False:
            if Ret == True:
                return False
            AllOK = False
# Begin checking the data streams.
# Selecting a trigger type "activates" a stream.
    if PDS1TType.get() == 0 and PDS2TType.get() == 0 and \
            PDS3TType.get() == 0 and PDS4TType.get() == 0:
        msgLn(0, "R", "No data stream trigger types have been selected.", \
                True, 2)
        if Ret == True:
            return False
        AllOK = False
    for d in arange(1, 1+4):
        if checkDSParms(Ret, d) == False:
            if Ret == True:
                return False
            AllOK = False
# Only certain combinations of sample rates are allowed between the different
# data streams.
    if checkSRCombinations(PDS1SampRate, PDS2SampRate, PDS3SampRate, \
                    PDS4SampRate) == False:
        if Ret == True:
            return False
        AllOK = False
# Check the trigger parameters.
    for d in arange(1, 1+4):
        if checkTrigParms(Ret, d) == False:
            if Ret == True:
                return False
            AllOK = False
# Auxiliary Data Parameters.
    if PAuxSPer.get() != 0:
        PAuxMarker.set(PAuxMarker.get().strip())
        if len(PAuxMarker.get()) > 2:
            msgLn(0, "R", "Auliliary data marker too long (2digts).", True, 2)
            if Ret == True:
                return False
            AllOK = False
        if PAuxDisk.get() == 0 and PAuxEther.get() == 0 and \
                PAuxSerial.get() == 0 and PAuxRam.get() == 0:
            msgLn(0, "R", "No auxiliary data destination selected.", True, 2)
            if Ret == True:
                return False
            AllOK = False
        if PAuxChan1.get() == 0 and PAuxChan2.get() == 0 and \
                PAuxChan3.get() == 0 and PAuxChan4.get() == 0 and \
                PAuxChan5.get() == 0 and PAuxChan6.get() == 0 and \
                PAuxChan7.get() == 0 and PAuxChan8.get() == 0 and \
                PAuxChan9.get() == 0 and PAuxChan10.get() == 0 and \
                PAuxChan11.get() == 0 and PAuxChan12.get() == 0 and \
                PAuxChan13.get() == 0 and PAuxChan14.get() == 0 and \
                PAuxChan15.get() == 0 and PAuxChan16.get() == 0:
            msgLn(0, "R", "No auxiliary data channel(s) selected.", True, 2)
            if Ret == True:
                return False
            AllOK = False
        PAuxRl.set(PAuxRl.get().strip())
        if checkValue(PAuxRl.get(), "F", 0.0, 99999999.0) == False or \
                len(PAuxRl.get()) > 8:
            msgLn(0, "R", \
                    "Auxiliary data record length is bad (0.000-99999999).", \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
# Auto-centering Parameters.
    if PACGroup.get() != 0:
# It's just a warning.
        if PACEnable.get() == 0:
            msgLn(0, "Y", "Auto-centering parameters are disabled.", True, 2)
        PACCycle.set(PACCycle.get().strip())
        if checkValue(PACCycle.get(), "I", 0, 99) == False:
            msgLn(0, "R", "Auto-centering cycle interval is bad (0-99).", \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        PACThresh.set(PACThresh.get().strip())
        if checkValue(PACThresh.get(), "F", 0.1, 9.9) == False:
            msgLn(0, "R", "Auto-centering threshold is bad (0.1-9.9).", True, \
                    2)
            if Ret == True:
                return False
            AllOK = False
        PACAttempt.set(PACAttempt.get().strip())
        if checkValue(PACAttempt.get(), "I", 0, 99) == False:
            msgLn(0, "R", "Auto-centering attempts per cycle is bad (0-99).", \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        PACRetry.set(PACRetry.get().strip())
        if checkValue(PACRetry.get(), "I", 1, 99) == False:
            msgLn(0, "R", "Auto-centering retry interval is bad (1-99).", \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
# Disk Parameters.
    PDThresh.set(PDThresh.get().strip())
    if checkValue(PDThresh.get(), "I", 1, 99) == False:
        msgLn(0, "R", "Disk dump threshold value is bad (1-99).", True, 2)
        if Ret == True:
            return False
        AllOK = False
    PDRetry.set(PDRetry.get().strip())
# Networking Parameters.
    if PNPort.get() == 1 or PNPort.get() == 3:
        if PNEPortPow.get() == 0:
            msgLn(0, "R", "Ethernet port power parameter not selected.", \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        if PNEKeep.get() == 0:
            msgLn(0, "R", \
                    "Ethernet port line down action parameter not selected.", \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        PNETDelay.set(PNETDelay.get().strip())
# Toss selected...
        if PNEKeep.get() == 2:
            if checkValue(PNETDelay.get(), "I", 2, 99) == False:
                msgLn(0, "R", \
                      "Toss delay Ethernet network parameter is bad (2-99).", \
                        True, 2)
                if Ret == True:
                    return False
                AllOK = False
# Format and check all of the addresses.
# Special check for the IP address.
        for i in arange(1, 1+4):
            Tup = intt(eval("PNEIP%d"%i).get())
            if Tup < 0 or Tup > 255:
                msgLn(0, "R", \
                        "IP address Ethernet network parameter is bad.", \
                        True, 2)
                if Ret == True:
                    return False
                AllOK = False
                break
            else:
                eval("PNEIP%d"%i).set(str(Tup))
        Addr = checkIP(PNEMask.get())
        if len(Addr) == 0:
            msgLn(0, "R", "Net mask Ethernet network parameter is bad.", \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        else:
            PNEMask.set(Addr)
        Addr = checkIP(PNEGateway.get())
        if len(Addr) == 0:
            msgLn(0, "R", \
                    "Gateway address Ethernet network parameter is bad.", \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        else:
            PNEGateway.set(Addr)
        Addr = checkIP(PNEHost.get())
        if len(Addr) == 0:
            msgLn(0, "R", \
                    "Remote host address Ethernet network parameter is bad.", \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        else:
            PNEHost.set(Addr)
# The serial host address must be "0.0.0.0" or "000.000.000.000" when using
# the Ethernet only (from Ref Tek manual).
        if PNSHost.get() != "0.0.0.0" and PNSHost.get() != "000.000.000.000":
            msgLn(1, "R", "Serial port host address must be set to 0.0.0.0", \
                    True, 2)
            msgLn(0, "R", "or 000.000.000.000.", True, 2)
            if Ret == True:
                return False
            AllOK = False
# If the serial port is being programmed...
    elif PNPort.get() == 2 or PNPort.get() == 3:
        if PNSKeep.get() == 0:
            msgLn(0, "R", \
                    "Serial port line down action parameter not selected.", \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        PNSTDelay.set(PNSTDelay.get().strip())
        if PNSKeep.get() == 2:
            if checkValue(PNSTDelay.get(), "I", 2, 99) == False:
                msgLn(0, "R", \
                        "Toss delay serial network parameter is bad (2-99).", \
                        True, 2)
                if Ret == True:
                    return False
                AllOK = False
        if PNSMode.get() == 0:
            msgLn(0, "R", "Serial port mode parameter not selected.", True, 2)
            if Ret == True:
                return False
            AllOK = False
        if PNSSpeed.get() == 0:
            msgLn(0, "R", "Serial port speed parameter not selected.", True, 2)
            if Ret == True:
                return False
            AllOK = False
# Special IP address-only stuff.
        for i in arange(1, 1+4):
            Tup = intt(eval("PNSIP%d"%i).get())
            if Tup < 0 or Tup > 255:
                msgLn(0, "R", "IP address serial network parameter is bad.", \
                        True, 2)
                if Ret == True:
                    return False
                AllOK = False
                break
            else:
                eval("PNSIP%d"%i).set(str(Tup))
        Addr = checkIP(PNSMask.get())
        if len(Addr) == 0:
            msgLn(0, "R", "Net mask serial network parameter is bad.", True, 2)
            if Ret == True:
                return False
            AllOK = False
        else:
            PNSMask.set(Addr)
        Addr = checkIP(PNSGateway.get())
        if len(Addr) == 0:
            msgLn(0, "R", "Gateway address serial network parameter is bad.", \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        else:
            PNSGateway.set(Addr)
        Addr = checkIP(PNSHost.get())
        if len(Addr) == 0:
            msgLn(0, "R", \
                    "Remote host address serial network parameter is bad.", \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        else:
            PNSHost.set(Addr)
# External clock control parameters (nothing to check).
    return AllOK
# END: checkParms




########################
# BEGIN: checkParmsCmd()
# FUNC:checkParmsCmd():2009.082
def checkParmsCmd():
    if checkParms(False) == True:
        msgLn(0, "G", "Parameters checked. All OK.")
    else:
        msgLn(0, "", "Parameters checked.")
    return
# END: checkParmsCmd




#########################
# BEGIN: checkRp(DAS, Rp)
# FUNC:checkRp():2018.289
#   Checks that the passed DAS number matches the Unit ID field in the passed
#   string (that supposedly came from a DAS's response)
#   Returns True if everything is OK.
def checkRp(DAS, Rp):
    if DAS == "0000":
        return True
    if len(Rp) > 6 and DAS == Rp[2:2+4]:
        return True
# We'll stick this message printing here since there are going to be so many
# calls to this function.
    if len(Rp) != 0:
        msgLn(1, "M", "A DAS may be talking out of turn!", True, 3)
        if len(Rp) > 6:
            msgLn(1, "", "It might be DAS %s"%Rp[2:2+4])
    return False
# END: checkRp




###################################
# BEGIN: rt72130CheckSampRate(Rate)
# LIB:rt72130CheckSampRate():2018.234
#   If the passed sample rate is OK return True.
def rt72130CheckSampRate(Rate):
    if isinstance(Rate, astring):
        Rate = intt(Rate)
    if Rate != 0 and Rate != 1000 and Rate != 500 and Rate != 250 and \
            Rate != 200 and Rate != 125 and Rate != 100 and Rate != 50 and \
            Rate != 40 and Rate != 25 and Rate != 20 and Rate != 10 and \
            Rate != 5 and Rate != 1:
        return False
    return True
# END: rt72130CheckSampRate




########################################################
# BEGIN: checkSRCombinations(Rate1, Rate2, Rate3, Rate4)
# FUNC:checkSRCombinations():2019.191
def checkSRCombinations(Rate1, Rate2, Rate3, Rate4):
    G1 = [1000, 500, 250, 125, 50, 25]
    G2 = [200, 100, 40, 20, 10, 5, 1]
    SR1 = intt(Rate1.get())
    SR2 = intt(Rate2.get())
    SR3 = intt(Rate3.get())
    SR4 = intt(Rate4.get())
    Status = True
    if SR1 > 0 and PDS1TType.get() != 0:
        if SR1 in G1:
            if SR2 > 0 and PDS2TType.get() != 0:
                if SR2 != SR1:
                    Status = False
            if SR3 > 0 and PDS3TType.get() != 0:
                if SR3 != SR1:
                    Status = False
            if SR4 > 0 and PDS4TType.get() != 0:
                if SR4 != SR1:
                    Status = False
        elif SR1 in G2:
            if SR2 > 0 and PDS2TType.get() != 0:
                if SR2 not in G2:
                    Status = False
            if SR3 > 0 and PDS3TType.get() != 0:
                if SR3 not in G2:
                    Status = False
            if SR4 > 0 and PDS4TType.get() != 0:
                if SR4 not in G2:
                    Status = False
    if SR2 > 0 and PDS2TType.get() != 0:
        if SR2 in G1:
            if SR3 > 0 and PDS3TType.get() != 0:
                if SR3 != SR2:
                    Status = False
            if SR4 > 0 and PDS4TType.get() != 0:
                if SR4 != SR2:
                    Status = False
        elif SR2 in G2:
            if SR3 > 0 and PDS3TType.get() != 0:
                if SR3 not in G2:
                    Status = False
            if SR4 > 0 and PDS4TType.get() != 0:
                if SR4 not in G2:
                    Status = False
    if SR3 > 0 and PDS3TType.get() != 0:
        if SR3 in G1:
            if SR4 > 0 and PDS4TType.get() != 0:
                if SR4 != SR3:
                    Status = False
        elif SR3 in G2:
            if SR4 > 0 and PDS4TType.get() != 0:
                if SR4 not in G2:
                    Status = False
# We don't have to check SR4 since any problems with it will be detected above.
# One disallowed above exception. Don't know if this extends to SR3 and SR4?
    if SR1 > 0 and PDS1TType.get() != 0:
        if SR1 in [1, 50]:
            if SR2 > 0 and PDS2TType.get() != 0:
                if SR2 in [1, 50]:
                    Status = True
    if Status == False:
        msgLn(0, "R", "Bad data stream sample rate combination.", True, 2)
        return False
    return True
# END: checkSRCombinations




########################################
# BEGIN: checkTMLTime(DS, Which, InTime)
# FUNC:checkTMLTime():2008.012
def checkTMLTime(d, Which, InTime):
    InTime.set(InTime.get().strip())
    if len(InTime.get()) != 13 and len(InTime.get()) != 0:
        msgLn(0, "R", "DS%d TML start time %d is bad."%(d, Which), 1, 1)
        return False
    return True
# END: checkTMLTime




################################
# BEGIN: checkTrigParms(Ret , d)
# FUNC:checkTrigParms():2018.289
#   Checks the trigger parameters. Returns True if everything is OK, False
#   if not. If Ret is False it will keep going through the parameters and
#   print all of the problems that it finds, otherwise it will return as soon
#   as a problem is found.
def checkTrigParms(Ret, d):
    AllOK = True
# Not used, so it must be good.
    if eval("PDS%dTType"%d).get() == 0:
        return True
# CON
    elif eval("PDS%dTType"%d).get() == 1:
        eval("PDS%dStart1"%d).set(eval("PDS%dStart1"%d).get().strip())
        if len(eval("PDS%dStart1"%d).get()) != 13:
            msgLn(0, "R", "DS%d CON start time is bad."%d, True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dRl"%d).set(eval("PDS%dRl"%d).get().strip())
        if checkValue(eval("PDS%dRl"%d).get(), "F", 0.0, \
                99999999.0) == False or len(eval("PDS%dRl"%d).get()) > 8:
            msgLn(0, "R", \
                    "DS%d CON record length is bad (0.000-99999999)."%d, True,
                    2)
            if Ret == True:
                return False
            AllOK = False
# CRS
    elif eval("PDS%dTType"%d).get() == 2:
        if eval("PDS%dTStrm"%d).get() == 0:
            msgLn(0, "R", "DS%d CRS no trigger stream selected."%d, True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dPretl"%d).set(eval("PDS%dPretl"%d).get().strip())
        if checkValue(eval("PDS%dPretl"%d).get(), "F", 0.0, 999.9) == False:
            msgLn(0, "R", \
                    "DS%d CRS pre-trigger length is bad (0.0-999.9)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dRl"%d).set(eval("PDS%dRl"%d).get().strip())
        if checkValue(eval("PDS%dRl"%d).get(), "F", 0.0, \
                99999999.0) == False or len(eval("PDS%dRl"%d).get()) > 8:
            msgLn(0, "R", \
                    "DS%d CRS record length is bad (0.000-99999999)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
# EVT
    elif eval("PDS%dTType"%d).get() == 3:
        if eval("PDS%dTChan1"%d).get() == 0 and \
                eval("PDS%dTChan2"%d).get() == 0 and \
                eval("PDS%dTChan3"%d).get() == 0 and \
                eval("PDS%dTChan4"%d).get() == 0 and \
                eval("PDS%dTChan5"%d).get() == 0 and \
                eval("PDS%dTChan6"%d).get() == 0:
            msgLn(0, "R", "DS%d EVT no trigger channels selected."%d, True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dTwin"%d).set(eval("PDS%dTwin"%d).get().strip())
        if checkValue(eval("PDS%dTwin"%d).get(), "F", 0.1, 99.9) == False:
            msgLn(0, "R", "DS%d EVT trigger window is bad (0.1-99.9)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dMc"%d).set(eval("PDS%dMc"%d).get().strip())
        if checkValue(eval("PDS%dMc"%d).get(), "I", 1, 6) == False:
            msgLn(0, "R", "DS%d EVT minimum channels is bad (1-6)."%d, True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dPretl"%d).set(eval("PDS%dPretl"%d).get().strip())
        if checkValue(eval("PDS%dPretl"%d).get(), "F", 0.0, 999.9) == False:
            msgLn(0, "R", \
                    "DS%d EVT pre-trigger length is bad (0.0-999.9)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dRl"%d).set(eval("PDS%dRl"%d).get().strip())
        if checkValue(eval("PDS%dRl"%d).get(), "F", 0.0, \
                99999999.0) == False or len(eval("PDS%dRl"%d).get()) > 8:
            msgLn(0, "R", \
                    "DS%d EVT record length is bad (0.000-99999999)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dPostl"%d).set(eval("PDS%dPostl"%d).get().strip())
        if checkValue(eval("PDS%dPostl"%d).get(), "F", 0.0, 999.9) == False:
            msgLn(0, "R", \
                    "DS%d EVT post-trigger length is bad (0.0-999.9)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dSta"%d).set(eval("PDS%dSta"%d).get().strip())
        if checkValue(eval("PDS%dSta"%d).get(), "F", 0.0, 999.9) == False:
            msgLn(0, "R", \
                    "DS%d EVT short term average is bad (0.0-999.9)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dLta"%d).set(eval("PDS%dLta"%d).get().strip())
        if checkValue(eval("PDS%dLta"%d).get(), "F", 0.1, 999.9) == False:
            msgLn(0, "R", "DS%d EVT long term average is bad (0.1-999.9)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dTr"%d).set(eval("PDS%dTr"%d).get().strip())
        if checkValue(eval("PDS%dTr"%d).get(), "F", 0.1, 99.9) == False:
            msgLn(0, "R", "DS%d EVT trigger ratio is bad (0.1-99.9)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dDtr"%d).set(eval("PDS%dDtr"%d).get().strip())
        if checkValue(eval("PDS%dDtr"%d).get(), "F", 0.0, 99.9) == False:
            msgLn(0, "R", "DS%d EVT de-trigger ratio is bad (0.0-99.9)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
# EXT
    elif eval("PDS%dTType"%d).get() == 4:
        eval("PDS%dPretl"%d).set(eval("PDS%dPretl"%d).get().strip())
        if checkValue(eval("PDS%dPretl"%d).get(), "F", 0.0, 999.9) == False:
            msgLn(0, "R", \
                    "DS%d EXT pre-trigger length is bad (0.0-999.9)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dRl"%d).set(eval("PDS%dRl"%d).get().strip())
        if checkValue(eval("PDS%dRl"%d).get(), "F", 0.0, \
                99999999.0) == False or len(eval("PDS%dRl"%d).get()) > 8:
            msgLn(0, "R", \
                    "DS%d EXT record length is bad (0.000-99999999)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
# LEV
    elif eval("PDS%dTType"%d).get() == 5:
# The trigger level has some special forms.
        eval("PDS%dTlvl"%d).set(eval("PDS%dTlvl"%d).get().strip().upper())
        TrigLevel = eval("PDS%dTlvl"%d).get()
        if TrigLevel.startswith("G"):
            if len(TrigLevel) == 1:
                msgLn(0, "R", \
                     "DS%d LEV \"G\" trigger level missing numerical value."% \
                        d, True, 2)
                if Ret == True:
                    return False
                AllOK = False
            else:
                if checkValue(TrigLevel[1:], "F", 0.0001, 99.9999) == False:
                    msgLn(0, "R", \
               "DS%d LEV trigger level G's value is bad (0.0001-99.9999)."%d, \
                            True, 2)
                    if Ret == True:
                        return False
                    AllOK = False
        elif TrigLevel.startswith("M"):
            if len(TrigLevel) == 1:
                msgLn(0, "R", \
                     "DS%d LEV \"M\" trigger level missing numerical value."% \
                        d, True, 2)
                if Ret == True:
                    return False
                AllOK = False
            else:
                if checkValue(TrigLevel[1:], "F", 0.01, 999.99) == False:
                    msgLn(0, "R", \
                   "DS%d LEV trigger level percentage is bad (0.01-999.99)."% \
                            d, True, 2)
                    if Ret == True:
                        return False
                    AllOK = False
        elif TrigLevel.startswith("%"):
            if len(TrigLevel) == 1:
                msgLn(0, "R", \
                     "DS%d LEV \"%\" trigger level missing numerical value."% \
                        d, True, 2)
                if Ret == True:
                    return False
                AllOK = False
            else:
                if checkValue(TrigLevel[1:], "I", 1, 99) == False:
                    msgLn(0, "R", \
                        "DS%d LEV trigger level percentage is bad (1-99)."%d, \
                            True, 2)
                    if Ret == True:
                        return False
                    AllOK = False
        elif TrigLevel.startswith("H"):
            if len(TrigLevel) == 1:
                msgLn(0, "R", \
                   "DS%d LEV \"Hex\" trigger level missing numerical value."% \
                        d, True, 2)
                if Ret == True:
                    return False
                AllOK = False
            else:
                if checkValue(TrigLevel[1:], "H", 0x0, 0xFFFFFFF) == False:
                    msgLn(0, "R", \
                      "DS%d LEV trigger level hex value is bad (0-FFFFFFF)."% \
                            d, True, 2)
                    if Ret == True:
                        return False
                    AllOK = False
        else:
            if checkValue(TrigLevel, "I", 0, 9999999) == False:
                msgLn(0, "R", \
                       "DS%d LEV trigger level counts is bad (0-9999999)."%d, \
                        True, 2)
                if Ret == True:
                    return False
                AllOK = False
        eval("PDS%dPretl"%d).set(eval("PDS%dPretl"%d).get().strip())
        if checkValue(eval("PDS%dPretl"%d).get(), "F", 0.0, 999.9) == False:
            msgLn(0, "R", \
                    "DS%d LEV pre-trigger length is bad (0.0-999.9)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dRl"%d).set(eval("PDS%dRl"%d).get().strip())
        if checkValue(eval("PDS%dRl"%d).get(), "F", 0.0, \
                99999999.0) == False or len(eval("PDS%dRl"%d).get()) > 8:
            msgLn(0, "R", \
                    "DS%d LEV record length is bad (0.000-99999999)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
# TIM
    elif eval("PDS%dTType"%d).get() == 6:
        eval("PDS%dStart1"%d).set(eval("PDS%dStart1"%d).get().strip())
        if len(eval("PDS%dStart1"%d).get()) != 13:
            msgLn(0, "R", "DS%d TIM start time is bad."%d, True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dRepInt"%d).set(eval("PDS%dRepInt"%d).get().strip())
        if len(eval("PDS%dRepInt"%d).get()) != 8:
            msgLn(0, "R", "DS%d TIM repeat interval is bad (1sec-45days)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dNumTrig"%d).set(eval("PDS%dNumTrig"%d).get().strip())
        if checkValue(eval("PDS%dNumTrig"%d).get(), "I", 0, 9999) == False:
            msgLn(0, "R", \
                    "DS%d TIM number of triggers is bad (0-9999)."%d, True, 2)
            if Ret == True:
                return False
            AllOK = False
        eval("PDS%dRl"%d).set(eval("PDS%dRl"%d).get().strip())
        if checkValue(eval("PDS%dRl"%d).get(), "F", 0.0, \
                99999999.0) == False or len(eval("PDS%dRl"%d).get()) > 8:
            msgLn(0, "R", \
                    "DS%d TIM record length is bad (0.000-99999999)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
# TML
    elif eval("PDS%dTType"%d).get() == 7:
        if checkTMLTime(d, 1, eval("PDS%dStart1"%d).get()) == False:
            if Ret == True:
                return False
            AllOK = False
# I'm assuming that the user will want at least one of these.
        if len(eval("PDS%dStart1"%d).get()) == 0:
            msgLn(0, "R", "DS%d TML no starting time entered."%d, True, 2)
            if Ret == True:
                return False
            AllOK = False
        for i in arange(2, 1+11):
            if checkTMLTime(d, i, eval("PDS%dStart%d"%(d,i))) == False:
                if Ret == True:
                    return False
                AllOK = False
        eval("PDS%dRl"%d).set(eval("PDS%dRl"%d).get().strip())
        if checkValue(eval("PDS%dRl"%d).get(), "F", 0.0, \
                99999999.0) == False or len(eval("PDS%dRl"%d).get()) > 8:
            msgLn(0, "R", \
                    "DS%d TML record length is bad (0.000-99999999)."%d, \
                    True, 2)
            if Ret == True:
                return False
            AllOK = False
    return AllOK
# END: checkTrigParms




###########################################
# BEGIN: checkValue(Value, Type, Low, High)
# FUNC:checkValue():2018.289
#   Checks the passed Value to make sure it is OK. Value should be a string.
#   Type tells the routine what kind of number the passed Value is:
#      "I" = integer, "" not allowed
#      "F" = float, "" not allowed
#      "H" = hex values, "" not allowed
#      "Ib" = integer, "" allowed
#      "Fb" = float, "" allowed
#   Returns True if OK, False if not.
def checkValue(Value, Type, Low, High):
    Value = Value.strip()
    if Type == "I" or Type == "F" or Type == "H":
        if len(Value) == 0:
            return False
    if Type == "I" or Type == "Ib":
# If there is a decimal point in an integer tsk tsk tsk.
        try:
            Value.index(".")
            return False
        except ValueError:
            pass
        if intt(Value) < Low or intt(Value) > High:
            return False
    elif Type == "F" or Type == "Fb":
        if len(Value) == 0:
            return True
        if floatt(Value) < Low or floatt(Value) > High:
            return False
    elif Type == "H":
        try:
            if int(Value, 16) < Low or int(Value, 16) > High:
                return False
        except ValueError:
            return False
    return True
# END: checkValue




##########################
# BEGIN: cleanCALS(VarSet)
# FUNC:cleanCALS():2008.012
def cleanCALS(VarSet):
    eval("%sDurationVar"%VarSet).set(eval("%sDurationVar"% \
            VarSet).get().strip())
    eval("%sAmplitudeVar"%VarSet).set(eval("%sAmplitudeVar"% \
            VarSet).get().strip())
    eval("%sStepIntervalVar"%VarSet).set(eval("%sStepIntervalVar"% \
            VarSet).get().strip())
    eval("%sStepWidthVar"%VarSet).set(eval("%sStepWidthVar"% \
            VarSet).get().strip())
    return
# END: cleanCALS




#################################
# BEGIN: cmdPortOpen(RDel = 0.75)
# FUNC:cmdPortOpen():2019.260
#   Open and close and read and write for the Command Port.
#   Latin-1 encoding is used, because of the non-printables that Reftek puts in
#   the commands.
#   RDel added in 2019 for Python 3, which started needing them. The
#   enumeration read was freezing if no DAS was there.
CmdPort = None

def cmdPortOpen(RDel = 0.75):
    global CmdPort
# In case this does not return for a bit because it runs into trouble getting
# the serial port open update everything that may be waiting.
    updateMe(0)
    if PROGSystem == "dar" or PROGSystem == "lin" or PROGSystem == "win":
        Port = CmdPortVar.get().strip()
        if len(Port) == 0:
            msgLn(0, "R", "No Command Port device has been entered.", True, 2)
            return False
# v2 uses the number part of "COM3", minus 1, v3 uses the whole name.
        if PROGSystem == "win":
            if SerialVERSION.startswith("2"):
                Ret = findInt(Port)
                if Ret == -1:
                    return (2, "RW", "Bad COM value.", 2, "")
                Port = Ret-1
    else:
        msgLn(0, "M", "Serial ports on this system are not supported: %s"% \
                PROGSystem, True, 3)
        return False
    try:
        CmdPort = Serial(port = Port, \
                baudrate = 9600, \
                bytesize = EIGHTBITS, \
                parity = PARITY_NONE, \
                stopbits = STOPBITS_ONE, \
                timeout = RDel)
        CmdPortLab.config(bg = Clr["G"])
        CmdPortLab.update()
    except Exception as e:
        print(e)
# Just in case.
        CmdPortLab.config(bg = Clr["D"])
        CmdPortLab.update()
        CmdPort = None
        msgLn(1, "R", "Error opening command port '%s'."%CmdPortVar.get(), \
                True, 2)
        msgLn(0, "R", "   "+str(e))
        return False
    return True
#######################
# BEGIN: cmdPortClose()
# FUNC:cmdPortClose():2018.270
def cmdPortClose():
    global CmdPort
    try:
        CmdPort.close()
        if OPTDebugCVar.get() == 1:
            stdout.write("cPC() Port closed.\n")
    except:
        if OPTDebugCVar.get() == 1:
            stdout.write("cPC() Port close exception.\n")
    CmdPortLab.config(bg = Clr["D"])
    CmdPortLab.update()
    CmdPort = None
    return
##########################
# BEGIN: cmdPortReadline()
# FUNC:cmdPortReadline():2019.189
def cmdPortReadline():
    if OPTDebugCVar.get() == 1:
        stdout.write("inWaiting: %d\n"%CmdPort.inWaiting())
    if PROG_PYVERS == 2:
        Ret = CmdPort.readline()
    elif PROG_PYVERS == 3:
        Ret = CmdPort.readline().decode("latin-1")
    return Ret
###############################
# BEGIN: cmdPortWrite(DAS, Str)
# FUNC:cmdPortWrite():2019.188
#   Does the actual writing to the serial port. This was done mostly for the
#   OPTDebugCVar function.
def cmdPortWrite(DAS, Str):
    if OPTDebugCVar.get() == 1:
        stdout.write("B>>>> %s\n"%makePrintable(Str))
        stdout.write("%s\n\n"%makeHex(Str))
    try:
# See POCUS code. Strangeness on macOS.
        sleep(.1)
        CmdPort.flushInput()
        CmdPort.flushInput()
        if PROG_PYVERS == 2:
            CmdPort.write(Str)
        elif PROG_PYVERS == 3:
            CmdPort.write(Str.encode("latin-1"))
    except Exception as e:
# Don't beep().
        msgLn(0, "M", "cmdPortWrite(): .flush() or .write() error.")
        msgLn(0, "M", "cmdPortWrite(): %s"%e)
# This is an oldie, but still may be a goody:
# KLUDGEWARNING: Linux, FC4 and a laptop with only a Keyspan converter for a
# serial port required this delay for some (probably electrical) reason. The
# commands would be garbled or truncated somehow by the time they got to the
# DASs and then the DASs would not respond. I suspect that might have
# something to do with the port getting closed too quickly after this function
# returns. So rather than stick in delays everywhere else...
    sleep(.1)
    return
# END: cmdPortOpen




#####################
# BEGIN: findInt(Str)
# LIB:findInt():2018.244
#   Returns the intt() of the first number found in Str. This is used when
#   trying to get at the 34 of something like "G34".
def findInt(Str):
    Index = 0
    for C in Str:
        if C.isdigit():
            Int = intt(Str[Index:])
            return Int
        Index += 1
    return -1
########################
# BEGIN: findPrefix(Str)
# FUNC:findPrefix():2006.243
#   Returns any characters at the start of Str upto the end or a digit. Like
#   "G34" returns "G".
def findPrefix(Str):
    Ret = ""
    try:
        while 1:
            c = Str[0]
            if c.isdigit():
                break
            Ret += c
            Str = Str[1:]
    except IndexError:
        pass
    return Ret
#######################
# BEGIN: findParts(Str)
# FUNC:findParts():2018.243
#   Returns the passed Str like G234K as a list of ["G", 274, "K"]. A List of
#   Str's broken up this way could then be sorted correctly so that items like
#   G05 and G4 will sort correctly (G4 then G05).
def findParts(Str):
    Ret = [""]
    Index = 0
    if len(Str) == 0:
        return Ret
    if Str[0].isdigit():
        What = "D"
    else:
        What = "A"
    for C in Str:
        if C.isdigit():
            if What == "D":
                Ret[Index] += C
            else:
                Ret.append(C)
                Index += 1
                What = "D"
        else:
            if What == "A":
                Ret[Index] += C
            else:
                Ret.append(C)
                Index += 1
                What = "A"
    for Index in arange(0, 0+len(Ret)):
        if Ret[Index][0].isdigit():
            Ret[Index] = intt(Ret[Index])
    return Ret
##############################
# BEGIN: getStrIntParts(InStr)
# FUNC:getStrIntParts():2018.243
#   Same idea as above, but only works for A123 or 123A, and it returns a
#   tuple, instead of a list.
#   Returns the integer and text portion of something like A234 as ("A", 234)
#   or 123BY as (123, "BY"). The str part will be "" if there is no str part
#   and the integer part will be -1 if there is no integer part.
def getStrIntParts(InStr):
    if len(InStr) == 0:
        return ("", -1)
    Str = ""
    Int = -1
    Index = 0
    FoundDigit = False
    First = ""
    for C in InStr:
        if C.isdigit() and FoundDigit == False:
            Int = intt(InStr[Index:])
            FoundDigit = True
# If the str part was first we've got everything. If the number part is first
# then we need to keep reading until the if() below finishes.
            if First == "s":
                break
            if len(First) == 0:
                First = "n"
        elif C.isdigit() == False:
            Str += C
            if len(First) == 0:
                First = "s"
        Index += 1
    if First == "n":
        return (Int, Str)
    elif First == "s":
        return (Str, Int)
    else:
        return ("", 0)
# END: findInt




#################################
# BEGIN: formClose(Who, e = None)
# LIB:formClose():2018.236
#   Handles closing a form. Who can be a PROGFrm[] item or a Tcl pointer.
def formClose(Who, e = None):
# In case it is a "busy" form that takes a long time to close.
    updateMe(0)
    if isinstance(Who, astring):
# The form may not exist or PROGFrm[Who] may be pointing to a "lost" form, or
# who knows what, so try.
        try:
            if PROGFrm[Who] is None:
                return
            PROGFrm[Who].destroy()
        except:
            pass
    else:
        Who.destroy()
    try:
        PROGFrm[Who] = None
    except:
        pass
    return
###############################
# BEGIN: formCloseAll(e = None)
# FUNC:formCloseAll():2018.231
#   Goes through all of the forms and shuts them down.
def formCloseAll(e = None):
    for Frmm in list(PROGFrm.keys()):
# Try to call a formXControl() function. Some may have them and some may not.
        try:
# Expecting from formXControl() functions:
# Ret[0] 0 == Continue.
# Ret[0] 1 == Problem solved, can continue.
# Ret[0] 2 == Problem has or has not been resolved, should stop.
# What actually gets done is handled by the caller this just passes back
# anything that is not 0.
            Ret = eval("form%sControl"%Frmm)("close")
            if Ret[0] != 0:
                return Ret
        except:
            formClose(Frmm)
    return (0, )
# END: formClose




######################
# BEGIN: class Command
# LIB:Command():2006.114
#   Pass arguments to functions from button presses and menu selections! Nice!
#   In your declaration:  ...command = Command(func, args,...)
#   Also use in bind() statements
#       x.bind("<****>", Command(func, args...))
class Command:
    def __init__(self, func, *args, **kw):
        self.func = func
        self.args = args
        self.kw = kw
    def __call__(self, *args, **kw):
        args = self.args+args
        kw.update(self.kw)
        self.func(*args, **kw)
# END: Command




###################
# BEGIN: debugCmd()
# FUNC:debugCmd():2008.090
def debugCmd():
    if OPTDebugCVar.get() == 0:
        msgLn(0, "", "Debug mode turned OFF.")
    else:
        msgLn(0, "", "Debug mode turned ON.")
    return
# END: debugCmd




#######################
# BEGIN: deciDeg(InPos)
# FUNC:deciDeg():2018.289
#   Returns the passed lat or long as decimal degrees.
def deciDeg(InPos):
    OutPos = ""
    if len(InPos) != 0:
        OutPos = InPos[0:0+1]
        M = InPos.index(":")
        OutPos = OutPos+InPos[1:M]
        Value = float(InPos[M+1:])/60.0
        OutPos = OutPos+("%.6f"%Value)[1:]
    return OutPos
# END: deciDeg




#######################
# BEGIN: delay(Seconds)
# FUNC:delay():2018.289
def delay(Seconds):
    if Seconds < 1:
        sleep(Seconds)
    else:
        for i in arange(0, Seconds*4):
            updateMe(0)
            sleep(.25)
            if WorkState == 999:
                break
    return
# END: delay




###############################################################
# BEGIN: dt2Time(InFormat, OutFormat, DateTime, Verify = False)
# LIB:dt2Time():2018.270
#   InFormat = -1 = An Epoch has been passed.
#               0 = Figure out what was passed.
#           other = Use if the caller knows exactly what they have.
#   OutFormat = -1 = Epoch
#                0 = Y M D D H M S.s
#                1 = Uses OPTDateFormatRVar value.
#            other = whatever supported format the caller wants
#   The format of the time will always be HH:MM:SS.sss.
#   Returns (0/1, <answer or error msg>) if Verify is True, or <answer/0/"">
#   if Verify is False. Confusing, but most of the calls are with Verify set
#   to False, and having to always put [1] at the end of each call was getting
#   old fast. This probably will cause havoc if there are other errors like
#   passing date/times that cannot be deciphered, or passing bad In/OutFormat
#   codes (just "" will be returned), but that's the price of progress. You'll
#   still have to chop off the milliseconds if they are not wanted ([:-4]).
#   Needs option_add(), intt(), floatt(), rtnPattern()
def dt2Time(InFormat, OutFormat, DateTime, Verify = False):
    global Y2EPOCH
    if InFormat == -1:
        YYYY = 1970
        while 1:
            if YYYY%4 != 0:
                if DateTime >= 31536000:
                    DateTime -= 31536000
                else:
                    break
            elif YYYY%100 != 0 or YYYY%400 == 0:
                if DateTime >= 31622400:
                    DateTime -= 31622400
                else:
                    break
            else:
                if DateTime >= 31536000:
                    DateTime -= 31536000
                else:
                    break
            YYYY += 1
        DOY = 1
        while DateTime >= 86400:
            DateTime -= 86400
            DOY += 1
        HH = 0
        while DateTime >= 3600:
            DateTime -= 3600
            HH += 1
        MM = 0
        while DateTime >= 60:
            DateTime -= 60
            MM += 1
        SS = DateTime
        MMM, DD = dt2Timeydoy2md(YYYY, DOY)
    else:
        DateTime = DateTime.strip().upper()
# The caller will have to decide if these returns are OK or not.
        if len(DateTime) == 0:
            if OutFormat -1:
                if Verify == False:
                    return 0.0
                else:
                    return (0, 0.0)
            elif OutFormat == 0:
                if Verify == False:
                    return 0, 0, 0, 0, 0, 0, 0.0
                else:
                    return (0, 0, 0, 0, 0, 0, 0, 0.0)
            elif InFormat == 5:
                if Verify == False:
                    return "00:00:00:00"
                else:
                    return (0, "00:00:00:00")
            else:
                if Verify == False:
                    return ""
                else:
                    return (0, "")
# The overall goal of the decode will be to get the passed string time into
# YYYY, MMM, DD, DOY, HH, MM integers and SS.sss float values then proceed to
# the encoding section ready for anything.
# These will try and figure out what the date is between the usual formats.
# Checks first to see if the date and time are together (like with a :) or if
# there is a space between them.
        if InFormat == 0:
            Parts = DateTime.split()
            if len(Parts) == 1:
# YYYY:DOY:... - 71:2 will pass.
                if DateTime.find(":") != -1:
                    Parts = DateTime.split(":")
# There has to be something that looks like YYYY:DOY.
                    if len(Parts) >= 2:
                        InFormat = 11
# YYYY-MM-DD:HH:...
# If there was only one thing and there are dashes then it could be Y-M-D or
# Y-M-D:H:M:S. Either way there must be 3 parts.
                elif DateTime.find("-") != -1:
                    Parts = DateTime.split("-")
                    if len(Parts) == 3:
                        InFormat = 21
# YYYYMMMDD:HH:... - 68APR3 will pass.
                elif (DateTime.find("A") != -1 or DateTime.find("E") != -1 or \
                        DateTime.find("O") != -1 or DateTime.find("U") != -1):
                    InFormat = 31
# YYYYDOYHHMMSS - Date/time must be exactly like this.
                elif len(DateTime) == 13:
                    if rtnPattern(DateTime) == "0000000000000":
                        InFormat = 41
# YYYYMMDDHHMMSS - Date/time must be exactly like this.
                elif len(DateTime) == 14:
                    if rtnPattern(DateTime) == "00000000000000":
                        InFormat = 51
# (There is no 1974JAN23235959 that I know of, but the elif for it would be
# here. ->
# There were two parts.
            else:
                Date = Parts[0]
                Time = Parts[1]
# YYYY:DOY HH:MM...
                if Date.find(":") != -1:
# Must have at least YYYY:DOY.
                    Parts = Date.split(":")
                    if len(Parts) >= 2:
                        InFormat = 12
# May be YYYY-MM-DD HH:MM...
                elif Date.find("-") != -1:
                    Parts = Date.split("-")
                    if len(Parts) == 3:
                        InFormat = 22
# YYYYMMMDD - 68APR3 will pass.
                elif (Date.find("A") != -1 or Date.find("E") != -1 or \
                    Date.find("O") != -1 or Date.find("U") != -1):
                        InFormat = 32
# If it is still 0 then something is wrong.
            if InFormat == 0:
                if Verify == False:
                    return ""
                else:
                    return (1, "RW", "Bad date/time(%d): '%s'"%(InFormat, \
                            DateTime), 2, "")
# These can be fed from the Format 0 stuff above, or called directly if the
# caller knows what DateTime is.
        if InFormat < 20:
# YYYY:DOY:HH:MM:SS.sss
# Sometimes this comes as  YYYY:DOY:HH:MM:SS:sss. We'll look for that here.
# (It's a Reftek thing.)
            if InFormat == 11:
                DT = DateTime.split(":")
                DT += (5-len(DT))*["0"]
                if len(DT) == 6:
                    DT[4] = "%06.3f"%(intt(DT[4])+intt(DT[5])/1000.0)
                    DT = DT[:-1]
# YYYY:DOY HH:MM:SS.sss
            elif InFormat == 12:
                Parts = DateTime.split()
                Date = Parts[0].split(":")
                Date += (2-len(Date))*["0"]
                Time = Parts[1].split(":")
                Time += (3-len(Time))*["0"]
                DT = Date+Time
            else:
                if Verify == False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)."% \
                            InFormat, 3, "")
# Two-digit years shouldn't happen a lot, so this is kinda inefficient.
            if DT[0] < "100":
                YYYY = intt(Date[0])
                if YYYY < 70:
                    YYYY += 2000
                else:
                    YYYY += 1900
                DT[0] = str(YYYY)
# After we have all of the parts then do the check if the caller wants.
            if Verify == True:
                Ret = dt2TimeVerify("ydhms", DT)
                if Ret[0] != 0:
                    return Ret
# I'm using intt() and floatt() throughout just because it's safer than the
# built-in functions.
# This trick makes it so the Epoch for a year only has to be calculated once
# during a program's run.
            YYYY = intt(DT[0])
            DOY = intt(DT[1])
            MMM, DD = dt2Timeydoy2md(YYYY, DOY)
            HH = intt(DT[2])
            MM = intt(DT[3])
            SS = floatt(DT[4])
        elif InFormat < 30:
# YYYY-MM-DD:HH:MM:SS.sss
            if InFormat == 21:
                Parts = DateTime.split(":", 1)
                Date = Parts[0].split("-")
                Date += (3-len(Date))*["0"]
# Just the date must have been supplied.
                if len(Parts) == 1:
                    Time = ["0", "0", "0"]
                else:
                    Time = Parts[1].split(":")
                    Time += (3-len(Time))*["0"]
                DT = Date+Time
# YYYY-MM-DD HH:MM:SS.sss
            elif InFormat == 22:
                Parts = DateTime.split()
                Date = Parts[0].split("-")
                Date += (3-len(Date))*["0"]
                Time = Parts[1].split(":")
                Time += (3-len(Time))*["0"]
                DT = Date+Time
# If parts of 23 are missing we will fill them in with Jan, 1st, or 00:00:00.
# If parts of 24 are missing we will format to the missing item then stop and
# return what we have.
            elif InFormat == 23 or InFormat == 24:
# The /DOY may or may not be there.
                if DateTime.find("/") == -1:
                    Parts = DateTime.split()
                    Date = Parts[0].split("-")
                    if InFormat == 23:
                        Date += (3-len(Date))*["1"]
                    else:
# The -1's will stand in from the missing items. OutFormat=24 will figure it
# out from there.
                        Date += (3-len(Date))*["-1"]
                    if len(Parts) == 2:
                        Time = Parts[1].split(":")
                        if InFormat == 23:
                            Time += (3-len(Time))*["0"]
                        else:
                            Time += (3-len(Time))*["-1"]
                    else:
                        if InFormat == 23:
                            Time = ["0", "0", "0"]
                        else:
                            Time = ["-1", "-1", "-1"]
# Has a /. We'll only use the YYYY-MM-DD part and assume nothing about the DOY
# part.
                else:
                    Parts = DateTime.split()
                    Date = Parts[0].split("-")
                    if InFormat == 23:
                        Date += (3-len(Date))*["0"]
                    elif InFormat == 24:
                        Date += (3-len(Date))*["-1"]
                    Date[2] = Date[2].split("/")[0]
                    if len(Parts) == 2:
                        Time = Parts[1].split(":")
                        if InFormat == 23:
                            Time += (3-len(Time))*["0"]
                        else:
                            Time += (3-len(Time))*["-1"]
                    else:
                        if InFormat == 23:
                            Time = ["0", "0", "0"]
                        else:
                            Time = ["1-", "-1", "-1"]
                DT = Date+Time
            else:
                if Verify == False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)."% \
                            InFormat, 3, "")
            if DT[0] < "100":
                YYYY = intt(DT[0])
                if YYYY < 70:
                    YYYY += 2000
                else:
                    YYYY += 1900
                DT[0] = str(YYYY)
            if Verify == True:
                if InFormat != 24:
                    Ret = dt2TimeVerify("ymdhms", DT)
                else:
                    Ret = dt2TimeVerify("xymdhms", DT)
                if Ret[0] != 0:
                    return Ret
            YYYY = intt(DT[0])
            MMM = intt(DT[1])
            DD = intt(DT[2])
# This will get done in OutFormat=24.
            if InFormat != 24:
                DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            else:
                DOY = -1
            HH = intt(DT[3])
            MM = intt(DT[4])
            SS = floatt(DT[5])
        elif InFormat < 40:
# YYYYMMMDD:HH:MM:SS.sss
            if InFormat == 31:
                Parts = DateTime.split(":", 1)
                Date = Parts[0]
                if len(Parts) == 1:
                    Time = ["0", "0", "0"]
                else:
                    Time = Parts[1].split(":")
                    Time += (3-len(Time))*["0"]
# YYYYMMMDD HH:MM:SS.sss
            elif InFormat == 32:
                Parts = DateTime.split()
                Date = Parts[0]
                Time = Parts[1].split(":")
                Time += (3-len(Time))*["0"]
            else:
                if Verify == False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)."% \
                            InFormat, 3, "")
# Date is still "YYYYMMMDD", so just make place holders.
            DT = ["0", "0", "0"]+Time
            YYYY = intt(Date)
            if YYYY < 100:
                if YYYY < 70:
                    YYYY += 2000
                else:
                    YYYY += 1900
            MMM = 0
            DD = 0
            M = 1
            for Month in PROG_CALMONS[1:]:
                try:
                    i = Date.index(Month)
                    MMM = M
                    DD = intt(Date[i+3:])
                    break
                except:
                    pass
                M += 1
            if Verify == True:
# DT values need to be strings for the dt2TimeVerify() intt() call. It's
# assumed the values would come from split()'ing something, so they would
# normally be strings to begin with.
                DT[0] = str(YYYY)
                DT[1] = str(MMM)
                DT[2] = str(DD)
                Ret = dt2TimeVerify("ymdhms", DT)
                if Ret[0] != 0:
                    return Ret
            DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            HH = intt(Time[0])
            MM = intt(Time[1])
            SS = intt(Time[2])
        elif InFormat < 50:
# YYYYDOYHHMMSS
            if InFormat == 41:
                if DateTime.isdigit() == False:
                    if Verify == False:
                        return ""
                    else:
                        return (1, "RW", "Non-digits in value.", 2)
                YYYY = intt(DateTime[:4])
                DOY = intt(DateTime[4:7])
                HH = intt(DateTime[7:9])
                MM = intt(DateTime[9:11])
                SS = floatt(DateTime[11:])
                if Verify == True:
                    DT = []
                    DT.append(str(YYYY))
                    DT.append(str(DOY))
                    DT.append(str(HH))
                    DT.append(str(MM))
                    DT.append(str(SS))
                    Ret = dt2TimeVerify("ydhms", DT)
                    if Ret[0] != 0:
                        return Ret
                MMM, DD = dt2Timeydoy2md(YYYY, DOY)
            else:
                if Verify == False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)."% \
                            InFormat, 3, "")
        elif InFormat < 60:
# YYYYMMDDHHMMSS
            if InFormat == 51:
                if DateTime.isdigit() == False:
                    if Verify == False:
                        return ""
                    else:
                        return (1, "RW", "Non-digits in value.", 2)
                YYYY = intt(DateTime[:4])
                MMM = intt(DateTime[4:6])
                DD = intt(DateTime[6:8])
                HH = intt(DateTime[8:10])
                MM = intt(DateTime[10:12])
                SS = floatt(DateTime[12:])
                if Verify == True:
                    DT = []
                    DT.append(str(YYYY))
                    DT.append(str(MMM))
                    DT.append(str(DD))
                    DT.append(str(HH))
                    DT.append(str(MM))
                    DT.append(str(SS))
                    Ret = dt2TimeVerify("ymdhms", DT)
                    if Ret[0] != 0:
                        return Ret
                DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            else:
                if Verify == False:
                    return ""
                else:
                    return (1, "MWX", "dt2Time: Unknown InFormat code (%d)."% \
                            InFormat, 3, "")
# If the caller just wants to work with the seconds we'll split it up and
# return the number of seconds into the day. In this case the OutFormat value
# will not be used.
        elif InFormat == 100:
            Parts = DateTime.split(":")
            Parts += (3-len(Parts))*["0"]
            if Verify == True:
                Ret = dt2TimeVerify("hms", Parts)
                if Ret[0] != 0:
                    return Ret
            if Verify == False:
                return (intt(Parts[0])*3600)+(intt(Parts[1])*60)+ \
                        float(Parts[2])
            else:
                return (0, (intt(Parts[0])*3600)+(intt(Parts[1])*60)+ \
                        float(Parts[2]))
# Now that we have all of the parts do what the caller wants and return the
# result.
# Return the Epoch.
    if OutFormat == -1:
        try:
            Epoch = Y2EPOCH[YYYY]
        except KeyError:
            Epoch = 0.0
            for YYY in arange(1970, YYYY):
                if YYY%4 != 0:
                    Epoch += 31536000.0
                elif YYY%100 != 0 or YYY%400 == 0:
                    Epoch += 31622400.0
                else:
                    Epoch += 31536000.0
            Y2EPOCH[YYYY] = Epoch
        Epoch += ((DOY-1)*86400.0)+(HH*3600.0)+(MM*60.0)+SS
        if Verify == False:
            return Epoch
        else:
            return (0, Epoch)
    elif OutFormat == 0:
        if Verify == False:
            return YYYY, MMM, DD, DOY, HH, MM, SS
        else:
            return (0, YYYY, MMM, DD, DOY, HH, MM, SS)
    elif OutFormat == 1:
        Format = OPTDateFormatRVar.get()
        if Format == "YYYY:DOY":
            OutFormat = 11
        elif Format == "YYYY-MM-DD":
            OutFormat = 22
        elif Format == "YYYYMMMDD":
            OutFormat = 32
# Usually used for troubleshooting.
        elif len(Format) == 0:
            try:
                Epoch = Y2EPOCH[YYYY]
            except KeyError:
                for YYY in arange(1970, YYYY):
                    if YYY%4 != 0:
                        Epoch += 31536000.0
                    elif YYY%100 != 0 or YYY%400 == 0:
                        Epoch += 31622400.0
                    else:
                        Epoch += 31536000.0
                Y2EPOCH[YYYY] = Epoch
            Epoch += ((DOY-1)*86400.0)+(HH*3600.0)+(MM*60.0)+SS
            if Verify == False:
                return Epoch
            else:
                return (0, Epoch)
# This is the easiest way I can think of to keep an SS of 59.9999 from being
# rounded and formatted to 60.000. Some stuff at some point may slip into the
# microsecond realm. Then this whole library of functions may have to be
# changed.
    if SS%1 > .999:
        SS = int(SS)+.999
# These OutFormat values are the same as InFormat values, plus others.
# YYYY:DOY:HH:MM:SS.sss
    if OutFormat == 11:
        if Verify == False:
            return "%d:%03d:%02d:%02d:%06.3f"%(YYYY, DOY, HH, MM, SS)
        else:
            return (0, "%d:%03d:%02d:%02d:%06.3f"%(YYYY, DOY, HH, MM, SS))
# YYYY:DOY HH:MM:SS.sss
    elif OutFormat == 12:
        if Verify == False:
            return "%d:%03d %02d:%02d:%06.3f"%(YYYY, DOY, HH, MM, SS)
        else:
            return (0, "%d:%03d %02d:%02d:%06.3f"%(YYYY, DOY, HH, MM, SS))
# YYYY:DOY - just because it's popular (for LOGPEEK) and it saves having the
# caller always doing  .split()[0]
    elif OutFormat == 13:
        if Verify == False:
            return "%d:%03d"%(YYYY, DOY)
        else:
            return (0, "%d:%03d"%(YYYY, DOY))
# YYYY:DOY:HH:MM:SS - just because it's really popular in POCUS and other
# programs.
    elif OutFormat == 14:
        if Verify == False:
            return "%d:%03d:%02d:%02d:%02d"%(YYYY, DOY, HH, MM, int(SS))
        else:
            return (0, "%d:%03d:%02d:%02d:%02d"%(YYYY, DOY, HH, MM, int(SS)))
# YYYY-MM-DD:HH:MM:SS.sss
    elif OutFormat == 21:
        if Verify == False:
            return "%d-%02d-%02d:%02d:%02d:%06.3f"%(YYYY, MMM, DD, HH, MM, SS)
        else:
            return (0, "%d-%02d-%02d:%02d:%02d:%06.3f"%(YYYY, MMM, DD, HH, \
                    MM, SS))
# YYYY-MM-DD HH:MM:SS.sss
    elif OutFormat == 22:
        if Verify == False:
            return "%d-%02d-%02d %02d:%02d:%06.3f"%(YYYY, MMM, DD, HH, MM, SS)
        else:
            return (0, "%d-%02d-%02d %02d:%02d:%06.3f"%(YYYY, MMM, DD, HH, \
                    MM, SS))
# YYYY-MM-DD/DOY HH:MM:SS.sss
    elif OutFormat == 23:
        if Verify == False:
            return "%d-%02d-%02d/%03d %02d:%02d:%06.3f"%(YYYY, MMM, DD, DOY, \
                    HH, MM, SS)
        else:
            return (0, "%d-%02d-%02d/%03d %02d:%02d:%06.3f"%(YYYY, MMM, DD, \
                    DOY, HH, MM, SS))
# Some portion of YYYY-MM-DD HH:MM:SS. Returns integer seconds.
# In that this is a human-entered thing (programs don't store partial
# date/times) we'll return whatever was entered without the /DOY, since the
# next step would be to do something like look it up in a database.
    elif OutFormat == 24:
        DateTime = "%d"%YYYY
        if MMM != -1:
            DateTime += "-%02d"%MMM
        else:
# Return what we have if this item was not provided, same on down.
            if Verify == False:
                return DateTime
            else:
                return (0, DateTime)
        if DD != -1:
            DateTime += "-%02d"%DD
        else:
            if Verify == False:
                return DateTime
            else:
                return (0, DateTime)
        if HH != -1:
            DateTime += " %02d"%HH
        else:
            if Verify == False:
                return DateTime
            else:
                return (0, DateTime)
        if MM != "-1":
            DateTime += ":%02d"%MM
        else:
            if Verify == False:
                return DateTime
            else:
                return (0, DateTime)
# Returns integer second since the caller has no idea what is coming back.
        if SS != "-1":
            DateTime += ":%02d"%SS
        if Verify == False:
            return DateTime
        else:
            return (0, DateTime)
# YYYY-MM-DD
    elif OutFormat == 25:
        if Verify == False:
            return "%d-%02d-%02d"%(YYYY, MMM, DD)
        else:
            return (0, "%d-%02d-%02d"%(YYYY, MMM, DD))
# YYYYMMMDD:HH:MM:SS.sss
    elif OutFormat == 31:
        if Verify == False:
            return "%d%s%02d:%02d:%02d:%06.3f"%(YYYY, PROG_CALMONS[MMM], DD, \
                    HH, MM, SS)
        else:
            return (0, "%d%s%02d:%02d:%02d:%06.3f"%(YYYY, PROG_CALMONS[MMM], \
                    DD, HH, MM, SS))
# YYYYMMMDD HH:MM:SS.sss
    elif OutFormat == 32:
        if Verify == False:
            return "%d%s%02d %02d:%02d:%06.3f"%(YYYY, PROG_CALMONS[MMM], DD, \
                    HH, MM, SS)
        else:
            return (0, "%d%s%02d %02d:%02d:%06.3f"%(YYYY, PROG_CALMONS[MMM], \
                    DD, HH, MM, SS))
# YYYYDOYHHMMSS.sss
    elif OutFormat == 41:
        if Verify == False:
            return "%d%03d%02d%02d%06.3f"%(YYYY, DOY, HH, MM, SS)
        else:
            return (0, "%d%03d%02d%02d%06.3f"%(YYYY, DOY, HH, MM, SS))
# YYYYMMDDHHMMSS.sss
    elif OutFormat == 51:
        if Verify == False:
            return "%d%02d%02d%02d%02d%06.3f"%(YYYY, MMM, DD, HH, MM, SS)
        else:
            return (0, "%d%02d%02d%02d%02d%06.3f"%(YYYY, MMM, DD, HH, MM, SS))
# Returns what ever OPTDateFormatRVar is set to.
# 80 is dt, 81 is d and 82 is t.
    elif OutFormat == 80 or OutFormat == 81 or OutFormat == 82:
        if OPTDateFormatRVar.get() == "YYYY:DOY":
            if OutFormat == 80:
                if Verify == False:
                    return "%d:%03d:%02d:%02d:%06.3f"%(YYYY, DOY, HH, MM, SS)
                else:
                    return (0, "%d:%03d:%02d:%02d:%06.3f"%(YYYY, DOY, HH, MM, \
                            SS))
            elif OutFormat == 81:
                if Verify == False:
                    return "%d:%03d"%(YYYY, DOY)
                else:
                    return (0, "%d:%03d"%(YYYY, DOY))
            elif OutFormat == 82:
                if Verify == False:
                    return "%02d:%02d:%06.3f"%(HH, MM, SS)
                else:
                    return (0, "%02d:%02d:%06.3f"%(HH, MM, SS))
        elif OPTDateFormatRVar.get() == "YYYY-MM-DD":
            if OutFormat == 80:
                if Verify == False:
                    return "%d-%02d-%02d %02d:%02d:%06.3f"%(YYYY, MMM, DD, \
                            HH, MM, SS)
                else:
                    return (0, "%d-%02d-%02d %02d:%02d:%06.3f"%(YYYY, MMM, \
                            DD, HH, MM, SS))
            elif OutFormat == 81:
                if Verify == False:
                    return "%d-%02d-%02d"%(YYYY, MMM, DD)
                else:
                    return (0, "%d-%02d-%02d"%(YYYY, MMM, DD))
            elif OutFormat == 82:
                if Verify == False:
                    return "%02d:%02d:%06.3f"%(HH, MM, SS)
                else:
                    return (0, "%02d:%02d:%06.3f"%(HH, MM, SS))
        elif OPTDateFormatRVar.get() == "YYYYMMMDD":
            if OutFormat == 80:
                if Verify == False:
                    return "%d%s%02d %02d:%02d:%06.3f"%(YYYY, \
                            PROG_CALMONS[MMM], DD, HH, MM, SS)
                else:
                    return (0, "%d%s%02d %02d:%02d:%06.3f"%(YYYY, \
                            PROG_CALMONS[MMM], DD, HH, MM, SS))
            elif OutFormat == 81:
                if Verify == False:
                    return "%d%s%02d"%(YYYY, PROG_CALMONS[MMM], DD)
                else:
                    return (0, "%d%s%02d"%(YYYY, PROG_CALMONS[MMM], DD))
            elif OutFormat == 82:
                if Verify == False:
                    return "%02d:%02d:%06.3f"%(HH, MM, SS)
                else:
                    return (0, "%02d:%02d:%06.3f"%(HH, MM, SS))
        elif len(OPTDateFormatRVar.get()) == 0:
            if Verify == False:
                return str(DateTime)
            else:
                return (0, str(DateTime))
    else:
        if Verify == False:
            return ""
        else:
            return (1, "MWX", "dt2Time: Unknown OutFormat code (%d)."% \
                    OutFormat, 3, "")
################################
# BEGIN: dt2Timedhms2Secs(InStr)
# FUNC:dt2Timedhms2Secs():2018.235
#   Returns the number of seconds in strings like 1h30m.
def dt2Timedhms2Secs(InStr):
    InStr = InStr.replace(" ", "").lower()
    if len(InStr) == 0:
        return 0
    Chars = list(InStr)
    Value = 0
    SubValue = ""
    for Char in Chars:
        if Char.isdigit():
            SubValue += Char
        elif Char == "s":
            Value += intt(SubValue)
            SubValue = ""
        elif Char == "m":
            Value += intt(SubValue)*60
            SubValue = ""
        elif Char == "h":
            Value += intt(SubValue)*3600
            SubValue = ""
        elif Char == "d":
            Value += intt(SubValue)*86400
            SubValue = ""
# Must have just been passed a number with no s m h or d or 1h30 which will be
# treated as 1 hour, 30 seconds.
    if len(SubValue) != 0:
        Value += intt(SubValue)
    return Value
#############################################
# BEGIN: dt2TimeDT(Format, DateTime, Fix = 2)
# FUNC:dt2TimeDT():2018.270
#   This is for the one-off time conversion items that have been needed here
#   and there for specific items. Input is some string of date and/or time.
#   Some of these can be done with dt2Time(), but these are more for when the
#   code knows exactly what it has and exactly what it needs. It's program
#   dependent and maybe useful to others.
#     Format = 1 = [dd]hhmmss or even blank to [DD:]HH:MM:SS
#                  Replaces dhms2DHMS().
#              2 = YYYY:DOY:HH:MM:SS to ISO8601 time YYYY-MM-DDTHH:MM:SSZ.
#                  No time zone conversion is done (or any error checking) so
#                  you need to make sure the passed time is really Zulu or this
#                  will return a lie.
#                  Replaces YDHMS28601().
#              3 = The opposite of above.
#                  Replaces iso86012YDHMS().
#              4 = Adds /DOY to a passed date/time that can be just the date,
#                  but that must be a YYYY-MM-DD format. Date and time must be
#                  separated by a space. It's the only date format that I've
#                  ever wanted to add the DOY to. Returns the fixed up
#                  date/time. Does NO error checking, and, as you can see, if
#                  the date is not the right format it will choke causing an
#                  embarrassing crash. You have been warned.
#                  Replaces addDDD().
#              5 = Anti-InFormat 4. Is OK if /DOY is not there.
#                  Replaces remDDD().
#              6 = yyydoyhhmmss... to YYYY:DOY:HH:MM...
#                  Replaces ydhmst2YDHMST().
#              7 = YYYY:DOY:HH:MM:SS -> YYYY-MM-DD:HH:MM:SS.
#                  Replaces ydhms2ymdhmsDash().
#              8 = YYYYMMDDHHMMSS to YYYY-MM-DD HH:MM:SS
#              9 = YYYYMMDDHHMMSS to YYYY-MM-DD
#             10 = HH:MM:SS to int H, M, S
#             11 = Seconds to HH:MM:SS (Use Fix to pad with extra blanks if
#                  large hour numbers are expected)
#             12 = HH:MM:SS/HH:MM/HH/blank to seconds
def dt2TimeDT(Format, DateTime, Fix = 2):
# Everything wants a string, except 11. Python 3 will probably pass DateTime
# as bytes.
    if Format != 11:
        if isinstance(DateTime, astring) == False:
            DateTime = DateTime.decode("latin-1")
    if Format == 1:
# Check the length before the .strip(). DateTime may be just blanks, but we'll
# use the length to determine what to return.
        Len = len(DateTime)
        DateTime = DateTime.strip()
        if len(DateTime) == 0:
            return "00:00:00:00"
        if Len == 6:
            return "%s:%s:%s"%(DateTime[:2], DateTime[2:4], DateTime[4:6])
        else:
            return "%s:%s:%s:%s"%(DateTime[:2], DateTime[2:4], DateTime[4:6], \
                    DateTime[6:8])
    elif Format == 2:
        Parts = DateTime.split(":")
        if len(Parts) != 5:
            return ""
        MM, DD, = dt2Timeydoy2md(intt(Parts[0]), intt(Parts[1]))
        return "%s-%02d-%02dT%s:%s:%sZ"%(Parts[0], MM, DD, Parts[2], \
            Parts[3], Parts[4])
    elif Format == 3:
        Parts = DateTime.split("T")
        if len(Parts) != 2:
            return ""
# Who knows what may be wrong with the passed time.
        try:
            Parts2 = Parts[0].split("-")
            YYYY = intt(Parts2[0])
            MMM = intt(Parts2[1])
            DD = intt(Parts2[2])
            DOY = dt2Timeymd2doy(YYYY, MMM, DD)
            Parts2 = Parts[1].split(":")
            HH = intt(Parts2[0])
            MM = intt(Parts2[1])
            SS = intt(Parts2[2])
        except:
            return ""
        return "%d:%03d:%02d:%02d:%02d"%(YYYY, DOY, HH, MM, SS)
    elif Format == 4:
        Parts = DateTime.split()
        if len(Parts) == 0:
            return ""
        Parts2 = Parts[0].split("-")
        YYYY = intt(Parts2[0])
        MMM = intt(Parts2[1])
        DD = intt(Parts2[2])
        DOY = dt2Timeymd2doy(YYYY, MMM, DD)
        if len(Parts) == 1:
            return "%s/%03d"%(Parts[0], DOY)
        else:
            return "%s/%03d %s"%(Parts[0], DOY, Parts[1])
    elif Format == 5:
        Parts = DateTime.split()
        if len(Parts) == 0:
            return ""
# A little bit of protection.
        try:
            i = Parts[0].index("/")
            Parts[0] = Parts[0][:i]
        except ValueError:
            pass
        if len(Parts) == 1:
            return Parts[0]
        else:
            return "%s %s"%(Parts[0], Parts[1])
# yyyydoyhhmmssttt or yyyydoyhhmmss.
    elif Format == 6:
        Len = len(DateTime)
        DateTime = DateTime.strip()
# yyyydoyhhmm
        if Len == 12:
            if len(DateTime) == 0:
                return "0000:000:00:00"
            return "%s:%s:%s:%s"%(DateTime[:4], DateTime[4:7], DateTime[7:9], \
                    DateTime[9:11])
# yyyydoyhhmmss  All of the fields that may contain this are 14 bytes long
# with a trailing space which got removed above (it was that way in LOGPEEK
# anyways).
        elif Len == 14:
            if len(DateTime) == 0:
                return "0000:000:00:00:00"
            return "%s:%s:%s:%s:%s"%(DateTime[:4], DateTime[4:7], \
                    DateTime[7:9], DateTime[9:11], DateTime[11:13])
# yyyydoyhhmmssttt
        elif Len == 16:
            if len(DateTime) == 0:
                return "0000:000:00:00:00:000"
            return "%s:%s:%s:%s:%s:%s"%(DateTime[:4], DateTime[4:7], \
                    DateTime[7:9], DateTime[9:11], DateTime[11:13], \
                    DateTime[13:])
        return ""
    elif Format == 7:
        Parts = DateTime.split(":")
        YYYY = intt(Parts[0])
        DOY = intt(Parts[1])
        MMM, DD = dt2Timeydoy2md(YYYY, DOY)
        return "%d-%02d-%02d:%s:%s:%s"%(YYYY, MMM, DD, Parts[2], Parts[3], \
                Parts[4])
# YYYYMMDDHHMMSS to YYYY-MM-DD HH:MM:SS
    elif Format == 8:
        if len(DateTime) == 14:
            return "%s-%s-%s %s:%s:%s"%(DateTime[0:4], DateTime[4:6], \
                    DateTime[6:8], DateTime[8:10], DateTime[10:12], \
                    DateTime[12:14])
        return ""
# YYYYMMDDHHMMSS to YYYY-MM-DD
    elif Format == 9:
        if len(DateTime) == 14:
            return "%s-%s-%s"%(DateTime[0:4], DateTime[4:6], DateTime[6:8])
        return ""
# HH:MM:SS to int H, M, S
    elif Format == 10:
        try:
            Parts2 = DateTime.split(":")
            Parts = []
            for Part in Parts2:
                Parts.append(intt(Part))
            Parts += (3-len(Parts))*[0]
        except:
            return 0, 0, 0
        return Parts[0], Parts[1], Parts[2]
# Seconds to HH:MM:SS (use Fix to pad with spaces)
    elif Format == 11:
        HH = DateTime//3600
        Sub = HH*3600
        MM = (DateTime-Sub)//60
        Sub += MM*60
        SS = (DateTime-Sub)
        H = "%02d"%HH
        if Fix > 2:
            H = "%*s"%(Fix, H)
        return "%s:%02d:%02d"%(H, MM, SS)
# HH:MM:SS to seconds
    elif Format == 12:
        try:
            Parts2 = DateTime.split(":")
            Parts = []
            for Part in Parts2:
                Parts.append(intt(Part))
            Parts += (3-len(Parts))*[0]
        except:
            return 0
        return Parts[0]*3600+Parts[1]*60+Parts[2]
######################################################################
# BEGIN: dt2TimeMath(DeltaDD, DeltaSS, YYYY, MMM, DD, DOY, HH, MM, SS)
# LIB:dt2TimeMath():2013.039
#   Adds or subtracts (depending on the sign of DeltaDD/DeltaSS) the requested
#   amount of time to/from the passed date.  Returns a tuple with the results:
#           (YYYY, MMM, DD, DOY, HH, MM, SS)
#   Pass -1 or 0 for MMM/DD, or DOY depending on which is to be used. If MMM/DD
#   are >=0 then MMM, DD, and DOY will be filled in on return. If MMM/DD is -1
#   then just DOY will be used/returned. If MMM/DD are 0 then DOY will be used
#   but MMM and DD will also be returned.
#   SS will be int or float depending on what was passed.
def dt2TimeMath(DeltaDD, DeltaSS, YYYY, MMM, DD, DOY, HH, MM, SS):
    DeltaDD = int(DeltaDD)
    DeltaSS = int(DeltaSS)
    if YYYY < 1000:
        if YYYY < 70:
            YYYY += 2000
        else:
            YYYY += 1900
# Work in DOY.
    if MMM > 0:
        DOY = dt2Timeymd2doy(YYYY, MMM, DD)
    if DeltaDD != 0:
        if DeltaDD > 0:
            Forward = 1
        else:
            Forward = 0
        while 1:
# Speed limit the change to keep things simpler.
            if DeltaDD < -365 or DeltaDD > 365:
                if Forward == 1:
                    DOY += 365
                    DeltaDD -= 365
                else:
                    DOY -= 365
                    DeltaDD += 365
            else:
                DOY += DeltaDD
                DeltaDD = 0
            if YYYY%4 != 0:
                Leap = 0
            elif YYYY%100 != 0 or YYYY%400 == 0:
                Leap = 1
            else:
                Leap = 0
            if DOY < 1 or DOY > 365+Leap:
                if Forward == 1:
                    DOY -= 365+Leap
                    YYYY += 1
                else:
                    YYYY -= 1
                    if YYYY%4 != 0:
                        Leap = 0
                    elif YYYY%100 != 0 or YYYY%400 == 0:
                        Leap = 1
                    else:
                        Leap = 0
                    DOY += 365+Leap
            if DeltaDD == 0:
                break
    if DeltaSS != 0:
        if DeltaSS > 0:
            Forward = 1
        else:
            Forward = 0
        while 1:
# Again, speed limit just to keep the code reasonable.
            if DeltaSS < -59 or DeltaSS > 59:
                if Forward == 1:
                    SS += 59
                    DeltaSS -= 59
                else:
                    SS -= 59
                    DeltaSS += 59
            else:
                SS += DeltaSS
                DeltaSS = 0
            if SS < 0 or SS > 59:
                if Forward == 1:
                    SS -= 60
                    MM += 1
                    if MM > 59:
                        MM = 0
                        HH += 1
                        if HH > 23:
                            HH = 0
                            DOY += 1
                            if DOY > 365:
                                if YYYY%4 != 0:
                                    Leap = 0
                                elif YYYY%100 != 0 or YYYY%400 == 0:
                                    Leap = 1
                                else:
                                    Leap = 0
                                if DOY > 365+Leap:
                                    YYYY += 1
                                    DOY = 1
                else:
                    SS += 60
                    MM -= 1
                    if MM < 0:
                        MM = 59
                        HH -= 1
                        if HH < 0:
                            HH = 23
                            DOY -= 1
                            if DOY < 1:
                                YYYY -= 1
                                if YYYY%4 != 0:
                                    DOY = 365
                                elif YYYY%100 != 0 or YYYY%400 == 0:
                                    DOY = 366
                                else:
                                    DOY = 365
            if DeltaSS == 0:
                 break
    if MMM != -1:
        MMM, DD = dt2Timeydoy2md(YYYY, DOY)
    return (YYYY, MMM, DD, DOY, HH, MM, SS)
####################################
# BEGIN: dt2TimeVerify(Which, Parts)
# FUNC:dt2TimeVerify():2015.048
#   This could figure out what to check just by passing [Y,M,D,D,H,M,S], but
#   that means the splitters() in dt2Time would need to work much harder to
#   make sure all of the Parts were there, thus it needs a Which value.
def dt2TimeVerify(Which, Parts):
    if Which.startswith("ydhms"):
        YYYY = intt(Parts[0])
        MMM = -1
        DD = -1
        DOY = intt(Parts[1])
        HH = intt(Parts[2])
        MM = intt(Parts[3])
        SS = floatt(Parts[4])
    elif Which.startswith("ymdhms") or Which.startswith("xymdhms"):
        YYYY = intt(Parts[0])
        MMM = intt(Parts[1])
        DD = intt(Parts[2])
        DOY = -1
        HH = intt(Parts[3])
        MM = intt(Parts[4])
        SS = floatt(Parts[5])
    elif Which.startswith("hms"):
        YYYY = -1
        MMM = -1
        DD = -1
        DOY = -1
        HH = intt(Parts[0])
        MM = intt(Parts[1])
        SS = floatt(Parts[2])
    if YYYY != -1 and (YYYY < 1000 or YYYY > 9999):
        return (1, "RW", "Bad year value: %d"%YYYY, 2, "")
# Normally do the normal checking.
    if Which.startswith("x") == False:
        if MMM != -1 and (MMM < 1 or MMM > 12):
            return (1, "RW", "Bad month value: %d"%MMM, 2, "")
        if DD != -1:
            if DD < 1 or DD > 31:
                return (1, "RW", "Bad day value: %d"%DD, 2, "")
            if YYYY%4 != 0:
                if DD > PROG_MAXDPMNLY[MMM]:
                    return (1, "RW", "Too many days for month %d: %d"%(MMM, \
                            DD), 2, "")
            elif YYYY%100 != 0 or YYYY%400 == 0:
                if DD > PROG_MAXDPMLY[MMM]:
                    return (1, "RW", "Too many days for month %d: %d"%(MMM, \
                            DD), 2, "")
            else:
                if DD > PROG_MAXDPMNLY[MMM]:
                    return (1, "RW", "Too many days for month %d: %d"%(MMM, \
                            DD), 2, "")
        if DOY != -1:
            if DOY < 1 or DOY > 366:
                return (1, "RW", "Bad day of year value: %d"%DOY, 2, "")
            if YYYY%4 != 0 and DOY > 365:
                return (1, "RW" "Too many days for non-leap year: %d"%DOY, \
                        2, "")
        if HH < 0 or HH >= 24:
            return (1, "RW", "Bad hour value: %d"%HH, 2, "")
        if MM < 0 or MM >= 60:
            return (1, "RW", "Bad minute value: %d"%MM, 2, "")
        if SS < 0 or SS >= 60:
            return (1, "RW", "Bad seconds value: %06.3f"%SS, 2, "")
# "x" checks.
# Here if Which starts with "x" then it is OK if month and day values are
# missing. This is for checking an entry like  2013-7  for example. If the
# portion of the date(/time) that was passed looks OK then (0,) will be
# returned. If it is something like  2013-13  then that will be bad.
    else:
# If the user entered just the year, then we are done, etc.
        if MMM != -1:
            if MMM < 1 or MMM > 12:
                return (1, "RW", "Bad month value: %d"%MMM, 2, "")
            if DD != -1:
                if DD < 1 or DD > 31:
                    return (1, "RW", "Bad day value: %d"%DD, 2, "")
                if YYYY%4 != 0:
                    if DD > PROG_MAXDPMNLY[MMM]:
                        return (1, "RW", "Too many days for month %d: %d"% \
                                (MMM, DD), 2, "")
                elif YYYY%100 != 0 or YYYY%400 == 0:
                    if DD > PROG_MAXDPMLY[MMM]:
                        return (1, "RW", "Too many days for month %d: %d"% \
                                (MMM, DD), 2, "")
                else:
                    if DD > PROG_MAXDPMNLY[MMM]:
                        return (1, "RW", "Too many days for month %d: %d"% \
                                (MMM, DD), 2, "")
                if DOY != -1:
                    if DOY < 1 or DOY > 366:
                        return (1, "RW", "Bad day of year value: %d"%DOY, 2, \
                                "")
                    if YYYY%4 != 0 and DOY > 365:
                        return (1, "RW" \
                                "Too many days for non-leap year: %d"%DOY, \
                                2, "")
                if HH != -1:
                    if HH < 0 or HH >= 24:
                        return (1, "RW", "Bad hour value: %d"%HH, 2, "")
                    if MM != -1:
                        if MM < 0 or MM >= 60:
                            return (1, "RW", "Bad minute value: %d"%MM, 2, "")
# Checking these special values are usually from user input and are date/time
# values and not timing values, so the seconds part here will just be an int.
                        if SS != -1:
                            if SS < 0 or SS >= 60:
                                return (1, "RW", "Bad seconds value: %d"%SS, \
                                        2, "")
    return (0,)
##########################################
# BEGIN: dt2TimeydoyMath(Delta, YYYY, DOY)
# LIB:dt2TimeydoyMath():2013.036
#   Adds or subtracts the passed number of days (Delta) to the passed year and
#   day of year values. Returns YYYY and DOY.
#   Replaces ydoyMath().
def dt2TimeydoyMath(Delta, YYYY, DOY):
    if Delta == 0:
        return YYYY, DOY
    if Delta > 0:
        Forward = 1
    elif Delta < 0:
        Forward = 0
    while 1:
# Speed limit the change to keep things simpler.
        if Delta < -365 or Delta > 365:
            if Forward == 1:
                DOY += 365
                Delta -= 365
            else:
                DOY -= 365
                Delta += 365
        else:
            DOY += Delta
            Delta = 0
# This was isLeap(), but it's such a simple thing I'm not sure it was worth the
# function call, so it became this in all library functions.
        if YYYY%4 != 0:
            Leap = 0
        elif YYYY%100 != 0 or YYYY%400 == 0:
            Leap = 1
        else:
            Leap = 0
        if DOY < 1 or DOY > 365+Leap:
            if Forward == 1:
                DOY -= 365+Leap
                YYYY += 1
            else:
                YYYY -= 1
                if YYYY%4 != 0:
                    Leap = 0
                elif YYYY%100 != 0 or YYYY%400 == 0:
                    Leap = 1
                else:
                    Leap = 0
                DOY += 365+Leap
            break
        if Delta == 0:
            break
    return YYYY, DOY
##################################
# BEGIN: dt2Timeydoy2md(YYYY, DOY)
# FUNC:dt2Timeydoy2md():2013.030
#   Does no values checking, so make sure you stuff is in one sock before
#   coming here.
def dt2Timeydoy2md(YYYY, DOY):
    if DOY < 32:
        return 1, DOY
    elif DOY < 60:
        return 2, DOY-31
    if YYYY%4 != 0:
        Leap = 0
    elif YYYY%100 != 0 or YYYY%400 == 0:
        Leap = 1
    else:
        Leap = 0
# Check for this special day.
    if Leap == 1 and DOY == 60:
        return 2, 29
# The PROG_FDOM values for Mar-Dec are set up for non-leap years. If it is a
# leap year and the date is going to be Mar-Dec (it is if we have made it this
# far), subtract Leap from the day.
    DOY -= Leap
# We start through PROG_FDOM looking for dates in March.
    Month = 3
    for FDOM in PROG_FDOM[4:]:
# See if the DOY is less than the first day of next month.
        if DOY <= FDOM:
# Subtract the DOY for the month that we are in.
            return Month, DOY-PROG_FDOM[Month]
        Month += 1
# If anything goes wrong...
    return 0, 0
######################################
# BEGIN: dt2Timeymd2doy(YYYY, MMM, DD)
# FUNC:dt2Timeymd2doy():2013.030
def dt2Timeymd2doy(YYYY, MMM, DD):
    if YYYY%4 != 0:
        return (PROG_FDOM[MMM]+DD)
    elif (YYYY%100 != 0 or YYYY%400 == 0) and MMM > 2:
        return (PROG_FDOM[MMM]+DD+1)
    else:
        return (PROG_FDOM[MMM]+DD)
##################################################################
# BEGIN: dt2Timeymddhms(OutFormat, YYYY, MMM, DD, DOY, HH, MM, SS)
# FUNC:dt2Timeymddhms():2018.235
#   The general-purpose time handler for when the time is already split up into
#   it's parts.
#   Make MMM, DD -1 if passing DOY and DOY -1 if passing MMM, DD.
def dt2Timeymddhms(OutFormat, YYYY, MMM, DD, DOY, HH, MM, SS):
    global Y2EPOCH
# Returns a float Epoch is SS is a float, otherwise an int value.
    if OutFormat == -1:
        Epoch = 0
        if YYYY < 70:
            YYYY += 2000
        if YYYY < 100:
            YYYY += 1900
        try:
            Epoch = Y2EPOCH[YYYY]
        except KeyError:
            for YYY in arange(1970, YYYY):
                if YYY%4 != 0:
                    Epoch += 31536000
                elif YYY%100 != 0 or YYY%400 == 0:
                    Epoch += 31622400
                else:
                    Epoch += 31536000
            Y2EPOCH[YYYY] = Epoch
        if DOY == -1:
            DOY = dt2Timeymd2doy(YYYY, MMM, DD)
        return Epoch+((DOY-1)*86400)+(HH*3600)+(MM*60)+SS
##########################################
# BEGIN: dt2Timeystr2Epoch(YYYY, DateTime)
# FUNC:dt2Timeystr2Epoch():2018.238
#   A slightly specialized time to Epoch converter where the input can be
#       YYYY, DOY:HH:MM:SS:TTT or
#       None,YYYY:DOY:HH:MM:SS:TTT or
#       None,YYYY-MM-DD:HH:MM:SS:TTT
#   DOY/DD and HH must be separated by a ':' -- no space.
#   The separator between SS and TTT may be : or .
#   Returns 0 if ANYTHING goes wrong.
#   Returns a float if the seconds were a float (SS:TTT or SS.ttt), or an int
#   if the time was incomplete or there was an error. The caller will just
#   have to do an int() if that is all they want.
#   Replaces str2Epoch(). Used a lot in RT130/72A data decoding.
def dt2Timeystr2Epoch(YYYY, DateTime):
    global Y2EPOCH
    Epoch = 0
    try:
        if DateTime.find("-") != -1:
            DT = DateTime.split(":", 1)
            Parts2 = DT[0].split("-")
            Parts = []
            for Parts in Parts2:
                Parts.append(float(Part))
            Parts2 = DT[1].split(":")
            for Parts in Parts2:
                Parts.append(float(Part))
# There "must" have been a :TTT part.
            if len(Parts) == 6:
                Parts[5] += Parts[6]/1000.0
        else:
            Parts2 = DateTime.split(":")
            Parts = []
            for Part in Parts2:
                Parts.append(float(Part))
# There "must" have been a :TTT part.
            if len(Parts) == 7:
                Parts[5] += Parts[6]/1000.0
        if YYYY is None:
# Change this since it gets used in loops and dictionary keys and stuff. If
# the year is passed (below) then we're already covered.
            Parts[0] = int(Parts[0])
# Just in case someone generates 2-digit years.
            if Parts[0] < 100:
                if Parts[0] < 70:
                    Parts[0] += 2000
                else:
                    Parts[0] += 1900
        else:
# Check this just in case. The caller will just have to figure this one out.
            if YYYY < 1970:
                YYYY = 1970
            Parts = [YYYY, ]+Parts
        Parts += (5-len(Parts))*[0]
# This makes each year's Epoch get calculated only once.
        try:
            Epoch = Y2EPOCH[Parts[0]]
        except KeyError:
            for YYY in arange(1970, Parts[0]):
                if YYY%4 != 0:
                    Epoch += 31536000
                elif YYY%100 != 0 or YYY%400 == 0:
                    Epoch += 31622400
                else:
                    Epoch += 31536000
            Y2EPOCH[Parts[0]] = Epoch
# This goes along with forcing YYYY to 1970 if things are bad.
        if Parts[1] < 1:
            Parts[1] = 1
        Epoch += ((int(Parts[1])-1)*86400)+(int(Parts[2])*3600)+ \
                (int(Parts[3])*60)+Parts[4]
    except ValueError:
        Epoch = 0
    return Epoch
##########################################################
# BEGIN: dtHoursFromNow(ToFormat, OutFormat, DateTime, TZ)
# FUNC:dtHoursFromNow():2018.264
#   Takes in a time (presumably in the future) and returns the decimal hours
#   from now until then.
#   ToFormat is the format of the time we are going to.
#   OutFormat is how to format the return time, "H" for decimal hours, or "H:M"
#   for HH:MM (no seconds).
#   TZ is either "GMT" or "LT" pertaining to the input DateTime.
#   The current time it uses (in YYYY:DOY:HH:MM:SS) is also returned.
#       (decimal hours, current time used)
def dtHoursFromNow(ToFormat, OutFormat, DateTime, TZ):
    InEpoch = dt2Time(ToFormat, -1, DateTime)
    if TZ == "GMT":
        NowDateTime = getGMT(0)
    elif TZ == "LT":
        NowDateTime = getGMT(13)
    NowEpoch = dt2Time(11, -1, NowDateTime)
    Diff = InEpoch-NowEpoch
    if OutFormat == "H":
        return ("%.2f hours"%(Diff/3600.0), NowDateTime)
    elif OutFormat == "H:M":
        H = Diff/3600.0
        M = int((H-int(H))*60.0)
        H = int(H)
# Rounding may cause this.
        if M > 59:
            H += 1
            M = M-60
        return ("%02d:%02d"%(H, M), NowDateTime)
    return ("0.00", "0000:000:00:00:00")
############################
# BEGIN: dt2DateDiff(D1, D2)
# FUNC:dt2DateDiff():2018.238
#   Takes two dates in YYYY-MM-DD format and returns the number of whole days
#   between them. Dates must be proper format or there will be crashing.
#   Returned is D2-D1, so D2 should be later than D1, but doesn't have to be.
def dt2DateDiff(D1, D2):
    Parts = D1.split("-")
    D1Y = int(Parts[0])
    D1M = int(Parts[1])
    D1D = int(Parts[2])
    Parts = D2.split("-")
    D2Y = int(Parts[0])
    D2M = int(Parts[1])
    D2D = int(Parts[2])
# Pick off the easy one.
    if D1Y == D2Y and D1M == D2M:
        return D2D-D1D
# Almost easy.
    elif D1Y == D2Y:
        D1Doy = dt2Timeymd2doy(D1Y, D1M, D1D)
        D2Doy = dt2Timeymd2doy(D2Y, D2M, D2D)
        return D2Doy-D1Doy
    else:
# The full monty.
        D1E = dt2Timeymddhms(-1, D1Y, D1M, D1D, -1, 0, 0, 0)
        D2E = dt2Timeymddhms(-1, D2Y, D2M, D2D, -1, 0, 0, 0)
        return int((D2E-D1E)/86400)
# END: dt2Time




#######################
# BEGIN: enumerateCmd()
# FUNC:enumerateCmd():2019.260
#   Handles more things itself (like update()s) because it's special (basically
#   we don't want the Stop button involved).
def enumerateCmd():
    global WorkState
# This command handles its own button logic. We (visually) don't want the Stop
# button involved since stopping enumeration before finishing will leave the
# communication board in a funny state. We only want people that know what
# they are doing to know about hitting the Enumerate button to stop. This will
# set WorkState in the UI thread and then the code further down will pick up on
# it and stop.
    if WorkState == 200:
        msgLn(0, "Y", "Stopping...")
        buttonBG("ENUM", "Y")
        PROGButs["ENUM"].update()
        WorkState = 999
        return
# If some other action is running don't start this one.
    Status, ListOfDASs = isItOKToStart(0)
    if Status == False:
        return
    WorkState = 200
    buttonBG("ENUM", "G")
    PROGButs["ENUM"].update()
    msgLn(1, "W", "Enumerating... (%s)"%getGMT(0))
# Clear out any DASs we had in the list.
    formSTATClearID(0)
    for i in arange(1, STATTXTOTAL+1):
        if OPTDebugCVar.get() == 1:
            stdout.write("enumerateCmd: Loop %d\n"%i)
        if WorkState == 999:
            break
# Open and close the port each time through the loop for the hardware end to
# detect (it toggles the RTS line for the CHANGEO board).
        if cmdPortOpen() == False:
            break
# Call out to any DAS connected. The CHANGEO board will keep it to 1 DAS at
# a time.
        Cmd = rt130CmdFormat("0000", "IDID")
        cmdPortWrite("0000", Cmd)
# Just to let everyone breathe.
        sleep(0.25)
        Rp = cmdPortReadline()
        if len(Rp) == 0:
           cmdPortClose()
           formSTATAddID("0000", True)
# Keep the port closed for a bit to cycle the RTS line for the CHANGEO board.
           sleep(0.1)
           continue
# Do a little QC on the line. The most common failure will be the user
# forgetting to hit the reset button on the CHANGEO board/box which may make
# the response less than 25 characters (that is about the length of a normal
# IDID command response) or the Unit ID portion not a valid hex number.
        if len(Rp) < 26:
            if OPTDebugCVar.get() == 1:
                stdout.write("A<<<< %s\n"%makePrintable(Rp))
                stdout.write("%s\n\n"%makeHex(Rp))
            msgLn(0, "R", "Did you forget to hit the reset button?", \
                    True, 2)
            cmdPortClose()
            WorkState = 0
            buttonBG("ENUM", "D")
# We don't need to do an update in places like this since the program will be
# going idle after the return.
            return
        DAS = Rp[2:2+4]
        try:
            ID = int(DAS, 16)
        except ValueError:
            if OPTDebugCVar.get() == 1:
                stdout.write("B<<<< %s\n"%makePrintable(Rp))
                stdout.write("%s\n\n"%makeHex(Rp))
            msgLn(0, "R", "Did you forget to hit the reset button?", \
                    True, 2)
            cmdPortClose()
            WorkState = 0
            buttonBG("ENUM", "D")
            return
# Just so we don't "find" other models.
        Ret = check130ID(DAS)
        if Ret[0] != 0:
# We'll override the color and not beep.
            msgLn(1, "Y", Ret[2])
            cmdPortClose()
            sleep(0.1)
            continue
        else:
            if OPTDebugCVar.get() == 1:
                stdout.write("C<<<< %s\n"%makePrintable(Rp))
                stdout.write("%s\n\n"%makeHex(Rp))
            formSTATAddID(DAS, True)
            if OPTGetCPUVerCVar.get() != 0:
                Cmd = rt130CmdFormat(DAS, "SSVS              SS")
                cmdPortWrite(DAS, Cmd)
                Rp = rt130GetRp(1, 5, 0)
                if len(Rp) == 0:
                    msgLn(1, "M", \
                           "No Version Status command response from DAS %s!"% \
                            DAS, True, 3)
                    cmdPortClose()
                    delay(0.1)
                    continue
                if checkRp(DAS, Rp) == False:
                    cmdPortClose()
                    delay(0.1)
                    continue
                msgLn(1, "", "%s: CPU version: "%DAS, False)
                msgLn(1, "G", Rp[32:32+16])
# Keep the port closed for a bit to cycle the RTS line for the CHANGEO board.
        cmdPortClose()
        sleep(0.1)
# This gets rid of any duplicate entries and 0000 unit IDs and sorts and
# displays the DASs that are left.
    ListOfDASs = formSTATRemoveDups(ListOfDASs)
    stoppedMe(WorkState, "")
    DASs = len(ListOfDASs)
    if DASs == 1:
        msgLn(0, "", "1 DAS found.")
    else:
        msgLn(0, "", "%d DASs found."%DASs)
    DASs = 0
    for DAS in ListOfDASs:
        msgLn(1, "", "  %s"%DAS, False)
        DASs += 1
        if DASs == 5:
            msgLn(1, "", "")
            DASs = 0
    if DASs != 0:
        msgLn(0, "", "")
    WorkState = 0
    buttonBG("ENUM", "D")
    return
# END: enumerateCmd




####################
# BEGIN: eraseMsgs()
# LIB:eraseMsgs():2018.244
HELPError += "eraseMsgs():\n\
------------\n\
Error erasing file <file> <message>\n\
-- A system error ocurred when trying to erase and recreate the messages \
file. The <message> should explain what happened.\n"

def eraseMsgs():
    global PROGMsgsFile
    TheMsgsDir = PROGMsgsDirVar.get()
    Answer = formMYD(Root, (("Start A New Messages File", TOP, "new"), \
            ("Erase Current File Contents", TOP, "erase"), ("(Cancel)", TOP, \
            "cancel")), "cancel", "", "Erase All Evidence Or What?", \
            "Erase the messages section and start a new messages file, OR erase the contents of the current messages file\n\n%s\n\nand keep using it (generally the old file should be preserved and not erased if in the middle of an experiment)?"% \
            PROGMsgsFile)
    if Answer == "cancel":
        return
    elif Answer == "new":
        msgLn(0, "", "", False, 0, False)
        if len(PROGMachineIDLCVar.get()) == 0:
            PROGMsgsFile = getGMT(1)[:7]+"-"+PROG_NAMELC+".msg"
        else:
            PROGMsgsFile = PROGMachineIDLCVar.get()+"-"+getGMT(1)[:7]+"-"+ \
                    PROG_NAMELC+".msg"
        msgLn(9, "", "Working with messages file\n   %s"%(TheMsgsDir+ \
                PROGMsgsFile))
        msgLn(9, "", "-----")
        loadPROGMsgs()
    elif Answer == "erase":
        try:
# If the file already exists erase it first.
            if exists(TheMsgsDir+PROGMsgsFile):
                remove(TheMsgsDir+PROGMsgsFile)
            Fp = open(TheMsgsDir+PROGMsgsFile, "w")
            Fp.close()
            writeFile(0, "MSG", "== Contents erased "+getGMT(0)+" ==\n")
            msgLn(0, "", "", False, 0, False)
            msgLn(9, "", "Working with messages file\n   %s"%(TheMsgsDir+ \
                    PROGMsgsFile))
            msgLn(9, "", "-----")
        except Exception as e:
            msgLn(1, "M", "Error erasing file\n   %s\n   %s"% \
                    (PROGMsgsFile, e), True, 3)
            return
    ready()
    return
# END: eraseMsgs




###################
# BEGIN: floatt(In)
# LIB:floatt():2018.256
#    Handles all of the annoying shortfalls of the float() function (vs C).
#    Does not handle scientific notation numbers.
def floatt(In):
    In = str(In).strip()
    if len(In) == 0:
        return 0.0
# At least let the system give it the ol' college try.
    try:
        return float(In)
    except:
        Number = ""
        for c in In:
            if c.isdigit() or c == ".":
                Number += c
            elif (c == "-" or c == "+") and len(Number) == 0:
                Number += c
            elif c == ",":
                continue
            else:
                break
        try:
            return float(Number)
        except ValueError:
            return 0.0
# END: floatt




####################
# BEGIN: fmti(Value)
# LIB:fmti():2018.235
#   Just couldn't rely on the system to do this, although this won't handle
#   European-style numbers.
def fmti(Value):
    Value = int(Value)
    if Value > -1000 and Value < 1000:
        return str(Value)
    Value = str(Value)
    NewValue = ""
# There'll never be a + sign.
    if Value[0] == "-":
        Offset = 1
    else:
        Offset = 0
    CountDigits = 0
    for i in arange(len(Value)-1, -1+Offset, -1):
        NewValue = Value[i]+NewValue
        CountDigits += 1
        if CountDigits == 3 and i != 0:
            NewValue = ","+NewValue
            CountDigits = 0
    if Offset != 0:
        if NewValue.startswith(","):
            NewValue = NewValue[1:]
        NewValue = Value[0]+NewValue
    return NewValue
# END: fmti




#################################
# BEGIN: formABOUT(Parent = Root)
# LIB:formABOUT():2018.324
def formABOUT(Parent = Root):
# For the versions info and calling __X functions.
    if PROG_PYVERS == 2:
        import Tkinter as tkntr
    elif PROG_PYVERS == 3:
        import tkinter as tkntr
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    Message = "%s (%s)\n   "%(PROG_LONGNAME, PROG_NAME)
    Message += "Version %s\n"%PROG_VERSION
    Message += "%s\n"%"PASSCAL Instrument Center"
    Message += "%s\n\n"%"Socorro, New Mexico USA"
    Message += "%s\n"%"Email: passcal@passcal.nmt.edu"
    Message += "%s\n"%"Phone: 575-835-5070"
    Message += "\n"
    Message += "--- Configuration ---\n"
    Message += \
            "%s %dx%d\nProportional font: %s (%d)\nFixed font: %s (%d)\n"% \
            (PROGSystemName, PROGScreenWidthNow, PROGScreenHeightNow, \
            PROGPropFont["family"], PROGPropFont["size"], \
            PROGMonoFont["family"], PROGMonoFont["size"])
# Some programs don't care about the geometry of the main display.
    try:
# If the variable has something in it get the current geometry. The variable
# value may still be the initial value from when the program was started.
        if len(PROGGeometryVar.get()) != 0:
            Message += "\n"
            Message += "Geometry: %s\n"%Root.geometry()
    except:
        pass
# Some don't have setup files.
    try:
        if len(PROGSetupsFilespec) != 0:
            Message += "\n"
            Message += "--- Setups File ---\n"
            Message += "%s\n"%PROGSetupsFilespec
    except:
        pass
# This is only for QPEEK.
    try:
        if len(ChanPrefsFilespec) != 0:
            Message += "\n"
            Message += "--- Channel Preferences File ---\n"
            Message += "%s\n"%ChanPrefsFilespec
    except:
        pass
    Message += "\n"
    Message += "--- Versions ---\n"
    Message += "Python: %s\n"%PROG_PYVERSION
    Message += "Tcl/Tk: %s/%s\n"%(tkntr.TclVersion, tkntr.TkVersion)
    try:
        Message += "pySerial: %s"%SerialVERSION
    except:
        pass
# Some do, some don't have this.
    try:
        if len(PROG_LASTWORK) != 0:
            Message += "\n"
            Message += "--- Last Worked On ---\n"
            Message += "%s\n"%PROG_LASTWORK
    except:
        pass
    formMYD(Parent, (("(OK)", LEFT, "ok"), ), "ok", "WB", "About", \
            Message.strip())
    return
# END: formABOUT




####################
# BEGIN: formatCmd()
# FUNC:formatCmd():2018.289
def formatCmd():
    global WorkState
    Status, ListOfDASs = isItOKToStart(3)
    if Status == False:
        return
    WorkState = 100
    buttonControl("FDISKS", "G", 1)
    formSTATSetStatus("all", "B")
    Which = FDiskVar.get()
    if cmdPortOpen() == False:
        buttonControl("FDISKS", "", 0)
        return
    Count = 0
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        formSTATSetStatus(DAS, "C")
        AllOK = True
        Count += 1
        msgLn(1, "C", "%d. %s: Formatting disk %s... (%s)"%(Count, DAS, Which,
                getGMT(0)))
        Cmd = rt130CmdFormat(DAS, "MFD%sMF"%Which)
        cmdPortWrite(DAS, Cmd)
        if DAS == "0000":
# Just so the user thinks something is really going on.
            delay(3)
            if WorkState == 999:
                continue
        else:
            Rp = rt130GetRp(1, 5, 2)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", "   No Format command response!", True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# Now check periodically until we get a response to the status command
# indicating that the operation is done or failed. We will give up after
# 30*(the delay() time).
            Cmd = rt130CmdFormat(DAS, "MFRQMF")
            Wait = 0
            Finished = False
            while 1:
                if WorkState == 999 or AllOK == False or Finished == True:
                    break
                delay(2)
                if OPTDebugCVar.get() == 1:
                    stdout.write("A>>>>%s\n"%makePrintable(Cmd))
                    stdout.write("%s\n\n"%makeHex(Cmd))
                cmdPortWrite(DAS, Cmd)
                Rp = rt130GetRp(1, 5, 0)
                if WorkState == 999:
                    continue
                if len(Rp) == 0:
                    formSTATSetStatus(DAS, "M")
                    msgLn(1, "M", "   No Media Format command response!", \
                            True, 3)
                    AllOK = False
                    continue
                if checkRp(DAS, Rp) == False:
                    formSTATSetStatus(DAS, "M")
                    AllOK = False
                    WorkState = 999
                    continue
# FF means that it is still working.
                if Rp[14:14+2] != "FF":
# Something went wrong.
                    if Rp[14:14+2] == "01":
                        formSTATSetStatus(DAS, "R")
                        msgLn(1, "R", \
                      "   Invalid format request (there may not be a disk).", \
                                True, 3)
                        AllOK = False
                    if Rp[14:14+2] == "02":
                        formSTATSetStatus(DAS, "M")
                        msgLn(1, "M", "   DAS is busy.", True, 3)
                        AllOK = False
# 00 is done.
                    Finished = True
                    continue
# Timout counter.
                Wait += 1
                if Wait > 30:
                    formSTATSetStatus(DAS, "M")
                    msgLn(1, "M", "   No done response timeout!", True, 3)
                    AllOK = False
                    continue
# Keep the user calm.
                if Wait%10 == 0:
                    msgLn(1, "", "   Working...")
        if WorkState == 999 or AllOK == False:
            continue
        formSTATSetStatus(DAS, "G")
    cmdPortClose()
    if WorkState == 999 and AllOK == True:
        formSTATSetStatus("working", "Y")
        msgLn(1, "Y", "   Some DASs may still be working.")
    stoppedMe(WorkState, DAS)
    buttonControl("FDISKS", "", 0)
    return
# END: formatCmd




################################################################
# BEGIN: formCAL(Parent, Months = 3, Show = True, AllowX = True,
#                OrigFont = False, CloseQuit = "close")
# LIB:formCAL():2019.058
#   Displays a 1 or 3-month calendar with dates and day-of-year numbers.
#   Pass in 1 or 3 as desired.
#   Show = Should the form show itself or no? (True/False)
#   AllowX = Should the [X] button be allowed for closing? (True/False)
#   OrigFont = False allows the font to change if the program has Font BIGGER,
#              and Font smaller functionality, otherwise the font is stuck
#              with the original font in use when the program started.
#   CloseQuit = "close" shows a Close button for the form, "quit" shows a
#               Quit button and calls progQuitter(True) when pressed.
PROGFrm["CAL"] = None
CALTmModeRVar = StringVar()
CALTmModeRVar.set("lt")
CALDtModeRVar = StringVar()
CALDtModeRVar.set("dates")
PROGSetups += ["CALTmModeRVar", "CALDtModeRVar"]
CALYear = 0
CALMonth = 0
CALText1 = None
CALText2 = None
CALText3 = None
CALMonths = 3
CALTipLastValue = ""

def formCAL(Parent, Months = 3, Show = True, AllowX = True, OrigFont = False, \
        CloseQuit = "close"):
    global CALText1
    global CALText2
    global CALText3
    global CALMonths
    global CALTipLastValue
    CALTipLastValue = ""
    if showUp("CAL"):
        return
    CALMonths = Months
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    LFrm = PROGFrm["CAL"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    if AllowX == True:
        if CloseQuit == "close":
            LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "CAL"))
        elif CloseQuit == "quit":
            LFrm.protocol("WM_DELETE_WINDOW", Command(progQuitter, True))
    if CALTmModeRVar.get() == "gmt":
        LFrm.title("Calendar (GMT)")
    elif CALTmModeRVar.get() == "lt":
        GMTDiff = getGMT(20)
        LFrm.title("Calendar (LT: GMT%+.2f hours)"%(float(GMTDiff)/3600.0))
    LFrm.iconname("Cal")
    Sub = Frame(LFrm)
    if CALMonths == 3:
        if OrigFont == False:
            CALText1 = Text(Sub, bg = Clr["D"], fg = Clr["B"], height = 11, \
                    width = 29, relief = SUNKEN, \
                    cursor = Button().cget("cursor"), state = DISABLED)
        else:
            CALText1 = Text(Sub, bg = Clr["D"], fg = Clr["B"], height = 11, \
                    width = 29, relief = SUNKEN, font = PROGOrigMonoFont, \
                    cursor = Button().cget("cursor"), state = DISABLED)
        CALText1.pack(side = LEFT, padx = 7)
    if OrigFont == False:
        CALText2 = Text(Sub, bg = Clr["W"], fg = Clr["B"], height = 11, \
                width = 29, relief = SUNKEN, \
                cursor = Button().cget("cursor"), state = DISABLED)
    else:
        CALText2 = Text(Sub, bg = Clr["W"], fg = Clr["B"], height = 11, \
                width = 29, relief = SUNKEN, font = PROGOrigMonoFont, \
                cursor = Button().cget("cursor"), state = DISABLED)
    CALText2.pack(side = LEFT, padx = 7)
    if CALMonths == 3:
        if OrigFont == False:
            CALText3 = Text(Sub, bg = Clr["D"], fg = Clr["B"], height = 11, \
                    width = 29, relief = SUNKEN, \
                    cursor = Button().cget("cursor"), state = DISABLED)
        else:
            CALText3 = Text(Sub, bg = Clr["D"], fg = Clr["B"], height = 11, \
                    width = 29, relief = SUNKEN, font = PROGOrigMonoFont, \
                    cursor = Button().cget("cursor"), state = DISABLED)
        CALText3.pack(side = LEFT, padx = 7)
    Sub.pack(side = TOP, padx = 3, pady = 3)
    if CALYear != 0:
        formCALMove("c")
    else:
        formCALMove("n")
    Sub = Frame(LFrm)
    BButton(Sub, text = "<<", command = Command(formCALMove, \
            "-y")).pack(side = LEFT)
    BButton(Sub, text = "<", command = Command(formCALMove, \
            "-m")).pack(side = LEFT)
    BButton(Sub, text = "Today", command = Command(formCALMove, \
            "n")).pack(side = LEFT)
    BButton(Sub, text = ">", command = Command(formCALMove, \
            "+m")).pack(side = LEFT)
    BButton(Sub, text = ">>", command = Command(formCALMove, \
            "+y")).pack(side = LEFT)
    Sub.pack(side = TOP, padx = 3, pady = 3)
    Sub = Frame(LFrm)
    SSub = Frame(Sub)
    SSSub = Frame(SSub)
    LRb = Radiobutton(SSSub, text = "GMT", value = "gmt", \
            variable = CALTmModeRVar, command = Command(formCALMove, "c"))
    LRb.pack(side = LEFT)
    ToolTip(LRb, 35, "Use what appears to be this computer's GMT time.")
    LRb = Radiobutton(SSSub, text = "LT", value = "lt", \
            variable = CALTmModeRVar, command = Command(formCALMove, "c"))
    LRb.pack(side = LEFT)
    ToolTip(LRb, 35, \
          "Use what appears to be this computer's time and time zone setting.")
    SSSub.pack(side = TOP, anchor = "w")
    SSub.pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    SSub = Frame(Sub)
    LRb = Radiobutton(SSub, text = "Dates", value = "dates", \
            variable = CALDtModeRVar, command = Command(formCALMove, "c"))
    LRb.pack(side = TOP, anchor = "w")
    ToolTip(LRb, 35, "Show the calendar dates.")
    LRb = Radiobutton(SSub, text = "DOY", value = "doy", \
            variable = CALDtModeRVar, command = Command(formCALMove, "c"))
    LRb.pack(side = TOP, anchor = "w")
    ToolTip(LRb, 35, "Show the day-of-year numbers.")
    SSub.pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    if CloseQuit == "close":
        BButton(Sub, text = "Close", fg = Clr["R"], \
                command = Command(formClose, "CAL")).pack(side = LEFT)
    elif CloseQuit == "quit":
        BButton(Sub, text = "Quit", fg = Clr["R"], \
                command = Command(progQuitter, True)).pack(side = LEFT)
    Sub.pack(side = TOP, padx = 3, pady = 3)
    if Show == True:
        center(Parent, LFrm, "CX", "I", True)
    return
####################################
# BEGIN: formCALMove(What, e = None)
# FUNC:formCALMove():2018.235
#   Handles changing the calendar form's display.
def formCALMove(What, e = None):
    global CALYear
    global CALMonth
    global CALText1
    global CALText2
    global CALText3
    PROGFrm["CAL"].focus_set()
    DtMode = CALDtModeRVar.get()
    Year = CALYear
    Month = CALMonth
    if What == "-y":
        Year -= 1
    elif What == "-m":
        Month -= 1
    elif What == "n":
        if CALTmModeRVar.get() == "gmt":
            Year, Month, Day = getGMT(4)
        elif CALTmModeRVar.get() == "lt":
            Year, DOY, HH, MM, SS = getGMT(11)
            GMTDiff = getGMT(20)
            if GMTDiff != 0:
                Year, Month, Day, DOY, HH, MM, SS = dt2TimeMath(0, GMTDiff, \
                        Year, 0, 0, DOY, HH, MM, SS)
    elif What == "+m":
        Month += 1
    elif What == "+y":
        Year += 1
    elif What == "c":
        if CALTmModeRVar.get() == "gmt":
            PROGFrm["CAL"].title("Calendar (GMT)")
        elif CALTmModeRVar.get() == "lt":
            GMTDiff = getGMT(20)
            PROGFrm["CAL"].title("Calendar (LT: GMT%+.2f hours)"% \
                    (float(GMTDiff)/3600.0))
    if Year < 1971:
        beep(1)
        return
    elif Year > 2050:
        beep(1)
        return
    if Month > 12:
        Year += 1
        Month = 1
    elif Month < 1:
        Year -= 1
        Month = 12
    CALYear = Year
    CALMonth = Month
# Only adjust this back one month if we are showing all three months.
    if CALMonths == 3:
        Month -= 1
    if Month < 1:
        Year -= 1
        Month = 12
    for i in arange(0, 0+3):
# Skip the first and last months if we are only showing one month.
        if CALMonths == 1 and (i == 0 or i == 2):
            continue
        LTxt = eval("CALText%d"%(i+1))
        LTxt.configure(state = NORMAL)
        LTxt.delete("0.0", END)
        LTxt.tag_delete(*LTxt.tag_names())
        DOM1date = 0
        DOM1doy = PROG_FDOM[Month]
        if (Year%4 == 0 and Year%100 != 0) or Year%400 == 0:
            if Month > 2:
                DOM1doy += 1
        if i == 1:
            LTxt.insert(END, "\n")
            LTxt.insert(END, "%s"%(PROG_CALMON[Month]+" "+ \
                    str(Year)).center(29))
            LTxt.insert(END, "\n\n")
            IdxS = LTxt.index(CURRENT)
            LTxt.tag_config(IdxS, background = Clr["W"], foreground = Clr["R"])
            LTxt.insert(END, " Sun ", IdxS)
            IdxS = LTxt.index(CURRENT)
            LTxt.tag_config(IdxS, background = Clr["W"], foreground = Clr["U"])
            LTxt.insert(END, "Mon Tue Wed Thu Fri", IdxS)
            IdxS = LTxt.index(CURRENT)
            LTxt.tag_config(IdxS, background = Clr["W"], foreground = Clr["R"])
            LTxt.insert(END, " Sat", IdxS)
            LTxt.insert(END, "\n")
        else:
            LTxt.insert(END, "\n")
            LTxt.insert(END, "%s"%(PROG_CALMON[Month]+" "+ \
                    str(Year)).center(29))
            LTxt.insert(END, "\n\n")
            LTxt.insert(END, " Sun Mon Tue Wed Thu Fri Sat")
            LTxt.insert(END, "\n")
        All = monthcalendar(Year, Month)
        if CALTmModeRVar.get() == "gmt":
            NowYear, NowMonth, NowDay = getGMT(4)
        elif CALTmModeRVar.get() == "lt":
# Do this so NowDay gets set. It may not get altered below.
            NowYear, NowMonth, NowDay = getGMT(18)
            NowYear, DOY, HH, MM, SS = getGMT(11)
            GMTDiff = getGMT(20)
            NowDay = 0
            if GMTDiff != 0:
                NowYear, NowMonth, NowDay, DOY, HH, MM, SS = dt2TimeMath(0, \
                        GMTDiff, NowYear, 0, 0, DOY, HH, MM, SS)
        if DtMode == "dates":
            TargetDay = DOM1date+NowDay
        elif DtMode == "doy":
            TargetDay = DOM1doy+NowDay
        for Week in All:
            LTxt.insert(END, " ")
            for DD in Week:
                if DD != 0:
                    if DtMode == "dates":
                        ThisDay = DOM1date+DD
                        ThisDay1 = DOM1date+DD
                        ThisDay2 = DOM1doy+DD
                    elif DtMode == "doy":
                        ThisDay = DOM1doy+DD
                        ThisDay1 = DOM1doy+DD
                        ThisDay2 = DOM1date+DD
                    IdxS = LTxt.index(CURRENT)
                    if ThisDay == TargetDay and Month == NowMonth and \
                            Year == NowYear:
                        LTxt.tag_config(IdxS, background = Clr["C"], \
                                foreground = Clr["B"])
                    if DtMode == "dates":
                        LTxt.tag_bind(IdxS, "<Enter>", \
                                Command(formCALStartTip, LTxt, "%s/%03d"% \
                                        (ThisDay1, ThisDay2)))
                    elif DtMode == "doy":
                        LTxt.tag_bind(IdxS, "<Enter>", \
                                Command(formCALStartTip, LTxt, "%03d/%s"% \
                                        (ThisDay1, ThisDay2)))
                    LTxt.tag_bind(IdxS, "<Leave>", formCALHideTip)
                    LTxt.tag_bind(IdxS, "<ButtonPress>", formCALHideTip)
                    if DtMode == "dates":
                        if ThisDay < 10:
                            LTxt.insert(END, "  ")
                        else:
                            LTxt.insert(END, " ")
                        LTxt.insert(END, "%d"%ThisDay, IdxS)
                    elif DtMode == "doy":
                        LTxt.insert(END, "%03d"%ThisDay, IdxS)
                    LTxt.insert(END, " ")
                else:
                    LTxt.insert(END, "    ")
            LTxt.insert(END, "\n")
        LTxt.configure(state = DISABLED)
        Month += 1
        if Month > 12:
            Year += 1
            Month = 1
    return
#################################################
# BEGIN: formCALStartTip(Parent, Value, e = None)
# FUNC:formCALStartTip():2011.110
#   Pops up a "tool tip" when mousing over the dates of Date/DOY.
CALTip = None

def formCALStartTip(Parent, Value, e = None):
# Multiple <Entry> events can be generated just by moving the cursor around.
# If we are still in the same date number just return.
    if Value == CALTipLastValue:
        return
    formCALHideTip()
    formCALShowTip(Parent, Value)
    return
######################################
# BEGIN: formCALShowTip(Parent, Value)
# FUNC:formCALShowTip():2019.058
def formCALShowTip(Parent, Value):
    global CALTip
    global CALTipLastValue
# The tooltips were not working under Python 3 and lesser versions of Tkinter
# until I added the .update(). Then AttributeErrors started showing up. So
# just try everything and hid the tip on any error.
    try:
        CALTip = Toplevel(Parent)
        CALTip.withdraw()
        CALTip.wm_overrideredirect(1)
        LLb = Label(CALTip, text = Value, bg = Clr["Y"], bd = 1, \
                fg = Clr["B"], relief = SOLID, padx = 3, pady = 3)
        LLb.pack()
        x = Parent.winfo_pointerx()+5
        y = Parent.winfo_pointery()+5
        CALTip.wm_geometry("+%d+%d"%(x, y))
        CALTip.update()
        CALTip.deiconify()
        CALTip.lift()
    except:
        formCALHideTip()
    CALTipLastValue = Value
    return
#################################
# BEGIN: formCALHideTip(e = None)
# FUNC:formCALHideTip():2011.110
def formCALHideTip(e = None):
    global CALTip
    global CALTipLastValue
    try:
        CALTip.destroy()
    except:
        pass
    CALTip = None
    CALTipLastValue = ""
    return
# END: formCAL




####################
# BEGIN: formCNOIS()
# FUNC:formCNOIS():2016.195
PROGFrm["CNOIS"] = None
# Not all of these are used by every cal function. They are just all here to
# make the rest of the cal functions() easier.
CNOISChannelsRVar = StringVar()
CNOISChannelsRVar.set("123")
CNOISDurationVar = StringVar()
CNOISDurationVar.set(8)
CNOISAmplitudeVar = StringVar()
CNOISAmplitudeVar.set("3.00")
CNOISStepIntervalVar = StringVar()
CNOISStepIntervalVar.set("1")
CNOISStepWidthVar = StringVar()
CNOISStepWidthVar.set("1")
CNOISFrequencyRVar = StringVar()
CNOISFrequencyRVar.set("2")
CNOISWhichRVar = StringVar()
CNOISWhichRVar.set("NOIS")
PROGSetups += ["CNOISChannelsRVar", "CNOISDurationVar", "CNOISAmplitudeVar", \
        "CNOISStepIntervalVar", "CNOISStepWidthVar", "CNOISFrequencyRVar", \
        "CNOISWhichRVar"]

def formCNOIS():
    if showUp("CNOIS"):
        return
    LFrm = PROGFrm["CNOIS"] = Toplevel(Root)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "CNOIS"))
    LFrm.title("Noise Calibration")
    LFrm.iconname("NoiseCal")
    Label(LFrm, \
            text = "Not all settings will apply to all\nnoise wave calibration signal types.").pack(side = TOP)
    Sub = Frame(LFrm)
    LLab = Label(Sub, text = "Channels:")
    LLab.pack(side = LEFT)
    ToolTip(LLab, 30, "The set of channels the signals should be sent to.")
    Radiobutton(Sub, text = "123", variable = CNOISChannelsRVar, \
            value = "123").pack(side = LEFT)
    Radiobutton(Sub, text = "  456", variable = CNOISChannelsRVar, \
            value = "456").pack(side = LEFT)
    Sub.pack(side = TOP, padx = 3)
    Sub = Frame(LFrm)
    labelEntry2(Sub, 11, "Duration (secs):", 0, "", CNOISDurationVar, 5)
    labelEntry2(Sub, 11, "  Signal Amplitude (V):", 0, "", CNOISAmplitudeVar, \
            5)
    Sub.pack(side = TOP, pady = 3)
    Sub = Frame(LFrm)
    Rb = Radiobutton(Sub, text = "Noise  ", variable = CNOISWhichRVar, \
            value = "NOIS")
    Rb.pack(side = LEFT)
    ToolTip(Rb, 30, "Generate random noise using the parameters set above.")
    Rb = Radiobutton(Sub, text = "Noise SP  ", variable = CNOISWhichRVar, \
            value = "RNSP")
    Rb.pack(side = LEFT)
    ToolTip(Rb, 30, \
            "Generate random noise for 60secs, sample rate of 1000sps, using the amplitude set above.")
    Rb = Radiobutton(Sub, text = "Noise LP  ", variable = CNOISWhichRVar, \
            value = "RNLP")
    Rb.pack(side = LEFT)
    ToolTip(Rb, 30, \
            "Generate random noise for 300secs, sample rate of 200sps, using the amplitude set above.")
    Rb = Radiobutton(Sub, text = "Noise BB", variable = CNOISWhichRVar, \
            value = "RNBB")
    Rb.pack(side = LEFT)
    ToolTip(Rb, 30, \
            "Generate random noise for 3000secs, sample rate of 20sps, using the amplitude set above.")
    Sub.pack(side = TOP, padx = 3)
    Sub = Frame(LFrm)
    BButton(Sub, text = "Start", command = Command(calCmd, \
            "CNOIS")).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Close", fg = Clr["R"], \
            command = Command(formClose, "CNOIS")).pack(side = LEFT)
    Sub.pack(side = TOP, pady = 3, padx = 3)
    PROGMsg["CNOIS"] = Text(LFrm, font = PROGPropFont, height = 2, \
            width = 45, wrap = WORD)
    PROGMsg["CNOIS"].pack(side = TOP, fill = X)
    center(Root, LFrm, "CX", "I", True)
    return
# END: formCNOIS




####################
# BEGIN: formCSINE()
# FUNC:formCSINE():2016.195
PROGFrm["CSINE"] = None
# Not all of these are used by every cal function. They are just all here to
# make the rest of the cal functions() easier.
CSINEChannelsRVar = StringVar()
CSINEChannelsRVar.set("123")
CSINEDurationVar = StringVar()
CSINEDurationVar.set(8)
CSINEAmplitudeVar = StringVar()
CSINEAmplitudeVar.set("3.00")
CSINEStepIntervalVar = StringVar()
CSINEStepIntervalVar.set("1")
CSINEStepWidthVar = StringVar()
CSINEStepWidthVar.set("1")
CSINEFrequencyRVar = StringVar()
CSINEFrequencyRVar.set("2")
CSINEWhichRVar = StringVar()
CSINEWhichRVar.set("SINE")
PROGSetups += ["CSINEChannelsRVar", "CSINEDurationVar", "CSINEAmplitudeVar", \
        "CSINEStepIntervalVar", "CSINEStepWidthVar", "CSINEFrequencyRVar", \
        "CSINEWhichRVar"]

def formCSINE():
    if showUp("CSINE"):
        return
    LFrm = PROGFrm["CSINE"] = Toplevel(Root)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "CSINE"))
    LFrm.title("Sine Calibration")
    LFrm.iconname("SineCal")
    Label(LFrm, \
            text = "Not all settings will apply to all\nsine wave calibration signal types.").pack(side = TOP)
    Sub = Frame(LFrm)
    LLab = Label(Sub, text = "Channels:")
    LLab.pack(side = LEFT)
    ToolTip(LLab, 30, "The set of channels the signals should be sent to.")
    Radiobutton(Sub, text = "123", variable = CSINEChannelsRVar, \
            value = "123").pack(side = LEFT)
    Radiobutton(Sub, text = "  456", variable = CSINEChannelsRVar, \
            value = "456").pack(side = LEFT)
    Sub.pack(side = TOP, padx = 3)
    Sub = Frame(LFrm)
    labelEntry2(Sub, 11, "Duration (secs):", 0, "", CSINEDurationVar, 5)
    labelEntry2(Sub, 11, "  Signal Amplitude (V):", 0, "", CSINEAmplitudeVar, \
            5)
    Sub.pack(side = TOP, pady = 3)
    Sub = Frame(LFrm)
    Label(Sub, text = "Freq:").pack(side = LEFT)
    Radiobutton(Sub, text = "1", variable = CSINEFrequencyRVar, \
            value = "1").pack(side = LEFT)
    Radiobutton(Sub, text = "2", variable = CSINEFrequencyRVar, \
            value = "2").pack(side = LEFT)
    Radiobutton(Sub, text = "4", variable = CSINEFrequencyRVar, \
            value = "4").pack(side = LEFT)
    Radiobutton(Sub, text = "5", variable = CSINEFrequencyRVar, \
            value = "5").pack(side = LEFT)
    Radiobutton(Sub, text = "8", variable = CSINEFrequencyRVar, \
            value = "8").pack(side = LEFT)
    Radiobutton(Sub, text = "10", variable = CSINEFrequencyRVar, \
            value = "10").pack(side = LEFT)
    Radiobutton(Sub, text = "20", variable = CSINEFrequencyRVar, \
            value = "20").pack(side = LEFT)
    Radiobutton(Sub, text = "25", variable = CSINEFrequencyRVar, \
            value = "25").pack(side = LEFT)
    Radiobutton(Sub, text = "40", variable = CSINEFrequencyRVar, \
            value = "40").pack(side = LEFT)
    Radiobutton(Sub, text = "50", variable = CSINEFrequencyRVar, \
            value = "50").pack(side = LEFT)
    Radiobutton(Sub, text = "100", variable = CSINEFrequencyRVar, \
            value = "100").pack(side = LEFT)
    Sub.pack(side = TOP, pady = 3)
    Sub = Frame(LFrm)
    Rb = Radiobutton(Sub, text = "Sine  ", variable = CSINEWhichRVar, \
            value = "SINE")
    Rb.pack(side = LEFT)
    ToolTip(Rb, 30, "Generate a sine wave using the parameters above.")
    Rb = Radiobutton(Sub, text = "Sweep SP  ", variable = CSINEWhichRVar, \
            value = "SWSP")
    Rb.pack(side = LEFT)
    ToolTip(Rb, 30, \
            "Generate a sine wave for 60secs, sweep 50sec-100Hz, sample rate 1000sps, using the amplitude set above.")
    Rb = Radiobutton(Sub, text = "Sweep LP  ", variable = CSINEWhichRVar, \
            value = "SWLP")
    Rb.pack(side = LEFT)
    ToolTip(Rb, 30, \
            "Generate a sine wave for 3000secs, sweep 2500sec-2Hz, sample rate of 20sps, using the amplitude set above.")
    Rb = Radiobutton(Sub, text = "Sweep BB", variable = CSINEWhichRVar, \
            value = "SWBB")
    Rb.pack(side = LEFT)
    ToolTip(Rb, 30, \
            "Generate a sine wave for 60secs, sweep 50sec-100Hz, sample rate of 100sps, no output for 300secs, then a sine wave for 3000secs, sweep 2500sec-2Hz, sample rate of 20sps, all using the the amplitude set above.")
    Sub.pack(side = TOP, padx = 3)
    Sub = Frame(LFrm)
    BButton(Sub, text = "Start", command = Command(calCmd, \
            "CSINE")).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Close", fg = Clr["R"], \
            command = Command(formClose, "CSINE")).pack(side = LEFT)
    Sub.pack(side = TOP, pady = 3, padx = 3)
    PROGMsg["CSINE"] = Text(LFrm, font = PROGPropFont, height = 2, \
            width = 45, wrap = WORD)
    PROGMsg["CSINE"].pack(side = TOP, fill = X)
    center(Root, LFrm, "CX", "I", True)
    return
# END: formCSINE




####################
# BEGIN: formCSTEP()
# FUNC:formCSTEP():2016.195
PROGFrm["CSTEP"] = None
# Not all of these are used by every cal function. They are just all here to
# make the rest of the cal functions() easier.
CSTEPChannelsRVar = StringVar()
CSTEPChannelsRVar.set("123")
CSTEPDurationVar = StringVar()
CSTEPDurationVar.set(8)
CSTEPAmplitudeVar = StringVar()
CSTEPAmplitudeVar.set("3.00")
CSTEPStepIntervalVar = StringVar()
CSTEPStepIntervalVar.set("1")
CSTEPStepWidthVar = StringVar()
CSTEPStepWidthVar.set("1")
CSTEPFrequencyRVar = StringVar()
CSTEPFrequencyRVar.set("2")
CSTEPWhichRVar = StringVar()
CSTEPWhichRVar.set("STEP")
PROGSetups += ["CSTEPChannelsRVar", "CSTEPDurationVar", "CSTEPAmplitudeVar", \
        "CSTEPStepIntervalVar", "CSTEPStepWidthVar", "CSTEPFrequencyRVar", \
        "CSTEPWhichRVar"]

def formCSTEP():
    if showUp("CSTEP"):
        return
    LFrm = PROGFrm["CSTEP"] = Toplevel(Root)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "CSTEP"))
    LFrm.title("Step Calibration")
    LFrm.iconname("StepCal")
    Sub = Frame(LFrm)
    LLab = Label(Sub, text = "Channels:")
    LLab.pack(side = LEFT)
    ToolTip(LLab, 30, "The set of channels the signals should be sent to.")
    Radiobutton(Sub, text = "123", variable = CSTEPChannelsRVar, \
            value = "123").pack(side = LEFT)
    Radiobutton(Sub, text = "  456", variable = CSTEPChannelsRVar, \
            value = "456").pack(side = LEFT)
    Sub.pack(side = TOP, padx = 3)
    Sub = Frame(LFrm)
    labelEntry2(Sub, 11, "Duration (secs):", 0, "", CSTEPDurationVar, 5)
    labelEntry2(Sub, 11, "  Signal Amplitude (V):", 0, "", CSTEPAmplitudeVar, \
            5)
    Sub.pack(side = TOP, pady = 3)
    Sub = Frame(LFrm)
    labelEntry2(Sub, 11, "Step Interval (secs):", 0, "", \
            CSTEPStepIntervalVar, 5)
    labelEntry2(Sub, 11, "  Step Width (secs):", 0, "", CSTEPStepWidthVar, 5)
    Sub.pack(side = TOP, pady = 3)
    Sub = Frame(LFrm)
    BButton(Sub, text = "Start", command = Command(calCmd, \
            "CSTEP")).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Close", fg = Clr["R"], \
            command = Command(formClose, "CSTEP")).pack(side = LEFT)
    Sub.pack(side = TOP, pady = 3, padx = 3)
    PROGMsg["CSTEP"] = Text(LFrm, font = PROGPropFont, height = 2, \
            width = 45, wrap = WORD)
    PROGMsg["CSTEP"].pack(side = TOP, fill = X)
    center(Root, LFrm, "CX", "I", True)
    return
# END: formCSTEP




##########################################
# BEGIN: formFind(Who, WhereMsg, e = None)
# LIB:formFind():2018.236
#   Implements a "find" function in a form's Text() field.
#   The caller must set up the global Vars:
#      <Who>FindLookForVar = StringVar()
#      <Who>FindLastLookForVar = StringVar()
#      <Who>FindLinesVar = StringVar()
#      <Who>FindIndexVar = IntVar()
#      <Who>FindUseCaseCVar = IntVar()
#      <Who>FindReplaceWithVar = StringVar()
#   <Who> must be the string "INE", "CKTRD", etc.
#   Then on the form set up an Entry field and two Buttons like:
#
#   LEnt = Entry(Sub, width = 20, textvariable = <Who>FindLookForVar)
#   LEnt.pack(side = LEFT)
#   LEnt.bind("<Return>", Command(formFind, "<Who>", "<Who>"))
#   LEnt.bind("<KP_Enter>", Command(formFind, "<Who>", "<Who>"))
#   BButton(Sub, text = "Find", command = Command(formFind, "<Who>", \
#           "<Who>")).pack(side = LEFT)
#   BButton(Sub, text = "Next", command = Command(formFindNext, "<Who>", \
#           "<Who>")).pack(side = LEFT)
#   Checkbutton(Sub, text = "Use Case", \
#           variable = <Who>FindUseCaseCVar).pack(side = LEFT)
#   Entry(Sub, width = 20, \
#           textvariable = <Who>FindReplaceWithVar).pack(side = LEFT)
#   BButton(SSSub, text = "<Replace With", command = Command(formFindReplace, \
#           <Who>, <Who>)).pack(side = LEFT)
#
#   Must be defined even if it not used.
#      <Who>FindUseCaseCVar = IntVar()
#   Does not need to be defined if there is no replacing going on.
#      <Who>FindReplaceWithVar = StringVar()
#
def formFind(Who, WhereMsg, e = None):
    if WhereMsg is not None:
        setMsg(WhereMsg, "CB", "Finding...")
    LTxt = PROGTxt[Who]
    Case = eval("%sFindUseCaseCVar"%Who).get()
    if Case == 0:
        LookFor = eval("%sFindLookForVar"%Who).get().lower()
    else:
        LookFor = eval("%sFindLookForVar"%Who).get()
    LTxt.tag_delete("Find%s"%Who)
    LTxt.tag_delete("FindN%s"%Who)
    if len(LookFor) == 0:
        eval("%sFindLinesVar"%Who).set("")
        eval("%sFindIndexVar"%Who).set(-1)
        if WhereMsg is not None:
            setMsg(WhereMsg)
        return 0
    Found = 0
# Do this in case there is a lot of text from any previous find, otherwise
# the display just sits there.
    updateMe(0)
    eval("%sFindLastLookForVar"%Who).set(LookFor)
    eval("%sFindLinesVar"%Who).set("")
    eval("%sFindIndexVar"%Who).set(-1)
    FindLines = ""
    N = 1
    while 1:
        if len(LTxt.get("%d.0"%N)) == 0:
            break
        if Case == 0:
            Line = LTxt.get("%d.0"%N, "%d.0"%(N+1)).lower()
        else:
            Line = LTxt.get("%d.0"%N, "%d.0"%(N+1))
        C = 0
        try:
# Keep going through here until we run out of Line to search.
            while 1:
                Index = Line.index(LookFor, C)
                TagStart = "%d.%d"%(N, Index)
                C = Index+len(LookFor)
                TagEnd = "%d.%d"%(N, C)
                LTxt.tag_add("Find%s"%Who, TagStart, TagEnd)
                LTxt.tag_config("Find%s"%Who, background = Clr["U"], \
                        foreground = Clr["W"])
                FindLines += " %s,%s"%(TagStart, TagEnd)
                Found += 1
        except:
            pass
        N += 1
    if Found == 0:
        if WhereMsg is not None:
            setMsg(WhereMsg, "", "No matches found.")
    else:
        eval("%sFindLinesVar"%Who).set(FindLines)
        formFindNext(Who, WhereMsg, True, True)
        if WhereMsg is not None:
            setMsg(WhereMsg, "", "Matches found: %d"%Found)
    return Found
#################################################
# BEGIN: formFindReplace(Who, WhereMsg, e = None)
# FUNC:formFindReplace():2018.236
#   A Simple-minded find and replace function.
#   If WhereMsg is None then the caller is responsible for displying all
#   messages.
def formFindReplace(Who, WhereMsg, e = None):
    if WhereMsg is not None:
        setMsg(WhereMsg, "CB", "Replacing...")
    LTxt = PROGTxt[Who]
    LookFor = eval("%sFindLookForVar"%Who).get()
    ReplaceWith = eval("%sFindReplaceWithVar"%Who).get()
# These come in as straight chars ("\" + "n"). Convert them to real \n's.
    if ReplaceWith.endswith("\\n"):
        ReplaceWith = "%s\n"%ReplaceWith[:-2]
    LTxt.tag_delete("Find%s"%Who)
    LTxt.tag_delete("FindN%s"%Who)
    updateMe(0)
    FindLines = ""
    N = 1
    Found = 0
    while 1:
        if len(LTxt.get("%d.0"%N)) == 0:
            break
        Line = LTxt.get("%d.0"%N, "%d.0"%(N+1))
        if Line.find(LookFor) != -1:
            Line = Line.replace(LookFor, ReplaceWith)
            LTxt.delete("%d.0"%N, "%d.0"%(N+1))
            LTxt.insert("%d.0"%N, Line)
            Line = LTxt.get("%d.0"%N, "%d.0"%(N+1))
            TagStart = "%d.%d"%(N, Line.find(ReplaceWith))
            TagEnd = "%d.%d"%(N, Line.find(ReplaceWith)+len(ReplaceWith))
            LTxt.tag_add("Find%s"%Who, TagStart, TagEnd)
            LTxt.tag_config("Find%s"%Who, background = Clr["U"], \
                    foreground = Clr["W"])
            Found += 1
        N += 1
    if Found > 0:
        chgPROGChgBar(WhereMsg, 1)
    if WhereMsg is not None:
        setMsg(WhereMsg, "GB", "Done. Replaced: %d"%Found)
    return Found
######################################################
# BEGIN: formFindReplaceTrunc(Who, WhereMsg, e = None)
# FUNC:formFindReplaceTrunc():2018.236
#   A Simple-minded find and replace function that finds LookFor, truncates
#   that line, then appends ReplaceWith in its place.
def formFindReplaceTrunc(Who, WhereMsg, e = None):
    if WhereMsg is not None:
        setMsg(WhereMsg, "CB", "Replacing...")
    LTxt = PROGTxt[Who]
    LookFor = eval("%sFindLookForVar"%Who).get()
    ReplaceWith = eval("%sFindReplaceWithVar"%Who).get()
# These come in as straight chars ("\" + "n"). Convert them to real \n's.
    if ReplaceWith.endswith("\\n"):
        ReplaceWith = "%s\n"%ReplaceWith[:-2]
    LTxt.tag_delete("Find%s"%Who)
    LTxt.tag_delete("FindN%s"%Who)
    updateMe(0)
    FindLines = ""
    N = 1
    Found = 0
    while 1:
        if len(LTxt.get("%d.0"%N)) == 0:
            break
        Line = LTxt.get("%d.0"%N, "%d.0"%(N+1))
        if Line.find(LookFor) != -1:
            Index = Line.index(LookFor)
            Line = Line[:Index]+ReplaceWith
            LTxt.delete("%d.0"%N, "%d.0"%(N+1))
            LTxt.insert("%d.0"%N, Line)
            Line = LTxt.get("%d.0"%N, "%d.0"%(N+1))
            TagStart = "%d.%d"%(N, Line.find(ReplaceWith))
            TagEnd = "%d.%d"%(N, Line.find(ReplaceWith)+len(ReplaceWith))
            LTxt.tag_add("Find%s"%Who, TagStart, TagEnd)
            LTxt.tag_config("Find%s"%Who, background = Clr["U"], \
                    foreground = Clr["W"])
            Found += 1
        N += 1
    if Found > 0:
        chgPROGChgBar(WhereMsg, 1)
    if WhereMsg is not None:
        setMsg(WhereMsg, "GB", "Done. Replaced: %d"%Found)
    return Found
###########################################################################
# BEGIN: formFindNext(Who, WhereMsg, Find = False, First = False, e = None)
# FUNC:formFindNext():2014.069
def formFindNext(Who, WhereMsg, Find = False, First = False, e = None):
    LTxt = PROGTxt[Who]
    LTxt.tag_delete("FindN%s"%Who)
    FindLines = eval("%sFindLinesVar"%Who).get().split()
    if len(FindLines) == 0:
        beep(1)
        return
# Figure out which line we are at (at the top of the Text()) and then go
# through the found lines and find the closest one. Only do this on the first
# go of a search and not on "next" finds.
    if First == True:
        Y0, Dummy = LTxt.yview()
        AtLine = LTxt.index("@0,%d"%Y0)
# If we are at the top of the scroll then just go on normally.
        if AtLine > "1.0":
            AtLine = intt(AtLine)
            Index = -1
            Found = False
            for Line in FindLines:
                Index += 1
                Line = intt(Line)
                if Line >= AtLine:
                    Found = True
                    break
# If the current position is past the last found item just let things happen
# normally (i.e. jump to the first item found).
            if Found == True:
                eval("%sFindIndexVar"%Who).set(Index-1)
    Index = eval("%sFindIndexVar"%Who).get()
    Index += 1
    try:
        Line = FindLines[Index]
        eval("%sFindIndexVar"%Who).set(Index)
    except IndexError:
        Index = 0
        Line = FindLines[Index]
        eval("%sFindIndexVar"%Who).set(0)
# Make the "current find" red.
    TagStart, TagEnd = Line.split(",")
    LTxt.tag_add("FindN%s"%Who, TagStart, TagEnd)
    LTxt.tag_config("FindN%s"%Who, background = Clr["R"], \
            foreground = Clr["W"])
    LTxt.see(TagStart)
# If this is the first find just let the caller set a message.
    if Find == False:
        setMsg(WhereMsg, "", "Match %d of %d."%(Index+1, len(FindLines)))
    return
# END: formFind




###################################
# BEGIN: formFONTSZ(Parent, Format)
# LIB:formFONTSZ():2019.051
#   Displays the current font sizes and allows the user to enter new ones.
#   Includes the BIGGER and smaller font stepper functions.
PROGFrm["FONTSZ"] = None
FONTSZPropVar = StringVar()
FONTSZMonoVar = StringVar()
PROGSetups += ["PROGPropFontSize", "PROGMonoFontSize"]

def formFONTSZ(Parent, Format):
    if showUp("FONTSZ"):
        return
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    LFrm = PROGFrm["FONTSZ"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "CAL"))
    LFrm.title("Set Font Sizes")
    Label(LFrm, text = "The current font sizes are shown.\nEnter new values and click the Set button.\nSome systems may use positive integers, and\nsome may use negative integers. Experiment.\n(Some systems may not have some font sizes.)").pack(side = TOP)
    FONTSZPropVar.set("%s"%PROGPropFont["size"])
    FONTSZMonoVar.set("%s"%PROGMonoFont["size"])
# In case different programs have more or fewer fonts to control.
    if Format == 0:
        Sub = Frame(LFrm)
        labelEntry2(Sub, 11, "Proportional Font: ", 35, \
                "Generally for buttons and labels and dialog box messages.", \
                FONTSZPropVar, 6)
        Sub.pack(side = TOP)
        Sub = Frame(LFrm)
        labelEntry2(Sub, 11, "Mono-Spaced Font: ", 35, \
                "Generally for text display fields.", FONTSZMonoVar, 6)
        Sub.pack(side = TOP)
    Sub = Frame(LFrm)
    BButton(Sub, text = "Set", command = formFONTSZGo).pack(side = LEFT)
    BButton(Sub, text = "Close", fg = Clr["R"], command = Command(formClose, \
            "FONTSZ")).pack(side = LEFT)
    Sub.pack(side = TOP, padx = 3, pady = 3)
    center(Parent, LFrm, "C", "I", True)
    return
#######################
# BEGIN: formFONTSZGo()
# FUNC:formFONTSZGo():2019.049
def formFONTSZGo():
    NewProp = intt(FONTSZPropVar.get())
    NewMono = intt(FONTSZMonoVar.get())
    if NewProp != PROGPropFont["size"]:
        PROGPropFontSize.set(NewProp)
        fontSetSize()
    if NewMono != PROGMonoFont["size"]:
        PROGMonoFontSize.set(NewMono)
        fontSetSize()
    return
#####################
# BEGIN: fontBigger()
# FUNC:fontBigger():2012.069
def fontBigger():
    PSize = PROGPropFont["size"]
    if PSize < 0:
        PSize -= 1
    else:
        PSize += 1
    MSize = PROGMonoFont["size"]
    if MSize < 0:
        MSize -= 1
    else:
        MSize += 1
# If either font is at the limit don't change the other one or they will get
# 'out of synch' with each other (like if the original prop font size was 12
# and the mono font size was 10).
    if abs(PSize) > 16 or abs(MSize) > 16:
        beep(1)
        return
    PROGPropFontSize.set(PSize)
    PROGMonoFontSize.set(MSize)
    fontSetSize()
    return
######################
# BEGIN: fontSmaller()
# FUNC:fontSmaller():2012.067
def fontSmaller():
    PSize = PROGPropFont["size"]
    if PSize < 0:
        PSize += 1
    else:
        PSize -= 1
    MSize = PROGMonoFont["size"]
    if MSize < 0:
        MSize += 1
    else:
        MSize -= 1
    if abs(PSize) < 6 or abs(MSize) < 6:
        beep(1)
        return
    PROGPropFontSize.set(PSize)
    PROGMonoFontSize.set(MSize)
    fontSetSize()
    return
######################
# BEGIN: fontSetSize()
# FUNC:fontSetSize():2019.049
def fontSetSize():
    global PROGPropFontHeight
    PROGPropFont["size"] = PROGPropFontSize.get()
    PROGMonoFont["size"] = PROGMonoFontSize.get()
    PROGPropFontHeight = PROGPropFont.metrics("ascent")+ \
            PROGPropFont.metrics("descent")
# For any additional stuff a program may need to do.
    if "formFONTSZGoLocal" in globals():
        formFONTSZGoLocal()
    return
# END: formFONTSZ




#################
# BEGIN: HELPText
# 2019.070
#   The text of the help form.
HELPText = "QUICK START\n\
===========\n\
1. START AND SET UP CHANGEO. Start CHANGEO and if they have not been \
   previously entered or set check the following items:\n\
    1. The initial working directory may need to be set. This\n\
       should be the directory where the messages file should\n\
       be saved to. If this is required a file selection dialog\n\
       box will appear.\n\
    2. ENTER COMMAND PORT DEVICE. Fill in the designator for the\n\
       serial port device if it was not filled in when CHANGEO\n\
       started. For Windows it will be something like COM4, for\n\
       Linux something like /dev/ttyUSB0, and for macOS something\n\
       like /dev/tty.USB0 (it will depend greatly on the driver\n\
       for the serial dongle used).\n\
\n\
2. RESET THE SERIAL LINE CONTROLLER. Press the reset button on the \
serial line controller. All of the blue LEDs should light up.\n\
\n\
3. ENUMERATE. After making the connections between the DASs and the \
serial line controller start enumeration by clicking the Enumerate \
button. If you only want to work with a small number of DASs their \
ID numbers may be entered directly using the ID field below the \
Enumerate button. Type one ID number at a time into this field and \
press the Enter/Return key on the keyboard. NOTE: If you enter the \
ID numbers manually you will have to click the All Ports On button \
afterwards to make the serial line controller ready to communicate \
with all connected DASs.\n\
\n\
4. GET TO WORK. Use CHANGEO's buttons and commands to work with the \
enumerated DASs.\n\
\n\
\n\
SELECTING DASs AND THE BROADCAST MODE\n\
=====================================\n\
Many commands may be sent using the Broadcast Mode where the commands are \
directed to a DAS with the ID number 0000. All listening DASs will carry \
out commands sent in that manner. Some commands cannot be sent in that \
manner but must be directed to a specific DAS like, for example, the \
Versions and Monitor commands (CHANGEO must send the command then wait \
for a response from each DAS).\n\
\n\
If all of the DASs listed in the DAS list are selected, AND a command is \
capable of being sent using the Broadcast Mode, AND the 0000 Pref \
checkbutton is selected, then the command will be sent using the \
broadcast method.\n\
\n\
If the 0000 Pref checkbutton is not selected then CHANGEO will send all \
commands only to the DASs in the DAS list that have been selected.\n\
\n\
If a subset of the DASs listed in the DAS list have been selected all \
commands will be sent to only those DASs no matter what the state of the \
0000 Pref checkbutton.\n\
\n\
Parameters may be received from only one DAS at a time.\n\
\n\
\n\
GENERAL STUFF\n\
=============\n\
1. The first time CHANGEO is started in a user's account it may be \
necessary for the user to direct CHANGEO to an initial working directory. \
This should be the directory where the messages file should be saved.\n\
\n\
2. When started CHANGEO checks to ensure that the Python pySerial \
package has been installed on the control computer. This package must be \
installed before CHANGEO can communicate. No other special packages \
are needed, except for the pywin32 package on Windows systems.\n\
\n\
3. CHANGEO records everything written to the sessages section to the file \
\"<date>-changeo.msg\" in the current work directory. <date> will be the \
YYYYDDD date of when the messages file was created.\n\
\n\
4. Setups are saved and loaded from a file called \"changeo.set\" in the \
current work directory.\n\
\n\
5. The colors of messages are not random:\n\
\n\
    Grey - Just information or statements of fact (e.g. Done.).\n\
\n\
    White - Actions where commands are sent to the DASs that will cause \
the DASs to do something, but that will not change the state of the \
DASs, but whose information will be used in support of another \
command (e.g. Getting data stream x parameters... for the Monitor \
Test is a white message, but the message [getting] Data Stream x... \
when receiving all of the parameters is a grey message.).\n\
\n\
    Cyan - Actions where commands are sent to the DASs that will change \
the state of the DASs (e.g. Sending new values...).\n\
\n\
    Red - Something is wrong and it is probably the user's fault (e.g. No \
sample rates have been set.).\n\
\n\
    Magenta - Something is wrong and it may be the fault of the hardware \
(e.g. No Reset command response!).\n\
\n\
    Yellow - Used for warnings or to draw attention to some abnormal \
condition (e.g. Stopping...).\n\
\n\
    Green - Information requested from and returned by the DASs, or used \
just to make the text more readable.\n\
\n\
6. Some items have \"tooltips\" that can be displayed by placing the mouse \
cursor over their associated label.\n\
\n\
\n\
COMMAND LINE ARGUMENTS\n\
======================\n\
The Command Port device may be supplied on the command line.\n\
\n\
\n\
MENU COMMANDS\n\
=============\n\
FILE MENU\n\
---------\n\
----- Erase Messages -----\n\
Erases all of the messages from the messages section. A dialog box will \
appear and an option will be offered to start a new messages file or \
clear the current file and keep using it.\n\
\n\
----- Search Messages -----\n\
Brings up a form to use to search through the current messages file or \
all files in the current messages directory that end with \".msg\".\n\
\n\
----- List Current Directories -----\n\
Simply lists the various directories and where they are pointing.\n\
\n\
----- Change Messages/Work Directory -----\n\
Brings up a directory selection box and allows the user to navigate to \
a different directory and/or create a new directory.\n\
\n\
----- Change All To... -----\n\
Brings up a directory selection box and allows the user to navigate to \
a different directory and/or create a new directory and set all of the \
directory paths to that directory.\n\
\n\
----- Delete Setups File -----\n\
Sometimes a bad setups file can cause the program to misbehave. This can \
be caused by upgrading the program, but reading the setups file created \
by an earlier version of the program. This function will delete the \
current setups file (which can be found in the About box) and then quit \
the program without saving the current parameters. This will reset \
all parameters to their default values. When the program is restarted \
a dialog box should appear saying that the setups file could not be found. \
If this does not happen the Delete Setups File function may need to be \
run again.\n\
\n\
----- Quit -----\n\
Quits CHANGEO.\n\
\n\
\n\
COMMANDS MENU\n\
-------------\n\
----- Set GPS Control to NORMAL -----\n\
----- Set GPS Control to CONTINUOUS -----\n\
----- Set GPS Control to OFF -----\n\
Sends the command to the selected DASs to change the operating mode of \
the GPS to the selected menu item.\n\
\n\
----- Set DAS Time -----\n\
This gets the GMT time from the control computer and transmits it to the \
DASs. This command can be sent using the Broadcast Mode. Since there is \
a delay in getting the time and sending the time this will not be very \
accurate, but is only intended to be used to get the DAS clock(s) close \
to the correct time.\n\
\n\
\n\
PARAMETERS MENU\n\
---------------\n\
----- Save Parameters To File As... -----\n\
Saves all of the currently set up DAS parameters to the designated file \
selected or entered in the file dialog box on the control computer. The \
routine that quality checks the parameters will be called and all problems \
found with the parameters will be printed to the messages section before \
saving.\n\
\n\
----- Load Parameters From File... -----\n\
Loads the set of previously saved DAS parameters from the designated file \
on the control computer.\n\
\n\
----- Set Ch123-DS1/40sps -----\n\
----- Set Ch123-DS1/250sps -----\n\
----- Set Ch123-DS1/40sps-DS2/1sps -----\n\
----- Set Ch123-DS1/40sps,Ch456-DS2/40sps -----\n\
----- Set Ch123-DS1/250sps,Ch456-DS2/250sps -----\n\
----- Set Ch123-DS1/40sps-DS2/1sps,Ch456-DS3/40sps-DS4/1sps -----\n\
These set the DAS parameters to the specified configurations. These are \
popular configurations for testing in the lab.\n\
\n\
\n\
OPTIONS MENU\n\
------------\n\
----- Get CPU Version On Enumerate -----\n\
Selecting this item will cause CHANGEO to query each DAS for its \
firmware version as it is found during the enumeration process.\n\
\n\
----- Summary Mode -----\n\
This will reduce the amount of information sent to the messages \
section for some commands.\n\
\n\
----- Beep When Done-----\n\
This will cause CHANGEO to beep when a command is done executing. The \
actual sound made is dependent on the control computer's operating \
system.\n\
\n\
----- ddd.ddddd -----\n\
Checking this item will cause CHANGEO to convert the GPS positions \
returned with the DAS Status command from their normal deg:min.mmmm \
format to a decimal degrees format.\n\
\n\
----- Debug Mode -----\n\
Causes CHANGEO to send the command strings sent to the DAS and the \
response strings from the DAS to be printed to the window where CHANGEO \
was started from. Lines with \">>>>\" preceding them are commands and \
lines with \"<<<<\" preceeding them are responses. Obviously, CHANGEO \
must have been started from the command line (or the icon double-clicked \
on in Windows) to have a place to see this output).\n\
\n\
----- Set Font Sizes -----\n\
Brings up a form so the proportional and mono-spaced font sizes can be \
changed. Some systems use positive integers and some use negative numbers. \
The user will need to experiment. A larger number value is always a \
larger font.\n\
\n\
Most systems do not support all numerical values, so a value may be \
changed with no resulting change in size of the displayed fonts.\n\
\n\
\n\
FORMS MENU\n\
----------\n\
The currently open forms of the program will show up here. Selecting one \
will bring that form to the foreground.\n\
\n\
\n\
HELP MENU\n\
---------\n\
----- Calendar -----\n\
Just a built-in calendar that shows dates or DOY values for three months \
in a row. Of course the control computer's clock must be correct for this \
to show the right date.\n\
\n\
----- Check For Updates -----\n\
If the control computer is connected to the Internet this function will \
contact PASSCAL's website and check the version of the program against \
a list of version numbers. Appropriate dialog boxes will be shown \
depending on the result of the check.\n\
\n\
If the current version is old the Download button may be clicked to \
obtain the new version. The new version will be a zipped file/folder and \
the name of the program will be preceeded by \"new\". A dialog box \
indicating the location of the downloaded file will be shown after the \
downloading finishes. Once it has been confirmed by the user that the \
\"new\" program file is OK, it should be renamed and placed in the proper \
location which depends on the operating system of the control computer.\n\
\n\
----- Reset Commands -----\n\
If CHANGEO crashes, or if a DAS hangs and causes the program to \
hang this command will reset the program's internal flags and possibly \
allow the problem to be reproduced for troubleshooting. When nothing \
is happening the command will have no effect, but the command should \
not be selected when some other action is in progress.\n\
\n\
\n\
BUTTONS AND FIELDS\n\
==================\n\
----- Enumerate -----\n\
Causes CHANGEO to query up to 15 DASs connected to the serial line \
controller by cycling the serial port open and closed 15 times and \
sending the Identify Unit and Software (ID) command while the port is \
open. Cycling the port causes the RTS line to change state which is \
the signal to the serial line controller to shift to the next serial \
communication line (i.e. the next DAS). After the 15th query all of the \
transmit lines from the DASs are activated by the serial line controller. \
The Reset button on the box must be pressed before enumerating so that \
only one transmit line from the DASs is initially active. Failing to do \
this will cause all of the responses from the DASs to the ID command to \
be transmitted at the same time which will make them unreadable.\n\
\n\
----- 0000 Pref -----\n\
CHANGEO has two general modes for sending commands to the DASs. \
The Broadcast Mode sends commands to a DAS with an ID number 0000. \
Any DAS that is listening will recognize this and execute the sent \
command. The other mode directs the commands to a specific DAS using \
its ID number. In this mode only the DAS with that ID number will \
respond to the command. The other listening DASs will ignore the \
command. Many commands and functions may be sent in the Broadcast \
Mode. Some, such as the Versions and Monitor Test functions, cannot. \
For those and other commands like them CHANGEO must send a command \
then wait for a response from an individual DAS before proceeding. \
For the commands that do support the Broadcast Mode there is the \
0000 Pref checkbutton.\n\
\n\
If all of the DASs in the DAS list have been selected, and the 0000 \
Pref checkbutton is selected, then the commands that support the \
Broadcast Mode will be sent using that mode. If all of the DASs in \
the DAS list have been selected, and the 0000 Pref checkbutton is not \
selected, then all commands will be directed to the individual DASs \
using the ID numbers in the DAS list. If all of the DASs in the DAS \
list have not been selected then CHANGEO will send the all commands \
to the individual DASs that are selected no matter which state the \
0000 Pref checkbutton is in.\n\
\n\
When commands are sent in the Broadcast Mode no checking of any of \
the response values from the DASs is done. It can't be done since \
all of the DASs will be responding at the same time.\n\
\n\
----- Select All -----\n\
This causes all of the DASs listed in the DAS list to be selected.\n\
\n\
----- Black, Cyan, Green, Yellow, Red, and Magenta buttons -----\n\
Use these to select those DASs whose status field's match the color \
of the button.\n\
\n\
----- ID field and Clear IDs button -----\n\
An individual DAS's ID number may be entered into the ID field and \
the Return key pressed. If that DAS is not in the DAS list it will be \
added to the list if there is an empty slot. If that DAS is already \
in the list it will be removed. The Clear List button will clear all \
DASs from the DAS list.\n\
\n\
----- All Ports On -----\n\
This button will open and close the command serial port quickly 15 \
times which should cause the serial line controller to activate all \
of the transmit lines to the connected DASs.\n\
\n\
-----Command Port field -----\n\
Enter the designation for the serial hardware device that should be \
used by CHANGEO to communicate with the DASs. For Windows this will \
be something like COM3, for Linux systems it will be something like \
/dev/ttyUSB0, and for macOS it will be /dev/<something> (it will depend \
on the driver for the serial dongle connected to the USB port of the \
control computer).\n\
\n\
Right-click on the \"Command Port:\" label to bring up a file selector \
showing all of the devices in the /dev directory for Linux and macOS.\n\
\n\
----- STOP -----\n\
When enabled and red this button allows the user to interrupt a \
function. CHANGEO will decide when the time is appropriate for \
stopping. This is generally before proceeding on to the next DAS \
in a series. Between the time when the button is pressed and when the \
program reaches a stopping point the STOP button will be yellow. \
Caution: Stopping in the middle of a command may leave functions \
running on the DASs or leave the DASs in a funny state.\n\
\n\
----- Get CC Time -----\n\
Writes the current GMT time on the control computer to the messages \
section. This could be used as a time stamp in the messages to record \
when something was started or finished for later use.\n\
\n\
----- Receive Parms -----\n\
This will cause CHANGEO to request all of the programming parameters \
from a selected DAS in the DAS list and set CHANGEO's DAS parameters to \
match. Only one DAS may be selected.\n\
\n\
----- Edit Parms -----\n\
This will bring up a separate window from which all of the DAS \
programming parameters may be edited. The parameters are grouped into \
major groups such as data stream and networking parameters. Use the \
buttons on the new window to select the major group that you want to \
work with. There is no 'cancel' button. When a parameter is changed \
you cannot 'undo' the change except by changing the parameter back to \
its previous value.\n\
\n\
----- Summary -----\n\
Prints to the messages section a text summary of the current parameter \
settings. After the listing the routine that quality checks the \
parameters will be called and any problems found will be listed.\n\
\n\
----- Send Parms -----\n\
Sends the DAS parameters to the selected DASs. This function can be \
performed using the Broadcast Mode. The routine that quality checks \
the parameters will be called before the sending operation. All \
problems will need to be corrected before the parameters will be \
allowed to be sent.\n\
\n\
----- Verify Parms -----\n\
Receives the DAS parameters from each DAS and compares them with the \
parameters currently set in CHANGEO. This can be used to verify \
that a set of parameters just sent to the DASs were received OK.\n\
\n\
----- Parms To Disk -----\n\
Sends the command to the selected DASs to write the currently loaded DAS \
parameters to the CompactFlash card drive(s) installed in the units.\n\
\n\
----- Acq/RAM, Parms, GPS, Disk, Volts, -----\n\
----- DAS Status and Loop Control -----\n\
The DAS Status button will query the DASs and write retrieved status \
information to the messages section. Which of the five associated \
checkbuttons that are checked will control which information is \
requested. The Loop Control radiobuttons will tell CHANGEO to \
loop through and request the selected information every 5, 30 or 60 \
seconds until the Stop button is clicked or the \"Off\" Loop Control \
radiobutton is selected.\n\
\n\
----- \"123\", \"456\" and Centering Volts -----\n\
Requests and displays the current sensor centering voltage readings. \
Use the Mass Center test button to send centering commands to the \
sensor(s). Voltages less than +/-1.5V will be green, greater than that \
will be yellow.\n\
\n\
----- Sensor Cal (not yet available) -----\n\
Allows the user to set up and send sensor calibration commands to the \
DASs.\n\
\n\
----- Memory Test -----\n\
This function will command the selected DASs to start their internal \
RAM test. The test on each DAS will begin about 25 seconds after the \
command is sent and will finish in about 40 seconds after it starts. \
CHANGEO will stop looking for DAS responses after about 1 minute and \
20 seconds after the test is started.\n\
\n\
----- Versions/SNs -----\n\
This function queries each DAS and retrieves the software and hardware \
version information. This function cannot be used in the Broadcast \
Mode since CHANGEO must wait for each DAS to respond.\n\
\n\
----- Versions/SNs to PIS -----\n\
This will perform the same function as the Versions/SNs button described \
above, but then send the results from each DAS to the PASSCAL Inventory \
System (PIS).\n\
\n\
----- 1, 2, and Format Disk -----\n\
Sends the command to the DASs to format the selected CompactFlash \
memory card/disk. This command can be sent using the Broadcast Mode.\n\
\n\
----- Clear RAM -----\n\
Sends the command to the DASs to perform the function that clears the \
DAS RAM. This command can be sent using the Broadcast Mode.\n\
\n\
----- Dump RAM -----\n\
Sends the command to the DASs to perform the function that writes the \
contents of the RAM to the CompactFlash memory card/disk. This \
command can be sent using the Broadcast Mode.\n\
\n\
----- Reset -----\n\
Sends the command to the DASs to perform a normal reset. This command \
can be sent using the Broadcast Mode.\n\
\n\
----- Check, Set and Offset Test -----\n\
Sends a sequence of commands to the DASs to check, or check and set \
the DC offset correction values of the DASs. A set of DAS parameters \
enabling the channels to be checked must be sent to the DASs at some \
point before performing this test. This test cannot be performed using the \
Broadcast Mode since CHANGEO must wait for the offset calculation \
results from each DAS.\n\
\n\
----- 1, 2, 3, 4, 5, 6, and Monitor Test buttons -----\n\
Steps through all of the programmed data streams and channels sending \
the commands necessary to perform a monitor test on each one. The \
1,2,3,4,5,6 checkbuttons may be used to tell CHANGEO to skip the \
unselected channels. A plot will appear for each channel tested. When \
all of the channels for a data stream have been plotted an additional \
plot with all of the channels overlayed will be plotted. Following that \
an overlay plot of the first 2 seconds of data (out of 8 seconds) will \
be displayed with the vertical axis exagerated and with CHANGEO attempting \
to shift the waveforms so that the first amplitude peak of each channel's \
data is lined up with all of the other channels so differences in \
amplitude between the different channels will possibly stand out.\n\
\n\
A set of DAS parameters enabling the channels to be checked must \
be sent to the DASs at some point before performing this test. This \
test cannot be performed using the Broadcast Mode since CHANGEO \
must wait for the monitor results from each DAS. Eight seconds of \
data is collected at a sample rate of 20sps, so the DAS parameters \
sent must set the sample rate to 20sps or more.\n\
\n\
When using the function for a \"stomp test\" the stomping should \
start when the message \"Monitoring channel x...\" appears in the \
messages section.\n\
\n\
----- Start Acq and Stop Acq -----\n\
These send the commands to the DASs to start and stop acquisition. No \
check is made to determine the acquisition state before the command is \
sent. The commands can be sent using the Broadcast Mode.\n\
\n\
----- Set Gain High and Set Gain Low -----\n\
These send the commands to the DASs to immediately change the gain \
state of all of the programmed channels. Acquisition does not need to \
be stopped to use this function. A set of parameters enabling the \
channels to be checked must be sent to the DASs at some point before \
performing this test. These functions cannot be performed using the \
Broadcast Mode since each DAS must be asked which of its channels are \
active before the commands are sent.\n\
\n\
----- Sine Cal/Step Cal/Noise Cal -----\n\
These bring up the forms from which to send the sequence of commands \
needed to cause the DASs to generate the associated calibration signal. \
The parameters used are set on the forms that will appear. To stop a \
function early you can either send a different calibration function or \
Reset the DASs. These commands can be sent using the Broadcast Mode.\n\
\n\
----- 123, 456 and Mass Center -----\n\
This button activates the mass centering pulse which lasts for about 7 \
seconds. The command can be sent using the Broadcast Mode. If the \
Broadcast Mode is not used a prompt will be displayed before proceeding \
to each DAS to allow the user time to move the cables used for checking \
the signal from one DAS to the next. The 123 and 456 buttons determine \
which sensor port the Mass Center function will be performed on.\n\
\n\
\n\
PARAMETER NOTES\n\
===============\n\
NETWORKING PARAMETERS\n\
---------------------\n\
When paramters are sent to the DAS all of the current ones are first \
erased from the unit. Normally the Reftek's erase command does not \
affect the network parameters, but CHANGEO sends extra commands to \
also erase those. If the \"None\" radio button on the Network page of \
the DAS parameters dialog box is selected then no setup parameters for \
either the Ethernet, or the serial port will be sent. If either, or \
\"Both\" ports are selected then setup parameters will always be sent \
for both ports. There is no clear method in the actual parameters of \
selecting which port the user intends to use, but the general feeling \
is that the DAS will always try to communicate over a network using \
the serial port first, and the Ethernet port second. This is why the \
parameters for both will be sent even when only one port is going to \
be used. If the Ethernet port is the intended port to program the \
host address for the serial port will always be set to 0.0.0.0.\n\
\n\
\n\
USING THE RT529A FIRMWARE RECOVERY BOARD\n\
========================================\n\
This board is used to recover the firmware on a DAS. This need is \
usually indicated by the Black Screen Of Death when both LCD screen \
lines turn black when powering up and then stay that way.\n\
1. Remove small nuts and bolts and tack DAS case apart.\n\
2. Remove screws holding the circuit boards in place.\n\
3. Locate the RT506C CPU board. Move the jumper on JP2 from\n\
   pins 1 and 2 to pins 2 and 3.\n\
4. Connect the RT529A board to the stack of circuit boards.\n\
5. Connect the DAS's power port to 12VDC.\n\
6. Watch the LCD on the the DAS to determine when the firmware\n\
   reflash is finished.\n\
7. Disconnect power. Remove the RT529A board. Reset the CPU board\n\
   jumper to pins 1 and 2.\n\
8. Power up the DAS again to see thast the reflashing worked.\n\
   Truobleshoot further if necessary.\n\
9. Replace the long screws holding the circuit boards in place.\n\
   Reassemble the DAS's case.\n\
10. Install the latest firmware from a CF card if the RT529A board\n\
    has an earlier version in its EPROMs.\n\
\n\
\n\
DAS LAB BENCH TESTING CHECKLIST\n\
===============================\n\
1. Connect all DASs and check that they all have the same firmware \
version.\n\
2. Select from the Parameters menu Channels 1-3 (or 1-6), Gain 1, 40sps.\n\
3. Send parameters to DASs.\n\
4. Verify parameters.\n\
5. Run CHANGEO tests:\n\
   a. Connect shorted pigtails and run Offset Test.\n\
   b. Connect signal generator and run Monitor Test.\n\
   c. Connect oscilliscope and run Sine, Step, and Noise tests.\n\
   d. Connect signal box and run Mass Center Test on each DAS.\n\
6. Connect interface cable that connects in disk well of DAS and run \
Mem Fill Test.\n\
   a. Open Hyperterminal.\n\
   b. Enter  [.]\n\
   c. Enter   ?\n\
   d. Enter   M  M  A  C  S\n\
   e. Check to see that RAM is completely filled. Clear RAM.\n\
7. Install disks 1 and 2. Format each.\n\
8. Select parameter set 250sps, Gain 1.\n\
9. Send parameters.\n\
10. Verify parameters.\n\
11. Check that all clocks are locked or have locked.\n\
12. Start acauisition.\n\
13. Set gain high after approximately 3 minutes. Wait until all are \
shown to be set to the new gain setting on CHANGEO.\n\
14. Stop acquisition after about 5-10 minutes from when the last \
DAS's gain went high.\n\
15. Offload, convert and look at data in PQL and LOGPEEK. If OK start \
the overnight test, otherwise troubleshoot.\n\
16. Change signal input to all DASs to 1ppm.\n\
17. Select parameter set 40sps, Gain 1.\n\
18. Send parameters.\n\
19. Verify parameters.\n\
20. Clear RAM.\n\
21. Reset. Start acquisition.\n\
22. Next day, offload, convert and look at data in PQL and LOGPEEK. \
Signoff on good DASs, cables and clocks. Make entries in PIS. \
Troubleshoot any bad DASs and clocks.\n\
\n\
END\n"


##############################################
# BEGIN: formHELP(Parent, AllowWriting = True)
# LIB:formHELP():2019.042
#   Put the help contents in global variable HELPText somewhere in the
#   program.
PROGFrm["HELP"] = None
HELPFindLookForVar = StringVar()
HELPFilespecVar  = StringVar()
PROGSetups += ["HELPFindLookForVar", "HELPFilespecVar"]
HELPFindIndexVar = IntVar()
HELPFindLastLookForVar = StringVar()
HELPFindLinesVar = StringVar()
HELPFindUseCaseCVar = IntVar()
HELPHeight = 25
HELPWidth = 80
HELPFont = PROGOrigMonoFont

def formHELP(Parent, AllowWriting = True):
    if PROGFrm["HELP"] is not None:
        PROGFrm["HELP"].deiconify()
        PROGFrm["HELP"].lift()
        return
    LFrm = PROGFrm["HELP"] = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "HELP"))
    LFrm.title("Help - %s"%PROG_NAME)
    LFrm.iconname("Help")
    Sub = Frame(LFrm)
    LTxt = PROGTxt["HELP"] = Text(Sub, font = HELPFont, height = HELPHeight, \
            width = HELPWidth, wrap = WORD, relief = SUNKEN)
    LTxt.pack(side = LEFT, expand = YES, fill = BOTH)
    LSb = Scrollbar(Sub, orient = VERTICAL, command = LTxt.yview)
    LSb.pack(side = RIGHT, fill = Y)
    LTxt.configure(yscrollcommand = LSb.set)
    Sub.pack(side = TOP, expand = YES, fill = BOTH)
    Sub = Frame(LFrm)
    labelTip(Sub, "Find:=", LEFT, 30, "[Find]")
    LEnt = Entry(Sub, width = 20, textvariable = HELPFindLookForVar)
    LEnt.pack(side = LEFT)
    LEnt.bind("<Return>", Command(formFind, "HELP", "HELP"))
    LEnt.bind("<KP_Enter>", Command(formFind, "HELP", "HELP"))
    BButton(Sub, text = "Find", command = Command(formFind, \
            "HELP", "HELP")).pack(side = LEFT)
    BButton(Sub, text = "Next", command = Command(formFindNext, \
            "HELP", "HELP")).pack(side = LEFT)
    if AllowWriting:
        Label(Sub, text = " ").pack(side = LEFT)
        BButton(Sub, text = "Write To File", \
                command = Command(formHELPWrite, LTxt)).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Close", fg = Clr["R"], \
            command = Command(formClose, "HELP")).pack(side = LEFT)
    Sub.pack(side = TOP, padx = 3, pady = 3)
    PROGMsg["HELP"] = Text(LFrm, font = PROGPropFont, height = 3, wrap = WORD)
    PROGMsg["HELP"].pack(side = TOP, fill = X)
    LTxt.insert(END, HELPText)
# Clear this so the formFind() routine does the right thing if this form was
# brought up previously.
    HELPFindLinesVar.set("")
    center(Parent, LFrm, "CX", "I", True)
    if len(HELPFilespecVar.get()) == 0 or \
            exists(dirname(HELPFilespecVar.get())) == False:
# Not all programs will have all directory vars. Look for them in this order.
        try:
            HELPFilespecVar.set(PROGWorkDirVar.get()+PROG_NAMELC+"help.txt")
        except NameError:
            try:
                HELPFilespecVar.set(PROGMsgsDirVar.get()+PROG_NAMELC+ \
                        "help.txt")
            except NameError:
# The program HAS to have one of these, so don't try here. Let it crash.
                HELPFilespecVar.set(PROGDataDirVar.get()+PROG_NAMELC+ \
                        "help.txt")
    return
#####################################
# BEGIN: formHELPWrite(Who, e = None)
# FUNC:formHELPWrite():2019.042
def formHELPWrite(Who, e = None):
    Dir = dirname(HELPFilespecVar.get())
    if Dir.endswith(sep) == False:
        Dir += sep
    File = basename(HELPFilespecVar.get())
    Filespec = formMYDF("HELP", 3, "Save Help To...", Dir, File)
    if len(Filespec) == 0:
        setMsg("HELP", "", "Nothing done.")
    try:
        Fp = open(Filespec, "w")
    except Exception as e:
        setMsg("HELP", "MW", "Error opening help file\n   %s\n   %s"% \
                (Filespec, e), 3)
        return
    Fp.write("Help for %s version %s\n\n"%(PROG_NAME, PROG_VERSION))
    N = 1
    while 1:
        if len(Who.get("%d.0"%N)) == 0:
            break
# The lines from the Text field do not come with a \n after each screen line,
# so we'll have to split the lines up ourselves.
        Line = Who.get("%d.0"%N, "%d.0"%(N+1))
        N += 1
        if len(Line) < 65:
            Fp.write(Line)
            continue
        Out = ""
        for c in Line:
            if c == " " and len(Out) > 60:
                Fp.write(Out+"\n")
                Out = ""
            elif c == "\n":
                Fp.write(Out+"\n")
                Out = ""
            else:
                Out += c
    Fp.close()
    HELPFilespecVar.set(Filespec)
    setMsg("HELP", "", "Help written to\n   %s"%Filespec)
    return
# END: formHELP




############################################################################
# BEGIN: formMYD(Parent, Clicks, Close, C, Title, Msg1, Msg2 = "", Bell = 0,
#                CenterX = 0, CenterY = 0, Width = 0)
# LIB:formMYD():2019.030
#   The built-in dialog boxes cannot be associated with a particular frame, so
#   the Root/Main window kept popping to the front and covering up everything
#   on some systems when they were used, so I wrote this. I'm sure there is a
#   "classy" way of doing this, but I couldn't figure out how to return a
#   value after the window was destroyed from a classy-way.
#
#   The dialog box can contain an input field by using something like:
#
#   Answer = formMYD(Root, (("Input60", TOP, "input"), ("Write", LEFT, \
#           "input"), ("(Cancel)", LEFT, "cancel")), "cancel", "YB"\
#           "Let it be written...", \
#   "Enter a message to write to the Messages section (60 characters max.):")
#   if Answer == "cancel":
#       return
#   What = Answer.strip()
#   if len(What) == 0 or len(What) > 60:
#       Root.bell()
#   stdout.write("%s\n"%What)
#
#   The "Input60" tells the function to make an entry field 60 characters
#   long.  The "input" return value for the "Write" button tells the routine
#   to return whatever was entered into the input field. The "Cancel" button
#   will return "cancel", which, of course, could be confusing if the user
#   entered "cancel" in the input field and hit the Write button. In the case
#   above the Cancel button should probably return something like "!@#$%^&",
#   or something else that the user would never normally enter.
#
#   A "default" button may be designated by enclosing the text in ()'s.
#   Pressing the Return or keypad Enter keys will return that button's value.
#   The Cancel button is the default button in this example.
#
#   A caller must clear, or may set MYDAnswerVar to clear a previous
#   call's entry or to provide a default value for the input field before
#   calling formMYD().
#
#   To bring up a "splash dialog" for messages like "Working..." use
#
#       formMYD(Root, (), "", "", "", "Working...")
#
#   Call formMYDReturn to get rid of the dialog:
#
#       formMYDReturn("")
#
#   CenterX and CenterY can be used to position the dialog box when there is
#   no Parent if they are set to something other than 0 and 0.
#
#   Width may be set to something other than 0 to not use the default message
#   width.
#
#   Adding a length and text to the end of a button definition like below will
#   cause a ToolTip item to be created for that button.
#       ("Stop", LEFT, "stop", 35, "Click this to stop the program.")
#
#   This is REALLY kinda kludgy, but for a good cause, since there's already a
#          lot of arguments...
#      The normal font will be PROGPropFont.
#      If the Msg1 value begins with "|" then PROGMonoFont.
#      Starts with "}" will be PROGOrigPropFont.
#      Starts with "]" will be PROGOrigMonoFont.
#
#   A secret Shift-Control-Button-1 click on the main message Label (the text
#   of Msg1) will return the string "woohoo" which can be used to exit a
#   while-loop of some kind to get the dialog box to keep the program held in
#   place until someone knows the secret to get the program to continue.
#   Passing
#       (("NB", TOP, "NB"),)
#   for the buttons will make no button show up. It's really only useful when
#   using this secret code. The user won't normally know how to break out of
#   a caller's loop.
#   Shift-Control-1 can also be used to indicate some kind of 'override'
#   answer to the caller.
MYDFrame = None
MYDLabel = None
MYDAnswerVar = StringVar()

def formMYD(Parent, Clicks, Close, C, Title, Msg1, Msg2 = "", Bell = 0, \
        CenterX = 0, CenterY = 0, Width = 0):
    global MYDFrame
    global MYDLabel
# This Should Never Happen, but I think I saw it once where MYDFrame seemed to
# no longer point to a dialog that was visible. It may have just been a XQuartz
# glitch. The other alternative here is to destroy MYDFrame. We'll see how this
# works.
    if MYDFrame is not None:
        MYDFrame.deiconify()
        MYDFrame.lift()
        beep(2)
        return
    if Parent is not None:
# Allow either way of passing the parent.
        if isinstance(Parent, astring) == True:
            Parent = PROGFrm[Parent]
# Without this update() sometimes when running a program through ssh everyone
# loses track of where the parent is and the dialog box ends up at 0,0.
# It's possible for this to fail if the program is being stopped in a funny
# way (like with a ^C over an ssh connection, etc.).
        try:
            Parent.update()
        except:
            pass
    TheFont = PROGPropFont
    MonoFont = False
    if Msg1.startswith("|"):
        Msg1 = Msg1[1:]
        TheFont = PROGMonoFont
        MonoFont = True
    if Msg1.startswith("}"):
        Msg1 = Msg1[1:]
        TheFont = PROGOrigPropFont
        MonoFont = False
    if Msg1.startswith("]"):
        Msg1 = Msg1[1:]
        TheFont = PROGOrigMonoFont
        MonoFont = True
    LFrm = MYDFrame = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.resizable(0, 0)
    LFrm.protocol("WM_DELETE_WINDOW", Command(formMYDReturn, Close))
# A number shows up in the title bar if you do .title(""), and if you don't
# set the title it ends up with the program name in it.
    if len(Title) == 0:
        Title = " "
    LFrm.title(Title)
    LFrm.iconname(Title)
# Gets rid of some of the extra title bar buttons.
    LFrm.transient(Parent)
# If C is X or it ends with X then set this so we get the modality right. Clean
# up C so the rest of the function doesn't need to worry about it.
    GSG = False
    if len(C) != 0:
        if C == "X":
            GSG = True
            C = ""
        else:
            if C.endswith("X"):
                GSG = True
                C = C.replace("X", "")
            LFrm.configure(bg = Clr[C[0]])
# Break up the incoming message about every 50 characters or whatever Width is
# set to.
    if Width == 0:
        Width = 50
    if len(Msg1) > Width:
        Count = 0
        Mssg = ""
        for c in Msg1:
            if Count == 0 and c == " ":
                continue
            if Count > Width and c == " ":
                Mssg += "\n"
                Count = 0
                continue
            if c == "\n":
                Mssg += c
                Count = 0
                continue
            Count += 1
            Mssg += c
        Msg1 = Mssg
# This is an extra line that gets added to the message after a blank line.
    if len(Msg2) != 0:
        if len(Msg2) > Width:
            Count = 0
            Mssg = ""
            for c in Msg2:
                if Count == 0 and c == " ":
                    continue
                if Count > Width and c == " ":
                    Mssg += "\n"
                    Count = 0
                    continue
                if c == "\n":
                    Mssg += c
                    Count = 0
                    continue
                Count += 1
                Mssg += c
            Msg1 += "\n\n"+Mssg
        else:
            Msg1 += "\n\n"+Msg2
    Sub = Frame(LFrm)
    if len(C) == 0:
        if MonoFont == False:
            MYDLabel = Label(Sub, text = Msg1, bd = 15, font = TheFont)
        else:
            MYDLabel = Label(Sub, text = Msg1, bd = 15, font = TheFont, \
                    justify = LEFT)
        MYDLabel.pack(side = LEFT)
        Sub.pack(side = TOP)
        Sub = Frame(LFrm)
    else:
        if MonoFont == False:
            MYDLabel = Label(Sub, text = Msg1, bd = 15, bg = Clr[C[0]], \
                    fg = Clr[C[1]], font = TheFont)
        else:
            MYDLabel = Label(Sub, text = Msg1, bd = 15, bg = Clr[C[0]], \
                    fg = Clr[C[1]], font = TheFont, justify = LEFT)
        MYDLabel.pack(side = LEFT)
        Sub.pack(side = TOP)
        Sub = Frame(LFrm, bg = Clr[C[0]])
    MYDLabel.bind("<Shift-Control-Button-1>", Command(formMYDReturn, "woohoo"))
    One = False
    InputField = False
    for Click in Clicks:
        if Click[0] == "NB" and Click[2] == "NB":
            continue
        if Click[0].startswith("Input"):
            InputEnt = Entry(LFrm, textvariable = MYDAnswerVar, \
                    width = intt(Click[0][5:]))
            InputEnt.pack(side = TOP, padx = 10, pady = 10)
# So we know to do the focus_set at the end.
            InputField = True
            continue
        if One == True:
            if len(C) == 0:
                Label(Sub, text = " ").pack(side = LEFT)
            else:
                Label(Sub, text = " ", bg = Clr[C[0]]).pack(side = LEFT)
        But = BButton(Sub, text = Click[0], command = Command(formMYDReturn, \
                Click[2]))
        if Click[0].startswith("Clear") or Click[0].startswith("(Clear"):
            But.configure(fg = Clr["U"], activeforeground = Clr["U"])
        elif Click[0].startswith("Close") or Click[0].startswith("(Close"):
            But.configure(fg = Clr["R"], activeforeground = Clr["R"])
        But.pack(side = Click[1])
# Check to see if there is ToolTip text.
        try:
            ToolTip(But, Click[3], Click[4])
        except:
            pass
        if Click[0].startswith("(") and Click[0].endswith(")"):
            LFrm.bind("<Return>", Command(formMYDReturn, Click[2]))
            LFrm.bind("<KP_Enter>", Command(formMYDReturn, Click[2]))
        if Click[1] != TOP:
            One = True
    Sub.pack(side = TOP, padx = 3, pady = 3)
# CX. Always keep these fully on the display.
    center(Parent, LFrm, "CX", "I", True, CenterX, CenterY)
# If the user clicks to dismiss the window before any one of these things get
# taken care of then there will be touble. This may only happen when the user
# is running the program over a network and not on the local machine. It has
# something to do with changes made to the beep() routine which fixed a
# problem of occasional missing beeps.
    try:
        LFrm.focus_set()
        if GSG == False:
# This is not working well on macOS with the "stock" Pythons/Tkinters. We'll
# try this for a while and see how it goes. What a pain. This is why PASSCAL
# created our own Python/Tkinter package.
            if PROGSystem == "dar" and PROG_PYVERS != 2:
                pass
            else:
                LFrm.grab_set()
        else:
            if PROGSystem == "dar" and PROG_PYVERS != 2:
                pass
            else:
                LFrm.grab_set_global()
        if InputField == True:
            InputEnt.focus_set()
            InputEnt.icursor(END)
        if Bell != 0:
            beep(Bell)
# Everything will pause here until one of the buttons are pressed if there are
# any, then the box will be destroyed, but the value will still be returned.
# since it was saved in a "global".
        if len(Clicks) != 0:
            LFrm.wait_window()
    except:
        pass
# At least do this much cleaning for the caller.
    MYDAnswerVar.set(MYDAnswerVar.get().strip())
    return MYDAnswerVar.get()
######################################
# BEGIN: formMYDReturn(What, e = None)
# FUNC:formMYDReturn():2018.256
def formMYDReturn(What, e = None):
# If What is "input" just leave whatever is in the var in there.
    global MYDFrame
    if What != "input":
        MYDAnswerVar.set(What)
    MYDAnswerVar.set(MYDAnswerVar.get().strip())
# This function may get called more than once if there are errors and stuff,
# especially when the dialog box is just being used for splash messages.
    try:
        MYDFrame.destroy()
    except:
        pass
    MYDFrame = None
    updateMe(0)
    return
############################
# BEGIN: formMYDMsg(Message)
# FUNC:formMYDMsg():2008.012
def formMYDMsg(Message):
    global MYDLabel
    MYDLabel.config(text = Message)
    updateMe(0)
    return
# END: formMYD




######################################################################
# BEGIN: formMYDF(Parent, Mode, Title, StartDir, StartFile, Mssg = "",
#                EndsWith = "", SameCase = True, CompFs = True, \
#                DePrefix = "", ReturnEmpty = False)
# LIB:formMYDF():2019.030
# NEEDS: PROGMsgsDirVar, PROGDataDirVar, PROGWorkDirVar if anyone tries to
#        use the Default codes in Mode.
#   Mode = (These must be the first character in Mode)
#          0 = allow picking a file
#          1 = just allow picking directories
#          2 = allow picking/making directories only
#          3 = pick/save directories and files (the works)
#          4 = pick multiple files and change dirs
#          5 = enter/pick a file, but not change directory
#          6 = enter/pick a file, but not change directory, and don't show the
#              directory
#   Mode = A second character that specifies one of the "main" directories
#          that can be selected using a "Default" button.
#          D = Main Data Directory (PROGDataDirVar.get())
#          W = Main Work Directory (PROGWorkDirVar.get())
#          M = Main Messages Directory (PROGMsgsDirVar.get())
#   Mode = Optional last character, X, that indicates that the form should use
#          grab_set_global(). This gets detected and stripped off right away so
#          the rest of the function doesn't have to worry about it.
#   Mssg = A message that can be displayed at the top of the form. It's a
#          Label, so \n's should be put in the passed Mssg string.
#   EndsWith = The module will only display files ending with EndsWith, the
#              entered filename's case will be matched with the case of
#              EndsWith (IF it is all one case or another). EndsWith may be
#              a passed string of file extensions seperated by commas. EndsWith
#              will be added to the entered filename if it is not there before
#              returning, but only if there is one extension passed.
#   SameCase = If True then the case of the filename must match the case of
#              the EndsWith items.
#   CompFs = If True then directory and file completion with the Tab key are
#            set up.
#   DePrefix = If not "" then only the files starting with DePrefix will be
#              loaded and the DePrefix will be removed.
#   ReturnEmpty = If True it allows clearing the file name field and clicking
#                 the OK button, instead of complaining that no file name was
#                 entered.
#   Tabbing in the directory field is supported.
MYDFFrame = None
MYDFModeVar = StringVar()
MYDFDirVar = StringVar()
MYDFFiles = None
MYDFDirField = None
MYDFFileVar = StringVar()
MYDFEndsWithVar = StringVar()
MYDFAnswerVar = StringVar()
MYDFHiddenCVar = IntVar()
MYDFSameCase = True
MYDFDePrefix = ""
MYDFReturnEmpty = False

def formMYDF(Parent, Mode, Title, StartDir, StartFile, Mssg = "", \
        EndsWith = "", SameCase = True, CompFs = True, DePrefix = "", \
        ReturnEmpty = False):
    global MYDFFrame
    global MYDFFiles
    global MYDFDirField
    global MYDFSameCase
    global MYDFDePrefix
    global MYDFReturnEmpty
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
# Mode may be passed as an integer 1 or as a string "1X".
    Mode = str(Mode)
    GSG = False
    if Mode.endswith("X"):
        GSG = True
        Mode = Mode.replace("X", "")
# Everyone else looks for the str() version.
    MYDFModeVar.set(Mode)
    MYDFEndsWithVar.set(EndsWith)
    MYDFSameCase = SameCase
    MYDFDePrefix = DePrefix
    MYDFReturnEmpty = ReturnEmpty
# Without this update() sometimes when running a program through ssh everyone
# loses track of where the parent is and the dialog box ends up at 0,0.
# It's possible for this to fail if the program is being stopped in a funny
# way (like with a ^C over an ssh connection, etc.).
    try:
        Parent.update()
    except:
        pass
    LFrm = MYDFFrame = Toplevel(Parent)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", Command(formMYDFReturn, None))
    LFrm.bind("<Button-1>", Command(setMsg, "MYDF", "", ""))
    LFrm.title(Title)
    LFrm.iconname("PickDF")
    Sub = Frame(LFrm)
# The incoming Mode may be something like "1W". There are special versions of
# intt() out there that don't convert the passed item to a string first, so
# now we want the beginning number part.
    ModeI = intt(Mode)
    if len(Mssg) != 0:
        Label(Sub, text = Mssg).pack(side = TOP)
    if ModeI != 5 and ModeI != 6:
        BButton(Sub, text = "Up", command = formMYDFUp).pack(side = LEFT)
        LLb = Label(Sub, text = ":=")
        LLb.pack(side = LEFT)
        ToolTip(LLb, 35, \
                "[List Files] Press the Return key to list the files in the entered directory after editing the directory name.")
        LEnt = MYDFDirField = Entry(Sub, textvariable = MYDFDirVar, width = 65)
        LEnt.pack(side = LEFT, fill = X, expand = YES)
        if CompFs == True:
            LEnt.bind("<FocusIn>", Command(formMYDFCompFsTabOff, LFrm))
            LEnt.bind("<FocusOut>", Command(formMYDFCompFsTabOn, LFrm))
            LEnt.bind("<Key-Tab>", Command(formMYDFCompFs, MYDFDirVar, None))
        LEnt.bind("<Return>", formMYDFFillFiles)
        LEnt.bind("<KP_Enter>", formMYDFFillFiles)
# KeyPress clears the field when typing. KeyRelease clears the field after it
# has been filled in.
        LEnt.bind("<KeyPress>", formMYDFClearFiles)
        if ModeI == 2 or ModeI == 3:
            But = BButton(Sub, text = "Mkdir", command = formMYDFNew)
            But.pack(side = LEFT)
            ToolTip(But, 25, \
                    "Add the name of the new directory to the end of the current contents of the entry field and click this button to create the new directory.")
# Just show the current directory.
    elif ModeI == 5:
        MYDFDirField = Entry(Sub, textvariable = MYDFDirVar, \
                state = DISABLED)
        MYDFDirField.pack(side = LEFT, fill = X, expand = YES)
# Just don't show the current directory. Create, but don't pack.
    elif ModeI == 6:
        MYDFDirField = Entry(Sub, textvariable = MYDFDirVar)
    Sub.pack(side = TOP, fill = X)
    Sub = Frame(LFrm)
    if ModeI == 4:
        LLb = MYDFFiles = Listbox(Sub, relief = SUNKEN, bd = 2, height = 15, \
                selectmode = EXTENDED, width = 65)
    else:
        LLb = MYDFFiles = Listbox(Sub, relief = SUNKEN, bd = 2, height = 15, \
                selectmode = SINGLE, width = 65)
    LLb.pack(side = LEFT, expand = YES, fill = BOTH)
    LLb.bind("<ButtonRelease-1>", formMYDFPicked)
    Scroll = Scrollbar(Sub, command = LLb.yview)
    Scroll.pack(side = RIGHT, fill = Y)
    LLb.configure(yscrollcommand = Scroll.set)
    Sub.pack(side = TOP, expand = YES, fill = BOTH)
# The user can type in the filename.
    if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
        Sub = Frame(LFrm)
        Label(Sub, text = "Filename:=").pack(side = LEFT)
        LEnt = Entry(Sub, textvariable = MYDFFileVar)
        LEnt.pack(side = LEFT, fill = X, expand = YES)
        if CompFs == True:
            LEnt.bind("<FocusIn>", Command(formMYDFCompFsTabOff, LFrm))
            LEnt.bind("<FocusOut>", Command(formMYDFCompFsTabOn, LFrm))
            LEnt.bind("<Key-Tab>", Command(formMYDFCompFs, None, MYDFFileVar))
        LEnt.bind("<Return>", Command(formMYDFReturn, ""))
        LEnt.bind("<KP_Enter>", Command(formMYDFReturn, ""))
        Sub.pack(side = TOP, fill = X, padx = 3)
    Sub = Frame(LFrm)
# Create a Default button and tooltip if the Mode says so.
    if isinstance(Mode, astring):
        if Mode.find("D") != -1:
            LBu = BButton(Sub, text = "Default", \
                    command = Command(formMYDFDefault, "D"))
            LBu.pack(side = LEFT)
            ToolTip(LBu, 35, "Set to Main Data Directory")
            Label(Sub, text = " ").pack(side = LEFT)
        elif Mode.find("W") != -1:
            LBu = BButton(Sub, text = "Default", \
                    command = Command(formMYDFDefault, "W"))
            LBu.pack(side = LEFT)
            ToolTip(LBu, 35, "Set to Main Work Directory")
            Label(Sub, text = " ").pack(side = LEFT)
        elif Mode.find("M") != -1:
            LBu = BButton(Sub, text = "Default", \
                    command = Command(formMYDFDefault, "M"))
            LBu.pack(side = LEFT)
            ToolTip(LBu, 35, "Set to Main Messages Directory")
            Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "OK", command = Command(formMYDFReturn, \
            "")).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Cancel", \
            command = Command(formMYDFReturn, None)).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    LCb = Checkbutton(Sub, text = "Show hidden\nfiles", \
            variable = MYDFHiddenCVar, command = formMYDFFillFiles)
    LCb.pack(side = LEFT)
    ToolTip(LCb, 35, "Select this to show hidden/system files in the list.")
    Sub.pack(side = TOP, pady = 3)
    PROGMsg["MYDF"] = Text(LFrm, font = PROGPropFont, height = 1, \
            width = 1, highlightthickness = 0, insertwidth = 0, takefocus = 0)
    PROGMsg["MYDF"].pack(side = TOP, expand = YES, fill = X)
    PROGMsg["MYDF"].bind("<Button-1>", formMYDFNullCall)
    if len(StartDir) == 0:
        StartDir = sep
    if StartDir.endswith(sep) == False:
        StartDir += sep
    MYDFDirVar.set(StartDir)
    Ret = formMYDFFillFiles()
    if Ret == True and (ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6):
        MYDFFileVar.set(StartFile)
    center(Parent, LFrm, "CX", "I", True)
# Set the cursor for the user.
    if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
        LEnt.focus_set()
        LEnt.icursor(END)
    if GSG == False:
# See formMYD().
        if PROGSystem == "dar" and PROG_PYVERS != 2:
            pass
        else:
            MYDFFrame.grab_set()
    else:
        if PROGSystem == "dar" and PROG_PYVERS != 2:
            pass
        else:
            MYDFFrame.grab_set_global()
# Everything will pause here until one of the buttons are pressed, then the
# box will be destroyed, but the value will still be returned since it was
# saved in a "global".
    MYDFFrame.wait_window()
    return MYDFAnswerVar.get()
#####################################
# BEGIN: formMYDFClearFiles(e = None)
# FUNC:formMYDFClearFiles():2013.207
def formMYDFClearFiles(e = None):
    if MYDFFiles.size() > 0:
        MYDFFiles.delete(0, END)
    return
#####################################################
# BEGIN: formMYDFFillFiles(FocusSet = True, e = None)
# FUNC:formMYDFFillFiles():2018.330
#   Fills the Listbox with a list of directories and files, or with drive
#   letters if in Windows (and the directory field is just sep).
def formMYDFFillFiles(FocusSet = True, e = None):
# Some directoryies may have a lot of files in them which could make listdir()
# take a long time, so do this.
    setMsg("MYDF", "CB", "Reading...")
    ModeI = intt(MYDFModeVar.get())
# Make sure whatever is in the directory field ends, or is at least a
# separator.
    if len(MYDFDirVar.get()) == 0:
        MYDFDirVar.set(sep)
    if MYDFDirVar.get().endswith(sep) == False:
        MYDFDirVar.set(MYDFDirVar.get()+sep)
    Dir = MYDFDirVar.get()
    if PROGSystem == "dar" or PROGSystem == "lin" or PROGSystem == "sun" or \
            (PROGSystem == "win" and Dir != sep):
        try:
            Files = listdir(Dir)
        except:
            MYDFFileVar.set("")
            setMsg("MYDF", "YB", " There is no such directory.", 2)
            return False
        MYDFDirField.icursor(END)
        MYDFFiles.delete(0, END)
# This is sortedLow(), but coded in here to reduce dependencies.
        USList = {}
        for Item in Files:
            USList[Item.lower()] = Item
        SKeys = list(USList.keys())
        SKeys.sort()
        Files = []
        for Key in SKeys:
            Files.append(USList[Key])
# Always add this to the top of the list unless we are not supposed to.
        if ModeI != 5 and ModeI != 6:
            MYDFFiles.insert(END, " .."+sep)
# To show or not to show.
        ShowHidden = MYDFHiddenCVar.get()
# Do the directories first.
        for File in Files:
# The DePrefix stuff is only applied to the files (below).
            if ShowHidden == 0:
# This may need/want to be system dependent at some point (i.e. more than just
# files that start with a . or _ in different OSs).
                if File.startswith(".") or File.startswith("_"):
                    continue
            if isdir(Dir+sep+File):
                MYDFFiles.insert(END, " "+File+sep)
# Check to see if we are going to be filtering.
        EndsWith = ""
        if len(MYDFEndsWithVar.get()) != 0:
            EndsWith = MYDFEndsWithVar.get()
        EndsWithParts = EndsWith.split(",")
        Found = False
        for File in Files:
# DeFile will be used for what the user ends up seeing, and File will be used
# internally.
            DeFile = File
            if len(MYDFDePrefix) != 0:
                if File.startswith(MYDFDePrefix) == False:
                    continue
                DeFile = File[len(MYDFDePrefix):]
            if ShowHidden == 0:
                if DeFile.startswith(".") or DeFile.startswith("_"):
                    continue
            if isdir(Dir+sep+File) == False:
                if len(EndsWith) == 0:
# We only want to see the file sizes when we are looking for files.
                    if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
# Trying to follow links will trip this so just show them.
                        try:
                            MYDFFiles.insert(END, " %s  (bytes: %s)"%(DeFile, \
                                    fmti(getsize(Dir+sep+File))))
                        except OSError:
                            MYDFFiles.insert(END, " %s  (a link?)"%DeFile)
                    else:
                        MYDFFiles.insert(END, " %s"%DeFile)
                    Found += 1
                else:
                    for EndsWithPart in EndsWithParts:
                        if File.endswith(EndsWithPart):
                            if ModeI == 0 or ModeI == 3 or ModeI == 5 or \
                                    ModeI == 6:
                                try:
                                    MYDFFiles.insert(END, " %s  (bytes: %s)"% \
                                            (DeFile, fmti(getsize(Dir+sep+ \
                                            File))))
                                except OSError:
                                    MYDFFiles.insert(END, " %s  (a link?)"% \
                                            DeFile)
                            else:
                                MYDFFiles.insert(END, " %s"%DeFile)
                            Found += 1
                            break
        if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
            setMsg("MYDF", "", "%d %s found."%(Found, sP(Found, ("file", \
                    "files"))))
        else:
            setMsg("MYDF")
    elif PROGSystem == "win" and Dir == sep:
        MYDFFiles.delete(0, END)
# This loop takes a while to run.
        updateMe(0)
        Found = 0
        for Drive in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
            Drivespec = "%s:%s"%(Drive, sep)
            if exists(Drivespec):
                MYDFFiles.insert(END, Drivespec)
                Found += 1
        if ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
            setMsg("MYDF", "", "%d %s found."%(Found, sP(Found, ("drive", \
                    "drives"))))
        else:
            setMsg("MYDF")
    if FocusSet == True:
        PROGMsg["MYDF"].focus_set()
    return True
###############################
# BEGIN: formMYDFDefault(Which)
# FUNC:formMYDFDefault():2012.343
def formMYDFDefault(Which):
    if Which == "D":
        MYDFDirVar.set(PROGDataDirVar.get())
    elif Which == "W":
        MYDFDirVar.set(PROGWorkDirVar.get())
    elif Which == "M":
        MYDFDirVar.set(PROGMsgsDirVar.get())
    formMYDFFillFiles()
    return
######################
# BEGIN: formMYDFNew()
# FUNC:formMYDFNew():2018.233
def formMYDFNew():
    setMsg("MYDF")
# Make sure whatever is in the directory field ends in a separator.
    if len(MYDFDirVar.get()) == 0:
        MYDFDirVar.set(sep)
    if MYDFDirVar.get().endswith(sep) == False:
        MYDFDirVar.set(MYDFDirVar.get()+sep)
    Dir = MYDFDirVar.get()
    if exists(Dir):
        setMsg("MYDF", "YB", " Directory already exists.", 2)
        return
    try:
        makedirs(Dir)
        formMYDFFillFiles()
        setMsg("MYDF", "GB", " New directory made.", 1)
    except Exception as e:
# The system messages can be a bit cryptic and/or long, so simplifiy them here.
        if str(e).find("ermission") != -1:
            setMsg("MYDF", "RW", " Permission denied.", 2)
        else:
            setMsg("MYDF", "RW", " "+str(e), 2)
        return
    return
#################################
# BEGIN: formMYDFPicked(e = None)
# FUNC:formMYDFPicked():2018.235
def formMYDFPicked(e = None):
# This is just to get the focus off of the entry field if it was there since
# the user is now clicking on things.
    PROGMsg["MYDF"].focus_set()
    setMsg("MYDF")
    ModeI = intt(MYDFModeVar.get())
# Make sure whatever is in the directory field ends with a separator.
    if len(MYDFDirVar.get()) == 0:
        MYDFDirVar.set(sep)
    if MYDFDirVar.get().endswith(sep) == False:
        MYDFDirVar.set(MYDFDirVar.get()+sep)
    Dir = MYDFDirVar.get()
# Otherwise the selected stuff will just keep piling up.
    if ModeI == 4:
        MYDFFileVar.set("")
# There could be multiple files selected. Go through all of them and see if any
# of them will make us change directories.
    Sel = MYDFFiles.curselection()
    SelectedFiles = ""
    for Index in Sel:
# If the user is not right on the money this will sometimes trip.
        try:
            Selected = MYDFFiles.get(Index).strip()
        except TclError:
            beep(1)
            return
        if Selected == ".."+sep:
            Parts = Dir.split(sep)
# If there are only two Parts then we have hit the top of the directory tree.
            NewDir = ""
            if len(Parts) == 2 and len(Parts[1]) == 0:
                if PROGSystem == "dar" or PROGSystem == "lin" or \
                        PROGSystem == "sun":
                    NewDir = Parts[0]+sep
                elif PROGSystem == "win":
                    NewDir = sep
            else:
                for i in arange(0, len(Parts)-2):
                    NewDir += Parts[i]+sep
# Insurance.
            if len(NewDir) == 0:
                NewDir = sep
            MYDFDirVar.set(NewDir)
            formMYDFFillFiles()
            return
# We have to do a Texas Two-Step here to get everyone in the right frame of
# mind. If Dir is just sep then we have just clicked on a directory and not
# on a file, so make it look like the former.
        if PROGSystem == "win" and Dir == sep:
            Dir = Selected
            Selected = ""
        if isdir(Dir+Selected):
            MYDFDirVar.set(Dir+Selected)
            formMYDFFillFiles()
            return
        if ModeI == 1 or ModeI == 2:
            setMsg("MYDF", "YB", "That is not a directory.", 2)
            MYDFFiles.selection_clear(0, END)
            return
# Must have clicked on a file with byte size and that must be allowed.
        if Selected.find("  (") != -1:
            Selected = Selected[:Selected.index("  (")]
# Build the whole path for the multiple file mode.
        if ModeI == 4:
            if len(SelectedFiles) == 0:
                SelectedFiles = Dir+Selected
            else:
# An * should be a safe separator, right?
                SelectedFiles += "*"+Dir+Selected
        else:
# This should end up the only file.
            SelectedFiles = Selected
    MYDFFileVar.set(SelectedFiles)
    return
#############################
# BEGIN: formMYDFUp(e = None)
# FUNC:formMYDFUp():2018.235
def formMYDFUp(e = None):
# This is just to get the focus off of the entry field if it was there.
    PROGMsg["MYDF"].focus_set()
    setMsg("MYDF")
    Dir = MYDFDirVar.get()
    Parts = Dir.split(sep)
# If there are only two Parts then we have hit the top of the directory tree.
    NewDir = ""
    if len(Parts) == 2 and len(Parts[1]) == 0:
        if PROGSystem == "dar" or PROGSystem == "lin" or PROGSystem == "sun":
            NewDir = Parts[0]+sep
        elif PROGSystem == "win":
            NewDir = sep
    else:
        for i in arange(0, len(Parts)-2):
            NewDir += Parts[i]+sep
# Insurance.
    if len(NewDir) == 0:
        NewDir = sep
    MYDFDirVar.set(NewDir)
    formMYDFFillFiles()
    return
#########################################
# BEGIN: formMYDFReturn(Return, e = None)
# FUNC:formMYDFReturn():2018.236
def formMYDFReturn(Return, e = None):
# This update keeps the "OK" button from staying pushed in when there is a lot
# to do before returning.
    MYDFFrame.update()
    setMsg("MYDF")
    ModeI = intt(MYDFModeVar.get())
    if Return is None:
        MYDFAnswerVar.set("")
    elif len(Return) != 0:
        MYDFAnswerVar.set(Return)
    elif len(Return) == 0:
# The programmer is responsible for making sure this is used correctly.
        if len(MYDFDirVar.get()) == 0 and MYDFReturnEmpty == True:
            MYDFAnswerVar.set("")
        else:
            if ModeI == 1 or ModeI == 2:
                if len(MYDFDirVar.get()) == 0:
                    setMsg("MYDF", "YB", "There is no directory name.", 2)
                    return
                if MYDFDirVar.get().endswith(sep) == False:
                    MYDFDirVar.set(MYDFDirVar.get()+sep)
                MYDFAnswerVar.set(MYDFDirVar.get())
            elif ModeI == 0 or ModeI == 3 or ModeI == 5 or ModeI == 6:
# Just in case the user tries to pull a fast one. Mode 5 above is just for
# completness since the directory must be right (it can't be changed by the
# user).
                if len(MYDFDirVar.get()) == 0:
                    setMsg("MYDF", "YB", "There is no directory name.", 2)
                    return
# What was the point of coming here??
                if len(MYDFFileVar.get()) == 0:
                    setMsg("MYDF", "YB", "No filename has been entered.", 2)
                    return
# File names only at this point.
                if MYDFFileVar.get().find(sep) != -1:
                    setMsg("MYDF", "RW", \
                            "Character  %s  cannot be in the Filename."%sep, 2)
                    return
                if len(MYDFEndsWithVar.get()) != 0:
# I'm sure this may come back to haunt US someday, but make sure that the case
# of the entered filename matches the passed 'extension'.  My programs, as a
# general rule, are written to handle all upper or lowercase file names, but
# not mixed. Of course, on some operating systems it won't make any difference.
                    if MYDFSameCase == True:
                        if MYDFEndsWithVar.get().isupper():
                            if MYDFFileVar.get().isupper() == False:
                                setMsg("MYDF", "YB", \
                              "Filename needs to be all uppercase letters.", 2)
                                return
                        elif MYDFEndsWithVar.get().islower():
                            if MYDFFileVar.get().islower() == False:
                                setMsg("MYDF", "YB", \
                              "Filename needs to be all lowercase letters.", 2)
                                return
# If the user didn't put the 'extension' on the file add it so the caller
# won't have to do it, unless the caller passed multiple extensions, then don't
# do anything.
                    if MYDFEndsWithVar.get().find(",") == -1:
                        if MYDFFileVar.get().endswith( \
                                MYDFEndsWithVar.get()) == False:
                            MYDFFileVar.set(MYDFFileVar.get()+ \
                                MYDFEndsWithVar.get())
                MYDFAnswerVar.set(MYDFDirVar.get()+MYDFFileVar.get())
            elif ModeI == 4:
                MYDFAnswerVar.set(MYDFFileVar.get())
    MYDFFrame.destroy()
    updateMe(0)
    return
##################################################
# BEGIN: formMYDFCompFs(DirVar, FileVar, e = None)
# FUNC:formMYDFCompFs():2018.236
#   Attempts to complete the directory or file name in an Entry field using
#   the Tab key.
#   - If DirVar is set to a field's StringVar and FileVar is None then the
#     routine only looks for directories and leaves completion results in
#     DirVar.
#   - If FileVar is set to a field's StringVar and DirVar is None then the
#     routine tries to complete a file name using the files loaded into the
#     file listbox and leaves the results in FileVar.
def formMYDFCompFs(DirVar, FileVar, e = None):
    setMsg("MYDF")
# ---- Directories...looking to find you.
    if DirVar is not None:
        Dir = dirname(DirVar.get())
# This is a slight gotchya. If the field is empty that might mean that the user
# means "/" should be the starting point. If it is they will have to enter the
# / since if we are on Windows I'd have no idea what the default should be.
        if len(Dir) == 0:
            beep(2)
            return
        if Dir.endswith(sep) == False:
            Dir += sep
# Now get what must be a partial directory name, treat it as a file name, but
# then only allow the result of everything to be a directory.
        PartialFile = basename(DirVar.get())
        if len(PartialFile) == 0:
            beep(2)
            return
        PartialFile += "*"
        Files = listdir(Dir)
        Matched = []
        for File in Files:
            if fnmatch(File, PartialFile):
                Matched.append(File)
        if len(Matched) == 0:
            beep(2)
            return
        elif len(Matched) == 1:
            Dir = Dir+Matched[0]
# If whatever matched is not a directory then just beep and return, otherwise
# make it look like a directory and put it into the field.
            if isdir(Dir) == False:
                beep(2)
                return
            if Dir.endswith(sep) == False:
                Dir += sep
            DirVar.set(Dir)
            e.widget.icursor(END)
            formMYDFFillFiles(False)
            return
        else:
# Get the max number of characters that matched and put the partial directory
# path into the Var. If Dir+PartialDir is really the directory the user wants
# they will have to add the sep themselves since with multiple matches I won't
# know what to do. Consider DIR DIR2 DIR3 with a formMYDFMaxMatch() return of
# DIR. The directory DIR would always be selected and set as the path which
# may not be what the user wanted. Now this could cause trouble downstream
# since I'm leaving a path in the field without a trailing sep (everything
# tries to avoid doing that), so the caller will have to worry about that.
            PartialDir = formMYDFMaxMatch(Matched)
            DirVar.set(Dir+PartialDir)
            e.widget.icursor(END)
            beep(1)
    elif FileVar is not None:
        PartialFile = FileVar.get().strip()
        if len(PartialFile) == 0:
            beep(1)
            return
        Files = MYDFFiles.get(0, END)
        Index = 0
        Found = 0
        for Sel in arange(0, len(Files)):
# .strip() off the leading space.
            File = Files[Sel].strip()
            if File.startswith(PartialFile):
                Index = Sel
                Found += 1
        if Found == 0:
            beep(1)
            return
        elif Found > 1:
            MYDFFiles.see(Index)
            beep(1)
            return
        MYDFFiles.see(Index)
        File = Files[Index].strip()
# File contains the matching line from the Listbox, but it should have an
# (x bytes) message at the end. Get rid of that.
        if File.find("  (") != -1:
            File = File[:File.index("  (")]
        FileVar.set(File)
        e.widget.icursor(END)
    return
#############################################
# BEGIN: formMYDFCompFsTabOff(LFrm, e = None)
# FUNC:formMYDFCompFsTabOff():2010.225
def formMYDFCompFsTabOff(LFrm, e = None):
    LFrm.bind("<Key-Tab>", formMYDFNullCall)
    return
############################################
# BEGIN: formMYDFCompFsTabOn(LFrm, e = None)
# FUNC:formMYDFCompFsTabOn():2010.225
def formMYDFCompFsTabOn(LFrm, e = None):
    LFrm.unbind("<Key-Tab>")
    return
##################################
# BEGIN: formMYDFMaxMatch(TheList)
# FUNC:formMYDFMaxMatch():2018.310
#   Goes through the items in TheList (should be str's) and returns the string
#   that matches the start of all of the items.
#   This is the same as the library function maxMatch().
def formMYDFMaxMatch(TheList):
# This should be the only special case. What is the sound of one thing matching
# itself?
    if len(TheList) == 1:
        return TheList[0]
    Accum = ""
    CharIndex = 0
# If anything goes wrong just return whatever we've accumulated. This will end
# by no items being in TheList or one of the items running out of characters
# (the try) or by the TargetChar not matching a character from one of the
# items (the raise).
    try:
        while 1:
            TargetChar = TheList[0][CharIndex]
            for ItemIndex in arange(1, len(TheList)):
                if TargetChar != TheList[ItemIndex][CharIndex]:
                    raise Exception
            Accum += TargetChar
            CharIndex += 1
    except:
        pass
    return Accum
###################################
# BEGIN: formMYDFNullCall(e = None)
# FUNC:formMYDFNullCall():2013.037
def formMYDFNullCall(e = None):
    return "break"
# END: formMYDF




#########################
# BEGIN: formPARMS(Which)
# FUNC:formPARMS():2019.044
PROGFrm["PARMS"] = None
PARMSFormatCodes = ("16", "32", "CO", "C2")
PARMSLastSubPacked = ""
PARMSSubs = {}

def formPARMS(Which):
    global PARMSLastSubPacked
    if PROGFrm["PARMS"] is None:
        LFrm = PROGFrm["PARMS"] = Toplevel(Root)
        LFrm.withdraw()
        LFrm.title("Edit Parms")
        LFrm.protocol("WM_DELETE_WINDOW", formPARMSClose)
# Selection buttons.
        Sub = Frame(LFrm, bd = 1, relief = GROOVE)
        SSub = Frame(Sub, bd = 3)
        for i in (("Station", "SubSta"), ("Channels", "SubChan"), \
                ("Streams", "SubDS"), ("Aux Data", "SubAux"), \
                ("Auto-center", "SubACent"), ("Disks", "SubDisk"), \
                ("Network", "SubNet"), ("GPS Control", "SubGPS")):
            BButton(SSub, text = i[0], command = Command(formPARMSShowSub, \
                    i[1])).pack(side = TOP, fill = X)
        SSub.pack(side = TOP, fill = X)
        SSub = Frame(Sub, bd = 3)
        BButton(SSub, text = "Check\nParameters", \
                command = checkParmsCmd).pack(side = TOP, fill = X)
        SSub.pack(side = TOP, fill = X)
        SSub = Frame(Sub, bd = 3)
        BButton(SSub, text = "Clear All\nParameters", \
                command = formPARMSClearParmsCmd).pack(side = TOP, fill = X)
        SSub.pack(side = TOP, fill = X)
        SSub = Frame(Sub, bd = 3)
        BButton(SSub, text = "Close", \
                command = formPARMSClose).pack(side = TOP, fill = X)
        SSub.pack(side = TOP, fill = X)
        Sub.pack(side = LEFT, expand = YES, fill = BOTH)
        SubFrame = Frame(LFrm)
# Station parameters.
        PARMSSubs["Sta"] = Frame(SubFrame)
        Label(PARMSSubs["Sta"], \
                text = "STATION PARAMETERS\n").pack(side = TOP, pady = 3)
        Sub = Frame(PARMSSubs["Sta"])
        Label(Sub, text = "Experiment name:").pack(side = LEFT)
        Entry(Sub, textvariable = PExpName, width = 24).pack(side = LEFT)
        Sub.pack(side = TOP, anchor = W, padx = 5)
        Sub = Frame(PARMSSubs["Sta"])
        Label(Sub, text = "Experiment comment:").pack(side = LEFT)
        Entry(Sub, textvariable = PExpComment, width = 40).pack(side = LEFT)
        Sub.pack(side = TOP, anchor = W, padx = 5)
# Channel parameters.
        PARMSSubs["Chan"] = Frame(SubFrame)
        Label(PARMSSubs["Chan"], \
                text = "CHANNEL PARAMETERS").pack(side = TOP, pady = 3)
        for i in arange(1, 1+6):
            Sub = Frame(PARMSSubs["Chan"], bd = 2, relief = GROOVE)
            Label(Sub, text = "Channel %d"%i, bd = 3).pack(side = TOP)
            SSub = Frame(Sub)
            Label(SSub, text = "Gain:").pack(side = LEFT)
            for j in (("Not Used", 0), ("1", 1), ("32", 32)):
                Radiobutton(SSub, text = j[0], value = j[1], \
                        variable = eval("PC%dGain"%i)).pack(side = LEFT, \
                        fill = X)
            SSub.pack(side = TOP, anchor = W)
            SSub = Frame(Sub)
            Label(SSub, text = "Channel Name:").pack(side = LEFT)
            Entry(SSub, textvariable = eval("PC%dName"%i), \
                    width = 10).pack(side = LEFT)
            SSub.pack(side = TOP, anchor = W)
            SSub = Frame(Sub)
            Label(SSub, text = "Comment:").pack(side = LEFT)
            Entry(SSub, textvariable = eval("PC%dComment"%i), \
                    width = 40).pack(side = LEFT)
            SSub.pack(side = TOP, anchor = W)
            Sub.pack(side = TOP, expand = YES, fill = BOTH)
# Data stream parameters.
        PARMSSubs["DS"] = Frame(SubFrame)
        Label(PARMSSubs["DS"], \
                text = "DATA STREAM PARAMETERS").pack(side = TOP, pady = 3)
        for d in arange(1, 1+4):
            PARMSSubs["DS%d"%d] = Frame(PARMSSubs["DS"], bd = 2, \
                    relief = GROOVE)
            Sub = Frame(PARMSSubs["DS%d"%d])
            SSub = Frame(Sub)
            Label(SSub, text = "Data Stream %d Name:"%d, \
                    bd = 3).pack(side = LEFT)
            Entry(SSub, textvariable = eval("PDS%dName"%d), \
                    width = 16).pack(side = LEFT)
            SSub.pack(side = TOP)
            SSub = Frame(Sub)
            Label(SSub, text = "Trigger Type:").pack(side = LEFT)
            SSS = Frame(SSub)
            SSSS = Frame(SSS)
            for i in (("Not Used", 0), ("CON", 1), ("CRS", 2), ("EVT", 3)):
                Radiobutton(SSSS, text = i[0], value = i[1], \
                        command = Command(formPARMSShowEvent, d), \
                        variable = eval("PDS%dTType"%d)).pack(side = LEFT, \
                        fill = X)
            SSSS.pack(side = TOP)
            SSSS = Frame(SSS)
            for i in (("EXT", 4), ("LEV", 5), ("TIM", 6), ("TML", 7), \
                    ("*VOT", 8)):
                Radiobutton(SSSS, text = i[0], value = i[1], \
                        command = Command(formPARMSShowEvent, d), \
                        variable = eval("PDS%dTType"%d)).pack(side = LEFT, \
                        fill = X)
            SSSS.pack(side = TOP)
            SSS.pack(side = LEFT)
            SSub.pack(side = TOP, anchor = W)
            SSub = Frame(Sub)
            Label(SSub, text = "Data Destination:").pack(side = LEFT)
            Checkbutton(SSub, text = "Disk", \
                    variable = eval("PDS%dDisk"%d)).pack(side = LEFT)
            Checkbutton(SSub, text = "Ethernet", \
                    variable = eval("PDS%dEther"%d)).pack(side = LEFT)
            Checkbutton(SSub, text = "Serial", \
                    variable = eval("PDS%dSerial"%d)).pack(side = LEFT)
            Checkbutton(SSub, text = "RAM", \
                    variable = eval("PDS%dRam"%d)).pack(side = LEFT)
            SSub.pack(side = TOP, anchor = W)
            SSub = Frame(Sub)
            Label(SSub, text = "Included Channels:").pack(side = LEFT)
            for c in arange(1, 1+6):
                Checkbutton(SSub, text = str(c), \
                        variable = eval("PDS%dChan%d"%(d,c))).pack(side = LEFT)
            SSub.pack(side = TOP, anchor = W)
            SSub = Frame(Sub)
            Label(SSub, text = "Sample Rate (sps):").pack(side = LEFT)
            Entry(SSub, textvariable = eval("PDS%dSampRate"%d), \
                    width = 4).pack(side = LEFT)
            SSub.pack(side = TOP, anchor = W)
            SSub = Frame(Sub)
            Label(SSub, text = "Data Format:").pack(side = LEFT)
            for i in PARMSFormatCodes:
                Radiobutton(SSub, text = i, value = i, \
                        variable = eval("PDS%dFormat"%d)).pack(side = LEFT, \
                        fill = X)
            SSub.pack(side = TOP, anchor = W)
            Sub.pack(side = LEFT)
# Data stream CON trigger.
            PARMSSubs["CON%d"%d] = Frame(PARMSSubs["DS%d"%d], bd = 1, \
                    relief = GROOVE)
            Sub = Frame(PARMSSubs["CON%d"%d])
            Label(Sub, text = "Start Time\n(yyyydddhhmmss):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dStart1"%d), \
                    width = 13).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["CON%d"%d])
            Label(Sub, text = "Record Length (sec):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dRl"%d), \
                    width = 8).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
# Data stream CRS trigger.
            PARMSSubs["CRS%d"%d] = Frame(PARMSSubs["DS%d"%d], bd = 1, \
                    relief = GROOVE)
            Sub = Frame(PARMSSubs["CRS%d"%d])
            Label(Sub, text = "Trigger Stream:").pack(side = LEFT)
            for i in arange(1, 1+6):
                Radiobutton(Sub, text = "%d"%i, value = i, \
                        variable = eval("PDS%dTStrm"%d)).pack(side = LEFT, \
                        fill = X)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["CRS%d"%d])
            Label(Sub, text = "Pre-trigger Length (sec):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dPretl"%d), \
                    width = 8).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["CRS%d"%d])
            Label(Sub, text = "Record Length (sec):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dRl"%d), \
                    width = 8).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
# Data stream EVT trigger.
            PARMSSubs["EVT%d"%d] = Frame(PARMSSubs["DS%d"%d], bd = 1, \
                    relief = GROOVE)
            Sub = Frame(PARMSSubs["EVT%d"%d])
            Label(Sub, text = "Trigger Channels:").pack(side = LEFT)
            for c in arange(1, 1+6):
                Checkbutton(Sub, text = str(c), \
                       variable = eval("PDS%dTChan%d"%(d,c))).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["EVT%d"%d])
            Label(Sub, text = "Trigger Window (sec):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dTwin"%d), \
                    width = 8).pack(side = LEFT)
            Label(Sub, text = "  Minimum Channels:").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dMc"%d), \
                    width = 2).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["EVT%d"%d])
            Label(Sub, text = "Record Length (sec):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dRl"%d), \
                    width = 8).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["EVT%d"%d])
            Label(Sub, text = "Pre-trigger (sec):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dPretl"%d), \
                    width = 8).pack(side = LEFT)
            Label(Sub, text = "  Post-trigger (sec):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dPostl"%d), \
                    width = 8).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["EVT%d"%d])
            Label(Sub, text = "STA (sec):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dSta"%d), \
                    width = 8).pack(side = LEFT)
            Label(Sub, text = "  LTA (sec):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dLta"%d), \
                    width = 8).pack(side = LEFT)
            Label(Sub, text = " LTA Hold:").pack(side = LEFT)
            Radiobutton(Sub, text = "Off", value = 0, \
                    variable = eval("PDS%dHold"%d)).pack(side = LEFT, fill = X)
            Radiobutton(Sub, text = "On", value = 1, \
                    variable = eval("PDS%dHold"%d)).pack(side = LEFT, fill = X)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["EVT%d"%d])
            Label(Sub, text = "Trigger Ratio:").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dTr"%d), \
                    width = 8).pack(side = LEFT)
            Label(Sub, text = "  De-trigger Ratio:").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dDtr"%d), \
                    width = 8).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["EVT%d"%d])
            Label(Sub, text = "LP Frequency (Hz):").pack(side = LEFT)
            for i in (("Off", 0), ("0.0", 1), ("12.0", 2)):
                Radiobutton(Sub, text = i[0], value = i[1], \
                        variable = eval("PDS%dLPFreq"%d)).pack(side = LEFT, \
                        fill = X)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["EVT%d"%d])
            Label(Sub, text = "HP Frequency (Hz):").pack(side = LEFT)
            for i in (("Off", 0), ("0.0", 1), ("0.1", 2), ("2.0", 3)):
                Radiobutton(Sub, text = i[0], value = i[1], \
                        variable = eval("PDS%dHPFreq"%d)).pack(side = LEFT, \
                        fill = X)
            Sub.pack(side = TOP, anchor = W)
# Data stream EXT trigger.
            PARMSSubs["EXT%d"%d] = Frame(PARMSSubs["DS%d"%d], bd = 1, \
                    relief = GROOVE)
            Sub = Frame(PARMSSubs["EXT%d"%d])
            Label(Sub, text = "Pre-trigger Length (sec):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dPretl"%d), \
                    width = 8).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["EXT%d"%d])
            Label(Sub, text = "Record Length (sec):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dRl"%d), \
                    width = 8).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
# Data stream LEV trigger.
            PARMSSubs["LEV%d"%d] = Frame(PARMSSubs["DS%d"%d], bd = 1, \
                    relief = GROOVE)
            Sub = Frame(PARMSSubs["LEV%d"%d])
            Label(Sub, \
                    text = "Trigger Level (G/M/%/H/counts):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dTlvl"%d), \
                    width = 8).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["LEV%d"%d])
            Label(Sub, text = "LP Frequency (Hz):").pack(side = LEFT)
            for i in (("Off", 0), ("0.0", 1), ("12.0", 2)):
                Radiobutton(Sub, text = i[0], value = i[1], \
                        variable = eval("PDS%dLPFreq"%d)).pack(side = LEFT, \
                        fill = X)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["LEV%d"%d])
            Label(Sub, text = "HP Frequency (Hz):").pack(side = LEFT)
            for i in (("Off", 0), ("0.0", 1), ("0.1", 2), ("2.0", 3)):
                Radiobutton(Sub, text = i[0], value = i[1], \
                        variable = eval("PDS%dHPFreq"%d)).pack(side = LEFT, \
                        fill = X)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["LEV%d"%d])
            Label(Sub, text = "Pre-trigger Length (sec):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dPretl"%d), \
                    width = 8).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["LEV%d"%d])
            Label(Sub, text = "Record Length (sec):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dRl"%d), \
                    width = 8).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
# Data stream TIM trigger.
            PARMSSubs["TIM%d"%d] = Frame(PARMSSubs["DS%d"%d], bd = 1, \
                    relief = GROOVE)
            Sub = Frame(PARMSSubs["TIM%d"%d])
            Label(Sub, text = "Start Time\n(yyyydddhhmmss):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dStart1"%d), \
                    width = 13).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["TIM%d"%d])
            Label(Sub, text = "Repeat Interval\n(ddhhmmss):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dRepInt"%d), \
                    width = 8).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["TIM%d"%d])
            Label(Sub, text = "Number Of Triggers:").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dNumTrig"%d), \
                    width = 4).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["TIM%d"%d])
            Label(Sub, text = "Record Length (sec):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dRl"%d), \
                    width = 8).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
# Data stream TML trigger.
            PARMSSubs["TML%d"%d] = Frame(PARMSSubs["DS%d"%d], bd = 1, \
                    relief = GROOVE)
            Sub = Frame(PARMSSubs["TML%d"%d])
            Label(Sub, \
                    text = "Start Time 1 (yyyydddhhmmss):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dStart1"%d), \
                    width = 13).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
            for i in arange(2, 1+11, 2):
                Sub = Frame(PARMSSubs["TML%d"%d])
                Label(Sub, text = "Time %d:"%i).pack(side = LEFT)
                Entry(Sub, textvariable = eval("PDS%dStart%d"%(d,i)), \
                        width = 13).pack(side = LEFT)
                Label(Sub, text = "  Time %d:"%(i+1)).pack(side = LEFT)
                Entry(Sub, textvariable = eval("PDS%dStart%d"%(d,(i+1))), \
                        width = 13).pack(side = LEFT)
                Sub.pack(side = TOP, anchor = W)
            Sub = Frame(PARMSSubs["TML%d"%d])
            Label(Sub, text = "Record Length (sec):").pack(side = LEFT)
            Entry(Sub, textvariable = eval("PDS%dRl"%d), \
                    width = 8).pack(side = LEFT)
            Sub.pack(side = TOP, anchor = W)
            PARMSSubs["DS%d"%d].pack(side = TOP, anchor = W)
# Auxiliary data parameters.
        PARMSSubs["Aux"] = Frame(SubFrame)
        Label(PARMSSubs["Aux"], \
                text = "AUXILIARY DATA PARAMETERS\n" \
                ).pack(side = TOP, pady = 3)
        Sub = Frame(PARMSSubs["Aux"])
        SSub = Frame(Sub)
        Label(SSub, text = "Sample Period (sec):").pack(side = LEFT)
        Radiobutton(SSub, text = "Not Used", variable = PAuxSPer, \
                value = 0).pack(side = LEFT)
        Radiobutton(SSub, text = "1", variable = PAuxSPer, \
                value = 1).pack(side = LEFT)
        Radiobutton(SSub, text = "10", variable = PAuxSPer, \
                value = 10).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Marker (2digits):").pack(side = LEFT)
        Entry(SSub, textvariable = PAuxMarker, width = 2).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Data Destination:").pack(side = LEFT)
        Checkbutton(SSub, text = "Disk", variable = PAuxDisk).pack(side = LEFT)
        Checkbutton(SSub, text = "Ethernet", \
                variable = PAuxEther).pack(side = LEFT)
        Checkbutton(SSub, text = "Serial", \
                variable = PAuxSerial).pack(side = LEFT)
        Checkbutton(SSub, text = "RAM", variable = PAuxRam).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Included Channels:").pack(side = LEFT)
        SSSub = Frame(SSub)
        SSSSub = Frame(SSSub)
        for c in arange(1, 1+8):
            Checkbutton(SSSSub, text = str(c), \
                    variable = eval("PAuxChan%d"%c)).pack(side = LEFT)
        SSSSub.pack(side = TOP, anchor = "w")
        SSSSub = Frame(SSSub)
        for c in arange(9, 9+8):
            Checkbutton(SSSSub, text = str(c), \
                    variable = eval("PAuxChan%d"%c)).pack(side = LEFT)
        SSSSub.pack(side = TOP, anchor = "w")
        SSSub.pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Record Length (sec):").pack(side = LEFT)
        Entry(SSub, textvariable = PAuxRl, width = 8).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        Sub.pack(side = TOP, anchor = W)
# Auto-centering parameters.
        PARMSSubs["ACent"] = Frame(SubFrame)
        Label(PARMSSubs["ACent"], text = "AUTO-CENTERING PARAMETERS\n", \
                ).pack(side = TOP, pady = 3)
        Sub = Frame(PARMSSubs["ACent"])
        SSub = Frame(Sub)
        Label(SSub, text = "Channel Group:").pack(side = LEFT)
        for i in (("Not used", 0), ("123", 123), ("456", 456)):
            Radiobutton(SSub, text = i[0], value = i[1], \
                    variable = PACGroup).pack(side = LEFT, fill = X)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Enabled:").pack(side = LEFT)
        for i in (("No", 0), ("Yes", 1)):
            Radiobutton(SSub, text = i[0], value = i[1], \
                    variable = PACEnable).pack(side = LEFT, fill = X)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Cycle Time (days):").pack(side = LEFT)
        Entry(SSub, textvariable = PACCycle, width = 2).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Threshold (Volts):").pack(side = LEFT)
        Entry(SSub, textvariable = PACThresh, width = 4).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Attempts Per Cycle:").pack(side = LEFT)
        Entry(SSub, textvariable = PACAttempt, width = 2).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Retry Interval (min):").pack(side = LEFT)
        Entry(SSub, textvariable = PACRetry, width = 2).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Sample Period (sec):").pack(side = LEFT)
        for i in (("10", 10), ("100", 100)):
            Radiobutton(SSub, text = i[0], value = i[1], \
                    variable = PACPeriod).pack(side = LEFT, fill = X)
        SSub.pack(side = TOP, anchor = W)
        Sub.pack(side = TOP, anchor = W)
# Disk parameters.
        PARMSSubs["Disk"] = Frame(SubFrame)
        Label(PARMSSubs["Disk"], text = "DISK PARAMETERS\n").pack(side = TOP, \
                pady = 3)
        Sub = Frame(PARMSSubs["Disk"])
        SSub = Frame(Sub)
        Label(SSub, text = "Disk wrap:").pack(side = LEFT)
        for i in (("Disabled", 0), ("Enabled", 1)):
            Radiobutton(SSub, text = i[0], value = i[1], \
                    variable = PDWrap).pack(side = LEFT, fill = X)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Disk Dump Threshold (%RAM):").pack(side = LEFT)
        Entry(SSub, textvariable = PDThresh, width = 2).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Dump On\nEvent Trailer?:").pack(side = LEFT)
        for i in (("No", 0), ("Yes", 1)):
            Radiobutton(SSub, text = i[0], value = i[1], \
                    variable = PDETDump).pack(side = LEFT, fill = X)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Disk Retry (days):").pack(side = LEFT)
        Entry(SSub, textvariable = PDRetry, width = 1).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        Sub.pack(side = TOP, anchor = W)
# Network parameters.
        PARMSSubs["Net"] = Frame(SubFrame)
        Label(PARMSSubs["Net"], text = "NETWORK PARAMETERS").pack(side = TOP, \
                pady = 3)
        Sub = Frame(PARMSSubs["Net"])
        Label(Sub, text = "Intend To Use\nWhich Port?:").pack(side = LEFT)
        for i in (("None", 0), ("Ethernet", 1), ("SerialPPP", 2), ("Both", 3)):
            Radiobutton(Sub, text = i[0], value = i[1], \
                    variable = PNPort).pack(side = LEFT)
        Sub.pack(side = TOP)
        Sub = Frame(PARMSSubs["Net"], bd = 2, relief = GROOVE)
        SSub = Frame(Sub)
        Label(SSub, text = "Ethernet Port").pack(side = LEFT)
        SSub.pack(side = TOP)
        SSub = Frame(Sub)
        Label(SSub, text = "Port Power:").pack(side = LEFT)
        for i in (("Auto", 2), ("Continuous", 1)):
            Radiobutton(SSub, text = i[0], value = i[1], \
                    variable = PNEPortPow).pack(side = LEFT, fill = X)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Line Down Action:").pack(side = LEFT)
        for i in (("Keep data", 1), ("Toss data", 2)):
            Radiobutton(SSub, text = i[0], value = i[1], \
                    variable = PNEKeep).pack(side = LEFT, fill = X)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Toss Delay (min):").pack(side = LEFT)
        Entry(SSub, textvariable = PNETDelay, width = 2).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "IP Address\n(xxx.xxx.xxx.xxx):").pack(side = LEFT)
        Entry(SSub, textvariable = PNEIP1, width = 3).pack(side = LEFT)
        Label(SSub, text = ".").pack(side = LEFT)
        Entry(SSub, textvariable = PNEIP2, width = 3).pack(side = LEFT)
        Label(SSub, text = ".").pack(side = LEFT)
        Entry(SSub, textvariable = PNEIP3, width = 3).pack(side = LEFT)
        Label(SSub, text = ".").pack(side = LEFT)
        Entry(SSub, textvariable = PNEIP4, width = 3).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "IP Fill:").pack(side = LEFT)
        for i in (("Use ID", 1), ("Sequential", 2), ("Use As Entered", 3)):
            Radiobutton(SSub, text = i[0], value = i[1], \
                    variable = PNEFill).pack(side = LEFT, fill = X)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Net Mask:").pack(side = LEFT)
        Entry(SSub, textvariable = PNEMask, width = 15).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Gateway Address:").pack(side = LEFT)
        Entry(SSub, textvariable = PNEGateway, width = 15).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Remote Host Address:").pack(side = LEFT)
        Entry(SSub, textvariable = PNEHost, width = 15).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        Sub.pack(side = TOP, anchor = W, expand = YES, fill = X)
# Ethernet part.
        Sub = Frame(PARMSSubs["Net"], bd = 2, relief = GROOVE)
        SSub = Frame(Sub)
        Label(SSub, text = "SerialPPP Port").pack(side = LEFT)
        SSub.pack(side = TOP)
        SSub = Frame(Sub)
        Label(SSub, text = "Line Down Action:").pack(side = LEFT)
        for i in (("Keep Data", 1), ("Toss Data", 2)):
            Radiobutton(SSub, text = i[0], value = i[0], \
                    variable = PNSKeep).pack(side = LEFT, fill = X)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Toss Delay (min):").pack(side = LEFT)
        Entry(SSub, textvariable = PNSTDelay, width = 2).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Mode:").pack(side = LEFT)
        for i in (("Direct", 1), ("AT/Modem", 2), ("Freewave", 3)):
            Radiobutton(SSub, text = i[0], value = i[1], \
                    variable = PNSMode).pack(side = LEFT, fill = X)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Speed:").pack(side = LEFT)
        for i in (("9600", 9600), ("19200", 19200), ("57600", 57600), \
                ("115200", 115200)):
            Radiobutton(SSub, text = i[0], value = i[1], \
                    variable = PNSSpeed).pack(side = LEFT, fill = X)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "IP Address\n(xxx.xxx.xxx.xxx):").pack(side = LEFT)
        Entry(SSub, textvariable = PNSIP1, width = 3).pack(side = LEFT)
        Label(SSub, text = ".").pack(side = LEFT)
        Entry(SSub, textvariable = PNSIP2, width = 3).pack(side = LEFT)
        Label(SSub, text = ".").pack(side = LEFT)
        Entry(SSub, textvariable = PNSIP3, width = 3).pack(side = LEFT)
        Label(SSub, text = ".").pack(side = LEFT)
        Entry(SSub, textvariable = PNSIP4, width = 3).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "IP Fill:").pack(side = LEFT)
        for i in (("Use ID", 1), ("Sequential", 2), ("Use As Entered", 3)):
            Radiobutton(SSub, text = i[0], value = i[1], \
                    variable = PNSFill).pack(side = LEFT, fill = X)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Net Mask:").pack(side = LEFT)
        Entry(SSub, textvariable = PNSMask, width = 15).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Gateway Address:").pack(side = LEFT)
        Entry(SSub, textvariable = PNSGateway, width = 15).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        SSub = Frame(Sub)
        Label(SSub, text = "Remote Host Address:").pack(side = LEFT)
        Entry(SSub, textvariable = PNSHost, width = 15).pack(side = LEFT)
        SSub.pack(side = TOP, anchor = W)
        Sub.pack(side = TOP, anchor = W, expand = YES, fill = X)
        SubFrame.pack(side = LEFT, expand = YES, fill = BOTH)
# GPS control parameters.
        PARMSSubs["GPS"] = Frame(SubFrame)
        Label(PARMSSubs["GPS"], text = "GPS CONTROL PARAMETERS\n" \
                ).pack(side = TOP, padx = 20, pady = 3)
        Radiobutton(PARMSSubs["GPS"], text = "Normal Duty Cycle", \
                variable = PGPSMode, value = "D").pack(side = TOP)
        Radiobutton(PARMSSubs["GPS"], text = "Continuous", \
                variable = PGPSMode, value = "C").pack(side = TOP)
        Radiobutton(PARMSSubs["GPS"], text = "Off", variable = PGPSMode, \
                value = "O").pack(side = TOP)
        center(Root, LFrm, "NW", "O", True)
    else:
        LFrm = PROGFrm["PARMS"]
# Pick a panel to show.
    if Which != PARMSLastSubPacked:
        PARMSSubs["Sta"].pack_forget()
        PARMSSubs["Chan"].pack_forget()
        PARMSSubs["DS"].pack_forget()
        PARMSSubs["Aux"].pack_forget()
        PARMSSubs["ACent"].pack_forget()
        PARMSSubs["Disk"].pack_forget()
        PARMSSubs["Net"].pack_forget()
        PARMSSubs["GPS"].pack_forget()
        if Which == "SubSta":
            PARMSSubs["Sta"].pack(side = TOP, expand = YES, fill = BOTH)
        elif Which == "SubChan":
            PARMSSubs["Chan"].pack(side = TOP, expand = YES, fill = BOTH)
        elif Which == "SubDS":
            PARMSSubs["DS"].pack(side = TOP, expand = YES, fill = BOTH)
            formPARMSShowEvents()
        elif Which == "SubAux":
            PARMSSubs["Aux"].pack(side = TOP, expand = YES, fill = BOTH)
        elif Which == "SubACent":
            PARMSSubs["ACent"].pack(side = TOP, expand = YES, fill = BOTH)
        elif Which == "SubDisk":
            PARMSSubs["Disk"].pack(side = TOP, expand = YES, fill = BOTH)
        elif Which == "SubNet":
            PARMSSubs["Net"].pack(side = TOP, expand = YES, fill = BOTH)
        elif Which == "SubGPS":
            PARMSSubs["GPS"].pack(side = TOP, expand = YES, fill = BOTH)
        PARMSLastSubPacked = Which
    LFrm.deiconify()
    LFrm.lift()
    updateMe(0)
    return
################################
# BEGIN: formPARMSShowSub(Which)
# FUNC:formPARMSShowSub():2009.327
def formPARMSShowSub(Which):
    formPARMS(Which)
    return
############################
# BEGIN: formPARMSShowEdit()
# FUNC:formPARMSShowEdit():2018.289
#   Show commands for parameter frames.
def formPARMSShowEdit():
    if PROGFrm["PARMS"] is None:
        formPARMS("SubSta")
    else:
       PROGFrm["PARMS"].deiconify()
       PROGFrm["PARMS"].lift()
    return
##############################
# BEGIN: formPARMSShowEvents()
# FUNC:formPARMSShowEvents():2018.289
#   Just calls formPARMSShowEvent() if it is safe.
def formPARMSShowEvents():
    if PROGFrm["PARMS"] is not None:
        formPARMSShowEvent(1)
        formPARMSShowEvent(2)
        formPARMSShowEvent(3)
        formPARMSShowEvent(4)
    return
##############################
# BEGIN: formPARMSShowEvent(e)
# FUNC:formPARMSShowEvent():2013.043
#   Handles making the right set of event parameters show up when
#   one of the event radiobuttons are pushed.
def formPARMSShowEvent(e):
    PARMSSubs["CON%d"%e].pack_forget()
    PARMSSubs["CRS%d"%e].pack_forget()
    PARMSSubs["EVT%d"%e].pack_forget()
    PARMSSubs["EXT%d"%e].pack_forget()
    PARMSSubs["LEV%d"%e].pack_forget()
    PARMSSubs["TIM%d"%e].pack_forget()
    PARMSSubs["TML%d"%e].pack_forget()
    if eval("PDS%dTType"%e).get() == 1:
        PARMSSubs["CON%d"%e].pack(side = LEFT, expand = YES, fill = BOTH)
    elif eval("PDS%dTType"%e).get() == 2:
        PARMSSubs["CRS%d"%e].pack(side = LEFT, expand = YES, fill = BOTH)
    elif eval("PDS%dTType"%e).get() == 3:
        PARMSSubs["EVT%d"%e].pack(side = LEFT, expand = YES, fill = BOTH)
    elif eval("PDS%dTType"%e).get() == 4:
        PARMSSubs["EXT%d"%e].pack(side = LEFT, expand = YES, fill = BOTH)
    elif eval("PDS%dTType"%e).get() == 5:
        PARMSSubs["LEV%d"%e].pack(side = LEFT, expand = YES, fill = BOTH)
    elif eval("PDS%dTType"%e).get() == 6:
        PARMSSubs["TIM%d"%e].pack(side = LEFT, expand = YES, fill = BOTH)
    elif eval("PDS%dTType"%e).get() == 7:
        PARMSSubs["TML%d"%e].pack(side = LEFT, expand = YES, fill = BOTH)
    return
#################################
# BEGIN: formPARMSClearParmsCmd()
# FUNC:formPARMSClearParmsCmd():2009.327
def formPARMSClearParmsCmd():
    formPARMSClearParms()
    formPARMSShowEvents()
    msgLn(0, "W", "All parameters cleared.")
    return
##############################
# BEGIN: formPARMSClearParms()
# FUNC:formPARMSClearParms():2019.044
def formPARMSClearParms():
    PExpName.set("")
    PExpComment.set("")
    for c in arange(1, 1+6):
        eval("PC%dName"%c).set("")
        eval("PC%dGain"%c).set(0)
        eval("PC%dComment"%c).set("")
    for d in arange(1, 1+4):
        eval("PDS%dName"%d).set("")
        eval("PDS%dDisk"%d).set(0)
        eval("PDS%dEther"%d).set(0)
        eval("PDS%dSerial"%d).set(0)
        eval("PDS%dRam"%d).set(0)
        for c in arange(1, 1+6):
            eval("PDS%dChan%d"%(d,c)).set(0)
        eval("PDS%dSampRate"%d).set("")
        eval("PDS%dFormat"%d).set("")
        eval("PDS%dTType"%d).set(0)
        for c in arange(1, 1+6):
            eval("PDS%dTChan%d"%(d,c)).set(0)
        eval("PDS%dMc"%d).set("")
        eval("PDS%dPretl"%d).set("")
        eval("PDS%dRl"%d).set("")
        eval("PDS%dSta"%d).set("")
        eval("PDS%dLta"%d).set("")
        eval("PDS%dHold"%d).set(0)
        eval("PDS%dTr"%d).set("")
        eval("PDS%dTwin"%d).set("")
        eval("PDS%dPostl"%d).set("")
        eval("PDS%dDtr"%d).set("")
        for i in arange(1, 1+11):
            eval("PDS%dStart%d"%(d,i)).set("")
        eval("PDS%dTStrm"%d).set(0)
        eval("PDS%dTlvl"%d).set("")
        eval("PDS%dLPFreq"%d).set(0)
        eval("PDS%dHPFreq"%d).set(0)
        eval("PDS%dRepInt"%d).set("")
        eval("PDS%dNumTrig"%d).set("")
    PAuxMarker.set("")
    for c in arange(1, 1+16):
        eval("PAuxChan%d"%c).set(0)
    PAuxDisk.set(0)
    PAuxEther.set(0)
    PAuxSerial.set(0)
    PAuxRam.set(0)
    PAuxSPer.set(0)
    PAuxRl.set("")
    PACGroup.set(0)
    PACEnable.set(0)
    PACCycle.set("")
    PACThresh.set("")
    PACAttempt.set("")
    PACRetry.set("")
    PACPeriod.set(0)
    PNPort.set(0)
    PNEPortPow.set(0)
    PNEKeep.set(0)
    PNETDelay.set("")
    for i in arange(1, 1+4):
        eval("PNEIP%d"%i).set("")
    PNEFill.set(1)
    PNEMask.set("")
    PNEHost.set("")
    PNEGateway.set("")
    PNSKeep.set(0)
    PNSTDelay.set("")
    PNSMode.set(0)
    PNSSpeed.set(0)
    for i in arange(1, 1+4):
        eval("PNSIP%d"%i).set("")
    PNSFill.set(1)
    PNSMask.set("")
    PNSHost.set("")
    PNSGateway.set("")
    PDETDump.set(0)
    PDRetry.set("")
    PDWrap.set(0)
    PDThresh.set("")
    PGPSMode.set("")
    return
#################################
# BEGIN: formPARMSSetParms(Which)
# FUNC:formPARMSSetParms():2018.289
def formPARMSSetParms(Which):
    formPARMSClearParms()
    if Which == "defaults":
        for c in arange(1, 1+6):
            eval("PC%dName"%c).set("Channel %d"%c)
        for d in arange(1, 1+4):
            eval("PDS%dName"%d).set("Stream %d"%d)
            eval("PDS%dMc"%d).set("1")
            eval("PDS%dPretl"%d).set("0.0")
            eval("PDS%dRl"%d).set("3600")
            eval("PDS%dSta"%d).set("0.0")
            eval("PDS%dLta"%d).set("0.1")
            eval("PDS%dTr"%d).set("0.1")
            eval("PDS%dTwin"%d).set("0")
            eval("PDS%dPostl"%d).set("0.0")
            eval("PDS%dDtr"%d).set("0.0")
            eval("PDS%dStart1"%d).set("2004001000000")
            eval("PDS%dTlvl"%d).set("0")
            eval("PDS%dNumTrig"%d).set("0")
        PACCycle.set("10")
        PACThresh.set("1.0")
        PACAttempt.set("1")
        PACRetry.set("5")
        PACPeriod.set(10)
        PDRetry.set("3")
        PDThresh.set("66")
    elif Which == "bench":
        formPARMSSetParms("defaults")
        PExpName.set("Bench")
        PExpComment.set("Ch123-DS1/40sps")
        PC1Gain.set(1)
        PC2Gain.set(1)
        PC3Gain.set(1)
        PDS1Disk.set(1)
        PDS1Chan1.set(1)
        PDS1Chan2.set(1)
        PDS1Chan3.set(1)
        PDS1SampRate.set("40")
        PDS1Format.set("CO")
        PDS1TType.set(1)
        PDS1Start1.set("2004001000000")
        PDS1Rl.set("3600")
        formPARMSShowEvents()
        msgLn(0, "", "Ch123-DS1/40sps parameters set.")
    elif Which == "bench6":
        formPARMSSetParms("defaults")
        PExpName.set("Bench-6")
        PExpComment.set("Ch123-DS1/40sps, Ch456-DS2/40sps")
        PC1Gain.set(1)
        PC2Gain.set(1)
        PC3Gain.set(1)
        PC4Gain.set(1)
        PC5Gain.set(1)
        PC6Gain.set(1)
        PDS1Disk.set(1)
        PDS1Chan1.set(1)
        PDS1Chan2.set(1)
        PDS1Chan3.set(1)
        PDS1SampRate.set("40")
        PDS1Format.set("CO")
        PDS1TType.set(1)
        PDS1Start1.set("2004001000000")
        PDS1Rl.set("3600")
        PDS2Disk.set(1)
        PDS2Chan4.set(1)
        PDS2Chan5.set(1)
        PDS2Chan6.set(1)
        PDS2SampRate.set("40")
        PDS2Format.set("CO")
        PDS2TType.set(1)
        PDS2Start1.set("2004001000000")
        PDS2Rl.set("3600")
        formPARMSShowEvents()
        msgLn(0, "", "Ch123-DS1/40sps, Ch456-DS2/40sps parameters set.")
    elif Which == "1231250":
        formPARMSSetParms("defaults")
        PExpName.set("Bench2")
        PExpComment.set("Ch123-DS1/250sps")
        PC1Gain.set(1)
        PC2Gain.set(1)
        PC3Gain.set(1)
        PDS1Disk.set(1)
        PDS1Chan1.set(1)
        PDS1Chan2.set(1)
        PDS1Chan3.set(1)
        PDS1SampRate.set("250")
        PDS1Format.set("CO")
        PDS1TType.set(1)
        PDS1Start1.set("2004001000000")
        PDS1Rl.set("3600")
        formPARMSShowEvents()
        msgLn(0, "", "Ch123-DS1/250sps parameters set.")
    elif Which == "12312506":
        formPARMSSetParms("defaults")
        PExpName.set("Bench2-6")
        PExpComment.set("Ch123-DS1/250sps, Ch456-DS2/250sps")
        PC1Gain.set(1)
        PC2Gain.set(1)
        PC3Gain.set(1)
        PC4Gain.set(1)
        PC5Gain.set(1)
        PC6Gain.set(1)
        PDS1Disk.set(1)
        PDS1Chan1.set(1)
        PDS1Chan2.set(1)
        PDS1Chan3.set(1)
        PDS1SampRate.set("250")
        PDS1Format.set("CO")
        PDS1TType.set(1)
        PDS1Start1.set("2004001000000")
        PDS1Rl.set("3600")
        PDS2Disk.set(1)
        PDS2Chan4.set(1)
        PDS2Chan5.set(1)
        PDS2Chan6.set(1)
        PDS2SampRate.set("250")
        PDS2Format.set("CO")
        PDS2TType.set(1)
        PDS2Start1.set("2004001000000")
        PDS2Rl.set("3600")
        formPARMSShowEvents()
        msgLn(0, "", "Ch123-DS1/250sps, Ch456-DS2/250sps parameters set.")
    elif Which == "12314021":
        formPARMSSetParms("defaults")
        PExpName.set("Overnight")
        PExpComment.set("Ch123-DS1/40sps-DS2/1sps")
        PC1Gain.set(1)
        PC2Gain.set(1)
        PC3Gain.set(1)
        PDS1Disk.set(1)
        PDS1Chan1.set(1)
        PDS1Chan2.set(1)
        PDS1Chan3.set(1)
        PDS1SampRate.set("40")
        PDS1Format.set("CO")
        PDS1TType.set(1)
        PDS1Start1.set("2004001000000")
        PDS1Rl.set("3600")
        PDS2Disk.set(1)
        PDS2Chan1.set(1)
        PDS2Chan2.set(1)
        PDS2Chan3.set(1)
        PDS2SampRate.set("1")
        PDS2Format.set("CO")
        PDS2TType.set(1)
        PDS2Start1.set("2004001000000")
        PDS2Rl.set("3600")
        formPARMSShowEvents()
        msgLn(0, "", "Ch123-DS1/40sps-DS2/1sps parameters set.")
    elif Which == "123140216":
        formPARMSSetParms("defaults")
        PExpName.set("Overnight")
        PExpComment.set("Ch123-DS1/40-DS2/1, Ch456-DS1/40-DS2/1")
        PC1Gain.set(1)
        PC2Gain.set(1)
        PC3Gain.set(1)
        PC4Gain.set(1)
        PC5Gain.set(1)
        PC6Gain.set(1)
        PDS1Disk.set(1)
        PDS1Chan1.set(1)
        PDS1Chan2.set(1)
        PDS1Chan3.set(1)
        PDS1SampRate.set("40")
        PDS1Format.set("CO")
        PDS1TType.set(1)
        PDS1Start1.set("2004001000000")
        PDS1Rl.set("3600")
        PDS2Disk.set(1)
        PDS2Chan1.set(1)
        PDS2Chan2.set(1)
        PDS2Chan3.set(1)
        PDS2SampRate.set("1")
        PDS2Format.set("CO")
        PDS2TType.set(1)
        PDS2Start1.set("2004001000000")
        PDS2Rl.set("3600")
        PDS3Disk.set(1)
        PDS3Chan4.set(1)
        PDS3Chan5.set(1)
        PDS3Chan6.set(1)
        PDS3SampRate.set("40")
        PDS3Format.set("CO")
        PDS3TType.set(1)
        PDS3Start1.set("2004001000000")
        PDS3Rl.set("3600")
        PDS4Disk.set(1)
        PDS4Chan4.set(1)
        PDS4Chan5.set(1)
        PDS4Chan6.set(1)
        PDS4SampRate.set("1")
        PDS4Format.set("CO")
        PDS4TType.set(1)
        PDS4Start1.set("2004001000000")
        PDS4Rl.set("3600")
        formPARMSShowEvents()
        msgLn(0, "", \
          "Ch123-DS1/40sps-DS2/1sps, Ch456-DS1/40sps-DS2/1sps parameters set.")
    PGPSMode.set("D")
    return
#########################
# BEGIN: formPARMSClose()
# FUNC:formPARMSClose():2018.289
def formPARMSClose():
    global PARMSLastSubPacked
    if PROGFrm["PARMS"] is not None:
        formClose("PARMS")
        PARMSLastSubPacked = ""
    return
# END: formPARMS




####################
# BEGIN: formSRCHM()
# LIB:formSRCHM():2019.021
PROGFrm["SRCHM"] = None
SRCHMSearchVar = StringVar()
SRCHMEraseCVar = IntVar()
SRCHMEraseCVar.set(1)
SRCHMMsgsDirVar = StringVar()
SRCHMLastFilespecVar = StringVar()
PROGSetups += ["SRCHMSearchVar", "SRCHMEraseCVar", "SRCHMMsgsDirVar", \
        "SRCHMLastFilespecVar"]

def formSRCHM():
    if showUp("SRCHM"):
        return
    LFrm = PROGFrm["SRCHM"] = Toplevel(Root)
    LFrm.withdraw()
    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, "SRCHM"))
    LFrm.title("Search Messages")
    Sub = Frame(LFrm)
    LBu = BButton(Sub, text = "Search Messages Directory:", pady = "2p", \
            command = Command(changeMainDirsCmd, "SRCHM", "self", "2M", \
            SRCHMMsgsDirVar, "Pick A Search Messages Directory", \
            "Search Messages"))
    LBu.pack(side = LEFT)
    ToolTip(LBu, 35, "Click to change the directory.")
    Entry(Sub, width = 1, textvariable = SRCHMMsgsDirVar, state = DISABLED, \
            relief = FLAT, bd = 0, bg = Clr["D"], \
            fg = Clr["B"]).pack(side = LEFT, expand = YES, fill = X)
    Sub.pack(side = TOP, anchor = "w", expand = NO, fill = X, padx = 3)
    Sub = Frame(LFrm)
    labelTip(Sub, "Find:=", LEFT, 35, \
            "[Search Current] The string to search for. Spaces are preserved.")
    LEnt = PROGEnt["SRCHM"] = Entry(Sub, textvariable = SRCHMSearchVar)
    LEnt.pack(side = LEFT, expand = YES, fill = X)
    LEnt.bind("<Return>", Command(formSRCHMGo, "current"))
    LEnt.bind("<KP_Enter>", Command(formSRCHMGo, "current"))
    LEnt.focus_set()
    LEnt.icursor(END)
    BButton(Sub, text = "Clear", fg = Clr["U"], \
            command = Command(formSRCHMClear, "msg")).pack(side = LEFT)
    Sub.pack(side = TOP, expand = NO, fill = X, padx = 3)
# Results.
    Sub = Frame(LFrm)
# Let the Msg area set the width.
    LTxt = PROGTxt["SRCHM"] = Text(Sub, height = 20, wrap = NONE, \
            relief = SUNKEN)
    LTxt.pack(side = LEFT, fill = BOTH, expand = YES)
    LSb = Scrollbar(Sub, orient = VERTICAL, command = LTxt.yview)
    LSb.pack(side = RIGHT, fill = Y)
    LTxt.configure(yscrollcommand = LSb.set)
    Sub.pack(side = TOP, fill = BOTH, expand = YES)
    LSb = Scrollbar(LFrm, orient = HORIZONTAL, command = LTxt.xview)
    LSb.pack(side = TOP, fill = X)
    LTxt.configure(xscrollcommand = LSb.set)
    Sub = Frame(LFrm)
    BButton(Sub, text = "Clear All", fg = Clr["U"], \
            command = Command(formSRCHMClear, "all")).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    Cb = Checkbutton(Sub, text = "Erase?", variable = SRCHMEraseCVar)
    Cb.pack(side = LEFT)
    ToolTip(Cb, 30, \
            "Erase the results on the form from a previous search or not?")
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Search Current\nMessages", \
            command = Command(formSRCHMGo, "current")).pack(side = LEFT)
    BButton(Sub, text = "Search All\n.msg Files...", \
            command = Command(formSRCHMGo, "files")).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Write Results\nTo File...", \
            command = Command(formWrite, "SRCHM", "SRCHM", "SRCHM", \
            "Write Search Results To...", SRCHMLastFilespecVar, \
            True, "", True, False)).pack(side = LEFT)
    Label(Sub, text = " ").pack(side = LEFT)
    BButton(Sub, text = "Close", fg = Clr["R"], \
            command = Command(formClose, "SRCHM")).pack(side = LEFT)
    Sub.pack(side = TOP, padx = 3, pady = 3)
# Status messages.
    PROGMsg["SRCHM"] = Text(LFrm, font = PROGPropFont, height = 3, \
            width = 100, wrap = WORD)
    PROGMsg["SRCHM"].pack(side = TOP, fill = X)
    setMsg("SRCHM", "WB", \
           "Enter what to search for in the field at the top of the window.")
    if len(SRCHMMsgsDirVar.get()) == 0 or \
            exists(SRCHMMsgsDirVar.get()) == False:
        SRCHMMsgsDirVar.set(PROGMsgsDirVar.get())
    if len(SRCHMLastFilespecVar.get()) == 0 or \
            exists(dirname(SRCHMLastFilespecVar.get())) == False:
        SRCHMLastFilespecVar.set(PROGMsgsDirVar.get()+"srchm.txt")
    center(Root, LFrm, "CX", "I", True)
    return
##############################
# BEGIN: formSRCHMClear(Which)
# FUNC:formSRCHMClear():2013.109
def formSRCHMClear(Which):
    SRCHMSearchVar.set("")
    if Which == "all":
        LTxt = PROGTxt["SRCHM"]
        LTxt.delete("0.0", END)
        LTxt.tag_delete(*LTxt.tag_names())
        setMsg("SRCHM")
    PROGEnt["SRCHM"].focus_set()
    PROGEnt["SRCHM"].icursor(END)
    return
#####################################
# BEGIN: formSRCHMGo(Which, e = None)
# FUNC:formSRCHMGo():2019.021
def formSRCHMGo(Which, e = None):
    LTxt = PROGTxt["SRCHM"]
    PROGFrm["SRCHM"].focus_set()
    setMsg("SRCHM", "CB", "Searching...")
# Don't .strip() it so you can search for things like " 2006:"
    What = SRCHMSearchVar.get().lower()
    if SRCHMEraseCVar.get() == 1:
        LTxt.delete("0.0", END)
        LTxt.tag_delete(*LTxt.tag_names())
    Lines = []
    N = 1
    if Which == "current":
        while 1:
            if len(MMsg.get("%d.0"%N)) == 0:
                break
            Line = MMsg.get("%d.0"%N, "%d.0"%(N+1)).lower()
            if What in Line:
                Lines.append(MMsg.get("%d.0"%N, "%d.0"%(N+1)))
            N += 1
# Special case. If all of the lines start with "USER:" then we should be able
# to sort them in time order by the second word which should be DOY:HH:MM.
# They should already be in time order, but may not be if several different
# messages files have been concatinated.
        AllUSER = True
        for Line in Lines:
            if Line.startswith("USER:") == False:
                AllUSER = False
                break
        if AllUSER == True:
# I like Python. Since "USER: DOY:HH:MM" is the first thing on each line
# then this should handle everything, except a year boundry, but we won't
# worry about that.
            Lines.sort()
        for Line in Lines:
            LTxt.insert(END, Line)
    elif Which == "files":
        TheMsgsDir = SRCHMMsgsDirVar.get()
# We just need a simple filter, so we're not using walkDirs().
        Files = listdir(TheMsgsDir)
# With the way some messages files are named (X-YYYYDOY-prog.msg) this might
# help things come out in chronological order.
        Files.sort()
        Searched = []
        for File in Files:
            if File.startswith(".") == False and File.endswith(".msg"):
                Searched.append(File)
                Ret = readFileLinesRB(TheMsgsDir+File)
                if Ret[0] != 0:
                    setMsg("SRSHM", Ret[1], Ret[2], Ret[3])
                    return
                FLines = Ret[1]
                for Line in FLines:
                    if What in Line.lower():
                        Lines.append("%s (%s)\n"%(Line, File))
        AllUSER = True
        for Line in Lines:
            if Line.startswith("USER:") == False:
                AllUSER = False
                break
        if AllUSER == True:
# I like Python. Since "USER: DOY:HH:MM:SS" is the first thing on each line
# then this should handle everything, except a year boundry, but we won't
# worry about that.
            Lines.sort()
        for Line in Lines:
            LTxt.insert(END, Line)
        if len(Searched) != 0:
            if len(Lines) != 0:
                LTxt.insert(END, "\n")
            LTxt.insert(END, "Files searched:\n")
            LTxt.insert(END, "   Dir: %s\n"%TheMsgsDir)
            for File in Searched:
                LTxt.insert(END, "   %s\n"%File)
    if len(Lines) == 1:
        setMsg("SRCHM", "WB", "1 match found.")
    else:
        setMsg("SRCHM", "WB", "%d matches found."%len(Lines))
    return
# END: formSRCHM




######################
# BEGIN: formSTATPt1()
# FUNC:formSTATPt1():2012.333
# FOR: CHANGEO
#   Everything needed to draw and control the status display.
STATTXTOTAL = 15
STATTCBOXES = 1
STATTCCOLS = 1
STATTCROWS = 15
# Change these as needed for the program.
STATBOX1START = 0
STATBOX1END = 0
STATBOX2START = 0
STATBOX2END = 0
STATBOX3START = 0
STATBOX3END = 0
STATBoxes = {}
STATUnit = {}
STATEnumBoxRVar = StringVar()
STATEnumBoxRVar.set("all")
PROGSetups += ["STATEnumBoxRVar", ]
STATLastEnumBoxRVar = StringVar()

#########################
# BEGIN: formSTAT(Parent)
# FUNC:formSTAT(Parent):2018.289
def formSTAT(Parent):
    TopSub = Frame(Parent)
    Key = 0
    for Box in arange(1, STATTCBOXES+1):
        STATBoxes[Box] = Frame(TopSub)
        for Col in arange(1, STATTCCOLS+1):
            SSub = Frame(STATBoxes[Box], bg = Clr["A"])
            for Unit in arange(1, STATTCROWS+1):
                SSSub = Frame(SSub, relief = GROOVE, bd = 1, bg = Clr["A"])
                IDLab = Label(SSSub, bd = 2, bg = Clr["A"], fg = Clr["B"])
                IDLab.pack(side = LEFT, fill = X, expand = YES, padx = 2, \
                        pady = 2)
                StatLab = Label(SSSub, text = " ", bg = Clr["B"], \
                        fg = Clr["B"], relief = SUNKEN, width = 5)
                StatLab.pack(side = RIGHT, fill = BOTH, padx = 2, pady = 2)
                SSSub.pack(side = TOP, fill = BOTH)
# Keys will be 1 thru STATTXTOTAL inclusive.
                Key += 1
                STATUnit[Key] = [IDLab, StatLab]
            SSub.pack(side = TOP, fill = BOTH, expand = YES)
        STATBoxes[Box].pack(side = TOP, fill = BOTH, expand = YES)
    TopSub.pack(side = TOP, fill = X)
    return
# END: formSTAT
# END: formSTATPt1




######################
# BEGIN: formSTATLib()
# LIB:formSTATLib():2019.072
#   The parts of formSTAT() that do not change between programs, although not
#   all programs will use all parts.
################################
# BEGIN: formSTATBoxControl(Box)
# FUNC:formSTATBoxControl():2018.291
#   Box = -1 = turns on STATEnumBoxRVar.get()'s border
#          0 = turns off all box borders ("All")
#      1,2,3 = turns on Box's border
#
#   If the user clicks a box button when the program is busy STATEnumBoxRVar
#   will be set to the new value, but this function will detect that the
#   program is busy and not do anything. The value in STATLastEnumBoxRVar will
#   be given to STATEnumBoxRVar to put the before-click value back. This
#   function gets called while the program is starting so STATLastEnumBoxRVar
#   will get initialized then.
def formSTATBoxControl(Box):
# For a resetCommands()-like call.
    if Box == "stopped":
        return
    Ret = isPROGRunning()
    if Ret[0] != 0:
        STATEnumBoxRVar.set(STATLastEnumBoxRVar.get())
        return
    if Box == -1:
        Value = STATEnumBoxRVar.get()
        if len(Value) == 0 or Value == "all":
            formSTATBoxControl(0)
        else:
            formSTATBoxControl(int(Value))
        return
    for i in arange(1, 1+STATTCBOXES):
        STATBoxes[i].config(bg = Clr["D"])
    if Box != 0:
        STATBoxes[Box].config(bg = Clr["Y"])
    STATLastEnumBoxRVar.set(STATEnumBoxRVar.get())
    updateMe(0)
    return
##################################################
# BEGIN: formSTATAddID(ID, AllowDupes, Color = "")
# FUNC:formSTATAddID():2018.249
def formSTATAddID(ID, AllowDupes, Color = ""):
    ID = str(ID).upper()
# You can only add to the currently selected box.
    Start, End = formSTATGetRange("0")
# Some programs/sections will want to check for this and some won't.
    Key = formSTATID2Key(ID, "box")
    if AllowDupes == False and Key != -1:
        if len(Color) != 0:
            STATUnit[Key][1].configure(bg = Clr[Color[0]])
        return (1, "RW", "%s: Has already been added."%ID, 2, "")
    else:
# Look for an open slot in the current box.
        for Key in arange(Start, End+1):
            if len(STATUnit[Key][0].cget("text")) == 0:
                Lab = STATUnit[Key][0]
                Lab.configure(text = ID, bg = Clr["W"], relief = RAISED)
                Lab.bind("<Button-1>", formSTATSelectOne)
                Lab.bind("<Shift-Button-1>", formSTATSelectRange)
                Lab.bind("<Control-Button-1>", formSTATSelectAdditional)
                if len(Color) == 0:
                    STATUnit[Key][1].configure(bg = Clr["B"])
                else:
                    STATUnit[Key][1].configure(bg = Clr[Color[0]])
                updateMe(0)
                return (0, )
        return (2, "YB", "There are no positions open for '%s'."%ID, 2, "")
############################
# BEGIN: formSTATCheckEnum()
# FUNC:formSTATCheckEnum():2018.249
#   Checks to see if any units have been enumerated.
def formSTATCheckEnum():
    for Key in arange(1, STATTXTOTAL+1):
        if len(STATUnit[Key][0].cget("text")) != 0:
            return True
    msgLn(0, "R", "You need to enumerate first.", True, 2)
    return False
#########################################
# BEGIN: formSTATCheckIfEnumed(ID, Scope)
# FUNC:formSTATCheckIfEnumed():2012.349
#   Checks to see if the passed ID has been enumed. This is just a better name
#   than formSTATID2Key() for others to call.
def formSTATCheckIfEnumed(ID, Scope):
    return formSTATID2Key(ID, Scope)
###################################
# BEGIN: formSTATCheckIfFull(Scope)
# FUNC:formSTATCheckIfFull():2018.249
#   Some programs may not want to rely on the return from formSTATAddID(),
#   especially with Ethernetted equipment (HOCUS) where the units transmit
#   their ID multiple times during startup. Returns True or False.
def formSTATCheckIfFull(Scope):
    Full = True
    if Scope == "any":
        Start = 1
        End = STATTXTOTAL
    elif Scope == "box":
        Start, End = formSTATGetRange("0")
    for Key in arange(Start, End+1):
        if len(STATUnit[Key][0].cget("text")) == 0:
            Full = False
    return Full
############################
# BEGIN: formSTATClearID(ID)
# FUNC:formSTATClearID():2018.248
#   ID = "0" = clears the current box
#      other = clears the box with that unit ID
def formSTATClearID(ID):
    ID = str(ID)
    if ID == "0":
        Start, End = formSTATGetRange("0")
        for Key in arange(Start, End+1):
            Lab = STATUnit[Key][0]
            Lab.configure(text = "", bg = Clr["A"], relief = FLAT)
            Lab.unbind("<Button-1>")
            Lab.unbind("<Shift-Button-1>")
            Lab.unbind("<Control-Button-1>")
            STATUnit[Key][1].configure(bg = Clr["B"])
    else:
        Key = formSTATID2Key(ID, "any")
        if Key == -1:
            msgLn(0, "R", "%s: Unit has not been enumerated."%ID, True, 2)
            return
        Lab = STATUnit[Key][0]
        Lab.configure(text = "", bg = Clr["A"], relief = FLAT)
        Lab.unbind("<Button-1>")
        Lab.unbind("<Shift-Button-1>")
        Lab.unbind("<Control-Button-1>")
        STATUnit[Key][1].configure(bg = Clr["B"])
    updateMe(0)
    return
###############################
# BEGIN: formSTATGetBoxIDs(Box)
# FUNC:formSTATGetBoxIDs():2018.249
#   Returns a List of unit IDs that are selected in the passed Box.
def formSTATGetBoxIDs(Box):
    Units = []
    Start, End = formSTATGetRange(Box)
    for Key in arange(Start, End+1):
        Lab = STATUnit[Key][0]
        if len(Lab.cget("text")) != 0:
            if Lab.cget("bg") == Clr["W"]:
                Units.append(Lab.cget("text"))
    return Units
##############################
# BEGIN: formSTATGetIDs(Which)
# FUNC:formSTATGetIDs():2018.249
#   Returns a List of unit IDs that depend on the value of Which.
#   Which = 0 = selected in the current box (the normal use)
#           1 = selected and non-selected in the current box
#           2 = all enumerated IDs
def formSTATGetIDs(Which):
    Units = []
    if Which == 0 or Which == 1:
        Start, End = formSTATGetRange("0")
        for Key in arange(Start, End+1):
            Lab = STATUnit[Key][0]
            if len(Lab.cget("text")) != 0:
                if Which == 0 and Lab.cget("bg") == Clr["W"]:
                    Units.append(Lab.cget("text"))
                elif Which == 1:
                    Units.append(Lab.cget("text"))
    elif Which == 2:
        for Key in arange(1, STATTXTOTAL+1):
            Lab = STATUnit[Key][0]
            if len(Lab.cget("text")) != 0:
                Units.append(Lab.cget("text"))
    return Units
################################
# BEGIN: formSTATGetRange(InBox)
# FUNC:formSTATGetRange():2018.249
#   Return the range of key values given the current box selected if InBox is
#   "0", or returns the range of key values for the passed InBox.
def formSTATGetRange(InBox):
    if InBox == "0":
        Box = STATEnumBoxRVar.get()
    else:
        Box = InBox
    if len(Box) == 0 or Box == "all":
        return 1, STATTXTOTAL
# WARNING: Alter this if any program has more than 3 boxes.
    elif Box == "1":
        return STATBOX1START, STATBOX1END
    elif Box == "2":
        return STATBOX2START, STATBOX2END
    elif Box == "3":
        return STATBOX3START, STATBOX3END
    return 0, 0
###################################
# BEGIN: formSTATGetStatusColor(ID)
# FUNC:formSTATGetStatusColor():2009.026
def formSTATGetStatusColor(ID):
    Key = formSTATID2Key(ID, "any")
    if Key == -1:
        return ""
    return STATUnit[Key][1].cget("bg")
##################################
# BEGIN: formSTATID2Key(ID, Scope)
# FUNC:formSTATID2Key():2018.248
#   Mostly for internal formSTAT() use to get the dict key of the passed ID,
#   but can be called by anyone to see if a unit with the passed ID has been
#   enumerated.
def formSTATID2Key(ID, Scope):
# ID can be a hex number (CHANGEO), so try and take our chances.
    try:
        ID = str(int(ID))
    except ValueError:
        ID = str(ID).upper()
    if Scope == "any":
        Start = 1
        End = STATTXTOTAL
    elif Scope == "box":
        Start, End = formSTATGetRange("0")
    for Key in arange(Start, End+1):
        if ID == STATUnit[Key][0].cget("text"):
            return Key
    return -1
############################
# BEGIN: formSTATKey2ID(Key)
# FUNC:formSTATKey2ID():2008.153
def formSTATKey2ID(Key):
    Key = intt(Key)
    if Key < 1 or Key > STATTXTOTAL:
        return ""
    return STATUnit[Key][0].cget("text")
######################################
# BEGIN: formSTATBRC2ID(Box, Row, Col)
# FUNC:formSTATBRC2ID():2009.242
#    Returns the ID of, for example, Box 2, Row 3, Column 2.
def formSTATBRC2ID(Box, Row, Col):
# Make them 0-based so we can just do math to get the index, but let the human
# keep their sanity by using 1-based values, but then add 1 at the end because
# the keys for the the boxes are 1 to STATTXTOTAL.
    Key = (((Box-1)*(STATTCROWS*STATTCCOLS))+(Col-1)*STATTCROWS+(Row-1))+1
    return formSTATKey2ID(Key)
#################################
# BEGIN: formSTATRRemoveDups(IDs)
# FUNC:formSTATRemoveDups():2019.072
#   Clears the ID labels and fills them in with the final list. Passes back
#   the final list with 0000's and duplicates removed.
def formSTATRemoveDups(IDs):
# Sort alphanumerically.
    IDs = formSTATGetIDs(0)
    IDs.sort()
# Put the final list of DASs in the enumerated list area. Throw out any 0000s.
    formSTATClearID(0)
    for DAS in IDs:
        if DAS == "0000":
            continue
# formSTATAddID() weeds out duplicates. Just ask for the final list at the end.
        formSTATAddID(DAS, False)
    return formSTATGetIDs(0)
###########################################
# BEGIN: formSTATSelectAdditional(e = None)
# FUNC:selectAdditional():2013.179
#    Just turns the clicked label on or off without turning off the rest IF the
#    clicked-on label is in the current box.
def formSTATSelectAdditional(e = None):
    Ret = isPROGRunning()
    if Ret[0] != 0:
        return
    ID = e.widget.cget("text")
    Key = formSTATID2Key(ID, "box")
    if Key == -1:
        beep(1)
        return
    Lab = STATUnit[Key][0]
    if Lab.cget("bg") == Clr["A"]:
        Lab.configure(bg = Clr["W"])
    else:
        Lab.configure(bg = Clr["A"])
    updateMe(0)
    return
####################################
# BEGIN: formSTATSelectAll(e = None)
# FUNC:formSTATSelectAll():2018.249
def formSTATSelectAll(e = None):
    Ret = isPROGRunning()
    if Ret[0] != 0:
        return
    Start, End = formSTATGetRange("0")
    AllOn = True
    for Key in arange(Start, End+1):
        if len(STATUnit[Key][0].cget("text")) != 0:
            if STATUnit[Key][0].cget("bg") != Clr["W"]:
                AllOn = False
            STATUnit[Key][0].configure(bg = Clr["W"])
    if AllOn == True:
        for Key in arange(Start, End+1):
            if len(STATUnit[Key][0].cget("text")) != 0:
                STATUnit[Key][0].configure(bg = Clr["A"])
    updateMe(0)
    return
#############################################
# BEGIN: formSTATSelectColor(Color, e = None)
# FUNC:formSTATSelectColor():2018.249
#   Selects all of the units with status labels matching the passed Color in
#   the current box.
def formSTATSelectColor(Color, e = None):
    Ret = isPROGRunning()
    if Ret[0] != 0:
        return
    Start, End = formSTATGetRange("0")
    for Key in arange(Start, End+1):
        Lab = STATUnit[Key][0]
        if len(Lab.cget("text")) != 0:
            if STATUnit[Key][1].cget("bg") == Clr[Color]:
                Lab.configure(bg = Clr["W"])
            else:
                Lab.configure(bg = Clr["A"])
    updateMe(0)
    return
############################################
# BEGIN: formSTATSwapColor(C1, C2, Str = "")
# FUNC:formSTATSwapColor():2018.249
#   Changes the status field color of anyone whose color matches C1 to C2.
#   If text is to be displayed then C2 can be the fg color.
#   Works on the current box. Returns a list of IDs that it changed.
def formSTATSwapColor(C1, C2, Str = ""):
    Start, End = formSTATGetRange("0")
    Changed = []
    for Key in arange(Start, End+1):
        if STATUnit[Key][1].cget("bg") == Clr[C1[0]]:
            STATUnit[Key][1].configure(bg = Clr[C2[0]])
            if len(Str) != 0 and len(C2) == 2:
                STATUnit[Key][1].configure(bg = Clr[C2[1]])
            STATUnit[Key][1].configure(text = Str)
            Changed.append(STATUnit[Key][0].cget("text"))
    updateMe(0)
    return Changed
####################################
# BEGIN: formSTATSelectOne(e = None)
# FUNC:formSTATSelectOne():2018.248
#   Turn off all of the ID labels and then turn the clicked one back on IF the
#   clicked on is in the current box.
def formSTATSelectOne(e = None):
    Ret = isPROGRunning()
    if Ret[0] != 0:
        return
    ID = e.widget.cget("text")
    Key = formSTATID2Key(ID, "box")
# If the clicked on label is not in the current box then just beep.
    if Key == -1:
        beep(1)
        return
    Start, End = formSTATGetRange("0")
    for Key2 in arange(Start, End+1):
        STATUnit[Key2][0].configure(bg = Clr["A"])
    STATUnit[Key][0].configure(bg = Clr["W"])
    updateMe(0)
    return
######################################
# BEGIN: formSTATSelectRange(e = None)
# FUNC:formSTATSelectRange():2018.249
#   Start from the first label (from the left or the top) and turn on all of
#   the labels up to the clicked-on one IF the clicked-on one is in the current
#   box. If no labels are turned on try going backwards from the first on label
#   to the clicked on label.
def formSTATSelectRange(e = None):
    Ret = isPROGRunning()
    if Ret[0] != 0:
        return
    On = False
    ID = e.widget.cget("text")
    Key = formSTATID2Key(ID, "box")
    if Key == -1:
        beep(1)
        return
    Start, End = formSTATGetRange("0")
    Found = False
    for Key2 in arange(Start, Key+1):
        Lab = STATUnit[Key2][0]
        if len(Lab.cget("text")) != 0:
            if Lab.cget("bg") != Clr["A"]:
                On = True
            if On == True:
                Lab.configure(bg = Clr["W"])
                Found = True
    if Found == False:
        for Key2 in arange(End, Key-1, -1):
            Lab = STATUnit[Key2][0]
            if len(Lab.cget("text")) != 0:
                if Lab.cget("bg") != Clr["A"]:
                    On = True
                if On == True:
                    Lab.configure(bg = Clr["W"])
    updateMe(0)
    return
###############################################
# BEGIN: formSTATSetStatus(ID, Color, Str = "")
# FUNC:formSTATSetStatus():2018.249
#   ID should be one of the values below, or an ID label value.
#   If text is to be displayed Color can have the fg color as the second char.
def formSTATSetStatus(ID, Color, Str = ""):
    ID = str(ID)
# These ones are for ALL status fields.
    if ID == "all":
        for Key in arange(1, STATTXTOTAL+1):
            STATUnit[Key][1].configure(bg = Clr[Color[0]])
            if len(Str) != 0 and len(Color) == 2:
                    STATUnit[Key][1].configure(bg = Clr[Color[1]])
# This still gets done in case we are clearing out previous text.
            STATUnit[Key][1].configure(text = Str)
        updateMe(0)
# The ones that have IDs and are white.
    elif ID == "sel":
        for Key in arange(1, STATTXTOTAL+1):
            if len(STATUnit[Key][0].cget("text")) != 0 and \
                    STATUnit[Key][0].cget("bg") == Clr["W"]:
                STATUnit[Key][1].configure(bg = Clr[Color[0]])
                if len(Str) != 0 and len(Color) == 2:
                    STATUnit[Key][1].configure(bg = Clr[Color[1]])
                STATUnit[Key][1].configure(text = Str)
    elif ID == "used":
        for Key in arange(1, STATTXTOTAL+1):
            if len(STATUnit[Key][0].cget("text")) != 0:
                STATUnit[Key][1].configure(bg = Clr[Color[0]])
                if len(Str) != 0 and len(Color) == 2:
                    STATUnit[Key][1].configure(bg = Clr[Color[1]])
                STATUnit[Key][1].configure(text = Str)
# The ones with a status color of Clr["C"].
    elif ID == "working":
        for Key in arange(1, STATTXTOTAL+1):
            if STATUnit[Key][1].cget("bg") == Clr["C"]:
                STATUnit[Key][1].configure(bg = Clr[Color[0]])
                if len(Str) != 0 and len(Color) == 2:
                    STATUnit[Key][1].configure(bg = Clr[Color[1]])
                STATUnit[Key][1].configure(text = Str)
# Special for CHANGEO (same as "used").
    elif ID == "0000":
        for Key in arange(1, STATTXTOTAL+1):
            if len(STATUnit[Key][0].cget("text")) != 0:
                STATUnit[Key][1].configure(bg = Clr[Color[0]])
                if len(Str) != 0 and len(Color) == 2:
                    STATUnit[Key][1].configure(bg = Clr[Color[1]])
                STATUnit[Key][1].configure(text = Str)
# These are for the current box.
    elif ID == "boxall":
        Start, End = formSTATGetRange("0")
        for Key in arange(Start, End+1):
            STATUnit[Key][1].configure(bg = Clr[Color[0]])
            if len(Str) != 0 and len(Color) == 2:
                STATUnit[Key][1].configure(bg = Clr[Color[1]])
            STATUnit[Key][1].configure(text = Str)
    elif ID == "boxsel":
        Start, End = formSTATGetRange("0")
        for Key in arange(Start, End+1):
            if len(STATUnit[Key][0].cget("text")) != 0 and \
                    STATUnit[Key][0].cget("bg") == Clr["W"]:
                STATUnit[Key][1].configure(bg = Clr[Color[0]])
                if len(Str) != 0 and len(Color) == 2:
                    STATUnit[Key][1].configure(bg = Clr[Color[1]])
                STATUnit[Key][1].configure(text = Str)
    elif ID == "boxused":
        Start, End = formSTATGetRange("0")
        for Key in arange(Start, End+1):
            if len(STATUnit[Key][0].cget("text")) != 0:
                STATUnit[Key][1].configure(bg = Clr[Color[0]])
                if len(Str) != 0 and len(Color) == 2:
                    STATUnit[Key][1].configure(bg = Clr[Color[1]])
                STATUnit[Key][1].configure(text = Str)
    elif ID == "boxworking":
        Start, End = formSTATGetRange("0")
        for Key in arange(Start, End+1):
            if STATUnit[Key][1].cget("bg") == Clr["C"]:
                STATUnit[Key][1].configure(bg = Clr[Color[0]])
                if len(Str) != 0 and len(Color) == 2:
                    STATUnit[Key][1].configure(bg = Clr[Color[1]])
                STATUnit[Key][1].configure(text = Str)
    else:
        Start, End = formSTATGetRange("0")
        ID = ID.upper()
        Key = formSTATID2Key(ID, "box")
# The passed ID should never trigger this, but you never know. This will at
# least point out what must be a bug.
        if Key != -1:
            STATUnit[Key][1].configure(bg = Clr[Color[0]])
            if len(Str) != 0 and len(Color) == 2:
                STATUnit[Key][1].configure(bg = Clr[Color[1]])
            STATUnit[Key][1].configure(text = Str)
        else:
            stdout.write("formSTATSetStatus(): Bad unit ID: %s\n"%ID)
    updateMe(0)
    return
###################################################################
# BEGIN: formSTATAddRemID(Var, Field, ID, WhereMsg, Prog, e = None)
# FUNC:formSTATAddRemID():2018.249
#   Handles adding and removing either the passed ID (adding only) or the
#   value of the ID field entered by the user from the status display.
#   Use Prog to get this to do additional checks depending on the program.
#   If passed, ID needs to be a str.
def formSTATAddRemID(Var, Field, ID, WhereMsg, Prog, e = None):
    if len(ID) == 0:
        Var.set(Var.get().strip().upper())
        ID = Var.get()
# Do extra checks for different programs. Some functions called from here may
# not even exist.
        if Prog == "CHANGEO":
            Ret = check130ID(ID)
            if Ret[0] != 0:
                PROGEnt[Field].focus_set()
                msgLn(0, Ret[1], Ret[2], True, Ret[3])
                return
    ID = ID.strip().upper()
# If it is still blank after looking at the field...
    if len(ID) == 0:
        if WhereMsg is None:
            msgLn(0, "R", "No unit ID was entered.", True, 2)
        else:
            setMsg(WhereMsg, "RW", "No unit ID was entered.", 2)
        return
# Check to see if the ID is in any of the boxes.
    Ret = formSTATID2Key(ID, "any")
    if Ret != -1:
        formSTATClearID(ID)
        Var.set("")
        if WhereMsg is None:
            msgLn(1, "", "%4s: Manually removed."%ID)
        else:
            setMsg(WhereMsg, "", "%4s: Manually removed."%ID)
    else:
# Try to add it.
        Ret = formSTATAddID(ID, False)
        if Ret[0] != 0:
            if WhereMsg is None:
                msgLn(0, Ret[1], Ret[2], True, Ret[3])
            else:
                setMsg(WhereMsg, Ret)
            return
        if WhereMsg is None:
            msgLn(1, "", "%4s: Manually added."%ID)
        else:
            setMsg(WhereMsg, "", "%4s: Manually added."%ID)
# We'll assume the next thing the user will want to do is add or remove some
# more units.
        Var.set("")
        PROGEnt[Field].focus_set()
    return
################################################################
# BEGIN: formSTATRemID(Var, Field, ID, WhereMsg, Prog, e = None)
# FUNC:formSTATRemID():2018.249
#   Same as formSTATAddRemID(), but this on can only remove the ID and
#   complains if the passed ID is not in the list.
def formSTATRemID(Var, Field, ID, WhereMsg, Prog, e = None):
    if len(ID) == 0:
        Var.set(Var.get().strip().upper())
        ID = Var.get()
# Do extra checks for different programs. Some functions called from here may
# not even exist.
        pass
    ID = ID.strip().upper()
# If it is still blank after looking at the field...
    if len(ID) == 0:
        if WhereMsg is None:
            msgLn(0, "R", "No unit ID was entered.", True, 2)
        else:
            setMsg(WhereMsg, "RW", "No unit ID was entered.", 2)
        return
# Check to see if the ID is in any of the boxes.
    Ret = formSTATID2Key(ID, "any")
    if Ret != -1:
        formSTATClearID(ID)
        Var.set("")
        if WhereMsg is None:
            msgLn(1, "", "%4s: Manually removed."%ID)
        else:
            setMsg(WhereMsg, "", "%4s: Manually removed."%ID)
    else:
        if WhereMsg is None:
            msgLn(1, "R", "%4s: Is not in the list."%ID)
        else:
            setMsg(WhereMsg, "RW", "%4s: Is not in the list."%ID)
# We'll assume the next thing the user will want to do is remove some more
# units.
        Var.set("")
        PROGEnt[Field].focus_set()
    return
# END: formSTATLib




############################################################################
# BEGIN: formWrite(Parent, WhereMsg, TextWho, Title, FilespecVar, AllowPick,
#                Backup, Confirm, Clean)
# LIB:formWrite():2019.037
#   Writes the contents of the passed TextWho Text() widget to a file.
#   FilespecVar can be a StringVar or an astring.
#   If Backup is "" no backup will be made, but if it is not then the passed
#   string, like "-" or "~", will be appended to the original file name, the
#   original file will be renamed to that, and then the new stuff written to
#   the original file name.
#   If Confirm is True then the popup dialog question will be asked. If it is
#   False then no question will be asked, and the file will be overwritten to
#   the passed FilespecVar. A backup will be made depending on the value of
#   Backup.
#   Clean=True will go through each line and make sure it has ASCII characters.
#   Non-ASCII characters will be "?".
#   A List of text lines may be passed as TextWho and those will be written to
#   the file, insted of the contents of a Text().
FW_BACKUPCHAR = "-"

def formWrite(Parent, WhereMsg, TextWho, Title, FilespecVar, AllowPick, \
        Backup, Confirm, Clean):
# Sometimes it can take a while if there is a lot of text.
    updateMe(0)
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
# Can be StringVar or a string.
    if isinstance(FilespecVar, StringVar):
        InFile = FilespecVar.get()
    if isinstance(FilespecVar, astring):
        InFile = FilespecVar
# Set this in case we are skipping the confirmation stuff.
    Filespec = InFile
    if Confirm == True:
        if AllowPick == True:
            setMsg(WhereMsg, "", "")
            Filespec = formMYDF(Parent, 0, Title, dirname(InFile), \
                    basename(InFile))
            if len(Filespec) == 0:
                return ""
    setMsg(WhereMsg, "CB", "Working...")
# Default to backing up and overwriting.
    Answer = "over"
    if Confirm == True and exists(Filespec):
        Answer = formMYD(Parent, (("Append", LEFT, "append"), \
                ("Overwrite", LEFT, "over"), ("Cancel", LEFT, "cancel")), \
                "cancel", "YB", "Keep Everything.", \
                "The file\n\n%s\n\nalready exists. Would you like to append to that file, overwrite it, or cancel?"% \
                Filespec)
        if Answer == "cancel":
            setMsg(WhereMsg, "", "Nothing done.")
            return ""
    try:
        if Answer == "over":
            if len(Backup) != 0:
# rename() on some systems, like Windows, get upset if the backup file already
# exists. No one else seems to care.
                if exists(Filespec+Backup):
                    remove(Filespec+Backup)
                rename(Filespec, Filespec+Backup)
            Fp = open(Filespec, "w")
        elif Answer == "append":
            Fp = open(Filespec, "a")
    except Exception as e:
        setMsg(WhereMsg, "MW", "Error opening file (2)\n   %s\n   %s"% \
                (Filespec, e), 3)
        return ""
# Now that the file is OK...
    if isinstance(FilespecVar, StringVar):
        FilespecVar.set(Filespec)
    if isinstance(TextWho, astring):
        LTxt = PROGTxt[TextWho]
        N = 1
# In case the text field is empty.
        Line = "\n"
        while 1:
# This little convolution keeps empty lines from piling up at the end of the
# file.
            if len(LTxt.get("%d.0"%N)) == 0:
                if Line != "\n":
                    Fp.write(Line)
                break
            if N > 1:
                if Clean == True:
                    Line = line2ASCII(Line)
# The caller may not want the line cleaned, but that's doesn't 'make it so'.
                try:
                    Fp.write(Line)
                except UnicodeEncodeError:
                    setMsg(WhereMsg, "RW", "Unicode in line:\n%s"%Line, 2)
                    return
            Line = LTxt.get("%d.0"%N, "%d.0"%(N+1))
            N += 1
    elif isinstance(TextWho, list):
        for Line in Lines:
            if Line.endswith("\n"):
                if Clean == True:
                    Line = line2ASCII(Line)
                try:
                    Fp.write(Line)
                except UnicodeEncodeError:
                    setMsg(WhereMsg, "RW", "Unicode in line:\n%s"%Line, 2)
                    return
            else:
                if Clean == True:
                    Line = line2ASCII(Line)
                try:
                    Fp.write(Line+"\n")
                except UnicodeEncodeError:
                    setMsg(WhereMsg, "RW", "Unicode in line:\n%s"%Line, 2)
                    return
    Fp.close()
    if Answer == "append":
        setMsg(WhereMsg, "GB", "Appended text to file\n   %s"%Filespec)
    elif Answer == "over":
        setMsg(WhereMsg, "GB", "Wrote text to file\n   %s"%Filespec)
# Return this in case the caller is interested.
    return Filespec
#################################################################
# BEGIN: formRead(Parent, TextWho, Title, FilespecVar, AllowPick)
# FUNC:formRead():2019.003
#   Same idea as formWrite(), but handles reading a file into a Text().
#   Only appends the text to the END of the passed Text() field.
def formRead(Parent, TextWho, Title, FilespecVar, AllowPick):
# Sometimes it can take a while if there is a lot of text.
    updateMe(0)
    if isinstance(Parent, astring):
        Parent = PROGFrm[Parent]
    Filespec = FilespecVar.get()
    if AllowPick == True:
        Filespec = formMYDF(Parent, 0, Title, dirname(Filespec), \
                basename(Filespec))
        if len(Filespec) == 0:
            return (2, "", "Nothing done.", 0, "")
    setMsg(WhereMsg, "CB", "Working...")
    if exists(Filespec) == False:
        return (2, "RW", "File to read does not exist:\n   %s"%Filespec, 2, \
                Filespec)
    if isdir(Filespec):
        return (2, "RW", "Selected file is not a normal file:\n   %s"% \
                Filespec, 2, Filespec)
# We don't know who saved the file, what the encoding is, what the line
# delimters are, so use this to get it split up. formRead() is generally just
# for ASCII text files, but you never know.
    Ret = readFileLinesRB(Filespec)
    if Ret[0] != 0:
        return Ret
    Lines = Ret[1]
# Now that the file is OK...
    LTxt = PROGTxt[TextWho]
    if int(LTxt.index('end-1c').split('.')[0]) > 1:
        LTxt.insert(END, "\n")
    for Line in Lines:
        LTxt.insert(END, "%s\n"%Line)
    FilespecVar.set(Filespec)
    if len(Lines) == 1:
        return (0, "", "Read %d line from file\n   %s"%(len(Lines), \
                Filespec), 0, Filespec)
    else:
        return (0, "", "Read %d lines from file\n   %s"%(len(Lines), \
                Filespec), 0, Filespec)
################################
# BEGIN: formWriteNoUni(TextWho)
# FUNC:formWriteNoUni():2019.037
#   A quick check before going through everything in formWrite().
def formWriteNoUni(TextWho):
# Sometimes it can take a while if there is a lot of text.
    updateMe(0)
    if isinstance(TextWho, astring):
        LTxt = PROGTxt[TextWho]
    Lines = LTxt.get("0.0", END).split("\n")
    for Line in Lines:
        try:
            Line.encode("ascii")
        except UnicodeEncodeError:
            return (1, "RW", "Unicode in line:\n   %s"%Line, 2)
    return (0,)
# END: formWrite




########################
# BEGIN: getAColor(What)
# LIB:getAColor():2010.146
# W->B no W (What = 1)
GACListWBNW = [("#FFBFFF", "black"), ("#FF7FFF", "black"), \
        ("#FF3FFF", "white"), ("#FF00FF", "white"), ("#FF00BF", "white"), \
        ("#FF007F", "white"), ("#FF003F", "white"), ("#FF0000", "white"), \
        ("#FF3F00", "white"), ("#FF7F00", "white"), ("#FFBF00", "black"), \
        ("#FFFF00", "black"), ("#BFFF00", "black"), ("#7FFF00", "black"), \
        ("#3FFF00", "black"), ("#00FF00", "black"), ("#00BF00", "white"), \
        ("#007F00", "white"), ("#003F00", "white"), ("#000000", "white")]
# W->B no B (What = 2)
GACListWBNB = [("#FFFFFF", "black"), ("#FFBFFF", "black"), \
        ("#FF7FFF", "black"), ("#FF3FFF", "white"), ("#FF00FF", "white"), \
        ("#FF00BF", "white"), ("#FF007F", "white"), ("#FF003F", "white"), \
        ("#FF0000", "white"), ("#FF3F00", "white"), ("#FF7F00", "white"), \
        ("#FFBF00", "black"), ("#FFFF00", "black"), ("#BFFF00", "black"), \
        ("#7FFF00", "black"), ("#3FFF00", "black"), ("#00FF00", "black"), \
        ("#00BF00", "white"), ("#007F00", "white"), ("#003F00", "white")]
# B->W no W (What = 3)
GACListBWNW = [("#003F00", "white"), ("#007F00", "white"), \
        ("#00BF00", "white"), ("#00FF00", "black"), ("#3FFF00", "black"), \
        ("#7FFF00", "black"), ("#BFFF00", "black"), ("#FFFF00", "black"), \
        ("#FFBF00", "black"), ("#FF7F00", "white"), ("#FF3F00", "white"), \
        ("#FF0000", "white"), ("#FF003F", "white"), ("#FF007F", "white"), \
        ("#FF00BF", "white"), ("#FF00FF", "white"), ("#FF3FFF", "white"), \
        ("#FF7FFF", "black"), ("#FFBFFF", "black"), ("#FFFFFF", "black")]
# B->W no B (What = 4)
GACListBWNB = [("#000000", "white"), ("#003F00", "white"), \
        ("#007F00", "white"), ("#00BF00", "white"), ("#00FF00", "black"), \
        ("#3FFF00", "black"), ("#7FFF00", "black"), ("#BFFF00", "black"), \
        ("#FFFF00", "black"), ("#FFBF00", "black"), ("#FF7F00", "white"), \
        ("#FF3F00", "white"), ("#FF0000", "white"), ("#FF003F", "white"), \
        ("#FF007F", "white"), ("#FF00BF", "white"), ("#FF00FF", "white"), \
        ("#FF3FFF", "white"), ("#FF7FFF", "black"), ("#FFBFFF", "black")]
# Primary Colors, White Background (What = 5)
GACListPCWB = [("#000000", "white"), ("#FF0000", "white"), \
        ("#FFBF00", "black"), ("#00FF00", "black"), ("#0000FF", "white"), \
        ("#00FFFF", "black"), ("#FFFF00", "black"), ("#00BF00", "white"), \
        ("#FF00FF", "white"), ("#7F7F7F", "white")]
# Primary Colors, Black Background (What = 6)
GACListPCBB = [("#FFFFFF", "black"), ("#FF0000", "white"), \
        ("#FFBF00", "black"), ("#00FF00", "black"), ("#0000FF", "white"), \
        ("#00FFFF", "black"), ("#FFFF00", "black"), ("#00BF00", "white"), \
        ("#FF00FF", "white"), ("#7F7F7F", "white")]
# 18 Colors, no black, no adjacent matches (What = 7)
# Green is first so "individual" graphs will be green on black in several
# routines that use getAColor().
GACListNBR = [("#00FF00", "black"), ("#FFBF00", "black"), \
        ("#FF0000", "white"), ("#0000FF", "white"), ("#00FFFF", "black"), \
        ("#7F7F7F", "white"), ("#00BF00", "white"), ("#FF00FF", "white"), \
        ("#FFFF00", "black"), ("#007F00", "white"), ("#007FFF", "white"), \
        ("#FF7F00", "black"), ("#BFFF00", "black"), ("#00007F", "white"), \
        ("#CFCFCF", "black"), ("#7F00FF", "white"), ("#339999", "white"), \
        ("#FFFFFF", "black")]
# Close to the above, but for white backgrounds so no white or light yellow
# colors (what = 8)
GACListWBG = [("#000000", "white"), ("#00FF00", "black"), \
        ("#FFBF00", "black"), ("#FF0000", "white"), ("#0000FF", "white"), \
        ("#00FFFF", "black"), ("#7F7F7F", "white"), ("#00BF00", "white"), \
        ("#FF00FF", "white"), ("#FFFF00", "black"), ("#007F00", "white"), \
        ("#007FFF", "white"), ("#FF7F00", "black"), ("#00007F", "white"), \
        ("#003F00", "white"), ("#7F00FF", "white"), ("#339999", "white"), \
        ("#BF0000", "white")]

def getAColor(What):
    global GACVar
    global GACLastClr
    if What == 0:
        GACVar = -1
        GACLastClr = ()
        return
    if What == -1:
        return GACLastClr
    GACVar += 1
    if What == 1:
        if GACVar >= len(GACListWBNW):
            GACVar = 0
        Clr = GACListWBNW[GACVar]
    elif What == 2:
        if GACVar >= len(GACListWBNB):
            GACVar = 0
        Clr = GACListWBNB[GACVar]
    elif What == 3:
        if GACVar >= len(GACListBWNW):
            GACVar = 0
        Clr = GACListBWNW[GACVar]
    elif What == 4:
        if GACVar >= len(GACListBWNB):
            GACVar = 0
        Clr = GACListBWNB[GACVar]
    elif What == 5:
        if GACVar >= len(GACListPCWB):
            GACVar = 0
        Clr = GACListPCWB[GACVar]
    elif What == 6:
        if GACVar >= len(GACListPCBB):
            GACVar = 0
        Clr = GACListPCBB[GACVar]
    elif What == 7:
        if GACVar >= len(GACListNBR):
            GACVar = 0
        Clr = GACListNBR[GACVar]
    elif What == 8:
        if GACVar >= len(GACListWBG):
            GACVar = 0
        Clr = GACListWBG[GACVar]
    GACLastClr = Clr
    return Clr
# END: getAColor




#######################
# BEGIN: getGMT(Format)
# LIB:getGMT():2015.007
#   Gets the time in various forms from the system.
def getGMT(Format):
# YYYY:DOY:HH:MM:SS (GMT)
    if Format == 0:
        return strftime("%Y:%j:%H:%M:%S", gmtime(time()))
# YYYYDOYHHMMSS (GMT)
    elif Format == 1:
        return strftime("%Y%j%H%M%S", gmtime(time()))
# YYYY-MM-DD (GMT)
    elif Format == 2:
        return strftime("%Y-%m-%d", gmtime(time()))
# YYYY-MM-DD HH:MM:SS (GMT)
    elif Format == 3:
        return strftime("%Y-%m-%d %H:%M:%S", gmtime(time()))
# YYYY, MM and DD (GMT) returned as ints
    elif Format == 4:
        GMT = gmtime(time())
        return (GMT[0], GMT[1], GMT[2])
# YYYY-Jan-01 (GMT)
    elif Format == 5:
        return strftime("%Y-%b-%d", gmtime(time()))
# YYYYMMDDHHMMSS (GMT)
    elif Format == 6:
        return strftime("%Y%m%d%H%M%S", gmtime(time()))
# Reftek Texan (year-1984) time stamp in BBBBBB format (GMT)
    elif Format == 7:
        GMT = gmtime(time())
        return pack(">BBBBBB", (GMT[0]-1984), 0, 1, GMT[3], GMT[4], GMT[5])
# Number of seconds since Jan 1, 1970 from the system.
    elif Format == 8:
        return time()
# YYYY-MM-DD/DOY HH:MM:SS (GMT)
    elif Format == 9:
        return strftime("%Y-%m-%d/%j %H:%M:%S", gmtime(time()))
# YYYY-MM-DD/DOY (GMT)
    elif Format == 10:
        return strftime("%Y-%m-%d/%j", gmtime(time()))
# YYYY, DOY, HH, MM, SS (GMT) returned as ints
    elif Format == 11:
        GMT = gmtime(time())
        return (GMT[0], GMT[7], GMT[3], GMT[4], GMT[5])
# HH:MM:SS (GMT)
    elif Format == 12:
        return strftime("%H:%M:%S", gmtime(time()))
# YYYY:DOY:HH:MM:SS (LT)
    elif Format == 13:
        return strftime("%Y:%j:%H:%M:%S", localtime(time()))
# HHMMSS (GMT)
    elif Format == 14:
        return strftime("%H%M%S", gmtime(time()))
# YYYY-MM-DD (LT)
    elif Format == 15:
        return strftime("%Y-%m-%d", localtime(time()))
# YYYY-MM-DD/DOY Day (LT)
    elif Format == 16:
        return strftime("%Y-%m-%d/%j %A", localtime(time()))
# MM-DD (LT)
    elif Format == 17:
        return strftime("%m-%d", localtime(time()))
# YYYY, MM and DD (LT) returned as ints
    elif Format == 18:
        LT = localtime(time())
        return (LT[0], LT[1], LT[2])
# YYYY-MM-DD/DOY HH:MM:SS Day (LT)
    elif Format == 19:
        return strftime("%Y-%m-%d/%j %H:%M:%S %A", localtime(time()))
# Return GMT-LT difference.
    elif Format == 20:
        Secs = time()
        LT = localtime(Secs)
        GMT = gmtime(Secs)
        return dt2Timeymddhms(-1, LT[0], -1, -1, LT[7], LT[3], LT[4], LT[5])- \
                dt2Timeymddhms(-1, GMT[0], -1, -1, GMT[7], GMT[3], GMT[4], \
                GMT[5])
# YYYY-MM-DD/DOY HH:MM:SS (LT)
    elif Format == 21:
        return strftime("%Y-%m-%d/%j %H:%M:%S", localtime(time()))
# YYYY-MM-DD HH:MM:SS (LT)
    elif Format == 22:
        return strftime("%Y-%m-%d %H:%M:%S", localtime(time()))
    return ""
# END: getGMT




###########################
# BEGIN: getRange(Min, Max)
# LIB:getRange():2008.360
#   Returns the absolute value of the difference between Min and Max.
def getRange(Min, Max):
    if Min <= 0 and Max >= 0:
        return Max+abs(Min)
    elif Min <= 0 and Max <= 0:
        return abs(Min-Max)
    elif Max >= 0 and Min >= 0:
        return Max-Min
# END: getRange




############################
# BEGIN: gpsControlCmd(Mode)
# FUNC:gpsControlCmd():2018.289
def gpsControlCmd(Mode):
    global WorkState
    Status, ListOfDASs = isItOKToStart(3)
    if Status == False:
        return
    WorkState = 100
# Call with None just to control the Stop button
    buttonControl(None, "G", 1)
    formSTATSetStatus("all", "B")
    if cmdPortOpen() == False:
        buttonControl(None, "", 0)
        return
    if Mode == "D":
        StrMode = "NORMAL DUTY CYCLE"
    elif Mode == "C":
        StrMode = "CONTINUOUS"
    elif Mode == "O":
        StrMode = "OFF"
    msgLn(1, "C", "Setting GPS mode to %s..."%StrMode)
    Count = 0
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        formSTATSetStatus(DAS, "C")
        AllOK = True
        Count += 1
        msgLn(1, "C", "   %d. %s: Setting GPS mode..."%(Count, DAS))
        Cmd = rt130CmdFormat(DAS, "GC%s       GC"%Mode)
        cmdPortWrite(DAS, Cmd)
        if DAS == "0000":
            delay(2)
            if WorkState == 999:
                continue
        else:
            Rp = rt130GetRp(1, 5, 2)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", "   No GPS Control command response!", True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
        formSTATSetStatus(DAS, "G")
    cmdPortClose()
    if WorkState == 999 and AllOK == True:
        formSTATSetStatus("working", "Y")
    stoppedMe(WorkState, DAS)
    buttonControl(None, "", 0)
    return
# END: gpsControlCmd




########################
# BEGIN: hexInvert(Hstr)
# FUNC:hexInvert():2008.012
def hexInvert(Hstr):
    Value = ""
    for Digit in Hstr:
        if Digit == "0":
            Value = Value+"F"
            continue
        if Digit == "1":
            Value = Value+"E"
            continue
        if Digit == "2":
            Value = Value+"D"
            continue
        if Digit == "3":
            Value = Value+"C"
            continue
        if Digit == "4":
            Value = Value+"B"
            continue
        if Digit == "5":
            Value = Value+"A"
            continue
        if Digit == "6":
            Value = Value+"9"
            continue
        if Digit == "7":
            Value = Value+"8"
            continue
        if Digit == "8":
            Value = Value+"7"
            continue
        if Digit == "9":
            Value = Value+"6"
            continue
        if Digit == "A":
            Value = Value+"5"
            continue
        if Digit == "B":
            Value = Value+"4"
            continue
        if Digit == "C":
            Value = Value+"3"
            continue
        if Digit == "D":
            Value = Value+"2"
            continue
        if Digit == "E":
            Value = Value+"1"
            continue
        if Digit == "F":
            Value = Value+"0"
            continue
    return Value
# END: hexInvert




#################
# BEGIN: intt(In)
# LIB:intt():2018.257
#   Handles all of the annoying shortfalls of the int() function (vs C).
def intt(In):
    In = str(In).strip()
    if len(In) == 0:
        return 0
# Let the system try it first.
    try:
        return int(In)
    except ValueError:
        Number = ""
        for c in In:
            if c.isdigit():
                Number += c
            elif (c == "-" or c == "+") and len(Number) == 0:
                Number += c
            elif c == ",":
                continue
            else:
                break
        try:
            return int(Number)
        except ValueError:
            return 0
# END: intt




##################################
# BEGIN: isgoodCALS(VarSet, Which)
# FUNC:isgoodCALS():2009.100
def isgoodCALS(VarSet, Which):
    try:
        Value = float(eval("%sAmplitudeVar"%VarSet).get())
        if Value < .01 or Value > 4.00:
            setMsg(VarSet, "RW", \
                    "The signal voltage must be .01 to 3.75 volts.", 2)
            return False
    except ValueError:
        setMsg(VarSet, "RW", "The signal voltage must be a number.", 2)
        return False
    eval("%sAmplitudeVar"%VarSet).set("%.2f"%Value)
# The user doesn't get to control anything else for the other signal types.
    if Which != "SINE" and Which != "STEP" and Which != "NOIS":
        return True
    try:
        Value = int(eval("%sDurationVar"%VarSet).get())
        if Value < 1 or Value > 500:
            setMsg(VarSet, "RW", "The duration must be 1 to 500 seconds.", 2)
            return False
    except ValueError:
        setMsg(VarSet, "RW", "The duration must be an integer.", 2)
        return False
    try:
        Value = int(eval("%sStepIntervalVar"%VarSet).get())
        if Value < 1 or Value > 250:
            setMsg(VarSet, "RW", \
                    "The step interval must be 1 to 250 seconds.", 2)
            return False
    except ValueError:
        setMsg(VarSet, "RW", "The step interval must be an integer.", 2)
        return False
    try:
        Value = int(eval("%sStepWidthVar"%VarSet).get())
        if Value < 1 or Value > 250:
            setMsg(VarSet, "RW", "The step width must be 1 to 250 seconds.", 2)
            return False
    except ValueError:
        setMsg(VarSet, "RW", "The step width must be an integer.", 2)
        return False
    return True
# END: isgoodCALS




###################
# BEGIN: isHex(Str)
# LIB:isHex():2009.026
def isHex(Hex):
    Hex = str(Hex).strip()
    for Char in Hex:
        if Char not in "0123456789abcdefABCDEF":
            return False
    return True
# END: isHex



#############################
# BEGIN: isItOKToStart(Which)
# FUNC:isItOKToStart():2018.289
#   Which = 0 = just checks to see if another action is already running
#           1 = one and only one DAS can be selected
#           2 = one or more selected, but ignores 0000 button
#           3 = uses 0000 button
#   Returns True if it is OK to continue, False if not.
def isItOKToStart(Which):
    if WorkState != 0:
        msgLn(0, "R", "Another action is already running.", True, 2)
        return False, []
# For functions that are only checking to see if something else is running.
    if Which == 0:
        return True, []
# Do this just to get the cursor out of any of the text fields when a command
# is invoked.
    Root.focus_set()
    ListOfDASs = formSTATGetIDs(0)
    HowManySelected = len(ListOfDASs)
    EnumeratedDASs = formSTATGetIDs(2)
    HowManyTotal = len(EnumeratedDASs)
# Must have one and only one selected in the list.
    if Which == 1:
        if HowManySelected != 1:
            msgLn(0, "R", "Enumerate or select only one DAS.", True, 2)
            return False, []
# May have more than one selected, but ignores the 0000 state.
    elif Which == 2:
        if HowManySelected == 0:
            msgLn(0, "R", "Enumerate or select one or more DASs.", True, 2)
            return False, []
# Uses the 0000 state.
    elif Which == 3:
        if OPTPref0000CVar.get() == 0:
            if HowManySelected == 0:
                msgLn(0, "R", \
          "Enumerate, select a DAS, or check the 0000 Pref checkbutton.", \
                        True, 2)
                return False, []
        elif OPTPref0000CVar.get() == 1:
            if HowManySelected == 0 or HowManySelected == HowManyTotal:
                ListOfDASs = ["0000"]
    return True, ListOfDASs
# END: isItOKToStart




########################
# BEGIN: isPROGRunning()
# FUNC:isPROGRunning():2013.180
def isPROGRunning():
    if WorkState != 0:
# Many callers don't print the message, so at least do this for them.
# FINISHME - Change over to using PROGRunning and std err msgs like everyone
# else if I get a chance to rewrite.
        beep(2)
        return (1, "YB", "Some command is running.", 0, "")
    return (0,)
# END: isPROGRunning




###################################################################
# BEGIN: labelEntry2(Sub, Format, LabTx, TTLen, TTTx, TxVar, Width)
# LIB:labelEntry2():2018.236
#   For making simple  Label(): Entry() pairs.
#   Format: 10 = Aligned LEFT-RIGHT, entry field is disabled
#           11 = Aligned LEFT-RIGHT, entry field is normal
#           20 = Aligned TOP-BOTTOM, entry field disabled
#           21 = Aligned TOP-BOTTOM, entry field is normal
#    LabTx = the text of the label. "" LabTx = no label
#    TTLen = The length of the tooltip text
#     TTTx = The text of the label's tooltip. "" TTTx = no tooltip
#    TxVar = Var for the entry field
#    Width = Width to make the field, 0 = .pack(side=LEFT, expand=YES, fill=X)
#            Width should only be 0 if Format is 10 or 11.
def labelEntry2(Sub, Format, LabTx, TTLen, TTTx, TxVar, Width):
    if len(LabTx) != 0:
        if len(TTTx) != 0:
            Lab = Label(Sub, text = LabTx)
            if Format == 11 or Format == 10:
                Lab.pack(side = LEFT)
            elif Format == 21 or Format == 20:
                Lab.pack(side = TOP)
            ToolTip(Lab, TTLen, TTTx)
        else:
# Don't create the extra object if we don't have to.
            if Format == 11 or Format == 10:
                Label(Sub, text = LabTx).pack(side = LEFT)
            elif Format == 21 or Format == 20:
                Label(Sub, text = LabTx).pack(side = TOP)
    if Width == 0:
        Ent = Entry(Sub, textvariable = TxVar)
    else:
        Ent = Entry(Sub, textvariable = TxVar, width = Width+1)
    if Format == 11 or Format == 10:
        if Width != 0:
            Ent.pack(side = LEFT)
        else:
            Ent.pack(side = LEFT, expand = YES, fill = X)
    elif Format == 21 or Format == 20:
        Ent.pack(side = TOP)
    if Format == 10 or Format == 20:
        Ent.configure(state = DISABLED, bg = Clr["D"])
    return Ent
# END: labelEntry2




####################################################
# BEGIN: labelTip(Sub, LText, Side, TTWidth, TTText)
# LIB:labelTip():2006.262
#   Creates a label and assignes the passed ToolTip to it. Returns the Label()
#   widget.
def labelTip(Sub, LText, Side, TTWidth, TTText):
    Lab = Label(Sub, text = LText)
    Lab.pack(side = Side)
    ToolTip(Lab, TTWidth, TTText)
    return Lab
# END: labelTip




###########################
# BEGIN: line2ASCII(InLine)
# LIB:line2ASCII():2019.014
#   Returns the passed line with anything not an ASCII character as a ?.
def line2ASCII(InLine):
    try:
        InLine.encode("ascii")
        return InLine
    except:
        OutLine = ""
        for C in InLine:
            try:
                Value = ord(C)
                OutLine += C
            except:
                OutLine += "?"
        return OutLine
# END: line2ASCII




######################
# BEGIN: listDirs(How)
# LIB:listDirs():2008.200changeo
def listDirs(How):
    msgLn(How, "W", "Current messages directory:\n   %s"%PROGMsgsDirVar.get())
    msgLn(How, "W", "Current work directory:\n   %s"%PROGWorkDirVar.get())
    return
# END: listDirs




#######################
# BEGIN: loadParmsCmd()
# FUNC:loadParmsCmd():2019.044
LastPRMFile = ""

def loadParmsCmd():
    global LastPRMFile
    if len(LastPRMFile) == 0:
        LoadFile = formMYDF(Root, 3, "Load Parameters From File...", \
                PROGWorkDirVar.get(), "", "", ".prm")
    else:
        LoadFile = formMYDF(Root, 3, "Load Parameters From File...", \
                dirname(LastPRMFile), basename(LastPRMFile), "", ".prm")
    if len(LoadFile) == 0:
        msgLn(0, "", "Parameters not loaded.")
        return
    try:
        Fp = open(LoadFile, "r")
    except Exception as e:
        msgLn(1, "M", "Error opening the saved parameters file", True, 3)
        msgLn(1, "M", "   %s"%LoadFile)
        msgLn(0, "M", "   "+str(e))
        return
    formPARMSClearParms()
    Version = Fp.readline().split(":")[1].strip()
    if Version != PFILE_VERSION:
        msgLn(0, "R", "Parameter file version mismatch!", True, 2)
        Fp.close()
        return
    PExpName.set(Fp.readline().split(":")[1].strip())
    PExpComment.set(Fp.readline().split(":")[1].strip())
    for c in arange(1, 1+6):
        eval("PC%dName"%c).set(Fp.readline().split(":")[1].strip())
        eval("PC%dGain"%c).set(int(Fp.readline().split(":")[1].strip()))
        eval("PC%dComment"%c).set(Fp.readline().split(":")[1].strip())
    for d in arange(1, 1+4):
        eval("PDS%dName"%d).set(Fp.readline().split(":")[1].strip())
        eval("PDS%dDisk"%d).set(int(Fp.readline().split(":")[1].strip()))
        eval("PDS%dEther"%d).set(int(Fp.readline().split(":")[1].strip()))
        eval("PDS%dSerial"%d).set(int(Fp.readline().split(":")[1].strip()))
        eval("PDS%dRam"%d).set(int(Fp.readline().split(":")[1].strip()))
        for c in arange(1, 1+6):
            eval("PDS%dChan%d"% \
                    (d,c)).set(int(Fp.readline().split(":")[1].strip()))
        eval("PDS%dSampRate"%d).set(Fp.readline().split(":")[1].strip())
        eval("PDS%dFormat"%d).set(Fp.readline().split(":")[1].strip())
        eval("PDS%dTType"%d).set(int(Fp.readline().split(":")[1].strip()))
        for c in arange(1, 1+6):
            eval("PDS%dTChan%d"% \
                    (d,c)).set(int(Fp.readline().split(":")[1].strip()))
        eval("PDS%dMc"%d).set(Fp.readline().split(":")[1].strip())
        eval("PDS%dPretl"%d).set(Fp.readline().split(":")[1].strip())
        eval("PDS%dRl"%d).set(Fp.readline().split(":")[1].strip())
        eval("PDS%dSta"%d).set(Fp.readline().split(":")[1].strip())
        eval("PDS%dLta"%d).set(Fp.readline().split(":")[1].strip())
        eval("PDS%dHold"%d).set(int(Fp.readline().split(":")[1].strip()))
        eval("PDS%dTr"%d).set(Fp.readline().split(":")[1].strip())
        eval("PDS%dTwin"%d).set(Fp.readline().split(":")[1].strip())
        eval("PDS%dPostl"%d).set(Fp.readline().split(":")[1].strip())
        eval("PDS%dDtr"%d).set(Fp.readline().split(":")[1].strip())
        for i in arange(1, 1+11):
            eval("PDS%dStart%d"% \
                    (d,i)).set(Fp.readline().split(":")[1].strip())
        eval("PDS%dTStrm"%d).set(int(Fp.readline().split(":")[1].strip()))
        eval("PDS%dTlvl"%d).set(Fp.readline().split(":")[1].strip())
        eval("PDS%dLPFreq"%d).set(int(Fp.readline().split(":")[1].strip()))
        eval("PDS%dHPFreq"%d).set(int(Fp.readline().split(":")[1].strip()))
        eval("PDS%dRepInt"%d).set(Fp.readline().split(":")[1].strip())
        eval("PDS%dNumTrig"%d).set(Fp.readline().split(":")[1].strip())
    PAuxMarker.set(Fp.readline().split(":")[1].strip())
    for c in arange(1, 1+16):
        eval("PAuxChan%d"%c).set(int(Fp.readline().split(":")[1].strip()))
    PAuxDisk.set(int(Fp.readline().split(":")[1].strip()))
    PAuxEther.set(int(Fp.readline().split(":")[1].strip()))
    PAuxSerial.set(int(Fp.readline().split(":")[1].strip()))
    PAuxRam.set(int(Fp.readline().split(":")[1].strip()))
    PAuxSPer.set(int(Fp.readline().split(":")[1].strip()))
    PAuxRl.set(Fp.readline().split(":")[1].strip())
    PACGroup.set(int(Fp.readline().split(":")[1].strip()))
    PACEnable.set(int(Fp.readline().split(":")[1].strip()))
    PACCycle.set(Fp.readline().split(":")[1].strip())
    PACThresh.set(Fp.readline().split(":")[1].strip())
    PACAttempt.set(Fp.readline().split(":")[1].strip())
    PACRetry.set(Fp.readline().split(":")[1].strip())
    PACPeriod.set(int(Fp.readline().split(":")[1].strip()))
    PNPort.set(int(Fp.readline().split(":")[1].strip()))
    PNEPortPow.set(int(Fp.readline().split(":")[1].strip()))
    PNEKeep.set(int(Fp.readline().split(":")[1].strip()))
    PNETDelay.set(Fp.readline().split(":")[1].strip())
    PNEIP1.set(Fp.readline().split(":")[1].strip())
    PNEIP2.set(Fp.readline().split(":")[1].strip())
    PNEIP3.set(Fp.readline().split(":")[1].strip())
    PNEIP4.set(Fp.readline().split(":")[1].strip())
    PNEFill.set(int(Fp.readline().split(":")[1].strip()))
    PNEMask.set(Fp.readline().split(":")[1].strip())
    PNEHost.set(Fp.readline().split(":")[1].strip())
    PNEGateway.set(Fp.readline().split(":")[1].strip())
    PNSKeep.set(int(Fp.readline().split(":")[1].strip()))
    PNSTDelay.set(Fp.readline().split(":")[1].strip())
    PNSMode.set(int(Fp.readline().split(":")[1].strip()))
    PNSSpeed.set(int(Fp.readline().split(":")[1].strip()))
    PNSIP1.set(Fp.readline().split(":")[1].strip())
    PNSIP2.set(Fp.readline().split(":")[1].strip())
    PNSIP3.set(Fp.readline().split(":")[1].strip())
    PNSIP4.set(Fp.readline().split(":")[1].strip())
    PNSFill.set(int(Fp.readline().split(":")[1].strip()))
    PNSMask.set(Fp.readline().split(":")[1].strip())
    PNSHost.set(Fp.readline().split(":")[1].strip())
    PNSGateway.set(Fp.readline().split(":")[1].strip())
    PDWrap.set(int(Fp.readline().split(":")[1].strip()))
    PDThresh.set(Fp.readline().split(":")[1].strip())
    PDETDump.set(int(Fp.readline().split(":")[1].strip()))
    PDRetry.set(Fp.readline().split(":")[1].strip())
    PGPSMode.set(Fp.readline().split(":")[1].strip())
    Fp.close()
    msgLn(1, "W", "Parameters loaded from file:")
    msgLn(0, "", "   "+LoadFile)
# Preserve the used file name for a next time
    LastPRMFile = LoadFile
    return
# END: loadParmsCmd




####################
# BEGIN: loopDelay()
# FUNC:loopDelay():2018.289
def loopDelay():
    global WorkState
# Don't even think about it.
    if WorkState == 999 or LoopRVar.get() == "off":
        return
# Sleep for 1/4sec, check, repeat. The Delay is 4x because of the 1/4sec cycle
    for i in arange(0, intt(LoopRVar.get())*4):
        sleep(0.25)
        updateMe(0)
# Jump out as soon as someone wants to
        if WorkState == 999 or LoopRVar.get() == "off":
            break
    return
# END: loopDelay




#################################################################
# BEGIN: makeIP(Format, DASID, IDType, Count, IP1, IP2, IP3, IP4)
# FUNC:makeIP():2018.289
#   Passes back an IP address based on the inputs. IPV4 only.
#   Format = 1: The last two tuples are filled in by using the DAS ID
#            2: The passed IP is "incremented" by Count.
#            3: The passed tuple values are just combined as is.
#   Any blank tuples are given the value of 0.
#   IDType = 16: The DASID is a hex number (Reftek RT130s)
#            10: The DASID is a decimal number (just about everything else)
def makeIP(Format, DASID, IDType, Count, IP1, IP2, IP3, IP4):
    OutIP = ""
    if Format == 1:
        if IDType == 16:
            OutIP = IP1.strip()+"."+IP2.strip()+"."+ \
                    "%d."%int(DASID[0:2], 16)+ \
                    "%d"%int(DASID[2:5], 16)
        elif IDType == 10:
            OutIP = IP1.strip()+"."+IP2.strip()+"."+ \
                    "%d."%int(intt(DASID)/256)+ \
                    "%d"%int(intt(DASID)%256)
    elif Format == 2:
# Build an IP based on what we have, then split it apart, do the math, and
# recombine
        for i in arange(1, 1+4):
            if len(eval("IP%d"%i).strip()) == 0:
                OutIP = OutIP+"0."
            else:
                OutIP = OutIP+eval("IP%d"%i).strip()+"."
        Tups = OutIP[:len(OutIP)-1].split(".")
        ITups = []
        for S in Tups:
            ITups.append(int(S))
        ITups[3] += Count
# Carry any 1 through the tuples
        for i in (3,2,1):
           if ITups[i] > 255:
                ITups[i] -= 256
                ITups[(i-1)] += 1
        if ITups[0] > 255:
            OutIP = ""
        else:
            OutIP = "%d.%d.%d.%d"%(ITups[0], ITups[1], ITups[2], ITups[3])
    elif Format == 3:
        for i in arange(1, 1+4):
            if len(eval("IP%d"%i).strip()) == 0:
                OutIP = OutIP+"0."
            else:
                OutIP = OutIP+eval("IP%d"%i).strip()+"."
        return OutIP[:len(OutIP)-1]
    return OutIP
# END: makeIP




###########################
# BEGIN: makePrintable(Str)
# LIB:makePrintable():2019.136
def makePrintable(Str):
    Ret = ""
    for C in Str:
        if C >= " " and C <= "z":
            Ret += C
        else:
            Ret += "_"
    return Ret
#####################
# BEGIN: makeHex(Str)
# FUNC:makeHex():2019.136
def makeHex(Str):
    Ret = ""
    ASCII = ""
    if len(Str) > 0:
        Count = 0
        for C in Str:
            if Count%10 == 0:
                if ASCII != "":
                    Ret += "  | %s |"%ASCII
                if Count != 0:
                    Ret += "\n%02d:"%Count
                else:
                    Ret += "%02d:"%Count
                ASCII = ""
            Ret += " %02X"%ord(C)
            if C >= " " and C <= "z":
                ASCII += C
            else:
                ASCII += " "
            Count += 1
    return Ret
# END: makePrintable




########################
# BEGIN: massCenterCmd()
# FUNC:massCenterCmd():2018.288
def massCenterCmd():
    global WorkState
    Cmd = ""
    Status, ListOfDASs = isItOKToStart(3)
    if Status == False:
        return
    WorkState = 100
    buttonControl("MASSCENT", "G", 1)
    formSTATSetStatus("all", "B")
    Chans = CalChansRVar.get()
    if cmdPortOpen() == False:
        buttonControl("MASSCENT", "", 0)
        return
    Count = 0
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        if DAS != "0000":
            Answer = formMYD(Root, (("Send", LEFT, "send"), \
                    ("Stop", LEFT, "stop")), "stop", "", "Next!", \
                    "Send mass center command to DAS %s?"%DAS)
            if Answer == "stop":
                WorkState = 999
                continue
            updateMe(0)
        formSTATSetStatus(DAS, "C")
        Count += 1
        msgLn(1, "C", "%d. %s: Mass Center Channels %s... (%s)"% \
              (Count, DAS, Chans, getGMT(0)))
        if Chans == "123":
            Cmd = rt130CmdFormat(DAS, "SK1MSK")
        elif Chans == "456":
            Cmd = rt130CmdFormat(DAS, "SK2MSK")
        cmdPortWrite(DAS, Cmd)
# The command response comes when the pulse ends (~7 sec), so we won't look for
# any response to this command. If the DASs don't get the command the person
# watching the test equipment will notice. Delay a little to make it look like
# something is happening.
        if DAS == "0000":
            delay(2)
        else:
            delay(.5)
        formSTATSetStatus(DAS, "G")
    cmdPortClose()
    if WorkState == 999:
        formSTATSetStatus("working", "Y")
        msgLn(1, "Y", "Some DASs may still be working.")
    stoppedMe(WorkState, DAS)
    buttonControl("MASSCENT", "", 0)
    return
# END: massCenterCmd




########################
# BEGIN: memoryTestCmd()
# FUNC:memoryTestCmd():2018.289
def memoryTestCmd():
    global WorkState
    Status, ListOfDASs = isItOKToStart(2)
    if Status == False:
        return
    Answer = formMYD(Root, (("Continue", LEFT, "cont"), ("Stop", LEFT, \
            "stop")), "stop", "YB", "As Good As Gone.", \
            "Testing the memory will wipe out everything in RAM. Are you sure you want to continue or do you want to stop?")
    if Answer == "stop":
        return
    WorkState = 100
    buttonControl("MEMTEST", "G", 1)
    formSTATSetStatus("all", "B")
    if cmdPortOpen() == False:
        buttonControl("MEMTEST", "", 0)
        return
    msgLn(1, "C", "Starting memory test... (%s)"%getGMT(0))
# The command will be sent by this section to all selected DASs without
# waiting for a response, then we will monitor for 3 minutes.
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        formSTATSetStatus(DAS, "C")
        Cmd = rt130CmdFormat(DAS, "MTRMMT")
        cmdPortWrite(DAS, Cmd)
# Sending the commands too quickly made it likely that more than one DAS
# would respond at the end of the test at the same time (with 15 DASs). This
# delay helped to make that less likely.
        sleep(1.0)
    if WorkState == 999:
        formSTATSetStatus("working", "Y")
        msgLn(1, "Y", "Some DASs may still be testing.")
        msgLn(0, "Y", "Stopped.")
    else:
        if len(ListOfDASs) == 1:
            msgLn(1, "W", "   Start test command sent...")
        else:
            msgLn(1, "W", "   Start test commands sent...")
# Everyone should finish by about 2m30s (small memory ones in about 1m10s and
# big memory ones in 2m25s). We'll enter a monitoring loop and wait for about
# 3m15s.
        msgLn(1, "W", \
                "   Monitoring (will stop in about 3 minutes or less)...")
        StartTime = getGMT(8)
        while 1:
            if WorkState == 999:
                break
            if len(ListOfDASs) == 0:
                break
            if getGMT(8)-StartTime > 195:
                break
            FoundDASs = []
# I'm not looking for any DAS in particular here I'm just looping through
# here the same number of times as there are DASs remaining to answer.
            for DAS in ListOfDASs:
                if WorkState == 999:
                    break
                Rp = rt130GetRp(0, 1, 0)
                if WorkState == 999:
                    break
                if len(Rp) == 0:
                    continue
# If the length is not right it probably means that more than one responded
# at the same time. We'll hear about them at the end of the test.
                if len(Rp) > 32:
                    msgLn(1, "Y", "   Bad response size. Response collision?")
                else:
                    RpDAS = Rp[2:2+4]
# Remove DASs from the list after we have scanned for them all this time
# through.
                    FoundDASs.append(RpDAS)
                    Status = Rp[14:14+2]
                    if Status == "00":
                        formSTATSetStatus(RpDAS, "G")
                        msgLn(1, "G", "   %s: Passed."%RpDAS)
                    elif Status == "01":
                        formSTATSetStatus(RpDAS, "Y")
                        msgLn(1, "Y", "   %s: Invalid request."%RpDAS)
                    elif Status == "03":
                        Addr = Rp[16:16+8]
                        formSTATSetStatus(RpDAS, "R")
                        msgLn(1, "R", "   %s: Failed. Address: %s"%(RpDAS, \
                                Addr))
                    else:
                        formSTATSetStatus(RpDAS, "R")
                        msgLn(1, "R", "   %s: Unknown status: %s"%(RpDAS, \
                                Status))
            for FoundDAS in FoundDASs:
                ListOfDASs.remove(FoundDAS)
    cmdPortClose()
    if WorkState == 999:
        formSTATSetStatus("working", "Y")
        msgLn(1, "Y", "Some DASs may still be working.")
        msgLn(0, "Y", "Stopped.")
    else:
        for DAS in ListOfDASs:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   %s: No memory test command response!"%DAS)
        msgLn(0, "", "Done.")
        if OPTDoneBeepCVar.get() != 0:
            beep(1)
    buttonControl("MEMTEST", "", 0)
    return
# END: memoryTestCmd




###############################
# BEGIN: messageEntry(e = None)
# LIB:messageEntry():2018.247
PROGMessageEntryVar = StringVar()
PROGSetups += ["PROGMessageEntryVar", ]

def messageEntry(e = None):
    Message = PROGMessageEntryVar.get().strip()
# Don't change the focus or erase what the user entered since they may want to
# do something like make multiple similar entries in one sitting. Add the day
# and time by default so all of the messages can be extracted and looked at in
# time order from a whole experiment.
    if len(Message) != 0:
        msgLn(0, "", "USER: "+getGMT(0)[5:-3]+": "+Message.strip())
    else:
        beep(1)
    return
###################################
# BEGIN: messageEntryExt(InMessage)
# FUNC:messageEntryExt():2018.247
#   For other functions to use to make USER entries.
def messageEntryExt(InMessage):
    if len(InMessage) != 0:
        msgLn(0, "", "USER: "+getGMT(0)[5:-3]+": "+InMessage.strip())
    else:
        beep(1)
    return
############################
# BEGIN: messageEntryClear()
# FUNC:messageEntryClear():2012.103
def messageEntryClear():
    PROGMessageEntryVar.set("")
# We'll assume the next thing the user will want to do is type something in.
    PROGEnt["MM"].focus_set()
    return
# END: messageEntry




#####################
# BEGIN: monitorCmd()
# FUNC:monitorCmd():2018.289
PROGFrm["MON"] = None
PROGCan["MON"] = None
MONSb = None

def monitorCmd():
    global MONSb
    global WorkState
    Status, ListOfDASs = isItOKToStart(2)
    if Status == False:
        return
    WorkState = 100
    buttonControl("MONITOR", "G", 1)
    formSTATSetStatus("all", "B")
    if cmdPortOpen() == False:
        buttonControl("MONITOR", "", 0)
        return
    Count = 0
# Clear out anything from a previous run.
    if PROGFrm["MON"] is not None:
        LFrm = PROGFrm["MON"]
        LCan = PROGCan["MON"]
        LCan.delete(ALL)
        LCan.configure(scrollregion = (0, 0, 0, 0))
# The plot window's starting point.
        GY = PROGPropFontHeight
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        formSTATSetStatus(DAS, "C")
        AllOK = True
        Count += 1
        msgLn(1, "", "%d. %s: Monitor testing... (%s)"%(Count, DAS, getGMT(0)))
# Get the parm status to find out which data streams we'll be working on.
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "W", "   Getting parameter status...")
        Cmd = rt130CmdFormat(DAS, "SSPR              SS")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   No Parameter Status request response!", True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
        Chans = intt(Rp[32:32+2])
        DSs = intt(Rp[34:34+1])
        DStreams = mrDash(Rp[40+Chans:40+Chans+DSs])
# Check to make sure there even are any data streams programmed.
        if DStreams == "--------":
            formSTATSetStatus(DAS, "R")
            msgLn(1, "R", "   No data streams programmed.", True, 2)
# Just go on to the next DAS.
            continue
        d = 0
# Clear this each data stream. The monitor points will be collected here by
# monitorDo(), so the channels can be plotted in an overlay-type graph
# following all of the channel graphs.
        StreamData = []
# For the overlay plot.
        DSMin = maxInt
        DSMax = -maxInt
# Get the current stream parameters from the DAS so we know what to check.
        for Check in DStreams:
            if WorkState == 999 or AllOK == False:
                break
            d += 1
# If the position is a dash then go on to the next data stream.
            if Check == "-":
                continue
            if OPTSummaryModeCVar.get() == 0:
                msgLn(1, "W", "   Getting data stream %d parameters..."%d)
            Cmd = rt130CmdFormat(DAS, "PRPD"+padR(str(d), 2)+"PR")
            cmdPortWrite(DAS, Cmd)
# No Lag time here since this is a request for info.
            Rp = rt130GetRp(1, 10, 0)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", \
                        "   No Data Stream Parameters request response!", \
                        True, 3)
                AllOK = False
                continue
# Stop everything if this routine thinks something is wrong.
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# If we come to a ds that has no channels assigned to it just make a note of it
# and go on to the next data stream.
            if Rp[42:42+6] == "      ":
                msgLn(1, "", "   No channels assigned to data stream %d."%d)
                continue
# Check that the sample rate is high enough for this test, if not go on to
# next data stream.
            SR = Rp[58:58+4].strip()
            if intt(SR) < 20:
                msgLn(1, "Y", \
                        "   Data stream %d sample rate is below 20sps."%d)
                continue
            Chans = Rp[42:42+6]
            for c in arange(1, 1+6):
                if Chans[(c-1)] == " ":
                    continue
# Only do the channels' whose checkbuttons are selected.
                if eval("Chan%dMonCVar"%c).get() == 0:
                    if OPTSummaryModeCVar.get() == 0:
                        msgLn(1, "", "      Skipping channel %d..."%c)
                    continue
# Do the monitor command and get the data.
                Ret = monitorDo(DAS, d, c, StreamData)
                if WorkState == 999:
                    break
                if Ret[0] != 0:
                    msgLn(1, Ret[1], Ret[2], True, Ret[3])
                    formSTATSetStatus(DAS, Ret[1])
                    AllOK = False
                    break
# Create the window if it has not already been done.
                if PROGFrm["MON"] is None:
                    LFrm = PROGFrm["MON"] = Toplevel(Root)
                    LFrm.protocol("WM_DELETE_WINDOW", Command(formClose, \
                            "MON"))
                    LFrm.title("Monitor Test Results")
                    LFrm.iconname("MonTest")
                    Sub = Frame(LFrm, borderwidth = 2, relief = SUNKEN)
                    LCan = PROGCan["MON"] = Canvas(Sub, bg = Clr["B"], \
                            bd = 0, width = 700, height = 500)
                    LCan.pack(side = LEFT, fill = BOTH, expand = YES)
                    MONSb = Scrollbar(Sub, orient = VERTICAL, \
                            command = LCan.yview)
                    MONSb.pack(side = RIGHT, fill = Y, expand = NO)
                    LCan.configure(yscrollcommand = MONSb.set)
                    Sub.pack(side = TOP, fill = BOTH, expand = YES)
# The plot window's starting point.
                    GY = PROGPropFontHeight
# StreamData: [[DAS, DS, Chan, [data points]], <next channel>...]
                DataPoints = StreamData[len(StreamData)-1][3]
                DMin = min(DataPoints)
# Remember the min/max of all of the channels for the combined plots.
                if DMin < DSMin:
                    DSMin = DMin
                DMax = max(DataPoints)
                if DMax > DSMax:
                    DSMax = DMax
                DRange = getRange(DMin, DMax)
                DMid = DMin+(DRange/2)
                msgLn(1, "", "         Max: ", False)
                msgLn(1, "G", "%s"%fmti(DMax), False)
                msgLn(1, "", "   Min: ", False)
                msgLn(1, "G", "%s"%fmti(DMin))
# The mid point should be somewhere around 0 (that's what the offset test is
# for), so check for that.
                if DMid > -100 and DMid < 100:
                    msgLn(1, "", "         Mid: ", False)
                    msgLn(1, "G", "%s"%fmti(DMid), False)
                    msgLn(1, "", "   Range: ", False)
                    msgLn(1, "G", "%s"%fmti(DRange))
                else:
                    msgLn(1, "", "         Mid: ", False)
                    msgLn(1, "Y", "%s"%fmti(DMid), False)
                    msgLn(1, "", "   Range: ", False)
                    msgLn(1, "G", "%s"%fmti(DRange))
                LCan.create_text(10, GY, fill = "white", anchor = "w", \
  text = "DAS: %s  DS: %d  Ch: %d   Max: %s   Min: %s   Mid: %s   Range: %s"% \
                        (DAS, d, c, fmti(DMin), fmti(DMax), fmti(DMid), \
                        fmti(DRange)))
                GY += PROGPropFontHeight
                Points = 160
                Y1 = GY+(100-100*.5)
                LCan.create_line(10, Y1, 10+160*4+20, Y1, fill = "blue", \
                        width = 1)
                Cmd = []
                for i in arange(0, Points-1):
                    X1 = 20+i*4
                    X2 = 20+(i*4)+4
                    if DRange != 0:
                        Y1 = GY+(100-100*(DataPoints[i]-DMin)/DRange)
                        Y2 = GY+(100-100*(DataPoints[i+1]-DMin)/DRange)
                    else:
                        Y1 = GY+50
                        Y2 = GY+50
                    Cmd += X1, Y1, X2, Y2
                LCan.create_line(Cmd, fill = "green", width = 1)
# Adjust the display. There may not be anything on the display so try (which
# at this point would be odd or a bug).
                try:
                    Top = LCan.bbox(ALL)[3]+10
                except TypeError:
                    Top = 10
# The RIGHT value (100) can be anything since there is no horizontal scroll
# bar.
                LCan.configure(scrollregion = (0, 0, 100, Top))
                if MONSb.get()[1] == 1.0:
                    LCan.yview_moveto(1.0)
                updateMe(0)
                GY += 100+PROGPropFontHeight*1.5
        if WorkState == 999 or AllOK == False:
            continue
# ----- OVERLAYED GRAPHS ----
# If StreamData has more than one graph's data in it...
        if len(StreamData) > 1:
# Plot the first half (80 points) of the returned data from each channel all
# on one plot just as the data came from the DAS (the phases may not match).
            getAColor(0)
            Chan = 0
            for Stream in StreamData:
                Chan += 1
                GColor = getAColor(7)[0]
                LCan.create_text(10, GY, fill = GColor, \
                        text = "DAS: %s  Channel: %d"%(DAS, Chan), \
                        anchor = "w")
                GY += PROGPropFontHeight
            getAColor(0)
            for Stream in StreamData:
                DataPoints = Stream[3]
                GColor = getAColor(7)[0]
                DMin = DSMin
                DMax = DSMax
                DRange = getRange(DMin, DMax)
                DMid = DMin+(DRange/2)
                Points = 160
                Cmd = []
                for i in arange(0, Points-1):
                    X1 = 20+i*4
                    X2 = 20+(i*4)+4
                    if DRange != 0:
                        Y1 = GY+(100-100*(DataPoints[i]-DMin)/DRange)
                        Y2 = GY+(100-100*(DataPoints[i+1]-DMin)/DRange)
                    else:
                        Y1 = GY+50
                        Y2 = GY+50
                    Cmd += X1, Y1, X2, Y2
                LCan.create_line(Cmd, fill = GColor, width = 1)
                try:
                    Top = LCan.bbox(ALL)[3]+10
                except TypeError:
                    Top = 10
                LCan.configure(scrollregion = (0, 0, 100, Top))
                if MONSb.get()[1] == 1.0:
                    LCan.yview_moveto(1.0)
                updateMe(0)
            GY += 100+PROGPropFontHeight*1.5
# ----- ZOOMED-IN ADJUSTED GRAPH -----
# Now go through each channels' points and find the first peak to try and
# plot the channels in phase.
            getAColor(0)
            Chan = 0
            PeakIndexes = {}
            for Stream in StreamData:
                DataPoints = Stream[3]
# This will be the Stream index of the highest point.
                PeakPoint = 0
                LastPoint = DataPoints[0]
# If the data starts off going down the loop below will trigger right away, so
# wait until we've seen the data values start back up before looking for the
# peak using the flag HitBottom.
                HitBottom = False
                for Point in DataPoints[1:]:
                   if Point < LastPoint:
                       if HitBottom == True:
# We've started down.
                           break
                   elif Point > LastPoint and HitBottom == False:
                       HitBottom = True
                   LastPoint = Point
                   PeakPoint += 1
# Just in case the input is some kind of flat line or something (which, from a
# testing point of view, would be kind of silly). This will keep the for i loop
# below from running out of DataPoints.
                   if PeakPoint > 40:
                       break
                Chan += 1
                PeakIndexes[Chan] = PeakPoint
                GColor = getAColor(7)[0]
                LCan.create_text(10, GY, fill = GColor, \
                        text = "DAS: %s  Channel: %d   Peak sample: %d"%(DAS, \
                        Chan, PeakPoint), anchor = "w")
                GY += PROGPropFontHeight
            getAColor(0)
            Chan = 0
            for Stream in StreamData:
                DataPoints = Stream[3]
                Chan += 1
                DataPoints = DataPoints[PeakIndexes[Chan]:]
                GColor = getAColor(7)[0]
                DMin = DSMin
                DMax = DSMax
                DRange = getRange(DMin, DMax)
                DMid = DMin+(DRange/2)
                Points = 40
                Cmd = []
                for i in arange(0, Points-1):
                    X1 = 20+i*16
                    X2 = 20+(i*16)+16
                    if DRange != 0:
                        Y1 = GY+(240-240*(DataPoints[i]-DMin)/DRange)
                        Y2 = GY+(240-240*(DataPoints[i+1]-DMin)/DRange)
                    else:
                        Y1 = GY+120
                        Y2 = GY+120
                    Cmd += X1, Y1, X2, Y2
                LCan.create_line(Cmd, fill = GColor, width = 1)
                try:
                    Top = LCan.bbox(ALL)[3]+10
                except TypeError:
                    Top = 10
                LCan.configure(scrollregion = (0, 0, 100, Top))
                if MONSb.get()[1] == 1.0:
                    LCan.yview_moveto(1.0)
                updateMe(0)
            GY += 240+PROGPropFontHeight*1.5
        formSTATSetStatus(DAS, "G")
    cmdPortClose()
    if WorkState == 999 and AllOK == True:
# Use "working" here since DAS no longer points at the DAS that we were working
# on.
        formSTATSetStatus("working", "Y")
# This may just be a normal stop.
        msgLn(1, "Y", "DAS may still be working.")
    stoppedMe(WorkState, "")
    buttonControl("MONITOR", "", 0)
    return
# END: monitorCmd




##########################################################################
# BEGIN: msgLn(Open, Color, What, Newline = True, Bell = 0, Update = True)
# LIB:msgLn():2018.247
#   Open is passed on to writeLine():
#      0 = close the messages file after writing the line
#      1 = leave the file open after writing the line (there's more to come)
#      9 = don't write anything to the messages file, just to the display
#
#   Instead of opening, writing, and closing the .msg file for everything
#   written to the messages section, just build everything in MSGLNLast,
#   and write to the file (via writeFile) only whole lines.
#   Requires MSGLNAlwaysScroll to be set.
#       True = Always scroll the messages section.
#       False = Only scroll when the scrollbar is all of the way "down".
MSGLNAlwaysScroll = False
MSGLNLast = ""

def msgLn(Open, Color, What, Newline = True, Bell = 0, Update = True):
    global MSGLNLast
# A standard error return may be passed and it will be decoded here.
    if isinstance(Color, (tuple, list)):
        What = Color[2]
        Bell = Color[3]
        Color = Color[1]
    if len(Color) == 0 and len(What) == 0 and Newline == False and \
            Bell == 0 and Update == False:
        MMsg.delete("0.0", END)
        MMsg.tag_delete(*MMsg.tag_names())
        MMsg.update()
        MSGLNLast = ""
        return
    if len(Color) != 0:
# This mouthful keeps user mouse clicks in the messages section from screwing
# up the tag indexes. END always points to the "next" line so we have to back
# that up one to get the "current" line which INSERT (which used to be used)
# doesn't always point to if the user has clicked somewhere in the Text widget
# (which I thought was covered by the value in CURSOR).
        IdxS = MMsg.index(str(intt(MMsg.index(END))-1)+".end")
# The Color[0] allows us to pass BgFg Color values like "YB" without incident.
        MMsg.tag_config(IdxS, foreground = Clr[Color[0]])
        MMsg.insert(END, What, IdxS)
        MSGLNLast += What
    else:
        MMsg.insert(END, What)
        MSGLNLast += What
    if Newline == True:
        MMsg.insert(END, "\n")
        if Open != 9:
            writeFile(Open, "MSG", MSGLNLast+"\n")
        MSGLNLast = ""
    if Newline == True:
# Always scroll on a newline.
        if MSGLNAlwaysScroll == True:
            MMsg.see(END)
# Only scroll when the scroll bar is at the bottom.
        elif MMVSb.get()[1] == 1.0:
            MMsg.see(END)
# Instead of updating everything.
    if Update == True:
        MMsg.update()
    if Bell != 0:
        beep(Bell)
    return
# END: msgLn




###################################
# BEGIN: monitorDo(DAS, d, c, Data)
# FUNC:monitorDo():2019.188
#   Gets called once for each DS/Channel.
def monitorDo(DAS, d, c, Data):
    global WorkState
    if WorkState == 999:
# There is no message. The caller will catch this.
        return (0, )
    if OPTSummaryModeCVar.get() == 0:
        msgLn(1, "W", "      Monitoring channel %d..."%c)
# Send the command to start the DAS recording.
    Cmd = rt130CmdFormat(DAS, "DM"+" "+str(d)+padR(str(c), 2)+"DM")
    cmdPortWrite(DAS, Cmd)
# It takes at least this long before the data start coming back.
    delay(8)
    if WorkState == 999:
        return (0, )
    GotAllData = 0
    DataPoints = []
# We'll give the DAS 20x3sec to send all of the data.
    Chances = 0
    if OPTSummaryModeCVar.get() == 0:
        msgLn(1, "", "         Waiting for data...")
# Delay is 0.0 below so reading the 8 or so sentences of monitor data will be
# quick (all 8 started coming in very quickly beginning with the 2.3.0 version
# of the RT130 firmware -- in fact all communications sped up with that
# version).
    while 1:
        Rp = rt130GetRp(0, 3, 0)
        if len(Rp) == 0:
            Chances += 1
# Keep the user calm if something should go wrong.
            if Chances%3 == 0:
                msgLn(1, "", "         Waiting...")
            if Chances == 7:
                msgLn(1, "", "         I feel like I'm in Casablanca...")
            if Chances == 10:
                break
            else:
                continue
        if checkRp(DAS, Rp) == False:
            WorkState = 999
            break
# At this point we should have a complete line so step through it and pick out
# the data points.
        DigPS = int(Rp[12:12+1])
        Samples = int(Rp[20:20+2], 16)
        Range = 1
        if DigPS == 4:
            Range = pow(2, 16)
        elif DigPS == 6:
            Range = pow(2, 24)
        elif DigPS == 8:
            Range = pow(2, 32)
        HRange = Range/2
        for dp in arange(22, 22+Samples*DigPS, DigPS):
# TESTING - 2004AUG03
# I think that static may have caused one of the data lines to be too short
# and it tried to interpret the ending "DM" and the CRC as a data point.
# long() didn't like that. We'll try to trap that condition here and see
# what is going on.
# 2019JUL09 - Must have been static. Have not seen this since.
            try:
                if PROG_PYVERS == 2:
                    Value = long(Rp[dp:dp+DigPS], 16)
                elif PROG_PYVERS == 3:
                    Value = int(Rp[dp:dp+DigPS], 16)
            except ValueError:
                msgLn(1, "R", "Data error!", True, 2)
                msgLn(1, "R", \
                      "DigPS %d  Samples %d  Range %f"%(DigPS, Samples, Range))
                msgLn(1, "R", \
                      "dp %s length %d"%(Rp[dp:dp+DigPS], len(Rp)))
                break
            if Value > HRange:
                Value = Value-Range
            DataPoints.append(Value)
            GotAllData = 1
# If we have received and processed all of the response packets then jump out.
        if int(Rp[16:16+1]) == int(Rp[17:17+1]):
            GotAllData = 2
            break
    if GotAllData == 2:
        Data.append([DAS, d, c, DataPoints])
        return (0, )
    elif GotAllData == 1:
        return (1, "M", "Only got %d data points of the monitor data!"% \
                len(DataPoints), 3)
    elif GotAllData == 0:
        return (1, "M", "Failed to get any of the monitor data!", 3)
# END: monitorDo




#########################
# BEGIN: mrDash(Incoming)
# FUNC:mrDash():2008.012
#   Inserts a dash character in any blank spot.
def mrDash(Incoming):
    Outgoing = ""
    for C in Incoming:
        if C == " ":
            Outgoing = Outgoing+"-"
        else:
            Outgoing = Outgoing+C
    return Outgoing
# END: mrDash




#######################
# BEGIN: notYet(Parent)
# LIB:notYet():2011.216
def notYet(Parent):
   formMYD(Parent, (("(OK)", LEFT, "ok"), ), "ok", "", "Coming Someday?", \
          "This feature is not yet available.", "", 1)
   return
##################################
# BEGIN: underConstruction(Parent)
# FUNC:underConstruction():2011.216
def underConstruction(Parent):
   formMYD(Parent, (("(OK)", LEFT, "ok"), ), "ok", "", "I'm Workin' On It!", \
   "This feature may or may not work/be finished yet. Use at your own risk.", \
           "", 1)
   return
# END: notYet




####################
# BEGIN: offsetCmd()
# FUNC:offsetCmd():2019.080
#   This whole sequence of commands seems to be a bit sensitive to the length
#   of the time delays.
def offsetCmd():
    global WorkState
    Status, ListOfDASs = isItOKToStart(2)
    if Status == False:
        return
# For converting the values.
    Range = pow(2, 32)
    WorkState = 100
    buttonControl("OFFSET", "G", 1)
    formSTATSetStatus("all", "B")
    if cmdPortOpen() == False:
        buttonControl("OFFSET", "", 0)
        return
    Action = OffSetRVar.get()
    Count = 0
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        formSTATSetStatus(DAS, "C")
        AllOK = True
        Count += 1
        if Action == "set":
           msgLn(1, "", "%d. %s: Setting offsets... (%s)"%(Count, DAS, \
                   getGMT(0)))
        else:
           msgLn(1, "", "%d. %s: Checking offsets... (%s)"%(Count, DAS, \
                   getGMT(0)))
        Channels = "      "   # One character for each channel.
# Get the parm status to find out which data streams we'll be working on.
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "W", "   Getting parameter status...")
        Cmd = rt130CmdFormat(DAS, "SSPR              SS")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   No Parameter Status request response!", True, 3)
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
        Chans = intt(Rp[32:32+2])
        DSs = intt(Rp[34:34+1])
        DStreams = mrDash(Rp[40+Chans:40+Chans+DSs])
# Check to make sure there even are any data streams programmed.
        if DStreams == "--------":
            formSTATSetStatus(DAS, "R")
            msgLn(1, "R", "   No data streams programmed.", True, 2)
            continue
        d = 0
# Get the current stream parameters from the DAS so we know what to check.
        for Check in DStreams:
            if WorkState == 999 or AllOK == False:
                break
            d += 1
# If the position is a dash then go on to the next data stream.
            if Check == "-":
                continue
            if OPTSummaryModeCVar.get() == 0:
                msgLn(1, "W", "   Getting data stream %d parameters..."%d)
            Cmd = rt130CmdFormat(DAS, "PRPD"+padR(str(d), 2)+"PR")
            cmdPortWrite(DAS, Cmd)
# No Lag, info request.
            Rp = rt130GetRp(1, 10, 0)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", \
                        "   No Data Stream Parameters request response!", \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# If we come to a ds that has no channels assigned to it just make a note of it
# and go on to the next data stream.
            if Rp[42:42+6] == "      ":
                msgLn(1, "", "   No channels assigned to data stream %d."%d)
                continue
# We don't need to know which channels have been assigned to a DS because the
# returned data will just contain data for each channel that is assigned.
# However, check that the sample rate is high enough for this test.
            SR = Rp[58:58+4].strip()
            if intt(SR) < 20:
                formSTATSetStatus(DAS, "Y")
                msgLn(1, "Y", \
                        "   Data stream %d sample rate is below 20sps."%d)
                continue
# Get the relative offsets and print them before changing anything.
            if OPTSummaryModeCVar.get() == 0:
                msgLn(1, "W", "   Checking...")
            Cmd = rt130CmdFormat(DAS, "DO"+padR(str(d), 1)+"R"+padR("3", 2)+ \
                    "DO")
            cmdPortWrite(DAS, Cmd)
            Rp = rt130GetRp(1, 7, 2)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", \
                        "   No Data Offset check command response!", True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
            StatColor = "G"
            for i in arange(0, intt(Rp[14:14+2])):
                Start = 16+(i*10)
                if PROG_PYVERS == 2:
                    Value = long(hexInvert(Rp[Start+2:Start+10]), 16)
                elif PROG_PYVERS == 3:
                    Value = int(hexInvert(Rp[Start+2:Start+10]), 16)
                if Value > Range/2:
                    Value = Value-Range
                msgLn(1, "", "      Ch"+Rp[Start+1:Start+2]+" diff: ", False)
                if Value > -100 and Value < 100:
                    msgLn(1, "G", str(Value))
                else:
                    msgLn(1, "R", str(Value))
                    StatColor = "R"
# Check to see if we will be continuing or if we are just checking values.
            if Action == "check":
                formSTATSetStatus(DAS, StatColor)
                continue
# Send the command.
            if OPTSummaryModeCVar.get() == 0:
                msgLn(1, "W", "   Calculating offsets...")
# Have the DAS calculate the absolute value. Let it integrate for 3 seconds.
            Cmd = rt130CmdFormat(DAS, "DO"+padR(str(d), 1)+"A"+padR("3", 2)+ \
                    "DO")
            cmdPortWrite(DAS, Cmd)
            Rp = rt130GetRp(1, 7, 2)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", "   No Data Offset command response!", True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# Begin constructing the command to set the values.
            Settings = []
            Cmd = "SO"+" "+"A"+Rp[14:14+2]
            for i in arange(0, intt(Rp[14:14+2])):
                Start = 16+(i*10)
                Cmd = Cmd+Rp[Start:Start+2]+hexInvert(Rp[Start+2:Start+10])
# Keep the new numbers so they can be printed out below.
                if PROG_PYVERS == 2:
                    Settings.append(long(Rp[Start+2:Start+10], 16))
                elif PROG_PYVERS == 3:
                    Settings.append(int(Rp[Start+2:Start+10], 16))
            Cmd = Cmd+"SO"
            Cmd = rt130CmdFormat(DAS, Cmd)
# Send the new numbers back to the DAS.
            if OPTSummaryModeCVar.get() == 0:
                msgLn(1, "C", "   Sending new values...")
            cmdPortWrite(DAS, Cmd)
# We have to give the DAS a bit of extra time after this command to finish up.
            Rp = rt130GetRp(1, 7, 5)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", "   No Set Offset command response!", True, 3)
                msgLn(1, "Y", "   Check offsets manually!")
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# Now get the relative offsets for something to print out.
            if OPTSummaryModeCVar.get() == 0:
                msgLn(1, "W", "   Checking...")
            Cmd = rt130CmdFormat(DAS, "DO"+padR(str(d), 1)+"R"+padR("5", 2)+ \
                    "DO")
            cmdPortWrite(DAS, Cmd)
            Rp = rt130GetRp(1, 7, 5)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", "   No Data Offset check command response!", \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
            for i in arange(0, intt(Rp[14:14+2])):
                Start = 16+(i*10)
                if PROG_PYVERS == 2:
                    Value = long(hexInvert(Rp[Start+2:Start+10]), 16)
                elif PROG_PYVERS == 3:
                    Value = int(hexInvert(Rp[Start+2:Start+10]), 16)
                if Value > Range/2:
                    Value = Value-Range
                msgLn(1, "", "      Ch"+Rp[Start+1:Start+2]+" Abs: ", False)
                msgLn(1, "G", str(Settings[i]), False)
                msgLn(1, "", "  Off: ", False)
                msgLn(1, "G", str(pow(2,32)-Settings[i]), False)
                msgLn(1, "", "  Diff: ", False)
                if Value > -100 and Value < 100:
                    msgLn(1, "G", str(Value))
                else:
                    msgLn(1, "R", str(Value))
        if WorkState == 999 or AllOK == False:
            continue
# The check routine has already set the colors.
        if Action == "set":
            formSTATSetStatus(DAS, "G")
    cmdPortClose()
    if WorkState == 999 and AllOK == True:
        formSTATSetStatus("working", "Y")
        msgLn(1, "Y", "DAS may still be working.")
    stoppedMe(WorkState, "")
    buttonControl("OFFSET", "", 0)
    return
# END: offsetCmd




###############################
# BEGIN: openFile(Filespec, RW)
# LIB:openFile():2018.344
#   For "text" files. Opens everything as latin-1 so everyone can just get
#   along, or else. No clue what happens if the file is corrupted.
def openFile(Filespec, RW):
# Any exception will just be passed back up.
    if PROG_PYVERS == 2:
        Fp = codecs.open(Filespec, RW, encoding = "latin-1")
    elif PROG_PYVERS == 3:
        Fp = open(Filespec, RW, encoding = "latin-1")
    return Fp
# END: openFile




###############################################
# BEGIN: padR(What, Howmuch, BlankZero = False)
# FUNC:padR():2019.044
#    A little different than using ljust().
def padR(What, Howmuch, BLankZero = False):
    if isinstance(What, anint):
# The caller doesn't want to see "   0", they want "    ".
        if What == 0 and BlankZero == True:
            return "%-*.*s"%(Howmuch, Howmuch, "")
        What = "%d"%What
    return "%-*.*s"%(Howmuch, Howmuch, What)
# END: padR




#########################
# BEGIN: parmsToDiskCmd()
# FUNC:parmsToDiskCmd():2018.289
def parmsToDiskCmd():
    global WorkState
    Status, ListOfDASs = isItOKToStart(2)
    if Status == False:
        return
    WorkState = 100
    buttonControl("WP", "G", 1)
    formSTATSetStatus("all", "B")
    if cmdPortOpen() == False:
        buttonControl("WP", "", 0)
        return
    Count = 0
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        formSTATSetStatus(DAS, "C")
        AllOK = True
        Count += 1
        msgLn(1, "C", "%d. %s: Writing Parameters to disk... (%s)"%(Count, \
                DAS, getGMT(0)))
        Cmd = rt130CmdFormat(DAS, "WPWP")
        cmdPortWrite(DAS, Cmd)
# This can take about 10 seconds with 2 disks to return, but we do want to
# wait for each one to get the status of the write(s) (see below).
        Rp = rt130GetRp(1, 20, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   No Write Parameters command response!", True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
# 2.8.8S just returns WPWP. 2.9.0 returns a 2-byte status about each disk.
        if Rp.find("WPWP") != -1:
            formSTATSetStatus(DAS, "G")
            msgLn(1, "", "   Parameters written.")
        elif Rp.find("WP0") != -1:
            msgLn(1, "", "   Parameters written: D1: ", False)
            if Rp[12:12+2] == "00":
                msgLn(1, "G", "Passed", False)
                RpAllOK = True
            else:
                msgLn(1, "Y", "Failed", False)
                RpAllOK = False
            msgLn(1, "", "  D2: ", False)
            if Rp[14:14+2] == "00":
                msgLn(1, "G", "Passed")
# Don't change RpAllOK here in case it was set to False above.
            else:
                msgLn(1, "Y", "Failed")
                RpAllOK = False
            if RpAllOK == True:
                formSTATSetStatus(DAS, "G")
            else:
                formSTATSetStatus(DAS, "Y")
        else:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   Unknown response.", True, 3)
    cmdPortClose()
    if WorkState == 999 and AllOK == True:
        formSTATSetStatus("working", "Y")
        msgLn(1, "Y", "DAS may still be working.")
    stoppedMe(WorkState, DAS)
    buttonControl("WP", "", 0)
    return
# END: parmsToDiskCmd




###########################################
# BEGIN: popDirDevice(Var, Title, e = None)
# FUNC:popDirDevice():2013.282
def popDirDevice(Var, Title, e = None):
    Xx = Root.winfo_pointerx()
    Yy = Root.winfo_pointery()
    PMenu = Menu(Root, font = PROGOrigPropFont, tearoff = 0, bg = Clr["D"], \
            bd = 2, relief = GROOVE)
    PMenu.add_command(label = "Select A Device...", \
            command = Command(popDirDeviceCmd, Var, Title))
    PMenu.tk_popup(Xx, Yy)
    return
####################################
# BEGIN: popDirDeviceCmd(Var, Title)
# FUNC:popDirDeviceCmd():2019.130
def popDirDeviceCmd(Var, Title):
    Dir = sep+"dev"+sep
    Device = formMYDF(Root, 0, Title, Dir, "")
# Don't change it if the return is blank.
    if Device != "":
        Var.set(Device)
    return
# END: popDirDevice




######################
# BEGIN: pref0000Cmd()
# FUNC:pref0000Cmd():2009.314
def pref0000Cmd():
    if OPTPref0000CVar.get() == 0:
        msgLn(0, "", "0000 Preference turned off.")
    else:
        msgLn(0, "", "0000 Preference turned on.")
    return
# END: pref0000Cmd




##########################
# BEGIN: progQuitter(Save)
# FUNC:progQuitter():2014.076
def progQuitter(Save):
    writeFile(0, "MSG", "==== Program stopped "+getGMT(0)+" ====\n")
    if Save == True:
        Ret = savePROGSetups()
        if Ret[0] != 0:
            formMYD(Root, (("(OK)", TOP, "ok"),), "ok", Ret[1], "Oops.", \
                    Ret[2])
    exit()
# END: progQuitter




###########################################################
# BEGIN: readFileLinesRB(Filespec, Strip = False, Bees = 0)
# LIB:readFileLinesRB():2019.239
#   This is the same idea as readFileLines(), but the Filespec is passed and
#   the file is treated as a 'hostile text file' that may be corrupted. This
#   can be used any time, but was developed for reading Reftek LOG files which
#   can be corrupted, or just have a lot of extra junk added by processing
#   programs.
#   This is based on the method used in rt72130ExtractLogData().
#   The return value is (0, [lines]) if things go OK, or a standard error
#   message if not, except the "e" of the exception also will be returned
#   after the passed Filespec, so the caller can construct their own error
#   message if needed.
#   If Bees is not 0 then that many bytes of the file will be returned and
#   converted to lines. If Bees is less than the size of the file the last
#   line will be discarded since it's a good bet that it will be a partial
#   line.
#   Weird little kludge: Setting Bees to -42 tells the function that Filespec
#   contains a bunch of text and that it should be split up into lines and
#   returned just as if the text had come from reading a file.
#   If Filespec has "http:" or "https:" in it then urlopen() will be used.
def readFileLinesRB(Filespec, Strip = False, Bees = 0):
    Lines = []
    if Bees != -42:
        try:
            if Filespec.find("http:") == -1 and Filespec.find("https:") == -1:
# These should be text files, but there's no way to know if they are ASCII or
# Unicode or garbage, especially if they are corrupted, so open binarially.
                Fp = open(Filespec, "rb")
            else:
                Fp = urlopen(Filespec)
# This will be trouble if the file is huge, but that should be rare. Bees can
# be used if the file is known to be yuge. This should result in one long
# string. This and the "rb" above seems to work on Py2 and 3.
            if Bees == 0:
                Raw = Fp.read().decode("latin-1")
            else:
                Raw = Fp.read(Bees).decode("latin-1")
            Fp.close()
            if len(Raw) == 0:
                return (0, Lines)
        except Exception as e:
            try:
                Fp.close()
            except:
                pass
            return (1, "MW", "%s: Error opening/reading file.\n%s"% \
                    (basename(Filespec), e), 3, Filespec, e)
    else:
        Raw = Filespec
        Filespec = "PassedLines"
# Yes, this is weird. These should be "text" files and in a non-corrupted file
# there should be either all \n or all \r or the same number of \r\n and \n
# and \r. Try and split the file up based on these results.
    RN = Raw.count("\r\n")
    N = Raw.count("\n")
    R = Raw.count("\r")
# Just one line by itself with no delimiter? OK.
    if RN == 0 and N == 0 and R == 0:
        return (0, [Raw])
# Perfect \n. May be the most popular, so we'll check for it first.
    if N != 0 and R == 0 and RN == 0:
        RawLines = Raw.split("\n")
# Perfect \r\n file. We checked for RN=0 above.
    elif RN == N and RN == R:
        RawLines = Raw.split("\r\n")
# Perfect \r.
    elif R != 0 and N == 0 and RN == 0:
        RawLines = Raw.split("\r")
    else:
# There was something in the file, so make a best guess based on the largest
# number. It might be complete crap, but what else can we do?
        if N >= RN and N >= R:
            RawLines = Raw.split("\n")
        elif N >= RN and N >= R:
            RawLines = Raw.split("\r\n")
        elif R >= N and R >= RN:
            RawLines = Raw.split("\n")
# If all of those if's couldn't figure it out.
        else:
            return (1, "RW", "%s: Unrecognized file format."% \
                    basename(Filespec), 2, Filespec)
# If Bees is not 0 then throw away the last line if the file is larger than
# the number of bytes requested.
    if Bees != 0 and Bees < getsize(Filespec):
        RawLines = RawLines[:-1]
# Get rid of trailing empty lines. They can sneak in from various places
# depending on who wrote the file. Do the strip in case there are something
# like leftover \r's when \n was used for splitting.
    while RawLines[-1].strip() == "":
        RawLines = RawLines[:-1]
# It must be all the file had in it.
        if len(RawLines) == 0:
            return (0, Lines)
# If the caller doesn't want anything else then just go through and get rid of
# any trailing spaces, else get rid of all the spaces.
    if Strip == False:
        for Line in RawLines:
            Lines.append(Line.rstrip())
    else:
        for Line in RawLines:
            Lines.append(Line.strip())
    return (0, Lines)
# END: readFileLinesRB




################
# BEGIN: ready()
# LIB:ready():2007.243
# Needs msgLn(), and setMsg().
def ready():
    try:
        msgLn(9, "R", "R", False)
        msgLn(9, "Y", "e", False)
        msgLn(9, "G", "a", False)
        msgLn(9, "C", "d", False)
        msgLn(9, "M", "y", False)
        msgLn(9, "", ".")
    except NameError:
        setMsg("MM", "", "Ready.")
    return
# END: ready




##########################
# BEGIN: receiveParmsCmd()
# FUNC:receiveParmsCmd():2019.044
def receiveParmsCmd():
    global WorkState
    Status, ListOfDASs = isItOKToStart(1)
    if Status == False:
        return
    WorkState = 100
    buttonControl("RECEIVE", "G", 1)
    formSTATSetStatus("all", "B")
    formPARMSClearParms()
    if cmdPortOpen() == False:
        buttonControl("RECEIVE", "", 0)
        return
# OK, so maybe someday more than one DAS will be allowed, so we'll do all of
# this inside of a for loop, but only allow ListOfDASs to contain one DAS
# ...maybe we'll print out the parameter summary for each DAS as it is
# received, or something like that.
    Count = 0
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        formSTATSetStatus(DAS, "C")
        AllOK = True
        Count += 1
        msgLn(1, "", "%d. %s: Receiving parameters... (%s)"%(Count, DAS, \
                getGMT(0)))
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "", "   Station...")
        Cmd = rt130CmdFormat(DAS, "PRPS  PR")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   No Station Parameters request response!", \
                    True, 3)
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
# You have to add 4 to the sending offsets of the parameters described in the
# Reftek documentation (i.e. the experiment name is at [14:14+24] when you
# send the station parameters, but at [18:18+24] when receiving).
        PExpName.set(Rp[18:18+24].strip())
        PExpComment.set(Rp[42:42+40].strip())
# Get the current setup of the DAS to figure out which channels and data
# streams to get.
        Cmd = rt130CmdFormat(DAS, "SSPR              SS")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   No Parameter Status request response!", True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
        Chans = intt(Rp[32:32+2])
        DSs = intt(Rp[34:34+1])
        Channels = mrDash(Rp[40:40+Chans])
        DStreams = mrDash(Rp[40+Chans:40+Chans+DSs])
        for c in arange(1, 1+6):
            if WorkState == 999:
                break
            if Channels[(c-1)] == "-":
                continue
            if OPTSummaryModeCVar.get() == 0:
                msgLn(1, "", "   Channel %d..."%c)
            Cmd = rt130CmdFormat(DAS, "PRPC%d PR"%c)
            cmdPortWrite(DAS, Cmd)
            Rp = rt130GetRp(1, 5, 0)
            if WorkState == 999:
                break
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", \
                        "   No Channel %d Parameters request response!"%c, \
                        True, 3)
                AllOK = False
                break
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                break
            eval("PC%dName"%c).set(Rp[18:18+10].strip())
            eval("PC%dComment"%c).set(Rp[114:114+40].strip())
# The high gain setting in the DAS may be "100" depending on which program
# was used to program it. CHANGEO uses "32".
            Gain = intt(Rp[86:86+4])
            if Gain == 0 or Gain == 1:
                eval("PC%dGain"%c).set(Gain)
            if Gain == 32 or Gain == 100:
                eval("PC%dGain"%c).set(32)
# Go on to the next DAS if something is wrong.
        if WorkState == 999 or AllOK == False:
            continue
        for d in arange(1, 1+4):
            if WorkState == 999:
                break
            if DStreams[(d-1)] == "-":
                continue
            if OPTSummaryModeCVar.get() == 0:
                msgLn(1, "", "   Data stream %d..."%d)
            Cmd = rt130CmdFormat(DAS, "PRPD%d PR"%d)
            cmdPortWrite(DAS, Cmd)
            Rp = rt130GetRp(1, 5, 0)
            if WorkState == 999:
                break
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", \
                        "   No DS%d Parameters request response!"%d, True, 3)
                AllOK = False
                break
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                break
            eval("PDS%dName"%d).set(Rp[18:18+16].strip())
            if Rp[34:34+1] != " ":
                eval("PDS%dRam"%d).set(1)
            if Rp[35:35+1] != " ":
                eval("PDS%dDisk"%d).set(1)
            if Rp[36:36+1] != " ":
                eval("PDS%dEther"%d).set(1)
            if Rp[37:37+1] != " ":
                eval("PDS%dSerial"%d).set(1)
            for c in arange(1, 1+6):
                if Rp[41+c:41+c+1] != " ":
                    eval("PDS%dChan%d"%(d,c)).set(1)
            eval("PDS%dSampRate"%d).set(Rp[58:58+4].strip())
            eval("PDS%dFormat"%d).set(Rp[62:62+2].strip())
            if Rp[64:64+4].strip() == "CON":
                eval("PDS%dTType"%d).set(1)
                eval("PDS%dRl"%d).set(Rp[68:68+8].strip())
                eval("PDS%dStart1"%d).set(Rp[76:76+14].strip())
            elif Rp[64:64+4].strip() == "CRS":
                eval("PDS%dTType"%d).set(2)
                eval("PDS%dTStrm"%d).set(intt(Rp[68:68+2]))
                eval("PDS%dPretl"%d).set(Rp[70:70+8].strip())
                eval("PDS%dRl"%d).set(Rp[78:78+8].strip())
            elif Rp[64:64+4].strip() == "EVT":
                eval("PDS%dTType"%d).set(3)
                for c in arange(1, 1+6):
                    if Rp[67+c:67+c+1] != " ":
                        eval("PDS%dTChan%d"%(d,c)).set(1)
                eval("PDS%dMc"%d).set(Rp[84:84+2].strip())
                eval("PDS%dTwin"%d).set(Rp[86:86+8].strip())
                eval("PDS%dPretl"%d).set(Rp[94:94+8].strip())
                eval("PDS%dPostl"%d).set(Rp[102:102+8].strip())
                eval("PDS%dRl"%d).set(Rp[110:110+8].strip())
                eval("PDS%dSta"%d).set(Rp[126:126+8].strip())
                eval("PDS%dLta"%d).set(Rp[134:134+8].strip())
                eval("PDS%dTr"%d).set(Rp[150:150+8].strip())
                eval("PDS%dDtr"%d).set(Rp[158:158+8].strip())
                Holding = Rp[166:166+4].strip()
                if Holding == "OFF":
                    eval("PDS%dHold"%d).set(0)
                elif Holding == "ON":
                    eval("PDS%dHold"%d).set(1)
                Pass = Rp[170:170+4].strip()
                if Pass == "OFF":
                    eval("PDS%dLPFreq"%d).set(0)
                elif Pass == "0":
                    eval("PDS%dLPFreq"%d).set(1)
                elif Pass == "12":
                    eval("PDS%dLPFreq"%d).set(2)
                Pass = Rp[174:174+4].strip()
                if Pass == "OFF":
                    eval("PDS%dHPFreq"%d).set(0)
                elif Pass == "0":
                    eval("PDS%dHPFreq"%d).set(1)
                elif Pass == "0.1":
                    eval("PDS%dHPFreq"%d).set(2)
                elif Pass == "12":
                    eval("PDS%dHPFreq"%d).set(3)
            elif Rp[64:64+4].strip() == "EXT":
                eval("PDS%dTType"%d).set(4)
                eval("PDS%dPretl"%d).set(Rp[68:68+8].strip())
                eval("PDS%dRl"%d).set(Rp[76:76+8].strip())
            elif Rp[64:64+4].strip() == "LEV":
                eval("PDS%dTType"%d).set(5)
                eval("PDS%dTlvl"%d).set(Rp[68:68+8].strip())
                eval("PDS%dPretl"%d).set(Rp[76:76+8].strip())
                eval("PDS%dRl"%d).set(Rp[84:84+8].strip())
                Pass = Rp[92:92+4].strip()
                if Pass == "OFF":
                    eval("PDS%dLPFreq"%d).set(0)
                elif Pass == "0":
                    eval("PDS%dLPFreq"%d).set(1)
                elif Pass == "12":
                    eval("PDS%dLPFreq"%d).set(2)
                Pass = Rp[96:96+4].strip()
                if Pass == "OFF":
                    eval("PDS%dHPFreq"%d).set(0)
                elif Pass == "0":
                    eval("PDS%dHPFreq"%d).set(1)
                elif Pass == "0.1":
                    eval("PDS%dHPFreq"%d).set(2)
                elif Pass == "12":
                    eval("PDS%dHPFreq"%d).set(3)
            elif Rp[64:64+4].strip() == "TIM":
                eval("PDS%dTType"%d).set(6)
                eval("PDS%dStart1"%d).set(Rp[68:68+14].strip())
                eval("PDS%dRepInt"%d).set(Rp[82:82+8].strip())
                eval("PDS%dNumTrig"%d).set(Rp[90:90+4].strip())
                eval("PDS%dRl"%d).set(Rp[102:102+8].strip())
            elif Rp[64:64+4].strip() == "TML":
                eval("PDS%dTType"%d).set(7)
                for i in arange(1, 1+11):
                    eval("PDS%dStart%d"%(d,i)).set \
                                (Rp[54+(i*14):54+(i*14)+14].strip())
                eval("PDS%dRl"%d).set(Rp[102:102+8].strip())
            elif Rp[64:64+4].strip() == "VOT":
                eval("PDS%dTType"%d).set(8)
        if WorkState == 999 or AllOK == False:
            continue
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "", "   Auxiliary data...")
        Cmd = rt130CmdFormat(DAS, "PRPA  PR")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", \
                    "   No Auxiliary Data Parameters request response!", \
                    True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
        PAuxMarker.set(Rp[16:16+2].strip())
# Channels are offset 18-33 (16 channels).
        for i in arange(1, 1+16):
            if Rp[17+i:17+i+1] != " ":
                eval("PAuxChan%d"%i).set(1)
        Value = intt(Rp[34:34+8].strip())
        PAuxSPer.set(intt(Rp[34:34+8].strip()))
        PAuxRl.set(Rp[44:44+8].strip())
        if Rp[52:52+1] != " ":
            PAuxRam.set(1)
        if Rp[53:53+1] != " ":
            PAuxDisk.set(1)
        if Rp[54:54+1] != " ":
            PAuxEther.set(1)
        if Rp[55:55+1] != " ":
            PAuxSerial.set(1)
# FINISHME - add Differential Control stuff for PA.
# These are a bit weird. Ask for both, but only use the one whose parameters
# are not blank (blanked by the 'erase parameters' command when the parameters
# were sent).
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "", "   Auto-centering...")
        Cmd = rt130CmdFormat(DAS, "PRPQ1 PR")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", \
                    "   No Auto-centering Parameters request response!", \
                    True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
# Just look at a portion of the parameter space.
        if len(Rp[15:15+10].strip()) == 0:
# Our DASs only have two places to physically connect sensors, so we only have
# to check "sensors" 1 and 2. Since 1 didn't yield anything...
            Cmd = rt130CmdFormat(DAS, "PRPQ2 PR")
            cmdPortWrite(DAS, Cmd)
            Rp = rt130GetRp(1, 15, 0)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", \
                        "   No Auto-centering Parameters request response!", \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# By this point we have all of the parameters we'll ever have.
        if len(Rp[15:15+10].strip()) != 0:
            if Rp[16:16+1] == "1":
                PACGroup.set(123)
            elif Rp[16:16+1] == "2":
                PACGroup.set(456)
            if Rp[17:17+1] == " ":
                PACEnable.set(0)
            elif Rp[17:17+1] == "Y":
                PACEnable.set(1)
            PACPeriod.set(intt(Rp[18:18+4]))
            PACCycle.set(Rp[22:22+2].strip())
            PACThresh.set(Rp[24:24+4].strip())
            PACAttempt.set(Rp[28:28+2].strip())
            PACRetry.set(Rp[30:30+2].strip())
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "", "   Disks...")
        Cmd = rt130CmdFormat(DAS, "PRPZ  PR")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   No Disk Parameters request response!", True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
        PDThresh.set(Rp[22:22+2].strip())
        if Rp[28:28+1] == "N":
            PDWrap.set(0)
        else:
            PDWrap.set(1)
        PDRetry.set(Rp[32:32+2].strip())
        if Rp[20:20+1] == "Y":
            PDETDump.set(1)
        else:
            PDETDump.set(0)
# PN1 is for the Ethernet parameters and PN2 is for the serial port parameters
# so we will need to ask for both. How to set PNPort to reflect which one
# the user wants to use remains a mystery, except to generate some warning
# messages when the parameters are summary'ed or sent to the DAS. If the
# parameters were sent with CHANGEO the port not in use may not have an IP
# address set. We'll check for that down below.
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "", "   Network...")
        Cmd = rt130CmdFormat(DAS, "PRPN1 PR")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   No Network Parameters request response!", \
                    True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
# Get what should be the IP address, check to see if there are any "."
# (as in 1.2.3.4), if not create an IP (0.0.0.0) then split it up into the
# individual tuples and .set() the vars.
        RawIP = Rp[18:18+15].replace(" ", "")
        try:
            RawIP.index(".")
        except ValueError:
            RawIP = "0.0.0.0"
        IPTups = RawIP.split(".")
        for i in arange(1, 1+4):
            eval("PNEIP%d"%i).set(IPTups[(i-1)].strip())
# Since we are getting the parameters from one DAS set this.
        PNEFill.set(3)
        if Rp[33:33+1] == "P":
            PNEPortPow.set(1)
        elif Rp[33:33+1] == "T":
            PNEPortPow.set(2)
# Just so checking the parameters won't squawk when all zeros for everything
# are received from a completely wiped DAS.
        else:
            PNEPortPow.set(2)
        PNEMask.set(Rp[34:34+16].replace(" ", ""))
        PNEHost.set(Rp[50:50+16].replace(" ", ""))
        PNEGateway.set(Rp[66:66+16].replace(" ", ""))
        PNETDelay.set(Rp[90:90+2].strip())
        if Rp[82:82+1] == "K":
            PNEKeep.set(1)
        elif Rp[82:82+1] == "T":
            PNEKeep.set(2)
# Make this assumption since the allowed delay for tossing is 2-99 mins.
        elif PNSTDelay.get() == "0":
            PNSKeep.set(1)
# Serial port parms.
        Cmd = rt130CmdFormat(DAS, "PRPN2 PR")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   No Network Parameters request response!", \
                    True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
# Work on the IP address.
        RawIP = Rp[18:18+15].replace(" ", "")
        try:
            RawIP.index(".")
        except ValueError:
            RawIP = "0.0.0.0"
        IPTups = RawIP.split(".")
        for i in arange(1, 1+4):
            eval("PNSIP%d"%i).set(IPTups[(i-1)].strip())
# Since we are getting the parameters from one DAS set this.
        PNSFill.set(3)
        PNSMask.set(Rp[34:34+16].replace(" ", ""))
        PNSHost.set(Rp[50:50+16].replace(" ", ""))
        PNSGateway.set(Rp[66:66+16].replace(" ", ""))
        PNSTDelay.set(Rp[90:90+2].strip())
        if Rp[82:82+1] == "K":
            PNSKeep.set(1)
        elif Rp[82:82+1] == "T":
            PNSKeep.set(2)
# Make this assumption since the allowed delay for tossing is 2-99 mins.
        elif PNSTDelay.get() == "0":
            PNSKeep.set(1)
        Mode = Rp[83:83+1]
        if Mode == "D":
            PNSMode.set(1)
        elif Mode == "A":
            PNSMode.set(2)
        elif Mode == "F":
            PNSMode.set(3)
        else:
            PNSMode.set(1)
        PNSSpeed.set(intt(Rp[84:84+6]))
# Try to determine what to set PNPort to by just looking at the first tuple
# If blanks are sent to a DAS for the IP address, then the DAS is reset,
# older versions of the firmware will return 000.000.000.000, while newer
# versions will return 0.0.0.0.  If the first tuple of both addresses is 0 or
# 000 we'll guess that the addresses are not real and set the intened port to
# "none".  If you don't do a reset the DASs will return just blanks for the
# IP addresses.
        if (len(PNEIP1.get()) == 0 and len(PNSIP1.get()) == 0) or \
                (PNEIP1.get() == "0" and PNSIP1.get() == "0") or \
                (PNEIP1.get() == "000" and PNSIP1.get() == "000"):
            PNPort.set(0)
        elif len(PNEIP1.get()) != 0 and len(PNSIP1.get()) == 0:
            PNPort.set(1)
        elif len(PNEIP1.get()) == 0 and len(PNSIP1.get()) != 0:
            PNPort.set(2)
        elif len(PNEIP1.get()) != 0 and \
                (PNSIP1.get() == "0" and PNSIP2.get() == "0" and \
                PNSIP3.get() == "0" and PNSIP4.get() == "0"):
            PNPort.set(1)
        else:
            PNPort.set(0)
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "", "   GPS control...")
            Cmd = rt130CmdFormat(DAS, "SSXC              SS")
            cmdPortWrite(DAS, Cmd)
            Rp = rt130GetRp(1, 5, 0)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", \
                        "   No External Clock Status command response!", \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
            PGPSMode.set(Rp[85:85+1])
        formSTATSetStatus(DAS, "G")
    cmdPortClose()
    if WorkState == 999 and AllOK == True:
        formSTATSetStatus("working", "Y")
    stoppedMe(WorkState, "")
# Just in case the parms window is up.
    formPARMSShowEvents()
    buttonControl("RECEIVE", "", 0)
    return
# END: receiveParmsCmd




#######################
# BEGIN: remAllDASCmd()
# FUNC:remAllDASCmd():2009.026
def remAllDASCmd():
# If some other action is running don't start this one.
    if isItOKToStart(0) == False:
        return
# Clear out all DASs we had in the list.
    formSTATClearID("0")
    return
# END: remAllDASCmd




#######################
# BEGIN: resetProgCmd()
# FUNC:resetProgCmd():2018.289
#   Resets the color of all of the buttons and clears WorkState (via
#   buttonControl) after something has gone wrong. Maybe.
def resetProgCmd():
    Keys = list(PROGButs.keys())
    for Key in Keys:
# The Stop button gets turned off every call.
        buttonControl(Key, "", 0)
    cmdPortClose()
    msgLn(0, "R", "Reset commands.")
    return
# END: resetProgCmd




#####################################
# BEGIN: rt130CmdFormat(Who, CmdBody)
# LIB:rt130CmdFormat():2006.297
#   Builds a command string with the proper control characters, DAS ID, and
#   CRC. The caller must have pre-formatted the body of the command. Returns
#   a command string ready for sending to the DAS.
def rt130CmdFormat(Who, CmdBody):
    Length = len(CmdBody)+6
#    ToCRC = Who+"%04d"%Length+CmdBody
    ToCRC = "%s%04d%s"%(Who, Length, CmdBody)
    CRC = "%04X"%rt130CRCMe(ToCRC, False)
#    Cmd = "%c"%0x84+"%c"%0x00+ToCRC+CRC+"%c"%0x0D+"%c"%0x0A
    Cmd = "%c%c%s%s%c%c"%(0x84, 0x00, ToCRC, CRC, 0x0D, 0x0A)
    return Cmd
# END: rt130CmdFormat




#################################
# BEGIN: rt130CRCMe(String, Flip)
# LIB:rt130CRCMe():2008.151
#   Returns the CRC-16/Reftek value of the passed character string.
#   Doing a lookup is faster than doing math.
crc16tab = (0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241, \
            0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440, \
            0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40, \
            0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841, \
            0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40, \
            0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41, \
            0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641, \
            0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040, \
            0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240, \
            0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441, \
            0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41, \
            0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840, \
            0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41, \
            0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40, \
            0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640, \
            0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041, \
            0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240, \
            0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441, \
            0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41, \
            0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840, \
            0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41, \
            0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40, \
            0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640, \
            0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041, \
            0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241, \
            0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440, \
            0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40, \
            0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841, \
            0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40, \
            0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41, \
            0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641, \
            0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040)

def rt130CRCMe(String, Flip):
    CCRC = 0xFFFF
    for Char in String:
        Temp = CCRC^(ord(Char))
        CCRC = (CCRC >> 8)^crc16tab[(Temp & 0xFF)]
    if Flip == True:
        CCRC = (CCRC & 0x00FF)*256+(CCRC & 0xFF00)/256
    return CCRC
# END: rt130CRCMe




#######################################
# BEGIN: rt130GetRp(Delay, Tryfor, Lag)
# LIB:rt130GetRp():2019.130
#   Delay = amount to delay before beginning to try to read the port
#           (this was really only added because the Monitor command response
#           comes back in about 8 sentences, so pausing for 1 second at the
#           top of the reading loop made it take much longer to draw the
#           graphs than necessary).
#   Tryfor = trys for that many seconds to wait for a response to come back.
#   Lag = lags for that many seconds after a response has been received.
def rt130GetRp(Delay, Tryfor, Lag):
    Rp = ""
    Rp1 = ""
    if Delay != 0:
        delay(Delay)
        if WorkState == 999:
            return ""
    for i in arange(0, Tryfor):
        Rp1 = cmdPortReadline()
        if OPTDebugCVar.get() == 1:
            stdout.write("D<%d<< %s\n"%(i, makePrintable(Rp1)))
            stdout.write("%s\n\n"%makeHex(Rp1))
        Rp = Rp+Rp1
# If we have a complete sentence then leave otherwise sleep a bit and try
# again.
        if len(Rp) != 0 and ord(Rp[0]) == 0x85 and \
                ord(Rp[len(Rp)-1]) == 0x0A:
            break
        if WorkState == 999:
            return ""
        delay(1)
    if OPTDebugCVar.get() == 1:
        print("Tries: %d"%i)
# If Rp has something in it, but it is not a complete sentence (i.e. we
# ran out of TryFor loop above) then pause a bit and try once more.
    if len(Rp) != 0 and ord(Rp[len(Rp)-1]) != 0x0A:
        if OPTDebugCVar.get() == 1:
            stdout.write("FU1<<<< %s\n"%makePrintable(Rp1))
            stdout.write("%s\n\n"%makeHex(Rp1))
        delay(2)
        Rp1 = cmdPortReadline()
        if OPTDebugCVar.get() == 1:
            stdout.write("FU2<<<< %s\n"%makePrintable(Rp1))
            stdout.write("%s\n\n"%makeHex(Rp1))
        Rp = Rp+Rp1
# Most commands will want to delay some amount of time after a response (the
# DAS may still be working). This is just a nice place to do it instead of
# putting a bunch of delay() lines throughout the code.
    if len(Rp) != 0 and ord(Rp[0]) == 0x85 and ord(Rp[len(Rp)-1]) == 0x0A:
        if Lag != 0:
            delay(Lag)
    else:
# If the sentence is incomplete after all of this don't return anything.
        if OPTDebugCVar.get() == 1:
            stdout.write("G<<<< \"%s\"\n"%makePrintable(Rp))
            stdout.write("%s\n\n"%makeHex(Rp))
        Rp = ""
    return Rp
# END: rt130GetRp




######################################
# BEGIN: rtnPattern(In, Upper = False)
# LIB:rtnPattern():2006.114
def rtnPattern(In, Upper = False):
    Rtn = ""
    for c in In:
        if c.isdigit():
            Rtn += "0"
        elif c.isupper():
            Rtn += "A"
        elif c.islower():
            Rtn += "a"
        else:
            Rtn += c
# So the A/a chars will always be A, so the caller knows what to look for.
    if Upper == True:
        return Rtn.upper()
    return Rtn
# END: rtnPattern




#######################
# BEGIN: saveParmsCmd()
# FUNC:saveParmsCmd():2019.070
def saveParmsCmd():
    global LastPRMFile
    if len(LastPRMFile) == 0:
        SaveFile = formMYDF(Root, 3, "Save Parameters To File As...", \
                PROGWorkDirVar.get(), "", "", ".prm")
    else:
        SaveFile = formMYDF(Root, 3, "Save Parameters To File As...", \
                dirname(LastPRMFile), basename(LastPRMFile), "", ".prm")
    if len(SaveFile) == 0:
        msgLn(0, "", "Parameters not saved.")
        return
    try:
        Fp = open(SaveFile, "w")
    except Exception as e:
        msgLn(1, "M", "Error opening the parameter file", True, 3)
        msgLn(1, "M", "   %s"%SaveFile)
        msgLn(0, "M", "   "+str(e))
        return
# Let the user know what problems are being saved
    checkParms(False)
    Fp.write("Version: %s\n"%PFILE_VERSION)
    Fp.write("ExpName: %s\n"%PExpName.get())
    Fp.write("ExpComment: %s\n"%PExpComment.get())
    for c in arange(1, 1+6):
        Fp.write("C%dName: %s\n"%(c, eval("PC%dName"%c).get()))
        Fp.write("C%dGain: %d\n"%(c, eval("PC%dGain"%c).get()))
        Fp.write("C%dComment: %s\n"%(c, eval("PC%dComment"%c).get()))
    for d in arange(1, 1+4):
        Fp.write("DS%dName: %s\n"%(d, eval("PDS%dName"%d).get()))
        Fp.write("DS%dDisk: %d\n"%(d, eval("PDS%dDisk"%d).get()))
        Fp.write("DS%dEther: %d\n"%(d, eval("PDS%dEther"%d).get()))
        Fp.write("DS%dSerial: %d\n"%(d, eval("PDS%dSerial"%d).get()))
        Fp.write("DS%dRam: %d\n"%(d, eval("PDS%dRam"%d).get()))
        for c in arange(1, 1+6):
            Fp.write("DS%dChan%d: %d\n"% \
                    (d, c, eval("PDS%dChan%d"%(d,c)).get()))
        Fp.write("DS%dSampRate: %s\n"%(d, eval("PDS%dSampRate"%d).get()))
        Fp.write("DS%dFormat: %s\n"%(d, eval("PDS%dFormat"%d).get()))
        Fp.write("DS%dTType: %d\n"%(d, eval("PDS%dTType"%d).get()))
        for c in arange(1, 1+6):
            Fp.write("DS%dTChan%d: %d\n"% \
                    (d, c, eval("PDS%dTChan%d"%(d,c)).get()))
        Fp.write("DS%dMc: %s\n"%(d, eval("PDS%dMc"%d).get()))
        Fp.write("DS%dPreTrigLen: %s\n"%(d, eval("PDS%dPretl"%d).get()))
        Fp.write("DS%dRecLen: %s\n"%(d, eval("PDS%dRl"%d).get()))
        Fp.write("DS%dSTA: %s\n"%(d, eval("PDS%dSta"%d).get()))
        Fp.write("DS%dLTA: %s\n"%(d, eval("PDS%dLta"%d).get()))
        Fp.write("DS%dLTAHold: %d\n"%(d, eval("PDS%dHold"%d).get()))
        Fp.write("DS%dTrigRatio: %s\n"%(d, eval("PDS%dTr"%d).get()))
        Fp.write("DS%dTrigWin: %s\n"%(d, eval("PDS%dTwin"%d).get()))
        Fp.write("DS%dPostTrigLen: %s\n"%(d, eval("PDS%dPostl"%d).get()))
        Fp.write("DS%dDeTrigRatio: %s\n"%(d, eval("PDS%dDtr"%d).get()))
        for i in arange(1, 1+11):
            Fp.write("DS%dStart%d: %s\n"% \
                    (d, i, eval("PDS%dStart%d"%(d,i)).get()))
        Fp.write("DS%dTrigStrm: %d\n"%(d, eval("PDS%dTStrm"%d).get()))
        Fp.write("DS%dTrigLevel: %s\n"%(d, eval("PDS%dTlvl"%d).get()))
        Fp.write("DS%dLPFreq: %d\n"%(d, eval("PDS%dLPFreq"%d).get()))
        Fp.write("DS%dHPFreq: %d\n"%(d, eval("PDS%dHPFreq"%d).get()))
        Fp.write("DS%dRepeatInt: %s\n"%(d, eval("PDS%dRepInt"%d).get()))
        Fp.write("DS%dNumTrig: %s\n"%(d, eval("PDS%dNumTrig"%d).get()))
    Fp.write("AuxMark: %s\n"%PAuxMarker.get())
    for c in arange(1, 1+16):
        Fp.write("AuxChan%d: %d\n"%(c, eval("PAuxChan%d"%c).get()))
    Fp.write("AuxDisk: %d\n"%PAuxDisk.get())
    Fp.write("AuxEthernet: %d\n"%PAuxEther.get())
    Fp.write("AuxSerial: %d\n"%PAuxSerial.get())
    Fp.write("AuxRAM: %d\n"%PAuxRam.get())
    Fp.write("AuxFreq: %d\n"%PAuxSPer.get())
    Fp.write("AuxRecLen: %s\n"%PAuxRl.get())
    Fp.write("ACGroup: %d\n"%PACGroup.get())
    Fp.write("ACEnable: %d\n"%PACEnable.get())
    Fp.write("ACCycle: %s\n"%PACCycle.get())
    Fp.write("ACThreshold: %s\n"%PACThresh.get())
    Fp.write("ACAttempt: %s\n"%PACAttempt.get())
    Fp.write("ACRetry: %s\n"%PACRetry.get())
    Fp.write("ACPeriod: %d\n"%PACPeriod.get())
    Fp.write("NPort: %d\n"%PNPort.get())
    Fp.write("NPortPower: %d\n"%PNEPortPow.get())
    Fp.write("NEthernetKeep: %d\n"%PNEKeep.get())
    Fp.write("NEthernetTossDelay: %s\n"%PNETDelay.get())
    Fp.write("NEthernetIP1:%s\n"%PNEIP1.get())
    Fp.write("NEthernetIP2:%s\n"%PNEIP2.get())
    Fp.write("NEthernetIP3:%s\n"%PNEIP3.get())
    Fp.write("NEthernetIP4:%s\n"%PNEIP4.get())
    Fp.write("NEthernetFill: %d\n"%PNEFill.get())
    Fp.write("NEthernetMask: %s\n"%PNEMask.get())
    Fp.write("NEthernetHost: %s\n"%PNEHost.get())
    Fp.write("NEhternetGateway: %s\n"%PNEGateway.get())
    Fp.write("NSerialKeep: %d\n"%PNSKeep.get())
    Fp.write("NSerialTossDelay: %s\n"%PNSTDelay.get())
    Fp.write("NSerialMode: %d\n"%PNSMode.get())
    Fp.write("NSerialSpeed: %d\n"%PNSSpeed.get())
    Fp.write("NSerialIP1: %s\n"%PNSIP1.get())
    Fp.write("NSerialIP2: %s\n"%PNSIP2.get())
    Fp.write("NSerialIP3: %s\n"%PNSIP3.get())
    Fp.write("NSerialIP4: %s\n"%PNSIP4.get())
    Fp.write("NSerialFill: %d\n"%PNSFill.get())
    Fp.write("NSerialMask: %s\n"%PNSMask.get())
    Fp.write("NSerialHost: %s\n"%PNSHost.get())
    Fp.write("NSerialGateway: %s\n"%PNSGateway.get())
    Fp.write("DWrap: %d\n"%PDWrap.get())
    Fp.write("DDumpThreshold: %s\n"%PDThresh.get())
    Fp.write("DEventTrailerDump: %d\n"%PDETDump.get())
    Fp.write("DRetry: %s\n"%PDRetry.get())
    Fp.write("GPSMode: %s\n"%PGPSMode.get())
    Fp.close()
    msgLn(1, "W", "Parameters saved to file:")
    msgLn(0, "", "   "+SaveFile)
# Preserve the used file name for a next time
    LastPRMFile = SaveFile
    return
# END: saveParmsCmd




####################################################
# BEGIN: sendChanParms(DAS, Ch, Name, Gain, Comment)
# FUNC:sendChanParms():2018.289
def sendChanParms(DAS, Ch, Name, Gain, Comment):
    global WorkState
# If the gain is not set then just return. This channel has not been
# "activated".
    if Gain.get() == 0:
        return True
    if OPTSummaryModeCVar.get() == 0:
        msgLn(1, "C", "   Channel %d..."%Ch)
    Cmd = rt130CmdFormat(DAS, "PC"+padR("%d"%Ch, 2)+padR(Name.get(), 10)+ \
            padR("", 58)+padR(str(Gain.get()), 4)+padR("", 24)+ \
            padR(Comment.get(), 40)+"PC")
    cmdPortWrite(DAS, Cmd)
    if DAS == "0000":
        delay(3)
        if WorkState == 999:
            return False
    else:
        Rp = rt130GetRp(1, 5, 2)
        if len(Rp) == 0:
            msgLn(1, "M", "   No Channel %d Parameters command response!"%Ch, \
                    True, 3)
            return False
        if checkRp(DAS, Rp) == False:
            WorkState = 999
            return False
    return True
# END: sendChanParms




############################
# BEGIN: sendDSParms(DAS, d)
# FUNC:sendDSParms():2018.289
def sendDSParms(DAS, d):
    global WorkState
# If the trigger type is set to Not Used we will consider the data stream.
# to not be "active"
    if eval("PDS%dTType"%d).get() == 0:
        return True
    if OPTSummaryModeCVar.get() == 0:
        msgLn(1, "C", "   Data stream %d..."%d)
# Get everything ready for the rt130CmdFormat call.
    Dest = ""
    for s in ("Ram", "Disk", "Ether", "Serial"):
        if eval("PDS%d%s"%(d,s)).get() != 0:
            Dest = Dest+"Y"
        else:
            Dest = Dest+" "
    Chans = ""
    for c in arange(1, 1+6):
        if eval("PDS%dChan%d"%(d,c)).get() != 0:
            Chans = Chans+"Y"
        else:
            Chans = Chans+" "
    Form = eval("PDS%dFormat"%d).get()
# This should never happen since the checkbuttons get checked.
    if len(Form) == 0:
        Form = "  "
# This part of the command is common to all trigger types.
    Cmd = "PD"+padR("%d"%d, 2)+padR(eval("PDS%dName"%d).get(), 16)+ \
            Dest+padR("", 4)+padR(Chans, 16)+ \
            padR(eval("PDS%dSampRate"%d).get(), 4)+Form
# Add on the parts of the command for each trigger type.
# CON
    if eval("PDS%dTType"%d).get() == 1:
        Cmd = Cmd+padR("CON", 4)+padR(eval("PDS%dRl"%d).get(), 8)+ \
                padR(eval("PDS%dStart1"%d).get(), 14)+padR("", 140)
# CRS
    elif eval("PDS%dTType"%d).get() == 2:
        Cmd = Cmd+padR("CRS", 4)+padR("%d"%eval("PDS%dTStrm"%d).get(), 2)+ \
                padR(eval("PDS%dPretl"%d).get(), 8)+ \
                padR(eval("PDS%dRl"%d).get(), 8)+padR("", 144)
# EVT
    elif eval("PDS%dTType"%d).get() == 3:
        TChans = ""
        for c in arange(1, 1+6):
            if eval("PDS%dTChan%d"%(d,c)).get() != 0:
                TChans = TChans+"Y"
            else:
                TChans = TChans+" "
        LP = ""
        if eval("PDS%dLPFreq"%d).get() == 0:
            LP = "OFF"
        elif eval("PDS%dLPFreq"%d).get() == 1:
            LP = "0"
        elif eval("PDS%dLPFreq"%d).get() == 2:
            LP = "12"
        HP = ""
        if eval("PDS%dHPFreq"%d).get() == 0:
            HP = "OFF"
        elif eval("PDS%dHPFreq"%d).get() == 1:
            HP = "0"
        elif eval("PDS%dHPFreq"%d).get() == 2:
            HP = "0.1"
        elif eval("PDS%dHPFreq"%d).get() == 3:
            HP = "2"
        Holdv = ""
        if eval("PDS%dHold"%d).get() == 0:
            Holdv = "OFF"
        else:
            Holdv = "ON"
        Cmd = Cmd+padR("EVT", 4)+ \
                padR(TChans, 16)+padR(eval("PDS%dMc"%d).get(), 2)+ \
                padR(eval("PDS%dTwin"%d).get(), 8)+ \
                padR(eval("PDS%dPretl"%d).get(), 8)+ \
                padR(eval("PDS%dPostl"%d).get(), 8)+ \
                padR(eval("PDS%dRl"%d).get(), 8)+padR("", 8)+ \
                padR(eval("PDS%dSta"%d).get(), 8)+ \
                padR(eval("PDS%dLta"%d).get(), 8)+padR("", 8)+ \
                padR(eval("PDS%dTr"%d).get(), 8)+ \
                padR(eval("PDS%dDtr"%d).get(), 8)+padR(Holdv, 4)+ \
                padR(LP, 4)+padR(HP, 4)+padR("", 52)
# EXT
    elif eval("PDS%dTType"%d).get() == 4:
        Cmd = Cmd+padR("EXT", 4)+ \
                padR(eval("PDS%dPretl"%d).get(), 8)+ \
                padR(eval("PDS%dRl"%d).get(), 8)+padR("", 146)
# LEV
    elif eval("PDS%dTType"%d).get() == 5:
        LP = ""
        if eval("PDS%dLPFreq"%d).get() == 0:
            LP = "OFF"
        elif eval("PDS%dLPFreq"%d).get() == 1:
            LP = "0"
        elif eval("PDS%dLPFreq"%d).get() == 2:
            LP = "12"
        HP = ""
        if eval("PDS%dHPFreq"%d).get() == 0:
            HP = "OFF"
        elif eval("PDS%dHPFreq"%d).get() == 1:
            HP = "0"
        elif eval("PDS%dHPFreq"%d).get() == 2:
            HP = "0.1"
        elif eval("PDS%dHPFreq"%d).get() == 3:
            HP = "2"
        Cmd = Cmd+padR("LEV", 4)+padR(eval("PDS%dTlvl"%d).get(), 8)+ \
                padR(eval("PDS%dPretl"%d).get(), 8)+ \
                padR(eval("PDS%dRl"%d).get(), 8)+ \
                padR(LP, 4)+padR(HP, 4)+padR("", 130)
# TIM
    elif eval("PDS%dTType"%d).get() == 6:
        Cmd = Cmd+padR("TIM", 4)+ \
                padR(eval("PDS%dStart1"%d).get(), 14)+ \
                padR(eval("PDS%dRepInt"%d).get(), 8)+ \
                padR(eval("PDS%dNumTrig"%d).get(), 4)+ \
                padR("", 8)+padR(eval("PDS%dRl"%d).get(), 8)+padR("", 120)
# TML
    elif eval("PDS%dTType"%d).get() == 7:
        Cmd = Cmd+padR("TML", 4)+ \
                padR(eval("PDS%dStart1"%d).get(), 14)+ \
                padR(eval("PDS%dStart2"%d).get(), 14)+ \
                padR(eval("PDS%dStart3"%d).get(), 14)+ \
                padR(eval("PDS%dStart4"%d).get(), 14)+ \
                padR(eval("PDS%dStart5"%d).get(), 14)+ \
                padR(eval("PDS%dStart6"%d).get(), 14)+ \
                padR(eval("PDS%dStart7"%d).get(), 14)+ \
                padR(eval("PDS%dStart8"%d).get(), 14)+ \
                padR(eval("PDS%dStart9"%d).get(), 14)+ \
                padR(eval("PDS%dStart10"%d).get(), 14)+ \
                padR(eval("PDS%dStart11"%d).get(), 14)+ \
                padR(eval("PDS%dRl"%d).get(), 8)
# VOT
    elif eval("PDS%dTType"%d).get() == 8:
        pass
# The last bit.
    Cmd = Cmd+"PD"
    Cmd = rt130CmdFormat(DAS, Cmd)
    cmdPortWrite(DAS, Cmd)
    if DAS == "0000":
        delay(3)
        if WorkState == 999:
            return False
    else:
        Rp = rt130GetRp(1, 5, 2)
        if len(Rp) == 0:
            msgLn(1, "M", \
                    "   No Data Stream %d Parameters command response!"%d, \
                    True, 3)
            return False
        if checkRp(DAS, Rp) == False:
            WorkState = 999
            return False
    return True
# END: sendDSParms




########################
# BEGIN: sendParmsCmd()
# FUNC:sendParmsCmd():2019.044
#   Does the actual work of sending the parameters to the DASs.
def sendParmsCmd():
    global WorkState
    if checkParms(True) == False:
        return
    Status, ListOfDASs = isItOKToStart(3)
    if Status == False:
        return
    WorkState = 100
    buttonControl("SEND", "G", 1)
    formSTATSetStatus("all", "B")
    if cmdPortOpen() == False:
        buttonControl("SEND", "", 0)
        return
# For use in setting IP addresses.
    DASCount = -1
    Count = 0
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        formSTATSetStatus(DAS, "C")
        AllOK = True
        Count += 1
        msgLn(1, "", "%d. %s: Programming... (%s)"%(Count, DAS, getGMT(0)))
        DASCount += 1
# Stop acquisition. Send the command, wait for a response.
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "C", "   Stopping acquisition...")
        Cmd = rt130CmdFormat(DAS, "AQH 0000AQ")
        cmdPortWrite(DAS, Cmd)
        if DAS == "0000":
            delay(3)
            if WorkState == 999:
                continue
        else:
            Rp = rt130GetRp(1, 5, 2)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", "   No Stop Acquisition command response!", \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# Erase parameters.
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "C", "   Erasing parameters...")
        Cmd = rt130CmdFormat(DAS, "PEPE")
        cmdPortWrite(DAS, Cmd)
        if DAS == "0000":
            delay(3)
            if WorkState == 999:
                continue
        else:
            Rp = rt130GetRp(1, 5, 2)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", "   No Erase Parameters command response!", \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# The network parameters do not get erased when the erase parameters command
# is sent (above), so we'll do that here.
        Cmd = rt130CmdFormat(DAS, "PN1 "+padR("", 76)+"PN")
        cmdPortWrite(DAS, Cmd)
        if DAS == "0000":
            delay(3)
            if WorkState == 999:
                continue
        else:
            Rp = rt130GetRp(1, 5, 2)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", \
                      "   No Network Ethernet Parameters clearing response!", \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
        Cmd = rt130CmdFormat(DAS, "PN2 "+padR("", 76)+"PN")
        cmdPortWrite(DAS, Cmd)
        if DAS == "0000":
            delay(3)
            if WorkState == 999:
                continue
        else:
            Rp = rt130GetRp(1, 5, 2)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", \
                        "   No Network Serial Parameters clearing response!", \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# Station parameters.
        if len(PExpName.get()) != 0 or len(PExpComment.get()) != 0:
            if OPTSummaryModeCVar.get() == 0:
                msgLn(1, "C", "   Station...")
            Cmd = rt130CmdFormat(DAS, "PS"+ \
                    padR("", 2)+ \
                    padR(PExpName.get(), 24)+ \
                    padR(PExpComment.get(), 40)+ \
                    padR("", 4)+padR("", 24)+padR("", 40)+"PS")
            cmdPortWrite(DAS, Cmd)
            if DAS == "0000":
                delay(3)
                if WorkState == 999:
                    continue
            else:
                Rp = rt130GetRp(1, 5, 2)
                if WorkState == 999:
                    continue
                if len(Rp) == 0:
                    formSTATSetStatus(DAS, "M")
                    msgLn(1, "M", \
                            "   No Station Parameters command response!", \
                            True, 3)
                    AllOK = False
                    continue
                if checkRp(DAS, Rp) == False:
                    formSTATSetStatus(DAS, "M")
                    AllOK = False
                    WorkState = 999
                    continue
# Channel parameters.
        for c in arange(1, 1+6):
            if sendChanParms(DAS, c, eval("PC%dName"%c), eval("PC%dGain"%c), \
                    eval("PC%dComment"%c)) == False:
                break
        if WorkState == 999:
            continue
# Data stream parameters.
        for d in arange(1, 1+4):
            if sendDSParms(DAS, d) == False:
                break
        if WorkState == 999:
            continue
# Auxiliary data parameters.
# Only send these if the sampling frequency has been set.
        if PAuxSPer.get() != 0:
            if OPTSummaryModeCVar.get() == 0:
                msgLn(1, "C", "   Auxiliary data...")
            Chans = ""
            for c in arange(1, 1+16):
                if eval("PAuxChan%d"%c).get() != 0:
                    Chans = Chans+"Y"
                else:
                    Chans = Chans+" "
            Dest = ""
            for s in ("Ram", "Disk", "Ether", "Serial"):
                if eval("PAux%s"%s).get() != 0:
                    Dest = Dest+"Y"
                else:
                    Dest = Dest+" "
# FINISHME - add Differential Control stuff.
            Cmd = rt130CmdFormat(DAS, "PA"+padR(PAuxMarker.get(), 2)+ \
                    padR(Chans, 16)+padR(PAuxSPer.get(), 8, True)+"16"+ \
                    padR(PAuxRl.get(), 8)+Dest+padR("", 12)+"PA")
            cmdPortWrite(DAS, Cmd)
            if DAS == "0000":
                delay(3)
                if WorkState == 999:
                    continue
            else:
                Rp = rt130GetRp(1, 5, 2)
                if WorkState == 999:
                    continue
                if len(Rp) == 0:
                    formSTATSetStatus(DAS, "M")
                    msgLn(1, "M", \
                            "   No Auxiliary Parameters command response!", \
                            True, 3)
                    AllOK = False
                    continue
                if checkRp(DAS, Rp) == False:
                    formSTATSetStatus(DAS, "Y")
                    AllOK = False
                    WorkState = 999
                    continue
# Auto-centering parameters.
# Only send these if the channel group has been set to something.
        if PACGroup.get() != 0:
            if OPTSummaryModeCVar.get() == 0:
                msgLn(1, "C", "   Auto-centering...")
            if PACGroup.get() == 123:
               Group = "1"
            elif PACGroup.get() == 456:
               Group = "2"
            if PACEnable.get() == 0:
               Enable = " "
            elif PACEnable.get() == 1:
               Enable = "Y"
            Cmd = rt130CmdFormat(DAS, "PQ"+Group+Enable+ \
                    padR(str(PACPeriod.get()), 4)+ \
                    padR(PACCycle.get(), 2)+ \
                    padR(PACThresh.get(), 4)+ \
                    padR(PACAttempt.get(), 2)+ \
                    padR(PACRetry.get(), 2)+"PQ")
            cmdPortWrite(DAS, Cmd)
            if DAS == "0000":
                delay(3)
                if WorkState == 999:
                    continue
            else:
                Rp = rt130GetRp(1, 5, 2)
                if WorkState == 999:
                    continue
                if len(Rp) == 0:
                    formSTATSetStatus(DAS, "M")
                    msgLn(1, "M", \
                         "   No Auto-centering Parameters command response!", \
                            True, 3)
                    AllOK = False
                    continue
                if checkRp(DAS, Rp) == False:
                    formSTATSetStatus(DAS, "M")
                    AllOK = False
                    WorkState = 999
                    continue
# Disk parameters.
# Always send these.
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "C", "   Disk...")
        if PDETDump.get() == 0:
            Dump = "N"
        else:
            Dump = "Y"
        if PDWrap.get() == 0:
            Wrap = "N"
        else:
            Wrap = "Y"
        Cmd = rt130CmdFormat(DAS, "PZ"+"    "+Dump+" "+ \
                padR(PDThresh.get(), 2)+"    "+Wrap+"   "+ \
                padR(PDRetry.get(), 1)+padR("", 11)+"PZ")
        cmdPortWrite(DAS, Cmd)
        if DAS == "0000":
            delay(3)
            if WorkState == 999:
                continue
        else:
            Rp = rt130GetRp(1, 5, 2)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", "   No Disk Parameters command response!", \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# Network parameters - we will send whatever the user has entered for both
# sets of parameters no matter which we intend to use with the exception of
# setting the serial host to 0.0.0.0 when we know that it is intended to use
# the Ethernet port.
# If we are not going to use either one then just leave the parameters erased.
        if PNPort.get() != 0:
            if OPTSummaryModeCVar.get() == 0:
                msgLn(1, "C", "   Ethernet Network...")
            if DAS == "0000":
# For lack of anything else.
                IPaddr = "192.168.1.1"
            else:
                IPaddr = makeIP(PNEFill.get(), DAS, 16, DASCount, \
                       PNEIP1.get(), PNEIP2.get(), PNEIP3.get(), PNEIP4.get())
            Power = " "
            if PNEPortPow.get() == 1:
                Power = "P"
            elif PNEPortPow.get() == 2:
                Power = "T"
            KeepToss = " "
            if PNEKeep.get() == 1:
                KeepToss = "K"
            elif PNEKeep.get() == 2:
                KeepToss = "T"
            Mode = " "
            Cmd = rt130CmdFormat(DAS, "PN"+padR("1", 2)+padR(IPaddr, 15)+ \
                    Power+padR(PNEMask.get(), 16)+ \
                    padR(PNEHost.get(), 16)+ \
                    padR(PNEGateway.get(), 16)+ \
                    KeepToss+Mode+padR("", 6)+ \
                    padR("%s"%PNETDelay.get(), 2)+"  "+"PN")
            cmdPortWrite(DAS, Cmd)
            if DAS == "0000":
                delay(3)
                if WorkState == 999:
                    continue
            else:
                Rp = rt130GetRp(1, 5, 2)
                if WorkState == 999:
                    continue
                if len(Rp) == 0:
                    formSTATSetStatus(DAS, "M")
                    msgLn(1, "M", \
                       "   No Network Ethernet Parameters command response!", \
                            True, 3)
                    AllOK = False
                    continue
                if checkRp(DAS, Rp) == False:
                    formSTATSetStatus(DAS, "M")
                    AllOK = False
                    WorkState = 999
                    continue
# Let the user know what the Ethernet network address was set to.
            if PNPort.get() == 1 or PNPort.get() == 3:
                msgLn(1, "", "   Ethernet IP address set to: "+IPaddr)
# Set all of the serial port stuff to whatever the user has entered.
            if OPTSummaryModeCVar.get() == 0:
                msgLn(1, "C", "   Serial Network...")
            if DAS == "0000":
# For lack of anything else.
                IPaddr = "192.168.1.1"
            else:
                IPaddr = makeIP(PNSFill.get(), DAS, 16, DASCount, \
                       PNSIP1.get(), PNSIP2.get(), PNSIP3.get(), PNSIP4.get())
            KeepToss = " "
            if PNSKeep.get() == 1:
                KeepToss = "K"
            elif PNSKeep.get() == 2:
                KeepToss = "T"
            Mode = " "
            if PNSMode.get() == 1:
                Mode = "D"
            elif PNSMode.get() == 2:
                Mode = "A"
            elif PNSMode.get() == 3:
                Mode = "F"
# If we intend to use just the Ethernet port set this which is about the only
# thing I can see that determines exactly which port is intended.
            if PNPort.get() == 1:
                PNSHost.set("0.0.0.0")
            Cmd = rt130CmdFormat(DAS, "PN"+padR("2", 2)+padR(IPaddr, 15)+ \
                    " "+padR(PNSMask.get(), 16)+ \
                    padR(PNSHost.get(), 16)+ \
                    padR(PNSGateway.get(), 16)+ \
                    KeepToss+Mode+padR("%d"%PNSSpeed.get(), 6)+
                    padR("%s"%PNSTDelay.get(), 2)+"  "+"PN")
            cmdPortWrite(DAS, Cmd)
            if DAS == "0000":
                delay(3)
                if WorkState == 999:
                    continue
            else:
                Rp = rt130GetRp(1, 5, 2)
                if WorkState == 999:
                    continue
                if len(Rp) == 0:
                    formSTATSetStatus(DAS, "M")
                    msgLn(1, "M", \
                         "   No Network Serial Parameters command response!", \
                            True, 3)
                    AllOK = False
                    continue
                if checkRp(DAS, Rp) == False:
                    formSTATSetStatus(DAS, "M")
                    AllOK = False
                    WorkState = 999
                    continue
# Let the user know what the network address was set to.
            if PNPort.get() == 2 or PNPort.get() == 3:
                msgLn(1, "", "   Serial IP address set to: "+IPaddr)
# GPS operaing mode.
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "C", "   GPS op mode...")
        Mode = PGPSMode.get()
# Just in case since it's kinda important.
        if Mode != "N" and Mode != "D" and Mode != "O":
            Mode = "N"
        Cmd = rt130CmdFormat(DAS, "GC%s       GC"%Mode)
        cmdPortWrite(DAS, Cmd)
        if DAS == "0000":
            delay(2)
            if WorkState == 999:
                continue
        else:
            Rp = rt130GetRp(1, 5, 2)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", "   No GPS Control command response!", True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# Implement the parameters.
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "C", "   Implementing...")
        Cmd = rt130CmdFormat(DAS, "PIPI")
        cmdPortWrite(DAS, Cmd)
        if DAS == "0000":
            delay(3)
            if WorkState == 999:
                continue
        else:
            Rp = rt130GetRp(1, 5, 2)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", "   No Implement Parameters command response!", \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# Set all of them or one of them green if we made it all of the way to here.
        formSTATSetStatus(DAS, "G")
    cmdPortClose()
# This will either set the one we were working on or all of them yellow.
    if WorkState == 999 and AllOK == True:
        formSTATSetStatus("working", "Y")
    stoppedMe(WorkState, DAS)
    buttonControl("SEND", "", 0)
    return
# END: sendParmsCmd




###################################################
# BEGIN: sendToPIS(PISData, Prefix, TestMe = False)
# LIB:sendToPIS():2019.056
#   Handles the low-level communications with the Incoming.py server program
#   on the database machine.
#   Prefix is just for adding spaces at the beginning of error message lines.
def sendToPIS(PISData, Prefix, TestMe = False):
    CSock = socket(AF_INET, SOCK_STREAM)
    try:
        CSock.connect((PROG_INCOMINGSERVER, PROG_INCOMINGPORT))
    except Exception as e:
        return (2, "MW", \
                "%sError connecting to server.\n%sData not sent.\n%s%s"% \
                (Prefix, Prefix, Prefix, str(e)), 3, "")
    if TestMe == False:
        if PROG_PYVERS == 2:
            CSock.sendall(PISData+" <ETX>")
        elif PROG_PYVERS == 3:
            CSock.sendall((PISData+" <ETX>").encode("latin-1"))
    else:
# incoming.py should just return an 'OK' message.
        stdout.write("Sending: TEST2\" <ETX>\n")
        if PROG_PYVERS == 2:
            CSock.sendall("TEST2\" <ETX>")
        elif PROG_PYVERS == 3:
            CSock.sendall(("TEST2\" <ETX>").encode("latin-1"))
# Wait for the response.
    InLine = ""
    In = ""
# WARNING - This won't really work without setting .settimeout, but I couldn't
# get a socket timeout to work with .recv if the server crashed. Maybe it is a
# bug in Python?  Hasn't been a problem.
    Trys = 0
    while 1:
        if OPTDebugCVar.get() != 0 or TestMe == True:
            stdout.write("Trying to recv %d\n"%Trys)
        if PROG_PYVERS == 2:
            In = CSock.recv(2048)
        elif PROG_PYVERS == 3:
            In = CSock.recv(2048).decode("latin-1")
        InLine += In
        if InLine.endswith("<ACK>"):
            break
        Trys += 1
        if Trys > 4:
            break
        sleep(1.0)
    CSock.close()
    if OPTDebugCVar.get() != 0 or TestMe == True:
        stdout.write("Received: >>> %s <<<\n"%InLine)
    if InLine.endswith("<ACK>"):
# Return what should be the tuple part of InLine.
        return eval(InLine[:-5])
    elif len(InLine[:-5].strip()) == 0:
        return (2, "MW", Prefix+"No response.", 3, "")
    else:
        return (2, "MW", Prefix+"Data transmission error.", 3, "")
# END: sendToPIS




##########################
# BEGIN: setGainCmd(Which)
# FUNC:setGainCmd():2018.289
def setGainCmd(Which):
    global WorkState
    Status, ListOfDASs = isItOKToStart(2)
    if Status == False:
        return
    WorkState = 100
    if Which == "high":
        buttonControl("SGAINHIGH", "G", 1)
    elif Which == "low":
        buttonControl("SGAINLOW", "G", 1)
    formSTATSetStatus("all", "B")
    if cmdPortOpen() == False:
        if Which == "high":
            buttonControl("SGAINHIGH", "", 0)
        elif Which == "low":
            buttonControl("SGAINLOW", "", 0)
        return
    Count = 0
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        formSTATSetStatus(DAS, "C")
        AllOK = True
        Count += 1
        msgLn(1, "", "%d. %s: Setting gain %s... (%s)"% \
                (Count, DAS, Which, getGMT(0)))
# Get the current setup of the DAS to figure out which channels to set
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "W", "   Getting parameter status...")
        Cmd = rt130CmdFormat(DAS, "SSPR              SS")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   No Parameter Status request response!", True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
        Chans = intt(Rp[32:32+2])
        Channels = mrDash(Rp[40:40+Chans])
        ChanNum = 0
        for Chan in Channels:
            if WorkState == 999 or AllOK == False:
                break
            ChanNum += 1
            if Chan != "-":
                if OPTSummaryModeCVar.get() == 0:
                    msgLn(1, "C", "   Setting channel "+str(ChanNum))
                if Which == "high":
                    Cmd = rt130CmdFormat(DAS, "IG"+padR(str(ChanNum), 2)+ \
                                        padR("100", 4)+"IG")
                elif Which == "low":
                    Cmd = rt130CmdFormat(DAS, "IG"+padR(str(Chan), 2)+ \
                                        padR("1", 4)+"IG")
                cmdPortWrite(DAS, Cmd)
                Rp = rt130GetRp(1, 5, 2)
                if WorkState == 999:
                    continue
                if len(Rp) == 0:
                    formSTATSetStatus(DAS, "M")
                    msgLn(1, "M", "   No Immediate Gain request response!", \
                            True, 3)
                    AllOK = False
                    continue
                if checkRp(DAS, Rp) == False:
                    formSTATSetStatus(DAS, "M")
                    AllOK = False
                    WorkState = 999
                    continue
        if WorkState == 999 or AllOK == False:
            continue
        formSTATSetStatus(DAS, "G")
    cmdPortClose()
    if WorkState == 999 and AllOK == True:
        formSTATSetStatus("working", "Y")
    stoppedMe(WorkState, "")
    if Which == "high":
        buttonControl("SGAINHIGH", "", 0)
    elif Which == "low":
        buttonControl("SGAINLOW", "", 0)
    return
# END: setGainCmd




########################################################################
# BEGIN: setMsg(WhichMsg, Colors = "", Message = "", Beep = 0, e = None)
# LIB:setMsg():2018.236
#   Be careful to pass all of the arguments if this is being called by an
#   event.
def setMsg(WhichMsg, Colors = "", Message = "", Beep = 0, e = None):
# So callers don't have to always be checking for this.
    if WhichMsg is None:
        return
# This might get called when a window is not, or never has been up so try
# everything.
    try:
# This will be the common way to call it.
        if isinstance(WhichMsg, astring):
            LMsgs = [PROGMsg[WhichMsg]]
        elif isinstance(WhichMsg, (tuple, list)):
            LMsgs = []
            for Which in WhichMsg:
                if isinstance(Which, astring):
                    LMsgs.append(PROGMsg[Which])
                else:
                    LMsgs.append(Which)
        else:
            LMsgs = [WhichMsg]
# Colors may be a standard error message. If it is break it up such that the
# rest of the function won't know the difference.
        if isinstance(Colors, tuple):
# Some callers may not pass a Beep value if it is 0.
            try:
                Beep = Colors[3]
            except IndexError:
                Beep = 0
# The passed Message may not be "". If it is 1 append the [4] part of the
# standard error message to part [2] with a space, if it is 2 append it with
# a \n, and if 3 append it with '\n   '. Leave the results in Message.
            if isinstance(Message, anint) == False and len(Message) == 0:
                Message = Colors[2]
            elif Message == 1:
                Message = Colors[2]+" "+Colors[4]
            elif Message == 2:
                Message = Colors[2]+"\n"+Colors[4]
            elif Message == 3:
                Message = Colors[2]+"\n   "+Colors[4]
            Colors = Colors[1]
        for LMsg in LMsgs:
            try:
                LMsg.configure(state = NORMAL)
                LMsg.delete("0.0", END)
# This might get passed. Just ignore it in this function.
                if Colors.find("X") != -1:
                    Colors = Colors.replace("X", "")
                if len(Colors) == 0:
                    LMsg.configure(bg = Clr["W"], fg = Clr["B"])
                else:
                    LMsg.configure(bg = Clr[Colors[0]], fg = Clr[Colors[1]])
                LMsg.insert(END, Message)
                LMsg.update()
                LMsg.configure(state = DISABLED)
            except:
                pass
# This may get called from a generated event with no Beep value set.
        if isinstance(Beep, anint) and Beep > 0:
            beep(Beep)
        updateMe(0)
    except (KeyError, TclError):
        pass
    return
########################################################################
# BEGIN: setTxt(WhichTxt, Colors = "", Message = "", Beep = 0, e = None)
# FUNC:setTxt():2018.236
#   Same as above, but for Text()s.
def setTxt(WhichTxt, Colors = "", Message = "", Beep = 0, e = None):
    if WhichTxt is None:
        return
    try:
        if isinstance(WhichTxt, astring):
            LTxt = PROGTxt[WhichTxt]
        else:
            LTxt = WhichTxt
        if isinstance(Colors, tuple):
            Message = Colors[2]
            try:
                Beep = Colors[3]
            except IndexError:
                Beep = 0
            Colors = Colors[1]
        LTxt.delete("0.0", END)
        if Colors.find("X") != -1:
            Colors = Colors.replace("X", "")
        if len(Colors) == 0:
            LTxt.configure(bg = Clr["W"], fg = Clr["B"])
        else:
            LTxt.configure(bg = Clr[Colors[0]], fg = Clr[Colors[1]])
        LTxt.insert(END, Message)
        LTxt.update()
        if isinstance(Beep, anint) and Beep > 0:
            beep(Beep)
        updateMe(0)
    except (KeyError, TclError):
        pass
    return
# END: setMsg




#####################
# BEGIN: setTimeCmd()
# FUNC:setTimeCmd():2018.289
def setTimeCmd():
    global WorkState
    Status, ListOfDASs = isItOKToStart(3)
    if Status == False:
        return
    WorkState = 100
# Call with None just to control the Stop button.
    buttonControl(None, "G", 1)
    formSTATSetStatus("all", "B")
    MYDAnswerVar.set("")
    Answer = formMYD(Root, (("Input25", TOP, "input"), \
            ("Use Input Above", TOP, "input"), ("Use CC Time", TOP, "pc"), \
            ("Cancel", TOP, "cancel")), "cancel", "", "Which One?", \
"Enter a time (YYYY:DDD:HH:MM:SS format) in the field below and click the Use Input Above button, or click the Use CC Time button to set the time using the current control computer time.")
    if Answer == "cancel":
        msgLn(0, "", "Nothing done.")
        buttonControl(None, "", 0)
        return
    elif Answer != "pc":
        TheTime = Answer.strip()
        if len(TheTime) != 17:
            msgLn(0, "R", "Bad input time: %s"%TheTime, True, 2)
            buttonControl(None, "", 0)
            return
        GMT = TheTime
        msgLn(1, "G", "Using time: %s"%GMT)
    else:
        GMT = getGMT(0)
        msgLn(1, "G", "Current CC time: %s"%GMT)
    if cmdPortOpen() == False:
        buttonControl(None, "", 0)
        return
    Count = 0
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        formSTATSetStatus(DAS, "C")
        AllOK = True
        Count += 1
        msgLn(1, "C", "   %d. %s: Setting time..."%(Count, DAS))
# Get the time from the CC for each DAS and send it. Wait for a response
        if Answer == "pc":
            GMT = getGMT(0)
        Cmd = rt130CmdFormat(DAS, "TS%sM00TS"%GMT)
        cmdPortWrite(DAS, Cmd)
        if DAS == "0000":
            delay(3)
            if WorkState == 999:
                continue
        else:
            Rp = rt130GetRp(1, 5, 2)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", "   No Time Set command response!", True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
    cmdPortClose()
    if WorkState == 999 and AllOK == True:
        formSTATSetStatus("working", "Y")
    stoppedMe(WorkState, DAS)
    buttonControl(None, "", 0)
    return
# END: setTimeCmd




########################################
# BEGIN: showCCCTimes(Name, Prefix = "")
# LIB:showCCCTimes():2013.037
def showCCCTimes(Name, Prefix = ""):
    msgLn(1, "", "%sCurrent %s time: %s GMT"%(Prefix, Name, getGMT(9)))
    msgLn(1, "", "%s                 %s LT"%(Prefix, getGMT(19)))
    return
# END: showCCCTimes




#####################
# BEGIN: showUp(Fram)
# LIB:showUp():2018.236
def showUp(Fram):
# If anything should go wrong just close the form and let the caller fix it
# (i.e. redraw it).
    try:
        if PROGFrm[Fram] is not None:
            PROGFrm[Fram].deiconify()
            PROGFrm[Fram].lift()
            PROGFrm[Fram].focus_set()
            return True
    except TclError:
# This call makes sure that the PROGFrm[] value gets set to None.
        formClose(Fram)
    return False
# END: showUp




##############################
# BEGIN: startSimpleCmd(Which)
# FUNC:startSimpleCmd():2018.289
def startSimpleCmd(Which):
    global WorkState
    Cmd = ""
    Status, ListOfDASs = isItOKToStart(3)
    if Status == False:
        return
    if Which == "CLEARRAM":
        Answer = formMYD(Root, (("Continue", LEFT, "cont"), ("Stop", LEFT, \
                "stop")), "stop", "YB", "As Good As Gone.", \
                "Clearing RAM will wipe out everything in RAM. Are you sure you want to continue or do you want to stop?")
        if Answer == "stop":
            return
    if Which == "RESET":
        Answer = formMYD(Root, (("Continue", LEFT, "cont"), ("Stop", LEFT, \
                "stop")), "stop", "YB", "Think.", \
                "Resetting may stop acquisition and disturb any test in progress. Are you sure you want to continue or do you want to stop?")
        if Answer == "stop":
            return
    WorkState = 100
    buttonControl(Which, "G", 1)
    formSTATSetStatus("all", "B")
    if cmdPortOpen() == False:
        buttonControl(Which, "", 0)
        return
    Count = 0
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        formSTATSetStatus(DAS, "C")
        AllOK = True
# Send the command and wait for a response.
        Count += 1
        if Which == "STARTACQ":
            msgLn(1, "C", "%d. %s: Starting acquisition... (%s)"% \
                    (Count, DAS, getGMT(0)))
            Cmd = rt130CmdFormat(DAS, "AQS 0000AQ")
        elif Which == "STOPACQ":
            msgLn(1, "C", "%d. %s: Stopping acquisition... (%s)"% \
                    (Count, DAS, getGMT(0)))
            Cmd = rt130CmdFormat(DAS, "AQH 0000AQ")
        elif Which == "CLEARRAM":
            msgLn(1, "C", "%d. %s: Clearing RAM... (%s)"% \
                    (Count, DAS, getGMT(0)))
            Cmd = rt130CmdFormat(DAS, "MFRMMF")
        elif Which == "DUMPRAM":
            msgLn(1, "C", "%d. %s: Dumping RAM... (%s)"% \
                    (Count, DAS, getGMT(0)))
            Cmd = rt130CmdFormat(DAS, "FD  FD")
        elif Which == "RESET":
            msgLn(1, "C", "%d. %s: Resetting... (%s)"% \
                    (Count, DAS, getGMT(0)))
            Cmd = rt130CmdFormat(DAS, "RS  RS")
        cmdPortWrite(DAS, Cmd)
        if DAS == "0000":
            delay(3)
            if WorkState == 999:
                continue
        else:
            Rp = rt130GetRp(1, 5, 3)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                if Which == "START":
                    msgLn(1, "M", "   No Start Acq command response!", True, 3)
                elif Which == "STOP":
                    msgLn(1, "M", "   No Stop Acq command response!", True, 3)
                elif Which == "CLEARRAM":
                    msgLn(1, "M", "   No Clear RAM command response!", True, 3)
                elif Which == "DUMPRAM":
                    msgLn(1, "M", "   No Dump RAM command response!", True, 3)
                elif Which == "RESET":
                    msgLn(1, "M", "   No Reset command response!", True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
        formSTATSetStatus(DAS, "G")
    cmdPortClose()
    if WorkState == 999 and AllOK == True:
        formSTATSetStatus("working", "Y")
    stoppedMe(WorkState, DAS)
    buttonControl(Which, "", 0)
    return
# END: startSimpleCmd




########################
# BEGIN: startupThings()
# LIB:startupThings():2019.262
#   A set of functions that programs call to get things going. Some programs
#   may or may not use these.
#   If START_GETGMT is defined it can be used to change the time format in the
#   .msg start/stop messages.
# This can be used if needed. Copy the lines up to the command line argument
# loop area. PROG_SETUPSUSECWD will need to be there even if not used.
#    PROG_SETUPSUSECWD = False
#        elif Arg == "-cwd":
#            PROG_SETUPSUSECWD = True
#########################
# BEGIN: loadPROGSetups()
# FUNC:loadPROGSetups():2019.262
#   A standard setups file loader for programs that don't require anything
#   special loaded from the .set file just the PROGSetups items.
#   PROGSetupsFilespec is just for formABOUT() to use.
#   Note the return codes are a bit specific.
PROGSetupsFilespec = ""

def loadPROGSetups():
    global PROGSetupsFilespec
# If there is a local function we'll call it as we go through the lines.
    CallLocal = "loadPROGSetupsLocal" in globals()
    SetupsDir = PROGSetupsDirVar.get()
    if len(SetupsDir) == 0:
        SetupsDir = "%s%s"%(abspath("."), sep)
    PROGSetupsFilespec = "%sset%s%s.set"%(SetupsDir, PROG_NAMELC, \
            PROGUserIDLC)
# The .set file may not be there. This may be the first time the program has
# been run. Try to create a blank file. If that fails just go on. The user
# will hear about it later.
    if exists(PROGSetupsFilespec) == False:
        try:
            Fp = open(PROGSetupsFilespec, "a")
            Fp.close()
        except:
            pass
        return (1, "", "Setups file not found. Was looking for\n   %s"% \
                PROGSetupsFilespec, 0, "")
    if PROGIgnoreSetups == True:
        return (0, "WB", "Setups ignored from\n   %s"%PROGSetupsFilespec, \
                0, "")
# The file should not be corrupted, but we don't know where it has been, so
# use readFileLinesRB().
    Ret = readFileLinesRB(PROGSetupsFilespec, True)
    if Ret[0] != 0:
        return (5, Ret[1], Ret[2], Ret[3], Ret[4], Ret[5])
    Lines = Ret[1]
    if len(Lines) == 0:
        return (0, "WB", "No setup lines found in\n   %s"%PROGSetupsFilespec, \
                0, "")
    Found = False
    VersGood = False
# Just catch anything that might go wrong and blame it on the .set file.
    try:
        for Line in Lines:
            if len(Line) == 0 or Line.startswith("#"):
                continue
            if Line.startswith(PROG_NAME+":"):
                Parts = Line.split()
                for Index in arange(0, len(Parts)):
                    Parts[Index] = Parts[Index].strip()
# In case an old setups file is read that does not have the version item.
                Parts += [""]*(3-len(Parts))
# If the version doesn't match what the program wants then don't set VersGood
# and everything will be left with it's default value.
                if Parts[2] == PROG_SETUPSVERS:
                    VersGood = True
                Found = True
                continue
            if Found == True and VersGood == True:
                if CallLocal == True:
# For handling a little more complicated return value.
                    Ret = loadPROGSetupsLocal(Line)
# The passed Line didn't have anything to do with the local stuff.
                    if Ret[0] == 0:
                        pass
# The local function processed the Line. Go on to the next.
                    elif Ret[0] == 1:
                        continue
# There was an exception. Do the same as the exception clause in here. The
# local function should return the same message as below.
                    elif Ret[0] == 2:
                        return Ret
                Parts = Line.split(";", 1)
                for Index in arange(0, len(Parts)):
                    Parts[Index] = Parts[Index].strip()
                for Item in PROGSetups:
                    if Parts[0] == Item:
                        if isinstance(eval(Item), StringVar):
                            eval(Item).set(Parts[1])
                            break
                        elif isinstance(eval(Item), IntVar):
                            eval(Item).set(intt(Parts[1]))
                            break
        if Found == False:
            return (3, "YB", "No %s setups found in\n   %s"%(PROG_NAME, \
                    PROGSetupsFilespec), 2, "")
        else:
            if VersGood == False:
                return (4, "YB", "Setups version mismatch. Using defaults.", \
                        0, "")
            return (0, "WB", "Setups loaded from\n   %s"%PROGSetupsFilespec, \
                    0, "")
    except Exception as e:
        return (5, "MW", \
           "Error loading setups from\n   %s\n   %s\n   Was loading line %s"% \
                (PROGSetupsFilespec, e, Line), 3, "")
#########################
# BEGIN: savePROGSetups()
# FUNC:savePROGSetups():2018.236
def savePROGSetups():
# If there is a local function we'll call it as we go through.
    CallLocal = "savePROGSetupsLocal" in globals()
# Same as above.
    try:
        Fp = open(PROGSetupsFilespec, "w")
    except Exception as e:
        stdout.write("savePROGSetups(): %s\n"%e)
        return (2, "MW", "Error opening setups file\n   %s\n   %s"% \
                (PROGSetupsFilespec, e), 3, "")
    try:
        Fp.write("%s: %s %s\n"%(PROG_NAME, PROG_VERSION, PROG_SETUPSVERS))
        for Item in PROGSetups:
            if isinstance(eval(Item), StringVar):
                Fp.write("%s; %s\n"%(Item, eval(Item).get()))
            elif isinstance(eval(Item), IntVar):
                Fp.write("%s; %d\n"%(Item, eval(Item).get()))
# Keep the local stuff last, since that's where stuff from individual programs
# usually needs to be.
        if CallLocal == True:
            Ret = savePROGSetupsLocal(Fp)
            if Ret[0] != 0:
                Fp.close()
                return Ret
        Fp.close()
        return (0, "", "Setups saved to\n   %s"%PROGSetupsFilespec, 0, "")
    except Exception as e:
        Fp.close()
        return (2, "MW", "Error saving setups to\n   %s\n   %s"% \
                (PROGSetupsFilespec, e), 3, "")
###########################
# BEGIN: deletePROGSetups()
# FUNC:deletePROGSetups():2018.235
def deletePROGSetups():
    Answer = formMYD(Root, (("Delete And Quit", TOP, "doit"), ("Cancel", \
            TOP, "cancel")), "cancel", "YB", "Be careful.", \
            "This will delete the current setups file and quit the program. This should only need to be done if the contents of the current setups file is known to be causing the program problems.")
    if Answer == "cancel":
        return
    SetupsDir = PROGSetupsDirVar.get()
    if len(SetupsDir) == 0:
        SetupsDir = "%s%s"%(abspath("."), sep)
    Filespec = SetupsDir+"set"+PROG_NAMELC+PROGUserIDLC+".set"
    try:
        remove(Filespec)
    except Exception as e:
        formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", "MW", "Oh No.", \
                "The setups file could not be deleted.\n\n%s"%e)
        return
    progQuitter(False)
    return
#######################
# BEGIN: setMachineID()
# FUNC:setMachineID():2018.235
#   Asks the user what the name of the computer is for log file names, etc.
PROGMachineIDUCVar = StringVar()
PROGMachineIDLCVar = StringVar()
PROGSetups += ["PROGMachineIDUCVar", "PROGMachineIDLCVar"]

def setMachineID():
    MYDAnswerVar.set(PROGMachineIDUCVar.get())
    if PROGKiosk == False:
        Answer = formMYD(Root, (("Input12", TOP, "input"), ("(OK)", LEFT, \
                "input"), ("Quit", LEFT, "quit")), "cancel", "", "Who Am I?", \
                "Enter the ID of this computer. The ID may be left blank if appropriate.")
    else:
        Answer = formMYD(Root, (("Input12", TOP, "input"), ("(OK)", LEFT, \
                "input")), "ok", "", "Who Am I?", \
                "Enter the ID of this computer. The ID may be left blank if appropriate.")
# Keep going in here until the user gives a valid, or no answer.
    while 1:
        if Answer == "quit":
            return (2, "", "Quit.", 0, "")
        elif Answer == "cancel" or len(Answer) == 0:
            Answer = ""
        else:
            if Answer.isalnum() == False:
                if PROGKiosk == False:
                    Answer = formMYD(Root, (("Input12", TOP, "input"), \
                            ("(OK)", LEFT, "input"), ("Quit", LEFT, "quit")), \
"cancel", "YB", "Who Am I?", \
                            "The ID may only be letters and numbers.\n\nEnter the ID of this computer. The ID may be left blank if appropriate.")
                else:
                    Answer = formMYD(Root, (("Input12", TOP, "input"), \
                            ("(OK)", LEFT, "input")), "ok", "YB", \
                            "Who Am I?", \
                            "The ID may only be letters and numbers.\n\nEnter the ID of this computer. The ID may be left blank if appropriate.")
                continue
        break
    PROGMachineIDUCVar.set(Answer.strip().upper())
    PROGMachineIDLCVar.set(Answer.strip().lower())
    return (0, )
##########################
# BEGIN: setPROGMsgsFile()
# FUNC:setPROGMsgsFile():2018.334
PROGMsgsFile = ""

def setPROGMsgsFile():
    global PROGMsgsFile
    PROGMsgsFile = ""
    StartsWith = PROGMachineIDLCVar.get()
    EndsWith = PROG_NAMELC+".msg"
    if len(StartsWith) != 0:
# Special case. Check to see if an "old style" (ccIDblah.msg) messages file
# is laying around before looking for the ccID-YYYYDOY-blah.msg style.
# If it is then use that one.
        if exists(PROGMsgsDirVar.get()+StartsWith+EndsWith):
            PROGMsgsFile = StartsWith+EndsWith
            msgLn(9, "", "Working with messages file\n   %s"% \
                    (PROGMsgsDirVar.get()+PROGMsgsFile))
            return
# If there isn't one just add the dash and look for the new style.
        StartsWith += "-"
# In case the setups file came from another machine (usually the reason).
    if exists(PROGMsgsDirVar.get()) == False:
# Hopefully PROGSetupsDirVar points to somewhere sensible.
        PROGMsgsDirVar.set(PROGSetupsDirVar.get())
    Files = listdir(PROGMsgsDirVar.get())
    Files.sort()
# Go through all of the files so we get the "latest" one (since they have been
# sorted).
    Temp = ""
    for File in Files:
        if File.endswith(EndsWith):
            if len(StartsWith) != 0:
                if File.startswith(StartsWith):
                    Temp = File
# If the user didn't enter anything then these are the only two acceptable
# possibilities.
            elif rtnPattern(File).startswith("0000000-"):
                Temp = File
            elif File == EndsWith:
                Temp = File
    if len(Temp) == 0:
        if len(StartsWith) != 0:
            PROGMsgsFile = StartsWith
        PROGMsgsFile += (getGMT(1)[:7]+"-"+EndsWith)
    else:
        PROGMsgsFile = Temp
# If this file doesn't exist just create an empty one so there will be
# something for the program to find next time it is started on the off chance
# that nothing gets written to it this time around.
    if exists(PROGMsgsDirVar.get()+PROGMsgsFile) == False:
        try:
            Fp = open(PROGMsgsDirVar.get()+PROGMsgsFile, "w")
            Fp.close()
        except Exception as e:
            msgLn(9, "MW", "Error creating messages file\n   %s\n   %s"% \
                    ((PROGMsgsDirVar.get()+PROGMsgsFile), e), True, 3)
    msgLn(9, "", "Working with messages file\n   %s"%(PROGMsgsDirVar.get()+ \
            PROGMsgsFile))
    return
###############################################
# BEGIN: loadPROGMsgs(Speak = True, Ask = True)
# FUNC:loadPROGMsgs():2018.340
def loadPROGMsgs(Speak = True, Ask = True):
    try:
        if exists(PROGMsgsDirVar.get()+PROGMsgsFile):
# These files should not be corrupted, but you never know, so use RB.
            Ret = readFileLinesRB(PROGMsgsDirVar.get()+PROGMsgsFile)
            if Ret[0] != 0:
# Don't write this to the messages file since we can't get it open.
                msgLn(9, "M", Ret[2], True, Ret[3])
                msgLn(9, "M", "This should be fixed before continuing.")
                return
            Lines = Ret[1]
# An empty messages file may get created before coming here.
            if len(Lines) != 0:
                if Ask == True:
                    Answer = formMYD(Root, (("(Yes)", LEFT, "yes"), ("No", \
                            LEFT, "no")), "no", "", "Load It?", \
                            "There already is a messages file named\n\n%s\n\nNumber of lines: %d\n\nThis file will be used for the messages. Do you want to load its current contents into the messages section?"% \
                            (PROGMsgsFile, len(Lines)))
                else:
                    Answer = "yes"
                if Answer == "yes":
# Write directly to the messages section rather than use msgLn. It's much
# faster.
                    for Line in Lines:
                        MMsg.insert(END, Line+"\n")
                    MMsg.see(END)
        if Speak == True:
            GetGMT = 0
            if "START_GETGMT" in globals():
                GetGMT = START_GETGMT
            writeFile(0, "MSG", "==== %s %s started %s ====\n"%(PROG_NAME, \
                    PROG_VERSION, getGMT(GetGMT)))
    except Exception as e:
# Same here.
        msgLn(9, "M", "Error reading %s"%PROGMsgsFile, True, 3)
        msgLn(9, "M", "   %s"%str(e))
        msgLn(9, "M", "This should be fixed before continuing.")
    return
###########################
# BEGIN: setPROGSetupsDir()
# FUNC:setPROGSetupsDir():2018.334
#   Figures out what the "home" directory is.
PROGSetupsDirVar = StringVar()
# Default to the current directory if this is never called.
PROGSetupsDirVar.set("%s%s"%(abspath("."), sep))
if PROG_SETUPSUSECWD == False:
    PROGSetupsDirVar.set("%s%s"%(abspath("."), sep))
else:
    PROGSetupsDirVar.set(getCWD())

def setPROGSetupsDir():
    if PROG_SETUPSUSECWD == True:
        return (0,)
    try:
        if PROGSystem == 'dar' or PROGSystem == 'lin' or PROGSystem == 'sun':
            Dir = environ["HOME"]
            LookFor = "HOME"
# Of course Windows had to be different.
        elif PROGSystem == 'win':
            Dir = environ["HOMEDRIVE"]+environ["HOMEPATH"]
            LookFor = "HOMEDRIVE+HOMEPATH"
        else:
            return (2, "RW", \
                    "I don't know how to get the HOME directory on this system. This system is not supported: %s"% \
                    PROGSystem, 2, "")
        if Dir.endswith(sep) == False:
            Dir += sep
    except Exception as e:
        return (2, "MW", \
                "There is an error building the directory to the setups file. The error is:\n\n%s\n\nThis will need to be corrected before this program can be used."% \
                e, 3, "")
# I guess it's possible for Dir to just be ":" on Windows, so we better check.
    if exists(Dir) == False:
        return (2, "MW", \
                "The %s directory\n\n%s\n\ndoes not exist. This will need to be corrected before this program may be used."% \
                (LookFor, Dir), 3, "")
    if access(Dir, W_OK) == False:
        return (2, "MW", \
                "The %s directory\n\n%s\n\nis not accessible for writing. This will need to be corrected before this program may be used."% \
                (LookFor, Dir), 3, "")
    PROGSetupsDirVar.set(Dir)
    return (0,)
####################
# BEGIN: setUserID()
# FUNC:setUserID():2018.334
#   For programs that need to know the name of the user.
#   Not set up for kiosk-type programs (it has a Quit button).
PROGUserIDUC = ""
PROGUserIDLC = ""

def setUserID():
    global PROGUserIDUC
    global PROGUserIDLC
# This (usually) comes up before the main form of a program, so we have to
# figure out where it should go. We'll just use the center of displays less
# than the 2560 pixels of a modern iMac, or the center of a 1024x768 display.
    if PROGScreenWidthNow > 2560:
        CX = 512
        CY = 384
    else:
        CX = PROGScreenWidthNow/2
        CY = PROGScreenHeightNow/2
    MYDAnswerVar.set("")
# None Parent, otherwise it doesn't show up in Windows.
    Answer = formMYD(None, (("Input12", TOP, "input"), ("(OK)", LEFT, \
            "input"), ("Quit", LEFT, "quit")), "cancel", "", "Who Are You?", \
            "Enter your name or an ID that will be used for finding/saving program related files. The field may be left blank if appropriate.", \
            "", 0, CX, CY)
# Keep going in here until the user gives a valid, or no, answer.
    while 1:
        if Answer == "quit":
            return (2, "", "Quit.", 0, "")
        elif Answer == "cancel" or len(Answer) == 0:
            Answer = ""
            break
        else:
            if Answer.isalnum() == False:
                Answer = formMYD(None, (("Input12", TOP, "input"), ("(OK)", \
                        LEFT, "input"), ("Quit", LEFT, "quit")), "cancel", \
                        "YB", "Who Are You?", \
                        "The ID may only be letters and numbers.\n\nEnter your name or an ID that will be used for finding/saving program related files. The field may be left blank if appropriate.", \
                        "", 0, CX, CY)
                continue
        break
    PROGUserIDUC = Answer.strip().upper()
    PROGUserIDLC = Answer.strip().lower()
    return (0, )
################################################
# BEGIN: setPROGStartDir(Which, WarnQuit = True)
# FUNC:setPROGStartDir():2018.334
#   The lines are about 60 chars long.
def setPROGStartDir(Which, WarnQuit = True):
# 60 chars.
# PETM
    if Which == 0:
        Answer = formMYDF(Root, 1, "Pick A Starting Directory", \
                PROGSetupsDirVar.get(), "", \
                "This may be the first time this program has been started on\nthis computer or in this account. Select a directory for\nthe program to use as the directory where all of its files\nshould start out being saved. Click the OK button if the\ndisplayed directory is OK to use.")
# POCUS, CHANGEO, HOCUS...
    elif Which == 1:
        Answer = formMYDF(Root, 1, "Pick A Starting Directory", \
                PROGSetupsDirVar.get(), "", \
                "This may be the first time this program has been started on\nthis computer or in this account. Select a directory for\nthe program to use as the directory where all of the bookkeeping\nfiles for the program should start out being saved -- NOT\nnecessarily where any data files may be. We'll get to that\nlater. Click the OK button if the displayed directory is\nOK to use.")
    elif Which == 2:
# LOGPEEK, QPEEK (no messages file)
        Answer = formMYDF(Root, 1, "Where's The Data?", \
                PROGSetupsDirVar.get(), "", \
                "This may be the first time this program has been started\non this computer or in this account. Select a directory\nfor the program to use as a starting point -- generally\nwhere some data files to read are located.\nClick the OK button to use the displayed\ndirectory.")
# This is here just so they all say the same thing. The caller should call the
# quitter (didn't want to do that here). The caller can supress the message if
# it just wants to set it's own defaults.
    if WarnQuit == True and len(Answer) == 0:
        formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", "YB", \
                "Have It Your Way.", \
                "You didn't enter anything, so I'm quitting.")
        quit(0)
    return Answer
# END: startupThings




###########################
# BEGIN: sP(Count, Phrases)
# LIB:sP():2012.223
def sP(Count, Phrases):
    if Count == 1 or Count == -1:
        return Phrases[0]
    else:
        return Phrases[1]
# END: sP




####################
# BEGIN: statusCmd()
# FUNC:statusCmd():2018.289
#   Steps through the list of DASs and prints the selected status items.
def statusCmd():
    global WorkState
    Status, ListOfDASs = isItOKToStart(2)
    if Status == False:
        return
    if RAMStatCVar.get() == 0 and ParmsStatCVar.get() == 0 and \
            GPSStatVar.get() == 0 and DStatVar.get() == 0 and \
            VStatVar.get() == 0:
        msgLn(0, "R", "Select something to get the status of.", True, 2)
        return
    WorkState = 100
    buttonControl("STATUS", "G", 1)
    formSTATSetStatus("all", "B")
    if cmdPortOpen() == False:
        buttonControl("STATUS", "", 0)
        return
    while 1:
        Count = 0
        for DAS in ListOfDASs:
            if WorkState == 999:
                break
# Since you can select what to check for on the fly...
            if RAMStatCVar.get() == 0 and ParmsStatCVar.get() == 0 and \
                    GPSStatVar.get() == 0 and DStatVar.get() == 0 and \
                    VStatVar.get() == 0:
                msgLn(1, "R", "Nothing is selected to get the status of.", \
                        True, 2)
                continue
            formSTATSetStatus(DAS, "C")
            AllOK = True
            Count += 1
            msgLn(1, "W", "%d. %s: DAS Status... (%s)"% \
                    (Count, DAS, getGMT(0)))
            DASTimeDone = False
# Acquisition Status request.
            if RAMStatCVar.get() == 1:
                Cmd = rt130CmdFormat(DAS, "SSAQ              SS")
                cmdPortWrite(DAS, Cmd)
# No Lag. It's just an info request.
                Rp = rt130GetRp(1, 5, 0)
                if WorkState == 999:
                    continue
                if len(Rp) == 0:
                    formSTATSetStatus(DAS, "M")
                    msgLn(1, "M", \
                            "   No Acquisition Status command response!", \
                            True, 3)
                    AllOK = False
                    continue
                if checkRp(DAS, Rp) == False:
                    formSTATSetStatus(DAS, "M")
                    AllOK = False
                    WorkState = 999
                    continue
                DASTime = Rp[14:14+18]
                AcqState = ""
                if Rp[32:32+1] == "Y":
                    AcqState = "START "
                else:
                    AcqState = "STOP "
                if Rp[33:33+1] == "Y":
                    AcqState = AcqState+"ON"
                else:
                   AcqState = AcqState+"OFF"
                Events = Rp[34:34+6].strip()
                RAMUsed = Rp[48:48+6].strip()
                RAMTotal = Rp[42:42+6].strip()
                if DASTimeDone == False:
                    msgLn(1, "", "   DAS Time: ", False)
                    msgLn(1, "G", DASTime)
                    DASTimeDone = True
                msgLn(1, "", "   Acq state: ", False)
                if AcqState == "START ON":
                    msgLn(1, "G", AcqState, False)
                elif AcqState == "STOP ON":
                    msgLn(1, "Y", AcqState, False)
                else:
                    msgLn(1, "R", AcqState, False)
                msgLn(1, "", "    Events: ", False)
                msgLn(1, "G", Events)
                msgLn(1, "", "   RAM Used: ", False)
                msgLn(1, "G", RAMUsed+" of "+RAMTotal+" KB")
# Parameters status (summary).
            if ParmsStatCVar.get() == 1:
                Cmd = rt130CmdFormat(DAS, "SSPR              SS")
                cmdPortWrite(DAS, Cmd)
                Rp = rt130GetRp(1, 5, 0)
                if WorkState == 999:
                    continue
                if len(Rp) == 0:
                    formSTATSetStatus(DAS, "M")
                    msgLn(1, "M", \
                            "   No Parameter Status request response!", \
                            True, 3)
                    AllOK = False
                    continue
                if checkRp(DAS, Rp) == False:
                    formSTATSetStatus(DAS, "M")
                    AllOK = False
                    WorkState = 999
                    continue
                DASTime = Rp[14:14+18]
                Chans = intt(Rp[32:32+2])
                DSs = intt(Rp[34:34+1])
                Channels = mrDash(Rp[40:40+Chans])
                DStreams = mrDash(Rp[40+Chans:40+Chans+DSs])
                if DASTimeDone == False:
                    msgLn(1, "", "   DAS Time: ", False)
                    msgLn(1, "G", DASTime)
                    DASTimeDone = True
                msgLn(1, "", "   Parms: Chs: ", False)
                if Channels == "------":
                    msgLn(1, "Y", Channels, False)
                else:
                    msgLn(1, "G", Channels, False)
                msgLn(1, "", "    DSs: ", False)
                if DStreams == "--------":
                    msgLn(1, "Y", DStreams)
                else:
                    msgLn(1, "G", DStreams)
# Xternal clock status
            if GPSStatVar.get() == 1:
                Cmd = rt130CmdFormat(DAS, "SSXC              SS")
                cmdPortWrite(DAS, Cmd)
                Rp = rt130GetRp(1, 5, 0)
                if WorkState == 999:
                    continue
                if len(Rp) == 0:
                    formSTATSetStatus(DAS, "M")
                    msgLn(1, "M", \
                            "   No External Clock Status command response!", \
                            True, 3)
                    AllOK = False
                    continue
                if checkRp(DAS, Rp) == False:
                    formSTATSetStatus(DAS, "M")
                    AllOK = False
                    WorkState = 999
                    continue
                DASTime = Rp[14:14+18]
                TSLL = Rp[32:32+8].strip()
                LockStatus = Rp[51:51+1]
                if Rp[84:84+1] == "Y":
                    GPSState = "ON"
                else:
                    GPSState = "SLEEP"
                LLPhase = Rp[40:40+11].strip()
                SVTracked = Rp[52:52+2].strip()
                if OPTPosDddd.get() == 1:
                    Latitude = deciDeg(Rp[54:54+12].replace(" ", ""))
                    Longitude = deciDeg(Rp[66:66+12])
                else:
                    Latitude = Rp[54:54+12].replace(" ", "")
                    Longitude = Rp[66:66+12]
                Elevation = Rp[78:78+6]
                GPSOpMode = Rp[85:85+1]
                if DASTimeDone == False:
                    msgLn(1, "", "   DAS Time: ", False)
                    msgLn(1, "G", DASTime)
                    DASTimeDone = True
                msgLn(1, "", "   GPS State: ", False)
                if GPSState == "ON":
                    msgLn(1, "G", GPSState, False)
                elif GPSState == "SLEEP":
                    msgLn(1, "Y", GPSState, False)
                else:
                    msgLn(1, "R", GPSState, False)
                msgLn(1, "", "  Lock: ", False)
                if LockStatus == "L":
                    msgLn(1, "G", LockStatus, False)
                else:
                    msgLn(1, "Y", LockStatus, False)
                msgLn(1, "", "  SV: ", False)
                if intt(SVTracked) < 4:
                    msgLn(1, "Y", "%s"%SVTracked, False)
                else:
                    msgLn(1, "G", "%s"%SVTracked, False)
                msgLn(1, "", "  TSLL: ", False)
                if TSLL < "00:00:25":
                    msgLn(1, "G", TSLL)
                else:
                    msgLn(1, "Y", TSLL)
                msgLn(1, "", "   Phase: ", False)
                if intt(LLPhase) > -10 and intt(LLPhase) < 10:
                    msgLn(1, "G", "%s"%LLPhase)
                else:
                    msgLn(1, "Y", "%s"%LLPhase)
                msgLn(1, "", "   Lat: ", False)
                if Latitude == "N00.00000" or Latitude == "N00:00.0000":
                    msgLn(1, "Y", Latitude, False)
                else:
                    msgLn(1, "G", Latitude, False)
                msgLn(1, "", "  Long: ", False)
                if Longitude == "E000.00000" or Longitude == "E000:00.0000":
                    msgLn(1, "Y", Longitude, False)
                else:
                    msgLn(1, "G", Longitude, False)
                msgLn(1, "", "  Elev: ", False)
                msgLn(1, "G", Elevation)
                msgLn(1, "", "   GPS Op Mode: ", False)
                if GPSOpMode == "D":
                    msgLn(1, "G", "Normal Duty Cycle")
                elif GPSOpMode == "C":
                    msgLn(1, "Y", "Continuous")
                elif GPSOpMode == "O":
                    msgLn(1, "R", "Off")
                else:
                    msgLn(1, "M", "Unknown")
# Get the status of the disk drives
            if DStatVar.get() == 1:
                Cmd = rt130CmdFormat(DAS, "SSDK              SS")
                cmdPortWrite(DAS, Cmd)
                Rp = rt130GetRp(1, 5, 0)
                if WorkState == 999:
                    continue
                if len(Rp) == 0:
                    formSTATSetStatus(DAS, "M")
                    msgLn(1, "M", "   No Disk Status command response!", \
                            True, 3)
                    AllOK = False
                    continue
                if checkRp(DAS, Rp) == False:
                    formSTATSetStatus(DAS, "M")
                    AllOK = False
                    WorkState = 999
                    continue
                DASTime = Rp[14:14+18]
                D1Used = Rp[38:38+6].strip()
                D1Total = Rp[32:32+6].strip()
                D2Used = Rp[56:56+6].strip()
                D2Total = Rp[50:50+6].strip()
                if Rp[68:68+1] == "1":
                    D1Curr = "*"
                    D2Curr = " "
                else:
                    D1Curr = " "
                    D2Curr = "*"
                if DASTimeDone == False:
                    msgLn(1, "", "   DAS Time: ", False)
                    msgLn(1, "G", DASTime)
                    DASTimeDone = True
                if D1Total != "0":
                    msgLn(1, "", "   Disk 1: ", False)
                    msgLn(1, "G", D1Curr+D1Used+" of "+D1Total+" MB")
                else:
                    msgLn(1, "", "   Disk 1: ", False)
                    msgLn(1, "Y", D1Curr+"Not installed")
                if D2Total != "0":
                    msgLn(1, "", "   Disk 2: ", False)
                    msgLn(1, "G", D2Curr+D2Used+" of "+D2Total+" MB")
                else:
                    msgLn(1, "", "   Disk 2: ", False)
                    msgLn(1, "Y", D2Curr+"Not installed")
# Unit Status (voltages)
            if VStatVar.get() == 1:
                Cmd = rt130CmdFormat(DAS, "SSUS              SS")
                cmdPortWrite(DAS, Cmd)
                Rp = rt130GetRp(1, 5, 0)
                if WorkState == 999:
                    continue
                if len(Rp) == 0:
                    formSTATSetStatus(DAS, "M")
                    msgLn(1, "M", "   No Unit Status command response!", \
                            True, 3)
                    AllOK = False
                    continue
                if checkRp(DAS, Rp) == False:
                    formSTATSetStatus(DAS, "M")
                    AllOK = False
                    WorkState = 999
                    continue
                DASTime = Rp[14:14+18]
                InVolts = Rp[32:32+4].strip()
                BUVolts = Rp[36:36+4].strip()
                Temp = Rp[40:40+6].strip()
                if DASTimeDone == False:
                    msgLn(1, "", "   DAS Time: ", False)
                    msgLn(1, "G", DASTime)
                    DASTimeDone = True
                msgLn(1, "", "   Input: ", False)
                if floatt(InVolts) > 10.0:
                    msgLn(1, "G", InVolts+"V", False)
                else:
                    msgLn(1, "R", InVolts+"V", False)
                msgLn(1, "", "    B/U: ", False)
                if floatt(BUVolts) > 2.6:
                    msgLn(1, "G", BUVolts+"V", False)
                else:
                    msgLn(1, "R", BUVolts+"V", False)
                msgLn(1, "", "    Temp: ", False)
                if floatt(Temp) > 30.0:
                    msgLn(1, "R", Temp+"C")
                elif floatt(Temp) > 10.0:
                    msgLn(1, "G", Temp+"C")
                else:
                    msgLn(1, "C", Temp+"C")
            formSTATSetStatus(DAS, "G")
        if LoopRVar.get() == "off":
            break
        else:
            loopDelay()
# loopDelay may have returned early because of one of these.
            if WorkState == 999 or LoopRVar.get() == "off":
                break
            msgLn(1, "", "")
    cmdPortClose()
    if WorkState == 999 and AllOK == True:
        formSTATSetStatus("working", "Y")
    stoppedMe(WorkState, "")
    buttonControl("STATUS", "", 0)
    return
# END: statusCmd




#####################
# BEGIN: stopAction()
# FUNC:stopAction():2008.040
def stopAction():
    global WorkState
    if WorkState == 999:
        msgLn(1, "Y", "Patience is a virtue...")
    elif WorkState != 0:
        msgLn(1, "Y", "Stopping...")
        buttonBG("STOP", "Y")
        WorkState = 999
    return
# END: stopAction




##################################
# BEGIN: stoppedMe(WorkState, DAS)
# FUNC:stoppedMe():2009.315
def stoppedMe(WorkState, DAS):
    if WorkState == 999:
        msgLn(0, "Y", "Stopped.")
    else:
        if DAS == "0000":
            msgLn(0, "", "Commands sent.")
        else:
            msgLn(0, "", "Done.")
            if OPTDoneBeepCVar.get() != 0:
                beep(1)
    return
# END: stoppedMe




#####################
# BEGIN: sumModeCmd()
# FUNC:sumModeCmd():2003.315
def sumModeCmd():
    if OPTSummaryModeCVar.get() == 0:
        msgLn(0, "", "Summary mode turned OFF.")
    else:
        msgLn(0, "", "Summary mode turned ON.")
    return
# END: sumModeCmd




#####################
# BEGIN: summaryCmd()
# FUNC:summaryCmd():2019.044
#   Prints a summary of the current set of parameters.
def summaryCmd():
    if isItOKToStart(0) == False:
        return
    msgLn(1, "", "")
    msgLn(1, "W", "Parameter summary... (%s)"%getGMT(0))
    if len(PExpName.get()) != 0 or len(PExpComment.get()) != 0:
        msgLn(1, "W", "==== Station Parameters ====")
        if len(PExpName.get()) != 0:
            msgLn(1, "", "   Experiment Name: %s"%PExpName.get())
        if len(PExpComment.get()) != 0:
            msgLn(1, "", "Experiment Comment: %s"%PExpComment.get())
    msgLn(1, "", "")
    if PC1Gain.get() != 0 or PC2Gain.get() != 0 or PC3Gain.get() != 0 or \
            PC4Gain.get() != 0 or PC5Gain.get() != 0 or PC6Gain.get() != 0:
        msgLn(1, "W", "==== Channel Parameters ====")
        for c in arange(1, 1+6):
            if eval("PC%dGain"%c).get() != 0:
                msgLn(1, "W", "----- Ch%d -----"%c)
                msgLn(1, "", "   Name: %s"%eval("PC%dName"%c).get())
                msgLn(1, "", "   Gain: %d"%eval("PC%dGain"%c).get())
                if len(eval("PC%dComment"%c).get()) != 0:
                    msgLn(1, "", "Comment: %s"%eval("PC%dComment"%c).get())
        msgLn(1, "", "")
    if PDS1TType.get() != 0 or PDS2TType.get() != 0 or PDS3TType.get() or \
            PDS4TType.get() != 0:
        msgLn(1, "W", "==== Data Stream Parameters ====")
        for d in arange(1, 1+4):
# If no trigger type is set go to the next data stream
            if eval("PDS%dTType"%d).get() == 0:
                continue
            msgLn(1, "W", "----- DS%d -----"%d)
            msgLn(1, "", "Stream name: %s"%eval("PDS%dName"%d).get())
            msgLn(1, "", "   Channels: ", False)
            for c in arange(1, 1+6):
                if eval("PDS%dChan%d"%(d,c)).get() != 0:
                    msgLn(1, "", "%d "%c, False)
            msgLn(1, "", "")
            msgLn(1, "", "Sample rate: %s"%eval("PDS%dSampRate"%d).get())
            msgLn(1, "", "Data format: ", False)
            if len(eval("PDS%dFormat"%d).get()) == 0:
                msgLn(1, "", "Not set")
            else:
                msgLn(1, "", eval("PDS%dFormat"%d).get())
            msgLn(1, "", "Destination: ", False)
            if eval("PDS%dDisk"%d).get() != 0:
                msgLn(1, "", "Disk ", False)
            if eval("PDS%dEther"%d).get() != 0:
                msgLn(1, "", "Ethernet ", False)
            if eval("PDS%dSerial"%d).get() != 0:
                msgLn(1, "", "Serial ", False)
            if eval("PDS%dRam"%d).get() != 0:
                msgLn(1, "", "RAM", False)
            msgLn(1, "", "")
# Print the stuff for the selected trigger type
# CON
            if eval("PDS%dTType"%d).get() == 1:
                msgLn(1, "", "    Trigger type: CON")
                msgLn(1, "", "      Start time: %s"% \
                        eval("PDS%dStart1"%d).get())
                msgLn(1, "", "   Record length: %s"%eval("PDS%dRl"%d).get())
# CRS
            elif eval("PDS%dTType"%d).get() == 2:
                msgLn(1, "", "         Trigger type: CRS")
                msgLn(1, "", "       Trigger stream: %d"% \
                        eval("PDS%dTStrm"%d).get())
                msgLn(1, "", "   Pre-trigger length: %s"% \
                        eval("PDS%dPretl"%d).get())
                msgLn(1, "", "        Record length: %s"% \
                        eval("PDS%dRl"%d).get())
# EVT
            elif eval("PDS%dTType"%d).get() == 3:
                msgLn(1, "", "          Trigger type: EVT")
                msgLn(1, "", "      Trigger channels: ", False)
                for c in arange(1, 1+6):
                    if eval("PDS%dTChan%d"%(d,c)).get() != 0:
                        msgLn(1, "", "%d "%c, False)
                msgLn(1, "", "")
                msgLn(1, "", "        Trigger window: %s"% \
                        eval("PDS%dTwin"%d).get())
                msgLn(1, "", "      Minimum channels: %s"% \
                        eval("PDS%dMc"%d).get())
                msgLn(1, "", "    Pre-trigger length: %s"% \
                        eval("PDS%dPretl"%d).get())
                msgLn(1, "", "         Record length: %s"% \
                        eval("PDS%dRl"%d).get())
                msgLn(1, "", "   Post-trigger length: %s"% \
                        eval("PDS%dPostl"%d).get())
                msgLn(1, "", "    Short-term average: %s"% \
                        eval("PDS%dSta"%d).get())
                msgLn(1, "", "     Long-term average: %s"% \
                        eval("PDS%dLta"%d).get())
                msgLn(1, "", "         Trigger ratio: %s"% \
                        eval("PDS%dTr"%d).get())
                msgLn(1, "", "      De-trigger ratio: %s"% \
                        eval("PDS%dDtr"%d).get())
                if eval("PDS%dHold"%d).get() == 0:
                    Holding = "OFF"
                else:
                    Holding = "ON"
                msgLn(1, "", "              LTA hold: %s"%Holding)
# EXT
            elif eval("PDS%dTType"%d).get() == 4:
                msgLn(1, "", "         Trigger type: EXT")
                msgLn(1, "", "   Pre-trigger length: %s"% \
                        eval("PDS%dPretl"%d).get())
                msgLn(1, "", "        Record length: %s"% \
                        eval("PDS%dRl"%d).get())
# LEV
            elif eval("PDS%dTType"%d).get() == 5:
                msgLn(1, "", "         Trigger type: LEV")
                msgLn(1, "", "        Trigger level: %s"% \
                        eval("PDS%dTlvl"%d).get())
                msgLn(1, "", "         LP frequency: ", False)
                if eval("PDS%dLPFreq"%d).get() == 0:
                    msgLn(1, "", "Off")
                elif eval("PDS%dLPFreq"%d).get() == 1:
                    msgLn(1, "", "0.0")
                elif eval("PDS%dLPFreq"%d).get() == 2:
                    msgLn(1, "", "12.0")
                msgLn(1, "", "         HP frequency: ", False)
                if eval("PDS%dHPFreq"%d).get() == 0:
                    msgLn(1, "", "Off")
                elif eval("PDS%dHPFreq"%d).get() == 1:
                    msgLn(1, "", "0.0")
                elif eval("PDS%dHPFreq"%d).get() == 2:
                    msgLn(1, "", "0.1")
                elif eval("PDS%dHPFreq"%d).get() == 3:
                    msgLn(1, "", "2.0")
                msgLn(1, "", "   Pre-trigger length: %s"% \
                        eval("PDS%dPretl"%d).get())
                msgLn(1, "", "        Record length: %s"% \
                        eval("PDS%dRl"%d).get())
# TIM
            elif eval("PDS%dTType"%d).get() == 6:
                msgLn(1, "", "         Trigger type: TIM")
                msgLn(1, "", "           Start time: %s"% \
                        eval("PDS%dStart1"%d).get())
                msgLn(1, "", "      Repeat interval: %s"% \
                        eval("PDS%dRepInt"%d).get())
                msgLn(1, "", "   Number of triggers: %s"% \
                        eval("PDS%dNumTrig"%d).get())
                msgLn(1, "", "        Record length: %s"% \
                        eval("PDS%dRl"%d).get())
# TML
            elif eval("PDS%dTType"%d).get() == 7:
                msgLn(1, "", "    Trigger type: TML")
                for i in arange(1, 1+11):
                    msgLn(1, "", "      Start time: %s"% \
                            eval("PDS%dStart%d"%(d,i)).get())
                msgLn(1, "", "   Record length: %s"%eval("PDS%dRl"%d).get())
# VOT - not yet
            elif eval("PDS%dTType"%d).get() == 8:
                pass
        msgLn(1, "", "")
    if PAuxSPer.get() != 0:
        msgLn(1, "W", "==== Auxiliary Data Parameters ====")
        msgLn(1, "", "           Marker: %s"%PAuxMarker.get())
        msgLn(1, "", "    Sample period: %d"%PAuxSPer.get())
        msgLn(1, "", " Data destination: ", False)
        if PAuxDisk.get() != 0:
            msgLn(1, "", "Disk ", False)
        if PAuxEther.get() != 0:
            msgLn(1, "", "Ethernet ", False)
        if PAuxSerial.get() != 0:
            msgLn(1, "", "Serial ", False)
        if PAuxRam.get() != 0:
            msgLn(1, "", "RAM", False)
        msgLn(1, "", "")
        msgLn(1, "", "Included channels: ", False)
        for c in arange(1, 1+16):
            if eval("PAuxChan%d"%c).get() != 0:
                msgLn(1, "", "%d "%c, False)
        msgLn(1, "", "")
        msgLn(1, "", "    Record length: %s"%PAuxRl.get())
        msgLn(1, "", "")
    if PACGroup.get() != 0:
        msgLn(1, "W", "==== Auto-centering Parameters ====")
        msgLn(1, "", " Channel group: %d"%PACGroup.get())
        if PACEnable.get() == 0:
            Enable = "No"
        else:
            Enable = "Yes"
        msgLn(1, "", "       Enabled: %s"%Enable)
        msgLn(1, "", "    Cycle time: %s"%PACCycle.get())
        msgLn(1, "", "     Threshold: %s"%PACThresh.get())
        msgLn(1, "", "Attempts/cycle: %s"%PACAttempt.get())
        msgLn(1, "", "Retry interval: %s"%PACRetry.get())
        msgLn(1, "", " Sample period: %d"%PACPeriod.get())
        msgLn(1, "", "")
    msgLn(1, "W", "==== Disk Parameters ====")
    msgLn(1, "", "     Disk wrap: ", False)
    if PDWrap.get() == 0:
        msgLn(1, "", "Disabled")
    else:
        msgLn(1, "", "Enabled")
    msgLn(1, "", "Dump threshold: %s%%"%PDThresh.get())
    msgLn(1, "", "    Dump on ET: ", False)
    if PDETDump.get() == 0:
        msgLn(1, "", "No")
    else:
        msgLn(1, "", "Yes")
    msgLn(1, "", "    Disk retry: %s"%PDRetry.get())
    msgLn(1, "", "")
    if PNPort.get() != 0:
        msgLn(1, "W", "==== Network Parameters ====")
        if PNPort.get() == 1 or PNPort.get() == 3:
            msgLn(1, "W", "----- Ethernet -----")
            msgLn(1, "", "       Port power: ", False)
            if PNEPortPow.get() == 0:
                msgLn(1, "", "Not set")
            elif PNEPortPow.get() == 1:
                msgLn(1, "", "Continuous")
            elif PNEPortPow.get() == 2:
                msgLn(1, "", "Auto")
            msgLn(1, "", " Line down action: ", False)
            if PNEKeep.get() == 0:
                msgLn(1, "", "Not set")
            elif PNEKeep.get() == 1:
                msgLn(1, "", "Keep data")
            elif PNEKeep.get() == 2:
                msgLn(1, "", "Toss data")
            if PNEKeep.get() == 2:
                msgLn(1, "", "       Toss delay: %s"%PNETDelay.get())
# Make up the IP address
            IP = ""
            for i in arange(1, 1+4):
                if len(eval("PNEIP%d"%i).get().strip()) == 0:
                    IP = IP+"___."
                else:
                    IP = IP+eval("PNEIP%d"%i).get().strip()+"."
            msgLn(1, "", "  IP address: %s"%IP[:len(IP)-1])
            if PNEFill.get() == 1:
                Fill = "Use DAS ID"
            elif PNEFill.get() == 2:
                Fill = "Sequential"
            elif PNEFill.get() == 3:
                Fill = "Use as entered"
            msgLn(1, "", "     IP fill: %s"%Fill)
            msgLn(1, "", "    Net mask: %s"%PNEMask.get())
            msgLn(1, "", "     Gateway: %s"%PNEGateway.get())
            msgLn(1, "", "Host address: %s"%PNEHost.get())
        if PNPort.get() == 2 or PNPort.get() == 3:
            msgLn(1, "W", "----- Serial -----")
            msgLn(1, "", " Line down action: ", False)
            if PNSKeep.get() == 0:
                msgLn(1, "", "Not set")
            elif PNSKeep.get() == 1:
                msgLn(1, "", "Keep data")
            elif PNSKeep.get() == 2:
                msgLn(1, "", "Toss data")
            if PNSKeep.get() == 2:
                msgLn(1, "", "       Toss delay: %s"%PNSTDelay.get())
            msgLn(1, "", " Serial port mode: ", False)
            if PNSMode.get() == 0:
                msgLn(1, "", "Not set")
            elif PNSMode.get() == 1:
                msgLn(1, "", "Direct")
            elif PNSMode.get() == 2:
                msgLn(1, "", "AT/Modem")
            elif PNSMode.get() == 3:
                msgLn(1, "", "Freewave")
            msgLn(1, "", "Serial port speed: %d"%PNSSpeed.get())
            IP = ""
            for i in arange(1, 1+4):
                if len(eval("PNSIP%d"%i).get().strip()) == 0:
                    IP = IP+"___."
                else:
                    IP = IP+eval("PNSIP%d"%i).get().strip()+"."
            msgLn(1, "", "  IP address: %s"%IP[:len(IP)-1])
            Fill = ""
            if PNSFill.get() == 1:
                Fill = "Use DAS ID"
            elif PNSFill.get() == 2:
                Fill = "Sequential"
            elif PNSFill.get() == 2:
                Fill = "Use as entered"
            msgLn(1, "", "     IP fill: %s"%Fill)
            msgLn(1, "", "    Net mask: %s"%PNSMask.get())
            msgLn(1, "", "     Gateway: %s"%PNSGateway.get())
            msgLn(1, "", "Host address: %s"%PNSHost.get())
        msgLn(0, "", "")
    msgLn(1, "W", "==== GPS Control Parameters ====")
    msgLn(1, "", "     GPS Mode: ", False)
    if PGPSMode.get() == "D":
        msgLn(1, "", "Normal Duty Cycle")
    elif PGPSMode.get() == "C":
        msgLn(1, "Y", "Continuous")
    elif PGPSMode.get() == "O":
        msgLn(1, "R", "Off")
    else:
        msgLn(1, "M", "Unknown")
    msgLn(1, "", "")
    checkParms(False)
    return
# END: summaryCmd




######################
# BEGIN: class ToolTip
# LIB:ToolTip():2019.220
#   Add tooltips to objects.
#   Usage: ToolTip(obj, Len, "text")
#   Nice and clever.
#   Starting the text with a ^ signals that the cursor for this item should be
#   set to right_ptr black white when the widget is entered.
class ToolTipBase:
    def __init__(self, button):
        self.button = button
        self.tipwindow = None
        self.id = None
        self.x = self.y = 0
        self.button.bind("<Enter>", self.enter)
        self.button.bind("<Leave>", self.leave)
        self.button.bind("<ButtonPress>", self.leave)
        return
    def enter(self, event = None):
        self.schedule()
        if self.text.startswith("^"):
            self.button.config(cursor = "right_ptr black white")
        return
    def leave(self, event = None):
        self.unschedule()
        self.hidetip()
        self.button.config(cursor = "")
        return
    def schedule(self):
        self.unschedule()
        self.id = self.button.after(500, self.showtip)
        return
    def unschedule(self):
        id = self.id
        self.id = None
        if id:
            self.button.after_cancel(id)
        return
    def showtip(self):
        if self.text == "^":
            return
        if self.tipwindow:
            return
# The tip window must be completely clear of the mouse pointer so offset the
# x and y a little. This is all in a try because I started getting TclErrors
# to the effect that the window no longer existed by the time the geometry and
# deiconify functions were reached after adding the 'keep the tooltip off the
# edge of the display' stuff. I think this additional stuff was adding enough
# time to the whole process that it would get caught trying to bring up a tip
# that was no longer needed as the user quickly moved the pointer around the
# display.
        try:
            self.tipwindow = tw = Toplevel(self.button)
            tw.withdraw()
            tw.wm_overrideredirect(1)
            self.showcontents()
            x = self.button.winfo_pointerx()
            y = self.button.winfo_pointery()
# After much trial and error...keep the tooltip away from the right edge of the
# screen and the bottom of the screen.
            tw.update()
            if x+tw.winfo_reqwidth()+5 > PROGScreenWidthNow:
                x -= (tw.winfo_reqwidth()+5)
            else:
                x += 5
            if y+tw.winfo_reqheight()+5 > PROGScreenHeightNow:
                y -= (tw.winfo_reqheight()+5)
            else:
                y += 5
            tw.wm_geometry("+%d+%d"%(x, y))
            tw.deiconify()
# FINISHME - May need to be removed if the tooltip 'flashes' when it shows up,
# or I may have recently added this, because without it the tooltips don't
# work in py3.
            tw.lift()
        except TclError:
            self.hidetip()
        return
    def showcontents(self, Len, text, BF):
# Break up the incoming message about every Len characters.
        if Len > 0 and len(text) > Len:
            Mssg = ""
            Count = 0
            for c in text:
                if Count == 0 and c == " ":
                    continue
                if Count > Len and c == " ":
                    Mssg += "\n"
                    Count = 0
                    continue
                if c == "\n":
                    Mssg += c
                    Count = 0
                    continue
                Count += 1
                Mssg += c
            text = Mssg
# Override this in derived class.
        Lab = Label(self.tipwindow, text = text, justify = LEFT, \
                bg = Clr[BF[0]], fg = Clr[BF[1]], bd = 1, relief = SOLID, \
                padx = 3, pady = 3)
        Lab.pack()
        return
    def hidetip(self):
# If it is already gone then just go back.
        try:
            tw = self.tipwindow
            self.tipwindow = None
            if tw:
                tw.destroy()
        except TclError:
            pass
        return
class ToolTip(ToolTipBase):
    def __init__(self, button, Len, text, BF = "YB"):
# If the caller doesn't pass any text then don't get this started.
        if len(text) == 0:
            return
        ToolTipBase.__init__(self, button)
        self.Len = Len
        self.text = text
        self.BF = BF
        return
    def showcontents(self):
        if self.text.startswith("^") == False:
            ToolTipBase.showcontents(self, self.Len, self.text, self.BF)
        else:
            ToolTipBase.showcontents(self, self.Len, self.text[1:], self.BF)
        return
# END: ToolTip




########################
# BEGIN: updateMe(Which)
# LIB:updateMe():2006.113
def updateMe(Which):
    if Which == 0:
        Root.update_idletasks()
        Root.update()
    else:
        PROGButs["STOP"].update()
    return
# END: updateMe




#########################
# BEGIN: verifyParmsCmd()
# FUNC:verifyParmsCmd():2019.044
#   Steps through the list of DASs, receives the parameters, and checks key
#   parameter items to make sure they match the current set of parameters
#   in the program.
def verifyParmsCmd():
    global WorkState
    Status, ListOfDASs = isItOKToStart(2)
    if Status == False:
        return
    WorkState = 100
    buttonControl("VERIFY", "G", 1)
    formSTATSetStatus("all", "B")
    if cmdPortOpen() == False:
        buttonControl("VERIFY", "", 0)
        return
    Count = 0
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        formSTATSetStatus(DAS, "C")
        AllOK = True
        ParmAllOK = True
        Count += 1
        msgLn(1, "W", "%d. %s: Verifying parameters... (%s)"%(Count, DAS, \
                getGMT(0)))
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "", "   Station...")
        Cmd = rt130CmdFormat(DAS, "PRPS  PR")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   No Station Parameters request response!", \
                    True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
# You have to add 4 to the sending offsets of the parameters described in the
# Reftek documentation (i.e. the experiment name is at [14:14+24] when you
# send the station parameters, but at [18:18+24] when receiving).
        msgLn(1, "", "     Experiment name: %s"%Rp[18:18+24].strip())
        if PExpName.get() != Rp[18:18+24].strip():
            msgLn(1, "Y", "      Experiment name does not match.")
            ParmAllOK = False
        if PExpComment.get() != Rp[42:42+40].strip():
            msgLn(1, "Y", "      Experiment comment does not match.")
            ParmAllOK = False
# Get the current setup of the DAS to figure out which channels and data
# streams to get.
        Cmd = rt130CmdFormat(DAS, "SSPR              SS")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   No Parameter Status request response!", True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
        Chans = intt(Rp[32:32+2])
        DSs = intt(Rp[34:34+1])
        Channels = mrDash(Rp[40:40+Chans])
        DStreams = mrDash(Rp[40+Chans:40+Chans+DSs])
# Check to see that the number of channels and data streams set up in CHANGEO
# match the number of channels and data streams set up in the DAS.
        CChans = 0
        DChans = 0
        CDSs = 0
        DDSs = 0
        for c in arange(1, 1+6):
            if eval("PC%dGain"%c).get() != 0:
                CChans += 1
            if Channels[(c-1)] != "-":
                DChans += 1
        if CChans != DChans:
            msgLn(1, "Y", "   Number of active channels does not match.")
            ParmAllOK = False
        for d in arange(1, 1+4):
            if eval("PDS%dTType"%d).get() != 0:
                CDSs += 1
            if DStreams[(d-1)] != "-":
                DDSs += 1
        if CDSs != DDSs:
            msgLn(1, "Y", "   Number of active data streams does not match.")
            ParmAllOK = False
        for c in arange(1, 1+6):
            if WorkState == 999 or AllOK == False:
                break
            if Channels[(c-1)] != "-":
                if OPTSummaryModeCVar.get() == 0:
                    msgLn(1, "", "   Channel %d..."%c)
            Cmd = rt130CmdFormat(DAS, "PRPC%d PR"%c)
            cmdPortWrite(DAS, Cmd)
            Rp = rt130GetRp(1, 5, 0)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", \
                        "   No Channel %d Parameters request response!"%c, \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# If this is blank then there are no parameters for this channel.
            if Rp[16:16+2] == "  ":
                continue
            if eval("PC%dName"%c).get() != Rp[18:18+10].strip():
                msgLn(1, "Y", "      Name does not match.")
                ParmAllOK = False
            if eval("PC%dComment"%c).get() != Rp[114:114+40].strip():
                msgLn(1, "Y", "      Comment does not match.")
                ParmAllOK = False
            if eval("PC%dGain"%c).get() != intt(Rp[86:86+4]):
                msgLn(1, "Y", "      Gain setting does not match.")
                ParmAllOK = False
        if WorkState == 999 or AllOK == False:
            continue
        for d in arange(1, 1+4):
            if WorkState == 999 or AllOK == False:
                break
            if DStreams[(d-1)] != "-":
                if OPTSummaryModeCVar.get() == 0:
                    msgLn(1, "", "   Data stream %d..."%d)
                Cmd = rt130CmdFormat(DAS, "PRPD%d PR"%d)
                cmdPortWrite(DAS, Cmd)
                Rp = rt130GetRp(1, 5, 0)
                if WorkState == 999:
                    continue
                if len(Rp) == 0:
                    formSTATSetStatus(DAS, "M")
                    msgLn(1, "M", \
                            "   No DS%d Parameters request response!"%d, \
                            True, 3)
                    AllOK = False
                    continue
                if checkRp(DAS, Rp) == False:
                    formSTATSetStatus(DAS, "M")
                    AllOK = False
                    WorkState = 999
                    continue
# If this is blank then there are no parameters for this stream.
                if Rp[16:16+2] == "  ":
                    continue
                if eval("PDS%dName"%d).get() != Rp[18:18+16].strip():
                    msgLn(1, "Y", "      Name does not match.")
                    ParmAllOK = False
                if (Rp[34:34+1] != " " and eval("PDS%dRam"%d).get() == 0) or \
                        (Rp[34:34+1] == " " and \
                        eval("PDS%dRam"%d).get() != 0) or \
                        (Rp[35:35+1] != " " and \
                        eval("PDS%dDisk"%d).get() == 0) or \
                        (Rp[35:35+1] == " " and \
                        eval("PDS%dDisk"%d).get() != 0) or \
                        (Rp[36:36+1] != " " and \
                        eval("PDS%dEther"%d).get() == 0) or \
                        (Rp[36:36+1] == " " and \
                        eval("PDS%dEther"%d).get() != 0) or \
                        (Rp[37:37+1] != " " and \
                        eval("PDS%dSerial"%d).get() == 0) or \
                        (Rp[37:37+1] == " " and \
                        eval("PDS%dSerial"%d).get() != 0):
                    msgLn(1, "Y", "      Data destination does not match.")
                    ParmAllOK = False
                for c in arange(1, 1+6):
                    if (Rp[41+c:41+c+1] != " " and \
                            eval("PDS%dChan%d"%(d,c)).get() == 0) or \
                            (Rp[41+c:41+c+1] == " " and \
                            eval("PDS%dChan%d"%(d,c)).get() != 0):
                        msgLn(1, "Y", \
                               "      Included channel settings do not match.")
                        ParmAllOK = False
                        break
                if eval("PDS%dSampRate"%d).get() != Rp[58:58+4].strip():
                    msgLn(1, "Y", "      Sample rate does not match.")
                    ParmAllOK = False
                if Rp[62:62+2] != eval("PDS%dFormat"%d).get():
                    msgLn(1, "Y", "      Data format setting does not match.")
                    ParmAllOK = False
                if Rp[64:64+4].strip() == "CON":
                    if eval("PDS%dTType"%d).get() != 1:
                        msgLn(1, "Y", \
                                "      Trigger type setting does not match.")
                        ParmAllOK = False
                    else:
                        if eval("PDS%dStart1"%d).get() != Rp[76:76+14].strip():
                            msgLn(1, "Y", "      Start time does not match.")
                            ParmAllOK = False
                        if eval("PDS%dRl"%d).get() != Rp[68:68+8].strip():
                            msgLn(1, "Y", \
                                    "      Record length does not match.")
                            ParmAllOK = False
                elif Rp[64:64+4].strip() == "CRS":
                    if eval("PDS%dTType"%d).get() != 2:
                        msgLn(1, "Y", "      Trigger type does not match.")
                        ParmAllOK = False
                    else:
                        if eval("PDS%dTStrm"%d).get() != intt(Rp[68:68+2]):
                            msgLn(1, "Y", \
                                "      Trigger stream setting does not match.")
                            ParmAllOK = False
                        if eval("PDS%dPretl"%d).get() != Rp[70:70+8].strip():
                            msgLn(1, "Y", \
                                    "      Pre-trigger length does not match.")
                            ParmAllOK = False
                        if eval("PDS%dRl"%d).get() != Rp[78:78+8].strip():
                            msgLn(1, "Y", \
                                    "      Record length does not match.")
                            ParmAllOK = False
                elif Rp[64:64+4].strip() == "EVT":
                    if eval("PDS%dTType"%d).get() != 3:
                        msgLn(1, "Y", "      Trigger type does not match.")
                        ParmAllOK = False
                    else:
                        for c in arange(1, 1+6):
                            if (Rp[67+c:67+c+1] != " " and \
                                    eval("PDS%dTChan%d"%(d,c)).get() == 0) or \
                                    (Rp[67+c:67+c+1] == " " and \
                                    eval("PDS%dTChan%d"%(d,c)).get() != 0):
                                msgLn(1, "Y", \
                                "      Trigger channel settings do not match.")
                                ParmAllOK = False
                                break
                        if eval("PDS%dTwin"%d).get() != Rp[86:86+8].strip():
                            msgLn(1, "Y", \
                                  "      Trigger window value does not match.")
                            ParmAllOK = False
                        if eval("PDS%dMc"%d).get() != Rp[84:84+2].strip():
                            msgLn(1, "Y", \
                            "      Minimum number of channels does not match.")
                            ParmAllOK = False
                        if eval("PDS%dRl"%d).get() != Rp[110:110+8].strip():
                            msgLn(1, "Y", \
                                    "      Record length does not match.")
                            ParmAllOK = False
                        if eval("PDS%dPretl"%d).get() != Rp[94:94+8].strip():
                            msgLn(1, "Y", \
                                    "      Pre-trigger length does not match.")
                            ParmAllOK = False
                        if eval("PDS%dPostl"%d).get() != Rp[102:102+8].strip():
                            msgLn(1, "Y", \
                                   "      Post-trigger length does not match.")
                            ParmAllOK = False
                        if eval("PDS%dSta"%d).get() != Rp[126:126+8].strip():
                            msgLn(1, "Y", "      STA length does not match.")
                            ParmAllOK = False
                        if eval("PDS%dLta"%d).get() != Rp[134:134+8].strip():
                            msgLn(1, "Y", "      LTA length does not match.")
                            ParmAllOK = False
                        Holding = Rp[166:166+4].strip()
                        if (Holding == "OFF" and \
                                eval("PDS%dHold"%d).get() != 0) or \
                                (Holding == "ON" and \
                                eval("PDS%dHold"%d).get() != 1):
                            msgLn(1, "Y", \
                                    "      LTA hold setting does not match.")
                            ParmAllOK = False
                        if eval("PDS%dTr"%d).get() != Rp[150:150+8].strip():
                            msgLn(1, "Y", \
                                    "      Trigger ratio does not match.")
                            ParmAllOK = False
                        if eval("PDS%dDtr"%d).get() != Rp[158:158+8].strip():
                            msgLn(1, "Y", \
                                    "      De-trigger ratio does not match.")
                        Pass = Rp[170:170+4].strip()
                        if (Pass == "OFF" and \
                                eval("PDS%dLPFreq"%d).get() != 0) or \
                                (Pass == "0" and \
                                eval("PDS%dLPFreq"%d).get() != 1) or \
                                (Pass == "12" and \
                                eval("PDS%dLPFreq"%d).get() != 2):
                            msgLn(1, "Y", \
                               "      Low pass filter setting does not match.")
                            ParmAllOK = False
                        Pass = Rp[174:174+4].strip()
                        if (Pass == "OFF" and \
                                eval("PDS%dHPFreq"%d).get() != 0) or \
                                (Pass == "0" and \
                                eval("PDS%dHPFreq"%d).get() != 1) or \
                                (Pass == "0.1" and \
                                eval("PDS%dHPFreq"%d).get() != 2) or \
                                (Pass == "12" and \
                                eval("PDS%dHPFreq"%d).get() != 3):
                            msgLn(1, "Y", \
                              "      High pass filter setting does not match.")
                            ParmAllOK = False
                elif Rp[64:64+4].strip() == "EXT":
                    if eval("PDS%dTType"%d).get() != 4:
                        msgLn(1, "Y", "      Trigger type does not match.")
                        ParmAllOK = False
                    else:
                        if eval("PDS%dPretl"%d).get() != Rp[68:68+8].strip():
                            msgLn(1, "Y", \
                                    "      Pre-trigger length does not match.")
                            ParmAllOK = False
                        if eval("PDS%dRl"%d).get() != Rp[76:76+8].strip():
                            msgLn(1, "Y", \
                                    "      Record length does not match.")
                            ParmAllOK = False
                elif Rp[64:64+4].strip() == "LEV":
                    if eval("PDS%dTType"%d).get() != 5:
                        msgLn(1, "Y", "      Trigger type does not match.")
                        ParmAllOK = False
                    else:
                        if eval("PDS%dTlvl"%d).get() != Rp[68:68+8].strip():
                            msgLn(1, "Y", \
                                    "      Trigger level does not match.")
                            ParmAllOK = False
                        Pass = Rp[92:92+4].strip()
                        if (Pass == "OFF" and \
                                eval("PDS%dLPFreq"%d).get() != 0) or \
                                (Pass == "0" and \
                                eval("PDS%dLPFreq"%d).get() != 1) or \
                                (Pass == "12" and \
                                eval("PDS%dLPFreq"%d).get() != 2):
                            msgLn(1, "Y", \
                               "      Low pass filter setting does not match.")
                            ParmAllOK = False
                        Pass = Rp[96:96+4].strip()
                        if (Pass == "OFF" and \
                                eval("PDS%dHPFreq"%d).get() != 0) or \
                                (Pass == "0" and \
                                eval("PDS%dHPFreq"%d).get() != 1) or \
                                (Pass == "0.1" and \
                                eval("PDS%dHPFreq"%d).get() != 2) or \
                                (Pass == "12" and \
                                eval("PDS%dHPFreq"%d).get() != 3):
                            msgLn(1, "Y",
                              "      High pass filter setting does not match.")
                            ParmAllOK = False
                        if eval("PDS%dPretl"%d).get() != Rp[76:76+8].strip():
                            msgLn(1, "Y", \
                                    "      Pre-trigger length does not match.")
                            ParmAllOK = False
                        if eval("PDS%dRl"%d).get() != Rp[84:84+8].strip():
                            msgLn(1, "Y", \
                                    "      Record length does not match.")
                            ParmAllOK = False
                elif Rp[64:64+4].strip() == "TIM":
                    if eval("PDS%dTType"%d).get() != 6:
                        msgLn(1, "Y", "      Trigger type does not match.")
                        ParmAllOK = False
                    else:
                        if eval("PDS%dStart1"%d).get() != Rp[68:68+14].strip():
                            msgLn(1, "Y", "      Start time does not match.")
                            ParmAllOK = False
                        if eval("PDS%dRepInt"%d).get() != Rp[82:82+8].strip():
                            msgLn(1, "Y", \
                                    "      Repeat interval does not match.")
                            ParmAllOK = False
                        if eval("PDS%dNumTrig"%d).get() != Rp[90:90+4].strip():
                            msgLn(1, "Y", \
                                    "      Number of triggers does not match.")
                            ParmAllOK = False
                        if eval("PDS%dRl"%d).get() != Rp[102:102+8].strip():
                            msgLn(1, "Y", \
                                    "      Record length does not match.")
                            ParmAllOK = False
                elif Rp[64:64+4].strip() == "TML":
                    if eval("PDS%dTType"%d).get() != 7:
                        msgLn(1, "Y", "      Trigger type does not match.")
                        ParmAllOK = False
                    else:
                        for i in arange(1, 1+12):
                            if eval("PDS%dStart%d"%(d,i)).get() != \
                                    Rp[54+(i*14):54+(i*14)+14].strip():
                                msgLn(1, "Y", \
                                       "      Start time %d does not match."%i)
                                ParmAllOK = False
                                break
                        if eval("PDS%dRl"%d).get() != Rp[102:102+8].strip():
                            msgLn(1, "Y", \
                                    "      Record length does not match.")
                            ParmAllOK = False
                elif Rp[64:64+4].strip() == "VOT":
                    msgLn(1, "R", \
                            "VOT trigger type not currently supported.", \
                            True, 2)
                    ParmAllOK = False
        if WorkState == 999 or AllOK == False:
            continue
# Auxiliary data.
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "", "   Auxiliary data...")
        Cmd = rt130CmdFormat(DAS, "PRPA  PR")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", \
                    "   No Auxiliary Data Parameters request response!", \
                    True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
        if PAuxSPer.get() != intt(Rp[34:34+8].strip()):
            msgLn(1, "Y", "      Sample period setting does not match.")
            ParmAllOK = False
        if PAuxMarker.get() != Rp[16:16+2].strip():
            msgLn(1, "Y", "      Marker value does not match.")
            ParmAllOK = False
        if (Rp[52:52+1] != " " and PAuxRam.get() == 0) or \
                (Rp[52:52+1] == " " and PAuxRam.get() != 0) or \
                (Rp[53:53+1] != " " and PAuxDisk.get() == 0) or \
                (Rp[53:53+1] == " " and PAuxDisk.get() != 0) or \
                (Rp[54:54+1] != " " and PAuxEther.get() == 0) or \
                (Rp[54:54+1] == " " and PAuxEther.get() != 0) or \
                (Rp[55:55+1] != " " and PAuxSerial.get() == 0) or \
                (Rp[55:55+1] == " " and PAuxSerial.get() != 0):
            msgLn(1, "Y", "      Data destination setting not match.")
            ParmAllOK = False
# First channel offset is 18.
        for c in arange(1, 1+16):
            if (Rp[17+c:17+c+1] != " " and \
                    eval("PAuxChan%d"%c).get() == 0) or \
                    (Rp[17+c:17+c+1] == " " and \
                    eval("PAuxChan%d"%c).get() != 0):
                msgLn(1, "Y", "      Included channel settings do not match.")
                ParmAllOK = False
                break
        if PAuxRl.get() != Rp[44:44+8].strip():
            msgLn(1, "Y", \
                  "      Record length does not match.")
            ParmAllOK = False
# FINISHME - Add Differential Control stuff here.
# Auto-centering parameters.
# These are a bit weird. Ask for both, but only use the one whose parameters
# are not blank (blanked by the 'erase parameters' command sent when the
# parameters were sent).
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "", "   Auto-centering...")
        Cmd = rt130CmdFormat(DAS, "PRPQ1 PR")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", \
                    "   No Auto-centering Parameters request response!", \
                    True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
# Just look at a portion of the parameter space to if it is blank or not.
        if len(Rp[15:15+10].strip()) == 0:
# Our DASs only have two places to physically connect sensors, so we only have
# to check "sensors" 1 and 2. Since 1 (the above if) didn't yield anything...
            Cmd = rt130CmdFormat(DAS, "PRPQ2 PR")
            cmdPortWrite(DAS, Cmd)
            Rp = rt130GetRp(1, 15, 0)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", \
                        "   No Auto-centering Parameters request response!", \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
# By this point we have all of the parameters we'll ever have. If everything
# is still blank then we don't need to check anything.
        if len(Rp[15:15+10].strip()) != 0:
            if (Rp[16:16+1] == "1" and PACGroup.get() != 123) or \
                    (Rp[16:16+1] == "2" and PACGroup.get() != 456):
                msgLn(1, "Y", "      Channel group setting does not match.")
                ParmAllOK = False
            if (Rp[17:17+1] == " " and PACEnable.get() != 0) or \
                    (Rp[17:17+1] == "Y" and PACEnable.get() != 1):
                msgLn(1, "Y", "      Enabled setting does not match.")
                ParmAllOK = False
            if PACCycle.get() != Rp[22:22+2].strip():
                msgLn(1, "Y", "      Cycle time does not match.")
                ParmAllOK = False
            if PACThresh.get() != Rp[24:24+4].strip():
                msgLn(1, "Y", "      Threshold does not match.")
                ParmAllOK = False
            if PACAttempt.get() != Rp[28:28+2].strip():
                msgLn(1, "Y", "      Attempts per cycle does not match.")
                ParmAllOK = False
            if PACRetry.get() != Rp[30:30+2].strip():
                msgLn(1, "Y", "      Retry interval does not match.")
                ParmAllOK = False
            if PACPeriod.get() != intt(Rp[18:18+4]):
                msgLn(1, "Y", "      Sample period setting does not match.")
                ParmAllOK = False
# Disk parameters.
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "", "   Disks...")
        Cmd = rt130CmdFormat(DAS, "PRPZ  PR")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   No Disk Parameters request response!", True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
        if (Rp[28:28+1] == "N" and PDWrap.get() != 0) or \
                (Rp[28:28+1] != "N" and PDWrap.get() != 1):
            msgLn(1, "Y", "      Disk wrap setting does not match.")
            ParmAllOK = False
        if PDThresh.get() != Rp[22:22+2].strip():
            msgLn(1, "Y", "      Dump threshold does not match.")
            ParmAllOK = False
        if (Rp[20:20+1] == "Y" and PDETDump.get() != 1) or \
                (Rp[20:20+1] != "Y" and PDETDump.get() == 1):
            msgLn(1, "Y", "      Dump on ET setting does not match.")
            ParmAllOK = False
        if PDRetry.get() != Rp[32:32+2].strip():
            msgLn(1, "Y", "      Retry value does not match.")
            ParmAllOK = False
# Network parameters.
# PN1 is for the Ethernet parameters and PN2 is for the serial port parameters
# so we will need to ask for both. How to set PNPort to reflect which one
# the DAS is setup to use remains a mystery, except to generate some warning
# messages when the parameters are summary'ed or sent to the DAS. If the
# parameters were sent with CHANGEO the port not in use should not have an IP
# address set. We'll check for that down below.
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "", "   Network...")
        Cmd = rt130CmdFormat(DAS, "PRPN1 PR")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", \
                    "   No Ethernet Network Parameters request response!", \
                    True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
        if (Rp[33:33+1] == "P" and PNEPortPow.get() != 1) or \
                (Rp[33:33+1] == "T" and PNEPortPow.get() != 2) or \
                (Rp[33:33+1] == " " and PNEPortPow.get() != 0):
            msgLn(1, "Y", "      Port power setting does not match.")
            ParmAllOK = False
        if (Rp[82:82+1] == "K" and PNEKeep.get() != 1) or \
                (Rp[82:82+1] == "T" and PNEKeep.get() != 2):
            msgLn(1, "Y", "      Line down action setting does not match.")
            ParmAllOK = False
        if PNETDelay.get() != Rp[90:90+2].strip():
            msgLn(1, "Y", "      Ethernet toss delay does not match.")
            ParmAllOK = False
# The IP addresses do not get checked since they may have all been programmed
# differently by CHANGEO.
#        if PNEIP.get() != Rp[18:18+15].strip():
#            msgLn(1, "Y", "      Ethernet IP address does not match.")
#            ParmAllOK = False
        if PNEMask.get() != Rp[34:34+16].strip():
            msgLn(1, "Y", "      Ethernet mask does not match.")
            ParmAllOK = False
        if PNEGateway.get() != Rp[66:66+16].strip():
            msgLn(1, "Y", "      Ethernet gateway address does not match.")
            ParmAllOK = False
        if PNEHost.get() != Rp[50:50+16].strip():
            msgLn(1, "Y", "      Ethernet remote host address does not match.")
            ParmAllOK = False
# Serial port parms.
        Cmd = rt130CmdFormat(DAS, "PRPN2 PR")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", \
                    "   No serial Network Parameters request response!", \
                    True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
        if (Rp[82:82+1] == "K" and PNSKeep.get() != 1) or \
                (Rp[82:82+1] == "T" and PNSKeep.get() != 2):
            msgLn(1, "Y", "      Serial keep/toss setting does not match.")
            ParmAllOK = False
        if PNSTDelay.get() != Rp[90:90+2].strip():
            msgLn(1, "Y", "      Serial toss delay does not match.")
            ParmAllOK = False
        if (Rp[83:83+1] == "D" and PNSMode.get() != 1) or \
                (Rp[83:83+1] == "A" and PNSMode.get() != 2) or \
                (Rp[83:83+1] == "F" and PNSMode.get() != 3):
            msgLn(1, "Y", "      Serial line mode setting does not match.")
            ParmAllOK = False
        if PNSSpeed.get() != intt(Rp[84:84+6]):
            msgLn(1, "Y", "      Serial port speed setting does not match.")
            ParmAllOK = False
# See above
#        if PNSIP.get() != Rp[18:18+15].strip():
#            msgLn(1, "Y", "      Serial IP address does not match.")
#            ParmAllOK = False
        if PNSMask.get() != Rp[34:34+16].strip():
            msgLn(1, "Y", "      Serial mask does not match.")
            ParmAllOK = False
        if PNSGateway.get() != Rp[66:66+16].strip():
            msgLn(1, "Y", "      Serial gateway address does not match.")
            ParmAllOK = False
        if PNSHost.get() != Rp[50:50+16].strip():
            msgLn(1, "Y", "      Serial remote host address does not match.")
            ParmAllOK = False
        if OPTSummaryModeCVar.get() == 0:
            msgLn(1, "", "   External clock...")
            Cmd = rt130CmdFormat(DAS, "SSXC              SS")
            cmdPortWrite(DAS, Cmd)
            Rp = rt130GetRp(1, 5, 0)
            if WorkState == 999:
                continue
            if len(Rp) == 0:
                formSTATSetStatus(DAS, "M")
                msgLn(1, "M", \
                        "   No External Clock Status command response!", \
                        True, 3)
                AllOK = False
                continue
            if checkRp(DAS, Rp) == False:
                formSTATSetStatus(DAS, "M")
                AllOK = False
                WorkState = 999
                continue
            GPSOpMode = Rp[85:85+1]
            if PGPSMode.get() != GPSOpMode:
                msgLn(1, "Y", "      GPS operating mode does not match.")
                ParmAllOK = False
        if ParmAllOK == True:
            formSTATSetStatus(DAS, "G")
            msgLn(1, "", "   All parameters OK.")
        else:
            formSTATSetStatus(DAS, "Y")
    cmdPortClose()
    if WorkState == 999 and AllOK == True:
        formSTATSetStatus("working", "Y")
    stoppedMe(WorkState, "")
    buttonControl("VERIFY", "", 0)
    return
# END: verifyParmsCmd




##########################
# BEGIN: versionsCmd(Send)
# FUNC:versionsCmd():2018.289
#   Steps through the list of DASs and prints the version numbers.
def versionsCmd(Send):
    global WorkState
    Status, ListOfDASs = isItOKToStart(2)
    if Status == False:
        return
    WorkState = 100
    if Send == "pis":
        buttonControl("PISVERSIONS", "G", 1)
    elif Send == "nopis":
        buttonControl("VERSIONS", "G", 1)
    formSTATSetStatus("all", "B")
    if cmdPortOpen() == False:
        if Send == "pis":
            buttonControl("PISVERSIONS", "G", 1)
        elif Send == "nopis":
            buttonControl("VERSIONS", "G", 1)
        return
    Count = 0
    for DAS in ListOfDASs:
        if WorkState == 999:
            break
        formSTATSetStatus(DAS, "C")
        AllOK = True
        Count += 1
        msgLn(1, "W", "%d. %s: Versions... (%s)"%(Count, DAS, \
                getGMT(0)))
# Do this to get the RAM size.
        Cmd = rt130CmdFormat(DAS, "SSAQ              SS")
        cmdPortWrite(DAS, Cmd)
# No Lag. Info request.
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   No Acquisition Status command response!", \
                    True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
        RAM = Rp[42:42+6].strip().upper()
        msgLn(1, "", "   RAM size: ", False)
        msgLn(1, "G", RAM)
# Board versions.
        Cmd = rt130CmdFormat(DAS, "SSVS              SS")
        cmdPortWrite(DAS, Cmd)
        Rp = rt130GetRp(1, 5, 0)
        if WorkState == 999:
            continue
        if len(Rp) == 0:
            formSTATSetStatus(DAS, "M")
            msgLn(1, "M", "   No Version Status command response!", True, 3)
            AllOK = False
            continue
        if checkRp(DAS, Rp) == False:
            formSTATSetStatus(DAS, "M")
            AllOK = False
            WorkState = 999
            continue
        CPU = Rp[32:32+16].strip().upper()
        msgLn(1, "", "   CPU version: ", False)
        msgLn(1, "G", CPU)
# Build this list of information as we go for sending to the PIS database.
        if Send == "pis":
            PISData = "CHANGEO\" DATE=NOW\" DASID=%s\" "%DAS
            PISData += "VERRAMSIZE=%s\" VERCPUFW=%s\" "%(RAM, CPU)
# There may be more than one ATD board in a unit.
        CountATD = 0
# Step through all of the boards that may be in the unit.
        for i in arange(0, intt(Rp[48:48+2])):
            Start = 50+(i*20)
            Board = Rp[Start+5:Start+5+3].upper()
            if Board == "ATD":
                CountATD += 1
                Board += "%d"%CountATD
            Board += " "+Rp[Start:Start+4].strip().upper()
            BoardSq = Board.replace(" ", "")
# Some of the versions and stuff have been things like "A 8" so squish them to
# make everyone's life easier.
            Rev = Rp[Start+4:Start+4+1].upper().replace(" ", "")
            SN = Rp[Start+8:Start+8+4].upper().replace(" ", "")
            FPGAMin = Rp[Start+16:Start+16+1].upper().replace(" ", "")
            FPGAVer = Rp[Start+17:Start+17+3].upper().replace(" ", "")
# The LID board does not have an FPGA.
            if Board[:3] == "LID":
                msgLn(1, "", "   "+"%-10s"%(Board+":")+" Rev: ", False)
                msgLn(1, "G", "%1s"%Rev, False)
                msgLn(1, "", "  SN: ", False)
                msgLn(1, "G", "%-4s"%SN)
                if Send == "pis":
                    PISData += "VER130%sREV=%s\" VER130%sSN=%s\" "%(BoardSq, \
                            Rev, BoardSq, SN)
            else:
                msgLn(1, "", "   "+"%-10s"%(Board+":")+" Rev: ", False)
                msgLn(1, "G", "%1s"%Rev, False)
                msgLn(1, "", "  SN: ", False)
                msgLn(1, "G", "%-4s"%SN, False)
                msgLn(1, "", "   FPGAVer: ", False)
                msgLn(1, "G", "%1s"%FPGAVer, False)
                msgLn(1, "", "  Min: ", False)
                msgLn(1, "G", "%-3s"%FPGAMin)
                if Send == "pis":
                    PISData += \
                            "VER130%sREV=%s\" VER130%sSN=%s\" VER130%sFPGAVER=%s\" VER130%sFPGAMIN=%s\" "% \
                            (BoardSq, Rev, BoardSq, SN, BoardSq, FPGAVer, \
                            BoardSq, FPGAMin)
        if Send == "pis":
            Ret = sendToPIS(PISData, "   ")
            if Ret[0] == 0 or Ret[0] == 1:
                msgLn(0, Ret[1], Ret[2], True, Ret[3])
                formSTATSetStatus(DAS, Ret[1])
            elif Ret[0] == 2:
                msgLn(0, Ret[1], Ret[2], True, Ret[3])
                formSTATSetStatus(DAS, Ret[1])
                msgLn(0, Ret[1], "   Stopping...")
                AllOK = False
                WorkState = 999
                continue
        else:
            formSTATSetStatus(DAS, "G")
    cmdPortClose()
    if WorkState == 999 and AllOK == True:
        formSTATSetStatus("working", "Y")
    stoppedMe(WorkState, "")
    if Send == "pis":
        buttonControl("PISVERSIONS", "", 0)
    else:
        buttonControl("VERSIONS", "", 0)
    return
# END: versionsCmd




###################################
# BEGIN: writeFile(Open, Who, What)
# LIB:writeFile():2019.003
#   Open = 0 = close file when finished
#          1 = leave file open when finished (just to save some time when
#              writing a lot of lines in a row)
#   WARNING: Must call setPROGMsgsFile() before coming here or at least set
#            the global variable PROGMsgsFile when Who is "MSG".
#   Right now it is just for the main messages file (Who="MSG"), but could be
#   extended.
PROGMsgFp = None

def writeFile(Open, Who, What):
    global PROGMsgFp
    if Who == "MSG":
# This might get called before anything is even set up.
        if len(PROGMsgsDirVar.get()) == 0 or len(PROGMsgsFile) == 0:
            return
        if PROGMsgFp is None:
            try:
                PROGMsgFp = open(PROGMsgsDirVar.get()+PROGMsgsFile, "a")
            except Exception as e:
# We can't use msgLn() or we'll end up right back here, etc. plus this will be
# REAL annoying, because it will get printed every time this gets called. This
# program may not have a main messages area, so try.
                try:
                    MMsg.insert(END, "Error opening messages file.\n")
                    MMsg.insert(END, "   "+PROGMsgsFile)
                    MMsg.insert(END, "   "+str(e))
                except:
                    pass
# Repeat them here for various reasons.
                stdout.write("Error opening messages file\n")
                stdout.write("   '%s'\n"%PROGMsgsFile)
                stdout.write("   %s\n"%str(e))
                return
        try:
            PROGMsgFp.write(What)
            PROGMsgFp.flush()
        except Exception as e:
            try:
                MMsg.insert(END, "Error writing to messages file.\n")
                MMsg.insert(END, "   "+PROGMsgsFile)
                MMsg.insert(END, "   "+str(e))
            except:
                pass
            stdout.write("Error writing to messages file\n")
            stdout.write("   '%s'\n"%PROGMsgsFile)
            stdout.write("   %s\n"%str(e))
            try:
                PROGMsgFp.close()
            except:
                pass
            PROGMsgFp = None
            return
        if Open == 0:
            try:
                PROGMsgFp.close()
            except:
                pass
            PROGMsgFp = None
            return
    return
# END: writeFile




# ==================================
# BEGIN: ========== SETUP ==========
# ==================================


######################
# BEGIN: menuMake(Win)
# FUNC:menuMake():2019.070
MENUMenus = {}
MENUFont = PROGOrigPropFont

def menuMake(Win):
    global MENUMenus
    MENUMenus.clear()
    Top = Menu(Win, font = MENUFont)
    Win.configure(menu = Top)
    Fi = Menu(Top, font = MENUFont, tearoff = 0)
    MENUMenus["File"] = Fi
    Top.add_cascade(label = "File", menu = Fi)
    Fi.add_command(label = "Erase Messages", command = eraseMsgs)
    Fi.add_command(label = "Search Messages", command = formSRCHM)
    Fi.add_separator()
    Fi.add_command(label = "List Current Main Directories", \
            command = Command(listDirs, 1))
    Fi.add_command(label = "Change Main Messages Directory...", \
            command = Command(changeMainDirsCmd, Root, "themsgs", 2, None, \
            "", ""))
    Fi.add_command(label = "Change Main Work Directory...", \
            command = Command(changeMainDirsCmd, Root, "thework", 2, None, \
            "", ""))
    Fi.add_command(label = "Change All Main Direcories To...", \
            command = Command(changeMainDirsCmd, Root, "theall", 2, None, \
            "", ""))
    Fi.add_separator()
    Fi.add_command(label = "Delete Setups File", command = deletePROGSetups)
    Fi.add_separator()
    Fi.add_command(label = "Quit %s"%PROG_NAME, \
            command = Command(progQuitter, True))
    Co = Menu(Top, font = MENUFont, tearoff = 0)
    MENUMenus["Commands"] = Co
    Top.add_cascade(label = "Commands", menu = Co)
    Co.add_command(label = "Set GPS Control to NORMAL", \
            command = Command(gpsControlCmd, "D"))
    Co.add_command(label = "Set GPS Control to CONTINUOUS", \
            command = Command(gpsControlCmd, "C"))
    Co.add_command(label = "Set GPS Control to OFF", \
            command = Command(gpsControlCmd, "O"))
    Co.add_separator()
    Co.add_command(label = "Set DAS Time", command = setTimeCmd)
    Pr = Menu(Top, font = MENUFont, tearoff = 0)
    MENUMenus["Parameters"] = Pr
    Top.add_cascade(label = "Parameters", menu = Pr)
    Pr.add_command(label = "Save Parameters To File As...", \
            command = saveParmsCmd)
    Pr.add_command(label = "Load Parameters From File...", \
            command = loadParmsCmd)
    Pr.add_separator()
    Pr.add_command(label = "Set Ch123-DS1/40sps", \
            command = Command(formPARMSSetParms, "bench"))
    Pr.add_command(label = "Set Ch123-DS1/250sps", \
            command = Command(formPARMSSetParms, "1231250"))
    Pr.add_command(label = "Set Ch123-DS1/40sps-DS2/1sps", \
            command = Command(formPARMSSetParms, "12314021"))
    Pr.add_separator()
    Pr.add_command(label = "Set Ch123-DS1/40sps,Ch456-DS2/40sps", \
            command = Command(formPARMSSetParms, "bench6"))
    Pr.add_command(label = "Set Ch123-DS1/250sps,Ch456-DS2/250sps", \
            command = Command(formPARMSSetParms, "12312506"))
    Pr.add_command( \
            label = "Set Ch123-DS1/40sps-DS2/1sps,Ch456-DS3/40sps-DS4/1sps", \
            command = Command(formPARMSSetParms, "123140216"))
    Op = Menu(Top, font = MENUFont, tearoff = 0)
    MENUMenus["Options"] = Op
    Top.add_cascade(label = "Options", menu = Op)
    Op.add_checkbutton(label = "Get CPU Version On Enumerate", \
            variable = OPTGetCPUVerCVar)
    Op.add_separator()
    Op.add_checkbutton(label = "Summary Mode", variable = OPTSummaryModeCVar, \
            command = sumModeCmd)
    Op.add_checkbutton(label = "Beep When Done", variable = OPTDoneBeepCVar)
    Op.add_checkbutton(label = "ddd.ddddd", variable = OPTPosDddd)
    Op.add_checkbutton(label = "Debug Mode", variable = OPTDebugCVar, \
            command = debugCmd)
    Op.add_separator()
    Op.add_command(label = "Set Font Sizes", command = Command(formFONTSZ, \
            Root, 0))
    Fo = Menu(Top, font = MENUFont, tearoff = 0, postcommand = menuMakeForms)
    MENUMenus["Forms"] = Fo
    Top.add_cascade(label = "Forms", menu = Fo)
    Hp = Menu(Top, font = MENUFont, tearoff = 0)
    MENUMenus["Help"] = Hp
    Top.add_cascade(label = "Help", menu = Hp)
    Hp.add_command(label = "Help", command = Command(formHELP, Root))
    Hp.add_command(label = "Calendar", command = Command(formCAL, Root))
    Hp.add_command(label = "Check For Updates", command = checkForUpdates)
    Hp.add_command(label = "About", command = formABOUT)
    Hp.add_separator()
    Hp.add_command(label = "Reset Commands", foreground = Clr["R"], \
            command = resetProgCmd)
    return
###############################################
# BEGIN: menuMakeSet(MenuText, ItemText, State)
# FUNC:menuMakeSet():2018.235
def menuMakeSet(MenuText, ItemText, State):
    try:
        Menu = MENUMenus[MenuText]
        Indexes = Menu.index("end")+1
        for Item in arange(0, Indexes):
            Type = Menu.type(Item)
            if Type in ("tearoff", "separator"):
                continue
            if Menu.entrycget(Item, "label") == ItemText:
                Menu.entryconfigure(Item, state = State)
                break
# Just in case. This should never go off if I've done my job.
    except:
        stdout.write("menuMakeSet: %s, %s\n\a"%(MenuText, ItemText))
    return
################################
# BEGIN: menuMakeForms(e = None)
# FUNC:menuMakeForms():2018.247
def menuMakeForms(e = None):
    FMenu = MENUMenus["Forms"]
    FMenu.delete(0, END)
# Build the list of forms so they can be sorted alphabetically.
    Forms = []
    for Frmm in list(PROGFrm.keys()):
        try:
            if PROGFrm[Frmm] is not None:
                Forms.append([PROGFrm[Frmm].title(), Frmm])
        except TclError:
            stdout.write("%s\n"%Frmm)
            formClose(Frmm)
    if len(Forms) == 0:
        FMenu.add_command(label = "No Forms Are Open", state = DISABLED)
    else:
        Forms.sort()
        for Title, Frmm in Forms:
            FMenu.add_command(label = Title, command = Command(showUp, Frmm))
        FMenu.add_separator()
        FMenu.add_command(label = "Close All Forms", command = formCloseAll)
    return
# END: menuMake




######################
# BEGIN: setPROGVars()
# FUNC:setPROGVars():2018.289
def setPROGVars():
    OPTGetCPUVerCVar.set(0)
    UIDFieldVar.set("")
    FDiskVar.set(1)
    OffSetRVar.set("set")
    for i in arange(1, 1+6):
        eval("Chan%dMonCVar"%i).set(1)
    CalChansRVar.set("123")
    RAMStatCVar.set(1)
    ParmsStatCVar.set(1)
    GPSStatVar.set(1)
    DStatVar.set(1)
    VStatVar.set(1)
    CV123Var.set(1)
    CV456Var.set(0)
    OPTPref0000CVar.set(1)
    OPTSummaryModeCVar.set(0)
    OPTDoneBeepCVar.set(0)
    OPTPosDddd.set(0)
    OPTDebugCVar.set(0)
    return
# END: setPROGVars




###############
# BEGIN: main()
# FUNC:main():2019.024
Root.title(PROG_NAME+" - "+PROG_VERSION)
Root.protocol("WM_DELETE_WINDOW", Command(progQuitter, True))
Root.resizable(0, 0)
PROGMsgsDirVar = StringVar()
PROGWorkDirVar = StringVar()
CmdPortVar = StringVar()
PROGSetups += ["PROGMsgsDirVar", "PROGWorkDirVar", "CmdPortVar"]
WorkState = 0
MSGLNAlwaysScroll = False
# Parameter variables.
PExpName = StringVar()
PExpComment = StringVar()
PC1Name = StringVar()
PC2Name = StringVar()
PC3Name = StringVar()
PC4Name = StringVar()
PC5Name = StringVar()
PC6Name = StringVar()
PC1Gain = IntVar()
PC2Gain = IntVar()
PC3Gain = IntVar()
PC4Gain = IntVar()
PC5Gain = IntVar()
PC6Gain = IntVar()
PC1Comment = StringVar()
PC2Comment = StringVar()
PC3Comment = StringVar()
PC4Comment = StringVar()
PC5Comment = StringVar()
PC6Comment = StringVar()
PDS1Name = StringVar()
PDS1Disk = IntVar()
PDS1Ether = IntVar()
PDS1Serial = IntVar()
PDS1Ram = IntVar()
PDS1Chan1 = IntVar()
PDS1Chan2 = IntVar()
PDS1Chan3 = IntVar()
PDS1Chan4 = IntVar()
PDS1Chan5 = IntVar()
PDS1Chan6 = IntVar()
PDS1SampRate = StringVar()
PDS1Format = StringVar()
PDS1TType = IntVar()
PDS1TChan1 = IntVar()
PDS1TChan2 = IntVar()
PDS1TChan3 = IntVar()
PDS1TChan4 = IntVar()
PDS1TChan5 = IntVar()
PDS1TChan6 = IntVar()
PDS1Mc = StringVar()
PDS1Pretl = StringVar()
PDS1Rl = StringVar()
PDS1Sta = StringVar()
PDS1Lta = StringVar()
PDS1Hold = IntVar()
PDS1Tr = StringVar()
PDS1Twin = StringVar()
PDS1Postl = StringVar()
PDS1Dtr = StringVar()
PDS1Start1 = StringVar()
PDS1Start2 = StringVar()
PDS1Start3 = StringVar()
PDS1Start4 = StringVar()
PDS1Start5 = StringVar()
PDS1Start6 = StringVar()
PDS1Start7 = StringVar()
PDS1Start8 = StringVar()
PDS1Start9 = StringVar()
PDS1Start10 = StringVar()
PDS1Start11 = StringVar()
PDS1TStrm = IntVar()
PDS1Tlvl = StringVar()
PDS1LPFreq = IntVar()
PDS1HPFreq = IntVar()
PDS1RepInt = StringVar()
PDS1NumTrig = StringVar()
PDS2Name = StringVar()
PDS2Disk = IntVar()
PDS2Ether = IntVar()
PDS2Serial = IntVar()
PDS2Ram = IntVar()
PDS2Chan1 = IntVar()
PDS2Chan2 = IntVar()
PDS2Chan3 = IntVar()
PDS2Chan4 = IntVar()
PDS2Chan5 = IntVar()
PDS2Chan6 = IntVar()
PDS2SampRate = StringVar()
PDS2Format = StringVar()
PDS2TType = IntVar()
PDS2TChan1 = IntVar()
PDS2TChan2 = IntVar()
PDS2TChan3 = IntVar()
PDS2TChan4 = IntVar()
PDS2TChan5 = IntVar()
PDS2TChan6 = IntVar()
PDS2Mc = StringVar()
PDS2Pretl = StringVar()
PDS2Rl = StringVar()
PDS2Sta = StringVar()
PDS2Lta = StringVar()
PDS2Hold = IntVar()
PDS2Tr = StringVar()
PDS2Twin = StringVar()
PDS2Postl = StringVar()
PDS2Dtr = StringVar()
PDS2Start1 = StringVar()
PDS2Start2 = StringVar()
PDS2Start3 = StringVar()
PDS2Start4 = StringVar()
PDS2Start5 = StringVar()
PDS2Start6 = StringVar()
PDS2Start7 = StringVar()
PDS2Start8 = StringVar()
PDS2Start9 = StringVar()
PDS2Start10 = StringVar()
PDS2Start11 = StringVar()
PDS2TStrm = IntVar()
PDS2Tlvl = StringVar()
PDS2LPFreq = IntVar()
PDS2HPFreq = IntVar()
PDS2RepInt = StringVar()
PDS2NumTrig = StringVar()
PDS3Name = StringVar()
PDS3Disk = IntVar()
PDS3Ether = IntVar()
PDS3Serial = IntVar()
PDS3Ram = IntVar()
PDS3Chan1 = IntVar()
PDS3Chan2 = IntVar()
PDS3Chan3 = IntVar()
PDS3Chan4 = IntVar()
PDS3Chan5 = IntVar()
PDS3Chan6 = IntVar()
PDS3SampRate = StringVar()
PDS3Format = StringVar()
PDS3TType = IntVar()
PDS3TChan1 = IntVar()
PDS3TChan2 = IntVar()
PDS3TChan3 = IntVar()
PDS3TChan4 = IntVar()
PDS3TChan5 = IntVar()
PDS3TChan6 = IntVar()
PDS3Mc = StringVar()
PDS3Pretl = StringVar()
PDS3Rl = StringVar()
PDS3Sta = StringVar()
PDS3Lta = StringVar()
PDS3Hold = IntVar()
PDS3Tr = StringVar()
PDS3Twin = StringVar()
PDS3Postl = StringVar()
PDS3Dtr = StringVar()
PDS3Start1 = StringVar()
PDS3Start2 = StringVar()
PDS3Start3 = StringVar()
PDS3Start4 = StringVar()
PDS3Start5 = StringVar()
PDS3Start6 = StringVar()
PDS3Start7 = StringVar()
PDS3Start8 = StringVar()
PDS3Start9 = StringVar()
PDS3Start10 = StringVar()
PDS3Start11 = StringVar()
PDS3TStrm = IntVar()
PDS3Tlvl = StringVar()
PDS3LPFreq = IntVar()
PDS3HPFreq = IntVar()
PDS3RepInt = StringVar()
PDS3NumTrig = StringVar()
PDS4Name = StringVar()
PDS4Disk = IntVar()
PDS4Ether = IntVar()
PDS4Serial = IntVar()
PDS4Ram = IntVar()
PDS4Chan1 = IntVar()
PDS4Chan2 = IntVar()
PDS4Chan3 = IntVar()
PDS4Chan4 = IntVar()
PDS4Chan5 = IntVar()
PDS4Chan6 = IntVar()
PDS4SampRate = StringVar()
PDS4Format = StringVar()
PDS4TType = IntVar()
PDS4TChan1 = IntVar()
PDS4TChan2 = IntVar()
PDS4TChan3 = IntVar()
PDS4TChan4 = IntVar()
PDS4TChan5 = IntVar()
PDS4TChan6 = IntVar()
PDS4Mc = StringVar()
PDS4Pretl = StringVar()
PDS4Rl = StringVar()
PDS4Sta = StringVar()
PDS4Lta = StringVar()
PDS4Hold = IntVar()
PDS4Tr = StringVar()
PDS4Twin = StringVar()
PDS4Postl = StringVar()
PDS4Dtr = StringVar()
PDS4Start1 = StringVar()
PDS4Start2 = StringVar()
PDS4Start3 = StringVar()
PDS4Start4 = StringVar()
PDS4Start5 = StringVar()
PDS4Start6 = StringVar()
PDS4Start7 = StringVar()
PDS4Start8 = StringVar()
PDS4Start9 = StringVar()
PDS4Start10 = StringVar()
PDS4Start11 = StringVar()
PDS4TStrm = IntVar()
PDS4Tlvl = StringVar()
PDS4LPFreq = IntVar()
PDS4HPFreq = IntVar()
PDS4RepInt = StringVar()
PDS4NumTrig = StringVar()
PAuxMarker = StringVar()
PAuxChan1 = IntVar()
PAuxChan2 = IntVar()
PAuxChan3 = IntVar()
PAuxChan4 = IntVar()
PAuxChan5 = IntVar()
PAuxChan6 = IntVar()
PAuxChan7 = IntVar()
PAuxChan8 = IntVar()
PAuxChan9 = IntVar()
PAuxChan10 = IntVar()
PAuxChan11 = IntVar()
PAuxChan12 = IntVar()
PAuxChan13 = IntVar()
PAuxChan14 = IntVar()
PAuxChan15 = IntVar()
PAuxChan16 = IntVar()
PAuxDisk = IntVar()
PAuxEther = IntVar()
PAuxSerial = IntVar()
PAuxRam = IntVar()
PAuxSPer = IntVar()
PAuxRl = StringVar()
PACGroup = IntVar()
PACEnable = IntVar()
PACCycle = StringVar()
PACThresh = StringVar()
PACAttempt = StringVar()
PACRetry = StringVar()
PACPeriod = IntVar()
PNPort = IntVar()
PNEPortPow = IntVar()
PNEKeep = IntVar()
PNETDelay = StringVar()
PNEIP1 = StringVar()
PNEIP2 = StringVar()
PNEIP3 = StringVar()
PNEIP4 = StringVar()
PNEFill = IntVar()
PNEMask = StringVar()
PNEHost = StringVar()
PNEGateway = StringVar()
PNSKeep = IntVar()
PNSTDelay = StringVar()
# PNSIP = StringVar() also removed from PROGSetups "PNSIP"
PNSIP1 = StringVar()
PNSIP2 = StringVar()
PNSIP3 = StringVar()
PNSIP4 = StringVar()
PNSFill = IntVar()
PNSMask = StringVar()
PNSHost = StringVar()
PNSGateway = StringVar()
PNSMode = IntVar()
PNSSpeed = IntVar()
PDETDump = IntVar()
PDRetry = StringVar()
PDWrap = IntVar()
PDThresh = StringVar()
PGPSMode = StringVar()
PROGSetups += ["PExpName", "PExpComment", "PC1Name", "PC2Name", "PC3Name", \
        "PC4Name", "PC5Name", "PC6Name", "PC1Gain", "PC2Gain", "PC3Gain", \
        "PC4Gain", "PC5Gain", "PC6Gain", "PC1Comment", "PC2Comment", \
        "PC3Comment", "PC4Comment", "PC5Comment", "PC6Comment", "PDS1Name", \
        "PDS1Disk", "PDS1Ether", "PDS1Serial", "PDS1Ram", "PDS1Chan1", \
        "PDS1Chan2", "PDS1Chan3", "PDS1Chan4", "PDS1Chan5", "PDS1Chan6", \
        "PDS1SampRate", "PDS1Format", "PDS1TType", "PDS1TChan1", \
        "PDS1TChan2", "PDS1TChan3", "PDS1TChan4", "PDS1TChan5", "PDS1TChan6", \
        "PDS1Mc", "PDS1Pretl", "PDS1Rl", "PDS1Sta", "PDS1Lta", "PDS1Hold", \
        "PDS1Tr", "PDS1Twin", "PDS1Postl", "PDS1Dtr", "PDS1Start1", \
        "PDS1Start2", "PDS1Start3", "PDS1Start4", "PDS1Start5", "PDS1Start6", \
        "PDS1Start7", "PDS1Start8", "PDS1Start9", "PDS1Start10", \
        "PDS1Start11", "PDS1TStrm", "PDS1Tlvl", "PDS1LPFreq", "PDS1HPFreq", \
        "PDS1RepInt", "PDS1NumTrig", "PDS2Name", "PDS2Disk", "PDS2Ether", \
        "PDS2Serial", "PDS2Ram", "PDS2Chan1", "PDS2Chan2", "PDS2Chan3", \
        "PDS2Chan4", "PDS2Chan5", "PDS2Chan6", "PDS2SampRate", "PDS2Format", \
        "PDS2TType", "PDS2TChan1", "PDS2TChan2", "PDS2TChan3", "PDS2TChan4", \
        "PDS2TChan5", "PDS2TChan6", "PDS2Mc", "PDS2Pretl", "PDS2Rl", \
        "PDS2Sta", "PDS2Lta", "PDS2Hold", "PDS2Tr", "PDS2Twin", "PDS2Postl", \
        "PDS2Dtr", "PDS2Start1", "PDS2Start2", "PDS2Start3", "PDS2Start4", \
        "PDS2Start5", "PDS2Start6", "PDS2Start7", "PDS2Start8", "PDS2Start9", \
        "PDS2Start10", "PDS2Start11", "PDS2TStrm", "PDS2Tlvl", "PDS2LPFreq", \
        "PDS2HPFreq", "PDS2RepInt", "PDS2NumTrig", "PDS3Name", "PDS3Disk", \
        "PDS3Ether", "PDS3Serial", "PDS3Ram", "PDS3Chan1", "PDS3Chan2", \
        "PDS3Chan3", "PDS3Chan4", "PDS3Chan5", "PDS3Chan6", "PDS3SampRate", \
        "PDS3Format", "PDS3TType", "PDS3TChan1", "PDS3TChan2", "PDS3TChan3", \
        "PDS3TChan4", "PDS3TChan5", "PDS3TChan6", "PDS3Mc", "PDS3Pretl", \
        "PDS3Rl", "PDS3Sta", "PDS3Lta", "PDS3Hold", "PDS3Tr", "PDS3Twin", \
        "PDS3Postl", "PDS3Dtr", "PDS3Start1", "PDS3Start2", "PDS3Start3", \
        "PDS3Start4", "PDS3Start5", "PDS3Start6", "PDS3Start7", "PDS3Start8", \
        "PDS3Start9", "PDS3Start10", "PDS3Start11", "PDS3TStrm", "PDS3Tlvl", \
        "PDS3LPFreq", "PDS3HPFreq", "PDS3RepInt", "PDS3NumTrig", "PDS4Name", \
        "PDS4Disk", "PDS4Ether", "PDS4Serial", "PDS4Ram", "PDS4Chan1", \
        "PDS4Chan2", "PDS4Chan3", "PDS4Chan4", "PDS4Chan5", "PDS4Chan6", \
        "PDS4SampRate", "PDS4Format", "PDS4TType", "PDS4TChan1", \
        "PDS4TChan2", "PDS4TChan3", "PDS4TChan4", "PDS4TChan5", "PDS4TChan6", \
        "PDS4Mc", "PDS4Pretl", "PDS4Rl", "PDS4Sta", "PDS4Lta", "PDS4Hold", \
        "PDS4Tr", "PDS4Twin", "PDS4Postl", "PDS4Dtr", "PDS4Start1", \
        "PDS4Start2", "PDS4Start3", "PDS4Start4", "PDS4Start5", \
        "PDS4Start6", "PDS4Start7", "PDS4Start8", "PDS4Start9", \
        "PDS4Start10", "PDS4Start11", "PDS4TStrm", "PDS4Tlvl", "PDS4LPFreq", \
        "PDS4HPFreq", "PDS4RepInt", "PDS4NumTrig", "PAuxMarker", "PAuxChan1", \
        "PAuxChan2", "PAuxChan3", "PAuxChan4", "PAuxChan5", "PAuxChan6", \
        "PAuxChan7", "PAuxChan8", "PAuxChan9", "PAuxChan10", "PAuxChan11", \
        "PAuxChan12", "PAuxChan13", "PAuxChan14", "PAuxChan15", "PAuxChan16", \
        "PAuxDisk", "PAuxEther", "PAuxSerial", "PAuxRam", "PAuxSPer", \
        "PAuxRl", "PACGroup", "PACEnable", "PACCycle", "PACThresh", \
        "PACAttempt", "PACRetry", "PACPeriod", "PNPort", "PNEPortPow", \
        "PNEKeep", "PNETDelay", "PNEIP1", "PNEIP2", "PNEIP3", "PNEIP4", \
        "PNEFill", "PNEMask", "PNEHost", "PNEGateway", "PNSKeep", \
        "PNSTDelay", "PNSIP1", "PNSIP2", "PNSIP3", "PNSIP4", \
        "PNSFill", "PNSMask", "PNSHost", "PNSGateway", "PNSMode", "PNSSpeed", \
        "PDETDump", "PDRetry", "PDWrap", "PDThresh", "PGPSMode"]
formPARMSClearParms()
formPARMSSetParms("defaults")
# Main screen and menu checkbutton and radiobutton variables.
OPTGetCPUVerCVar = IntVar()
UIDFieldVar = StringVar()
FDiskVar = IntVar()
OffSetRVar = StringVar()
Chan1MonCVar = IntVar()
Chan2MonCVar = IntVar()
Chan3MonCVar = IntVar()
Chan4MonCVar = IntVar()
Chan5MonCVar = IntVar()
Chan6MonCVar = IntVar()
CalChansRVar = StringVar()
RAMStatCVar = IntVar()
ParmsStatCVar = IntVar()
GPSStatVar = IntVar()
DStatVar = IntVar()
VStatVar = IntVar()
CV123Var = IntVar()
CV456Var = IntVar()
OPTPref0000CVar = IntVar()
OPTSummaryModeCVar = IntVar()
OPTDoneBeepCVar = IntVar()
OPTPosDddd = IntVar()
OPTDebugCVar = IntVar()
LoopRVar = StringVar()
LoopRVar.set("off")
PROGSetups += ["OPTGetCPUVerCVar", "UIDFieldVar", "FDiskVar", "OffSetRVar", \
        "Chan1MonCVar", "Chan2MonCVar", "Chan3MonCVar", "Chan4MonCVar", \
        "Chan5MonCVar", "Chan6MonCVar", "CalChansRVar", "RAMStatCVar", \
        "ParmsStatCVar", "GPSStatVar", "DStatVar", "VStatVar", \
        "CV123Var", "CV456Var", "OPTPref0000CVar", "OPTSummaryModeCVar", \
        "OPTDoneBeepCVar", "OPTPosDddd", "OPTDebugCVar", "LoopRVar"]
setPROGVars()
menuMake(Root)
LFrm = Frame(Root)
# ----- Enum panel -----
SubP = Frame(LFrm, bd = 1, relief = GROOVE)
Sub = Frame(SubP, bd = 3)
PROGButs["ENUM"] = BButton(Sub, text = "Enumerate", command = enumerateCmd)
PROGButs["ENUM"].pack(side = TOP, fill = X)
Cb = Checkbutton(Sub, text = "0000 Pref", variable = OPTPref0000CVar, \
        command = pref0000Cmd)
Cb.pack(side = TOP)
ToolTip(Cb, 35, \
        "Checking this tell CHANGEO to broadcast commands to all DASs at the same time when possible.")
SSub = Frame(Sub)
formSTAT(SSub)
SSub.pack(side = TOP, fill = BOTH, expand = YES, pady = 2)
BButton(Sub, text = "Select All", \
        command = formSTATSelectAll).pack(side = TOP, fill = X)
# Every operating system draws buttons differently, but they all draw labels
# about the same way. So we'll use Label()s and make them look like Button()s.
SSub = Frame(Sub)
LLb = Label(SSub, bg = Clr["B"], pady = 3, padx = 0, relief = RAISED)
LLb.pack(side = LEFT, fill = X, expand = YES, padx = 1)
LLb.bind("<Button-1>", Command(formSTATSelectColor, "B"))
LLb = Label(SSub, bg = Clr["G"], pady = 3, padx = 0, relief = RAISED)
LLb.pack(side = LEFT, fill = X, expand = YES, padx = 1)
LLb.bind("<Button-1>", Command(formSTATSelectColor, "G"))
LLb = Label(SSub, bg = Clr["C"], pady = 3, padx = 0, relief = RAISED)
LLb.pack(side = LEFT, fill = X, expand = YES, padx = 1)
LLb.bind("<Button-1>", Command(formSTATSelectColor, "C"))
LLb = Label(SSub, bg = Clr["Y"], pady = 3, padx = 0, relief = RAISED)
LLb.pack(side = LEFT, fill = X, expand = YES, padx = 1)
LLb.bind("<Button-1>", Command(formSTATSelectColor, "Y"))
LLb = Label(SSub, bg = Clr["R"], pady = 3, padx = 0, relief = RAISED)
LLb.pack(side = LEFT, fill = X, expand = YES, padx = 1)
LLb.bind("<Button-1>", Command(formSTATSelectColor, "R"))
LLb = Label(SSub, bg = Clr["M"], pady = 3, padx = 0, relief = RAISED)
LLb.pack(side = LEFT, fill = X, expand = YES, padx = 1)
LLb.bind("<Button-1>", Command(formSTATSelectColor, "M"))
SSub.pack(side = TOP, fill = X, expand = YES, pady = 2)
SSub = Frame(Sub)
Lab = Label(SSub, text = "ID:=")
Lab.pack(side = LEFT)
ToolTip(Lab, 30, \
        "[Add/Remove] Enter the ID number of the DAS to add or remove from the DAS list. If the DAS is already in the list it will be removed. If it is not in the list it will be added.")
LEnt = PROGEnt["UID"] = Entry(SSub, width = 6, textvariable = UIDFieldVar)
LEnt.pack(side = LEFT, pady = 3)
LEnt.bind("<Return>", Command(formSTATAddRemID, UIDFieldVar, "UID", "", None, \
        PROG_NAME))
LEnt.bind("<KP_Enter>", Command(formSTATAddRemID, UIDFieldVar, "UID", "", \
        None, PROG_NAME))
SSub.pack(side = TOP)
BButton(Sub, text = "Clear IDs", command = remAllDASCmd, \
        fg = Clr["U"]).pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
Sub = Frame(SubP, bd = 3)
PROGButs["ALLPORTS"] = BButton(Sub, text = "All Ports On", \
        command = allPortsCmd)
PROGButs["ALLPORTS"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
Frame(SubP, height = 2, bd = 1, relief = GROOVE).pack(side = TOP, fill = BOTH)
Sub = Frame(SubP, bd = 3)
CmdPortLab = labelTip(Sub, "Command Port:", TOP, 30, \
        "Device designation for the command serial port. For example:\nWindows: COM4\nLinux: /dev/ttyUSB0\nmacOS: /dev/tty.USB<something>")
if PROGSystem == "dar" or PROGSystem == "lin" or PROGSystem == "sun":
    if B2Glitch == True:
        CmdPortLab.bind("<Button-2>", Command(popDirDevice, CmdPortVar, \
                "Select Command Port"))
    CmdPortLab.bind("<Button-3>", Command(popDirDevice, CmdPortVar, \
            "Command Port"))
Entry(Sub, textvariable = CmdPortVar, width = 16).pack(side = TOP, fill = X, \
        pady = 3)
Sub.pack(side = TOP, fill = X)
SubP.pack(side = LEFT, expand = YES, fill = BOTH)
# ----- Parms panel -----
SubP = Frame(LFrm, bd = 1, relief = GROOVE)
Sub = Frame(SubP, bd = 3)
PROGButs["STOP"] = BButton(Sub, text = "Stop", command = stopAction, \
        state = DISABLED)
PROGButs["STOP"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
Frame(SubP, height = 2, bd = 1, relief = GROOVE).pack(side = TOP, fill = BOTH)
Sub = Frame(SubP, bd = 3)
PROGButs["CCGET"] = BButton(Sub, text = "Get CC Time", \
        command = Command(showCCCTimes, "CC"))
PROGButs["CCGET"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
Frame(SubP, height = 2, bd = 1, relief = GROOVE).pack(side = TOP, fill = BOTH)
Sub = Frame(SubP, bd = 3)
PROGButs["RECEIVE"] = BButton(Sub, text = "Receive Parms", \
        command = receiveParmsCmd)
PROGButs["RECEIVE"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
BButton(Sub, text = "Edit Parms", \
        command = formPARMSShowEdit).pack(side = TOP, fill = X)
BButton(Sub, text = "Summary", command = summaryCmd).pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
Sub = Frame(SubP, bd = 3)
PROGButs["SEND"] = BButton(Sub, text = "Send Parms", command = sendParmsCmd)
PROGButs["SEND"].pack(side = TOP, fill = X)
PROGButs["VERIFY"] = BButton(Sub, text = "Verify Parms", \
        command = verifyParmsCmd)
PROGButs["VERIFY"].pack(side = TOP, fill = X)
PROGButs["WP"] = BButton(Sub, text = "Parms To Disk", command = parmsToDiskCmd)
PROGButs["WP"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
Frame(SubP, height = 2, bd = 1, relief = GROOVE).pack(side = TOP, fill = BOTH)
Sub = Frame(SubP, bd = 3)
SSub = Frame(Sub)
Checkbutton(SSub, text = "Acq/RAM", variable = RAMStatCVar).pack(side = TOP)
SSub.pack(side = TOP)
SSub = Frame(Sub)
Checkbutton(SSub, text = "Parms", variable = ParmsStatCVar).pack(side = LEFT)
Checkbutton(SSub, text = "GPS", variable = GPSStatVar).pack(side = LEFT)
SSub.pack(side = TOP)
SSub = Frame(Sub)
Checkbutton(SSub, text = "Disk", variable = DStatVar).pack(side = LEFT)
Checkbutton(SSub, text = "Volts", variable = VStatVar).pack(side = LEFT)
SSub.pack(side = TOP)
PROGButs["STATUS"] = BButton(Sub, text = "DAS Status", command = statusCmd)
PROGButs["STATUS"].pack(side = TOP, fill = X)
Lab = Label(Sub, text = "Loop Control")
Lab.pack(side = TOP)
ToolTip(Lab, 30, "Controls how often the DAS Status command(s) are repeated.")
SSub = Frame(Sub)
Radiobutton(SSub, text = "Off", variable = LoopRVar, \
        value = "off").pack(side = LEFT)
Radiobutton(SSub, text = "5s", variable = LoopRVar, \
        value = "5").pack(side = LEFT)
SSub.pack(side = TOP)
SSub = Frame(Sub)
Radiobutton(SSub, text = "30s", variable = LoopRVar, \
        value = "30").pack(side = LEFT)
Radiobutton(SSub, text = "60s", variable = LoopRVar, \
        value = "60").pack(side = LEFT)
SSub.pack(side = TOP)
Sub.pack(side = TOP, fill = X)
Frame(SubP, height = 2, bd = 1, relief = GROOVE).pack(side = TOP, fill = BOTH)
Sub = Frame(SubP, bd = 3)
SSub = Frame(Sub)
Checkbutton(SSub, text = "123", variable = CV123Var).pack(side = LEFT)
Checkbutton(SSub, text = "456", variable = CV456Var).pack(side = LEFT)
SSub.pack(side = TOP)
PROGButs["CENTVOLTS"] = BButton(Sub, text = "Centering Volts", \
        command = centeringVoltsCmd)
PROGButs["CENTVOLTS"].pack(side = TOP, fill = X)
PROGButs["SENSCAL"] = BButton(Sub, text = "Sensor Cal", \
        command = Command(notYet, Root))
PROGButs["SENSCAL"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
Frame(SubP, height = 2, bd = 1, relief = GROOVE).pack(side = TOP, fill = BOTH)
Sub = Frame(SubP, bd = 3)
PROGButs["MEMTEST"] = BButton(Sub, text = "Memory Test", \
        command = memoryTestCmd)
PROGButs["MEMTEST"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
Frame(SubP, height = 2, bd = 1, relief = GROOVE).pack(side = TOP, fill = BOTH)
Sub = Frame(SubP, bd = 3)
PROGButs["VERSIONS"] = BButton(Sub, text = "Versions/SNs", \
        command = Command(versionsCmd, "nopis"))
PROGButs["VERSIONS"].pack(side = TOP, fill = X)
PROGButs["PISVERSIONS"] = BButton(Sub, text = "Versions/SNs\nto PIS", \
        command = Command(versionsCmd, "pis"))
PROGButs["PISVERSIONS"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
SubP.pack(side = LEFT, expand = YES, fill = BOTH)
# ----- Commands panel -----
SubP = Frame(LFrm, bd = 1, relief = GROOVE)
Sub = Frame(SubP, bd = 3)
SSub = Frame(Sub)
Lab = Label(SSub, text = "Disk:")
Lab.pack(side = LEFT)
ToolTip(Lab, 30, "Select which disk slot to attempt to format.")
Radiobutton(SSub, text = "1", value = 1, \
        variable = FDiskVar).pack(side = LEFT)
Radiobutton(SSub, text = "2", value = 2, \
        variable = FDiskVar).pack(side = LEFT)
SSub.pack(side = TOP)
PROGButs["FDISKS"] = BButton(Sub, text = "Format Disk", command = formatCmd)
PROGButs["FDISKS"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
PROGButs["CLEARRAM"] = BButton(Sub, text = "Clear RAM", \
        command = Command(startSimpleCmd, "CLEARRAM"))
PROGButs["CLEARRAM"].pack(side = TOP, fill = X)
PROGButs["DUMPRAM"] = BButton(Sub, text = "Dump RAM", \
        command = Command(startSimpleCmd, "DUMPRAM"))
PROGButs["DUMPRAM"].pack(side = TOP, fill = X)
Frame(SubP, height = 2, bd = 1, relief = GROOVE).pack(side = TOP, fill = BOTH)
Sub = Frame(SubP, bd = 3)
PROGButs["RESET"] = BButton(Sub, text = "Reset", \
        command = Command(startSimpleCmd, "RESET"))
PROGButs["RESET"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
Frame(SubP, height = 2, bd = 1, relief = GROOVE).pack(side = TOP, fill = BOTH)
Sub = Frame(SubP, bd = 3)
SSub = Frame(Sub)
Radiobutton(SSub, text = "Check", value = "check", \
        variable = OffSetRVar).pack(side = LEFT)
Radiobutton(SSub, text = "Set", value = "set", \
        variable = OffSetRVar).pack(side = LEFT)
SSub.pack(side = TOP)
PROGButs["OFFSET"] = BButton(Sub, text = "Offset Test", command = offsetCmd)
PROGButs["OFFSET"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
Frame(SubP, height = 2, bd = 1, relief = GROOVE).pack(side = TOP, fill = BOTH)
Sub = Frame(SubP, bd = 3)
Lab = Label(Sub, text = "Channels")
Lab.pack(side = TOP)
ToolTip(Lab, 30, \
        "Select the channels a monitor test should be performed on (if the RT130(s) have been suitably programmed).")
SSub = Frame(Sub)
Checkbutton(SSub, text = "1", variable = Chan1MonCVar).pack(side = LEFT)
Checkbutton(SSub, text = "2", variable = Chan2MonCVar).pack(side = LEFT)
Checkbutton(SSub, text = "3", variable = Chan3MonCVar).pack(side = LEFT)
SSub.pack(side = TOP)
SSub = Frame(Sub)
Checkbutton(SSub, text = "4", variable = Chan4MonCVar).pack(side = LEFT)
Checkbutton(SSub, text = "5", variable = Chan5MonCVar).pack(side = LEFT)
Checkbutton(SSub, text = "6", variable = Chan6MonCVar).pack(side = LEFT)
SSub.pack(side = TOP)
PROGButs["MONITOR"] = BButton(Sub, text = "Monitor Test", command = monitorCmd)
PROGButs["MONITOR"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
Frame(SubP, height = 2, bd = 1, relief = GROOVE).pack(side = TOP, fill = BOTH)
Sub = Frame(SubP, bd = 3)
PROGButs["STARTACQ"] = BButton(Sub, text = "Start Acq", \
        command = Command(startSimpleCmd, "STARTACQ"))
PROGButs["STARTACQ"].pack(side = TOP, fill = X)
PROGButs["STOPACQ"] = BButton(Sub, text = "Stop Acq", \
        command = Command(startSimpleCmd, "STOPACQ"))
PROGButs["STOPACQ"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
Frame(SubP, height = 2, bd = 1, relief = GROOVE).pack(side = TOP, fill = BOTH)
Sub = Frame(SubP, bd = 3)
PROGButs["SGAINHIGH"] = BButton(Sub, text = "Set Gain High", \
        command = Command(setGainCmd, "high"))
PROGButs["SGAINHIGH"].pack(side = TOP, fill = X)
PROGButs["SGAINLOW"] = BButton(Sub, text = "Set Gain Low", \
        command = Command(setGainCmd, "low"))
PROGButs["SGAINLOW"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
Frame(SubP, height = 2, bd = 1, relief = GROOVE).pack(side = TOP, fill = BOTH)
Sub = Frame(SubP, bd = 3)
SSub.pack(side = TOP)
PROGButs["SINECAL"] = BButton(Sub, text = "Sine Cal", command = formCSINE)
PROGButs["SINECAL"].pack(side = TOP, fill = X)
PROGButs["STEPCAL"] = BButton(Sub, text = "Step Cal", command = formCSTEP)
PROGButs["STEPCAL"].pack(side = TOP, fill = X)
PROGButs["NOISECAL"] = BButton(Sub, text = "Noise Cal", command = formCNOIS)
PROGButs["NOISECAL"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
Sub = Frame(SubP, bd = 3)
SSub = Frame(Sub)
Radiobutton(SSub, text = "123", value = "123", \
        variable = CalChansRVar).pack(side = LEFT)
Radiobutton(SSub, text = "456", value = "456", \
        variable = CalChansRVar).pack(side = LEFT)
SSub.pack(side = TOP)
PROGButs["MASSCENT"] = BButton(Sub, text = "Mass Center", command = massCenterCmd)
PROGButs["MASSCENT"].pack(side = TOP, fill = X)
Sub.pack(side = TOP, fill = X)
SubP.pack(side = LEFT, expand = YES, fill = BOTH)
# ----- Messages panel -----
Sub = Frame(LFrm, bd = 1, relief = GROOVE)
Label(Sub, text = "Messages").pack(side = TOP)
SSub = Frame(Sub)
labelTip(SSub, "Msg:=", LEFT, 40, \
        "[Enter] Enter the message to write to the messages section followed by the Return key.")
LEnt = PROGEnt["MM"] = Entry(SSub, textvariable = PROGMessageEntryVar)
LEnt.pack(side = LEFT, expand = YES, fill = X)
LEnt.bind("<Return>", messageEntry)
LEnt.bind("<KP_Enter>", messageEntry)
BButton(SSub, text = "Clear", fg = Clr["U"], \
        command = messageEntryClear).pack(side = LEFT)
SSub.pack(side = TOP, fill = X, padx = 3, pady = 3)
MMsg = Text(Sub, width = 65, height = 0, wrap = WORD, bg = Clr["B"], \
        fg = "lightgrey")
MMsg.pack(side = LEFT, expand = YES, fill = BOTH)
MMVSb = Scrollbar(Sub, orient = VERTICAL, command = MMsg.yview)
MMVSb.pack(side = RIGHT, fill = Y)
MMsg.configure(yscrollcommand = MMVSb.set)
Sub.pack(side = LEFT, expand = YES, fill = BOTH)
LFrm.pack(side = TOP)
msgLn(0, "", "", False, 0, False)
# Begin startup sequence.
Ret = setPROGSetupsDir()
if Ret[0] != 0:
    formMYD(None, (("(OK)", TOP, "ok"),), "ok", Ret[1], "That's Not Good.", \
            Ret[2])
    progQuitter(False)
# We'll check the errors later.
Ret = loadPROGSetups()
fontSetSize()
center(None, Root, "CX", "I", True)
# Now look at any loadPROGSetups() messages.
# Setups file could not be found.
if Ret[0] == 1:
    formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", Ret[1], "Really?", Ret[2])
# Error opening setups file. We'll just continue.
elif Ret[0] == 2 or Ret[0] == 3 or Ret[0] == 4:
    formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", Ret[1], "Oh Oh. Maybe.", \
            Ret[2])
# Normally 5 with quit the program, but we will go on in this one.
elif Ret[0] == 5:
   formMYD(Root, (("(OK)", TOP, "ok"), ), "ok", Ret[1], "Oh Oh.", Ret[2])
# Something is wrong so don't save the setups. They may be the problem.
#   progQuitter(False)
if len(PROGMsgsDirVar.get()) == 0 and len(PROGWorkDirVar.get()) == 0:
    Ret = setPROGStartDir(0, True)
    if len(Ret) == 0:
        progQuitter(False)
    elif len(Ret) != 0:
        PROGMsgsDirVar.set(Ret)
        PROGWorkDirVar.set(Ret)
# Some of these may never be true, but are here just in case.
if len(PROGMsgsDirVar.get()) == 0:
    PROGMsgsDirVar.set(PROGSetupsDirVar.get())
if len(PROGWorkDirVar.get()) == 0:
    PROGWorkDirVar.set(PROGMsgsDirVar.get())
# Place this at the beginning of the messages section just for troubleshooting
# purposes, but not in the messages since they have not been loaded yet.
msgLn(9, "", "Setups directory\n   %s"%PROGSetupsDirVar.get())
setPROGMsgsFile()
msgLn(9, "", "-----")
loadPROGMsgs()
listDirs(9)
ready()
# Let the user enter the port on the command line like /dev/ttyUSB0. If there
# is none then look for a previously saved setup file. This will only work on
# OSs like Linux.
if len(CLAPort) != 0:
    if exists(CLAPort):
        CmdPortVar.set(CLAPort)
    else:
        msgLn(1, "R", "Supplied serial port device not found:", True, 2)
        msgLn(0, "", "   \"%s\""%CLAPort)
# Try to load the serial package way down here so the dialog box can be
# displayed.
try:
    from serial import Serial, EIGHTBITS, PARITY_NONE, STOPBITS_ONE
    from serial import VERSION as SerialVERSION
except ImportError as e:
    formMYD(Root, (("(OK)", LEFT, "ok"), ), "ok", "RW", "Welcome.", \
            "The serial interface package (pySerial) is having trouble.\n\n%s\n\nThis will need to be corrected before %s will be capable of doing anything very useful.\nHave a nice day!"% \
            (e, PROG_NAME), "", 2)
if PROGSystem != "dar" and PROGSystem != "lin" and PROGSystem != "win":
    formMYD(Root, (("(OK)", LEFT, "ok"), ), "ok", "YB", "Surprise!", \
            "There is only full support for macOS, Linux and Windows at this time.\n\nThis system identifies itself as \"%s\"."% \
            PROGSystem, "", 2)
if PROG_PYVERS == 3:
    formMYD(Root, (("(OK)", TOP, "ok"),), "ok", "YB", "Be Careful.", \
            "Running under Python 3 is new, and possibly exciting, so be sure to report any bugs to PASSCAL.", \
            "", 1)
Root.mainloop()
# END: main
# END PROGRAM: CHANGEO
