# -*- coding: utf-8 -*-

"""Top-level package for changeo."""

__author__ = """IRIS PASSCAL"""
__email__ = 'software-support@passcal.nmt.edu'
__version__ = '2018.177'
