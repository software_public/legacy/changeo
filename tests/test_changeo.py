#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `changeo` package."""

import unittest
import sys

try:
    import changeo
except ImportError:
     pass

class TestChangeo(unittest.TestCase):
    """Tests for `changeo` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_import(self):
        if 'changeo' in sys.modules:
            self.assert_(True, "changeo loaded")
        else:
            self.fail()

