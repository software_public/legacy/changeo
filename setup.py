#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()


setup(
    author="IRIS PASSCAL",
    author_email='software-support@passcal.nmt.edu',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved ::  GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 2.7',
    ],
    description="Used to gang pprogram and test RT130's",
    entry_points={
        'console_scripts': [
            'changeo=changeo.changeo:main',
        ],
    },
    install_requires=['pyserial'],
    setup_requires = [],
    extras_require={
        'dev': [
            'pip',
            'bumpversion',
            'wheel',
            'watchdog',
            'flake8',
            'tox',
            'coverage',
            'Sphinx',
            'twine',
        ]
    },
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='changeo',
    name='changeo',
    packages=find_packages(include=['changeo']),
    test_suite='tests',
    url='https://git.passcal.nmt.edu/passoft/changeo',
    version='2019.262',
    zip_safe=False,
)
